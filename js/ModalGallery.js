function ModalGallery($collection, params) {
    params = params || {};

    var _this = this;

    _this.$collection = $collection;

    _this.disabling = {
        mini: {
            next: false,
            prev: false
        }
    };
    _this.removeOnClose = !!params.removeOnClose;
    _this.mobile = isMobile();

    _this.hideMiniGalleryTimeout = 3;
    _this.hiderMiniGallery = [];
    _this.autohideMiniGallery = true;

    _this.handlers = {
        miniGalleryItem: (params.handlers != undefined && typeof params.handlers.miniGalleryItem == 'function')
            ? params.handlers.miniGalleryItem
            : function () {
                return false;
            }
    };

    if (_this.$collection.size()) {
        _this.$collection.each(function (item) {
            var $imageLink = $(this);

            $imageLink.click(function () {
                if (!$(this).hasClass('manualClick')) {
                    _this.open(item);

                    return false;
                }
            })
        });

        _this.currentItem = 0;

        return this;
    }

    return false;
}

ModalGallery.prototype.open = function (item) {
    var _this = this,
        itemsCount = _this.$collection.size();

    item = (item != undefined) ? item : _this.currentItem;

    if (0 <= item && item < itemsCount) {
        var $content = $('<div class="modalGalleryContent"></div>'),
            $nextBtn = $($.parseHTML(
                '<div class="controlBtn next">' +
                    '<img src="/js/plugins/sliderengine/lightbox-next.png">' +
                '</div>'
            )),
            $prevBtn = $($.parseHTML(
                '<div class="controlBtn prev">' +
                    '<img src="/js/plugins/sliderengine/lightbox-prev.png">' +
                '</div>'
            ));

        $nextBtn.click(function () {
            _this.next();
        });

        $prevBtn.click(function () {
            _this.prev();
        });

        if (itemsCount > 1)
            $content.append($prevBtn);

        if (_this.mobile && itemsCount > 1) {
            _this.$mobileContainer = _this.getMobileContainer(item, itemsCount);
        } else {
            _this.$imageContainer = $('<div class="imageCont currentImage"><img class="modalGalleryImage" src="' + this.$collection.get(item).href + '"></div>');

            _this.$imageContainer.find('*').click(function (e) {
                e.stopPropagation();
            });

            _this.$imageContainer.click(function (e) {
                e.stopPropagation();

                _this.close();
            });

            $content.append(_this.$imageContainer);
        }

        if (itemsCount > 1)
            $content.append($nextBtn);

        _this.$miniGallery = _this.getMiniGallery();

        $content.append(_this.$miniGallery);

        _this.$miniGalleryItems.each(function () {
            var $this = $(this);

            $this.find('img').ready(function () {
                _this.updateMiniGalleryItemImage($this);
            });
        });

        this.$modal = modal(
             $content,
             {
                 class: 'modalGallery' + (_this.mobile ? ' mobile' : ''),
                 clear: true
             }
        );

        _this.$content = $content;

        _this.currentItem = item;

        _this.setImage();
        _this.checkMiniGalleryDisabling();
        _this.updateDisabling();

        $(window).resize($.throttle(66, function () {
            _this.checkMiniGalleryDisabling();
            _this.updateDisabling();
            _this.setActiveMiniImage();
        }));
    }
    initNoAgent();
};

ModalGallery.prototype.getMobileContainer = function (item, itemsCount) {
    itemsCount = itemsCount || this.$collection.size();

    var _this = this,
        items = [
            {
                number: (item - 1 < 0) ? itemsCount - 1 : item - 1,
                left: -100,
                classes: 'prevImage'
            },
            {
                number: item,
                left: 0,
                classes: 'currentImage active'
            },
            {
                number: (item + 1 >= itemsCount) ? 0 : item + 1,
                left: 100,
                classes: 'nextImage'
            }
        ];

    return items.map(function (item) {
        item.dom = $('<div class="imageCont ' + item.classes + '" data-item="' + item.number + '"><img class="modalGalleryImage" src="' + _this.$collection.get(item.number).href + '"></div>')

        return item;
    });
};

ModalGallery.prototype.checkMiniGalleryDisabling = function () {
    var _this = this,
        offseterWidth = _this.$miniGalleryContainer.width(),
        miniItemsWidth = _this.getItemsWidth(_this.$miniGalleryItems);

    if (offseterWidth >= miniItemsWidth) {
        _this.disabling.mini.prev = true;
        _this.disabling.mini.next = true;
        _this.setMiniGalleryOffset(0);
    } else {
        _this.disabling.mini.prev = (_this.currentItem == 0);
        _this.disabling.mini.next = (_this.currentItem == _this.$miniGalleryItems.size() - 1);
    }
};

ModalGallery.prototype.getMiniGallery = function () {
    var $miniGallery = $($.parseHTML(
            '<div class="miniGallery">' +
                '<div class="controlBtn prev">' +
                    '<img src="/js/plugins/sliderengine/lightbox-prev.png">' +
                '</div>' +
                '<div class="miniImageCont"><div class="miniImageOffseter"></div></div>' +
                '<div class="controlBtn next">' +
                    '<img src="/js/plugins/sliderengine/lightbox-next.png">' +
                '</div>' +
            '</div>'
        )),
        $imagesContainer = $miniGallery.find('.miniImageOffseter'),
        _this = this;

    this.miniGalleryOffset = 0;

    this.$collection.each(function (item) {
        var $item = $('<div class="miniGalleryItem"></div>'),
            $collectionItem = $(this),
            $handlerItem = _this.handlers.miniGalleryItem($collectionItem);

        if ($handlerItem !== false) {
            $item.append($handlerItem);
        } else {
            $item.append($collectionItem.find('img').clone());
        }

        if (_this.currentItem == item)
            $item.addClass('active');

        $item.data('item', item);

        $imagesContainer.append($item);
    });

    _this.$miniGalleryItems = $imagesContainer.children();
    _this.$miniGalleryContainer = $miniGallery.find('.miniImageCont');

    _this.$miniGalleryItems.click(function (e) {
        e.stopPropagation();

        var $this = $(this);

        if (!$this.hasClass('active'))
            _this.setImage($this.data('item'));
    });

    $miniGallery.find('.controlBtn.next').click(function (e) {
        e.stopPropagation();

        if (!_this.disabling.mini.next) {
            _this.disabling.mini.prev = false;

            var width = _this.$miniGalleryContainer.width() + Math.abs(_this.miniGalleryOffset),
                items = _this.$miniGalleryItems.get(),
                item = 0,
                curWidth = 0;

            for (var i = 0; i < items.length; i++) {
                curWidth += $(items[i]).outerWidth(true);

                if (curWidth > width) {
                    item = i;

                    break;
                }
            }

            if (_this.$miniGalleryContainer.width() > _this.getItemsWidth($(_this.$miniGalleryItems.get(item)).nextAll())) {
                _this.setLastMiniImage(_this.$collection.size() - 1);

                _this.disabling.mini.next = true;
            } else {
                _this.setFirstMiniImage(item);
            }
        }

        _this.checkMiniGalleryDisabling();
        _this.updateDisabling();
    });

    $miniGallery.find('.controlBtn.prev').click(function (e) {
        e.stopPropagation();

        if (!_this.disabling.mini.prev) {
            _this.disabling.mini.next = false;

            var width = Math.abs(_this.miniGalleryOffset),
                items = _this.$miniGalleryItems.get(),
                item = 0,
                curWidth = 0;

            for (var i = 0; i < items.length; i++) {
                curWidth += $(items[i]).outerWidth(true);

                if (curWidth > width) {
                    item = i;

                    break;
                }
            }

            if (_this.$miniGalleryContainer.width() > _this.getItemsWidth($(_this.$miniGalleryItems.get(item)).prevAll()) + $(items[item]).outerWidth(true)) {
                _this.setFirstMiniImage(0);

                _this.disabling.mini.prev = true;
            } else {
                _this.setLastMiniImage(item);
            }
        }

        _this.checkMiniGalleryDisabling();
        _this.updateDisabling();
    });

    if (_this.mobile) {
        var miniGalleryScroll = false,
            prevX = 0,
            getWidths = function () {
                return {
                    container: _this.$miniGalleryContainer.width(),
                    items: array_sum(_this.$miniGalleryItems.get().map(function (item) {
                        return $(item).outerWidth(true);
                    }))
                };
            };

        $miniGallery.on('vmousedown', function () {
            miniGalleryScroll = true;

            _this.stopHideMiniGallery();
        });

        $miniGallery.on('vmousemove', $.throttle(33, function (e) {
            if (miniGalleryScroll) {
                var width = getWidths(),
                    allowedDX = Math.round(width.container * 0.1);

                if (width.container < width.items) {
                    if (!prevX) {
                        prevX = e.pageX;
                    } else {
                        var newOffset = _this.miniGalleryOffset + e.pageX - prevX;

                        if (newOffset < allowedDX && newOffset > width.container - width.items - allowedDX)
                            _this.setMiniGalleryOffset(newOffset);

                        prevX = e.pageX;
                    }
                }
            }
        }));

        $miniGallery.on('vmouseup', function () {
            miniGalleryScroll = false;
            prevX = 0;

            _this.startHideMiniGallery();
        });
    }

    $miniGallery.click(function () {
        _this.close();
    });

    return $miniGallery;
};

ModalGallery.prototype.updateDisabling = function () {
    var $prevMiniControlBtn = this.$miniGallery.find('.controlBtn.prev'),
        $nextMiniControlBtn = this.$miniGallery.find('.controlBtn.next');

    if (this.disabling.mini.prev) {
        $prevMiniControlBtn.attr('disabled', 'disabled');
    } else {
        $prevMiniControlBtn.removeAttr('disabled');
    }

    if (this.disabling.mini.next) {
        $nextMiniControlBtn.attr('disabled', 'disabled');
    } else {
        $nextMiniControlBtn.removeAttr('disabled');
    }
};

ModalGallery.prototype.setLastMiniImage = function (item) {
    item = (item == undefined) ? this.currentItem : item;

    if (0 <= item && item < this.$collection.size()) {
        this.miniGalleryOffset = this.$miniGalleryContainer.width()
            - this.getItemsWidth($(this.$miniGalleryItems.get(item)).prevAll())
            - $(this.$miniGalleryItems.get(item)).outerWidth(true);

        this.setMiniGalleryOffset(this.miniGalleryOffset);
    }
};

ModalGallery.prototype.setMiniGalleryOffset = function (offset) {
    this.miniGalleryOffset = (offset != undefined) ? offset : (this.miniGalleryOffset || 0);

    this.$miniGallery.find('.miniImageOffseter').css(
        'marginLeft',
        this.miniGalleryOffset
    );
};

ModalGallery.prototype.getItemsWidth = function ($items) {
    return array_sum(
        $items.get().map(
            function (pervItem) {
                return $(pervItem).outerWidth(true);
            }
        )
    );
};

ModalGallery.prototype.setFirstMiniImage = function (item) {
    item = (item == undefined) ? this.currentItem : item;

    if (0 <= item && item < this.$collection.size()) {
        this.miniGalleryOffset = -this.getItemsWidth($(this.$miniGalleryItems.get(item)).prevAll());

        this.setMiniGalleryOffset(this.miniGalleryOffset);
    }
};

ModalGallery.prototype.close = function () {
    if (this.$modal != undefined)
        this.$modal.arcticmodal('close');

    if (this.removeOnClose)
        this.remove();
};

ModalGallery.prototype.remove = function () {
    var _this = this;
    delete _this;
};

ModalGallery.prototype.setImage = function (item) {
    var _this = this;

    if (_this.$imageContainer != undefined && !_this.mobile || _this.mobile && _this.$mobileContainer != undefined) {
        item = (item == undefined) ? _this.currentItem : item;
        _this.currentItem = item;

        var $image;

        if (_this.mobile) {
            _this.$mobileContainer = _this.getMobileContainer(item);

            var $currentActiveImageCont = _this.$content.find('.imageCont.active');

            if ($currentActiveImageCont.length && $currentActiveImageCont.data('item') == item) {
                _this.$content.find('.imageCont').not($currentActiveImageCont).remove();

                $currentActiveImageCont.css('left', '0%');
                $currentActiveImageCont.removeClass('prevImage nextImage');
                $currentActiveImageCont.addClass('currentImage');

                $currentActiveImageCont.before(_this.$mobileContainer[0].dom);
                $currentActiveImageCont.after(_this.$mobileContainer[2].dom);

                _this.$mobileContainer[1].dom = $currentActiveImageCont;

                $currentActiveImageCont.css('left', '');
            } else {
                _this.$content.find('.imageCont').remove();

                for (var i in _this.$mobileContainer)
                    _this.$content.append(_this.$mobileContainer[i].dom);
            }

            $image = _this.$mobileContainer[1].dom.find('img');

            if (_this.$miniGallery != undefined) {
                _this.startHideMiniGallery();

                $image.on('tap', function (e) {
                    _this.stopHideMiniGallery();

                    if (!_this.$miniGallery.data('hided')) {
                        _this.hideMiniGallery();
                    } else {
                        _this.showMiniGallery();

                        _this.startHideMiniGallery();
                    }
                });
            }

            for (var i in _this.$mobileContainer) {
                _this.$mobileContainer[i].dom.on('tap', function (e) {
                    e.stopPropagation();

                    _this.close();
                });

                _this.$mobileContainer[i].dom.find('img').on('tap', function (e) {
                    e.stopPropagation();
                });
            }

            var wrappImage = false,
                prev = {
                    x: 0,
                    y: 0
                },
                d = {
                    x: 0,
                    y: 0
                },
                percentDX = 0;

            $image.on('vmousedown', function () {
                if (!wrappImage) {
                    wrappImage = true;

                    _this.stopHideMiniGallery();
                }
            });

            $image.on('vmousemove', $.throttle(33, function (e) {
                if (wrappImage) {
                    if (!prev.x) {
                        prev.x = e.screenX;
                        prev.y = e.screenY;
                    } else {
                        d.x += e.screenX - prev.x;
                        d.y += e.screenY - prev.y;

                        prev.x = e.screenX;
                        prev.y = e.screenY;
                    }

                    var screenWidth = $(window).width(),
                        maxDX = screenWidth;

                    percentDX = Math.ceil(Math.abs(d.x) / maxDX * 100) * (d.x / Math.abs(d.x));

                    $(_this.$mobileContainer).each(function () {
                        this.dom.css('left', (this.left + percentDX) + '%');
                    });
                }
            }));

            $image.on('vmouseup', function () {
                $(_this.$mobileContainer).each(function () {
                    var $dom = this.dom,
                        slideOrNot = Math.round(Math.abs(percentDX) / 100),
                        nextOrPerv = percentDX / Math.abs(percentDX);

                    $dom.animate({'left': (this.left + slideOrNot * 100 * nextOrPerv) + '%'}, 200, null, function () {
                        if ($dom.hasClass('currentImage')) {
                            wrappImage = false;

                            if (d.x && slideOrNot) {
                                $(_this.$mobileContainer).each(function () {
                                    this.dom.removeClass('active');
                                });

                                _this.$mobileContainer[1 - nextOrPerv].dom.addClass('active');

                                if (d.x < 0) {
                                    _this.next();
                                } else {
                                    _this.prev();
                                }
                            }

                            prev.x = 0;
                            prev.y = 0;
                            d.x = 0;
                            d.y = 0;
                            percentDX = 0;
                        }
                    });

                    _this.startHideMiniGallery();
                });
            });
        } else {
            $image = $('<img class="modalGalleryImage" src="' + ($(_this.$collection.get(item)).data('href') || _this.$collection.get(item).href) + '">');

            _this.$imageContainer.empty();

            _this.$imageContainer.append($image);

            $image.click(function (e) {
                e.stopPropagation();
            });
        }


        if (_this.$miniGallery != undefined) {
            _this.setActiveMiniImage(item);

            _this.disabling.mini.prev = (_this.currentItem == 0);
            _this.disabling.mini.next = (_this.currentItem == _this.$miniGalleryItems.size() - 1);

            _this.checkMiniGalleryDisabling();
            _this.updateDisabling();
        }
    }
};

ModalGallery.prototype.stopHideMiniGallery = function () {
    var _this = this;

    for (var i in _this.hiderMiniGallery)
        clearTimeout(_this.hiderMiniGallery[i]);

    _this.hiderMiniGallery = [];
};

ModalGallery.prototype.showMiniGallery = function () {
    var _this = this;

    _this.$miniGallery.css('bottom', -_this.$miniGallery.outerHeight(true));
    _this.$miniGallery.animate({'bottom': 0}, 500);
    _this.$miniGallery.data('hided', 0);
};

ModalGallery.prototype.hideMiniGallery = function () {
    var _this = this;

    _this.$miniGallery.css('bottom', 0);
    _this.$miniGallery.animate({
        'bottom': -_this.$miniGallery.outerHeight(true)
    }, 500);
    _this.$miniGallery.data('hided', 1);
};

ModalGallery.prototype.startHideMiniGallery = function () {
    var _this = this;

    _this.stopHideMiniGallery();

    if (_this.$miniGallery != undefined && _this.autohideMiniGallery)
        _this.hiderMiniGallery.push(setTimeout(
            function () {
                _this.hideMiniGallery();
            },
            _this.hideMiniGalleryTimeout * 1000
        ));
};

ModalGallery.prototype.setActiveMiniImage = function (item) {
    item = (item == undefined) ? this.currentItem : item;

    this.$miniGalleryItems.removeClass('active');

    var $item = $(this.$miniGalleryItems.get(item)),
        prevItemsWidth = this.getItemsWidth($item.prevAll()),
        _this = this;

    $item.addClass('active');

    if (prevItemsWidth < Math.abs(this.miniGalleryOffset)) {
        this.setFirstMiniImage(item);
    } else if (prevItemsWidth + $item.outerWidth(true) > Math.abs(this.miniGalleryOffset) + this.$miniGalleryContainer.width()) {
        this.setLastMiniImage(item);
    }

    this.$miniGalleryItems.each(function () {
        _this.updateMiniGalleryItemImage($(this));
    });
};

ModalGallery.prototype.updateMiniGalleryItemImage = function($item) {
    var $image = $item.find('img'),
        imageDX = ($image.width() - $item.width()) / 2;

    /*if (imageDX)
        $image.css('left', -imageDX);*/
};

ModalGallery.prototype.next = function () {
    this.currentItem++;

    if (this.currentItem > this.$collection.size() - 1)
        this.currentItem = 0;

    this.setImage();
};
ModalGallery.prototype.prev = function () {
    this.currentItem--;

    if (this.currentItem < 0)
        this.currentItem = this.$collection.size() - 1;

    this.setImage();
};