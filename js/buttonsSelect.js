function ButtonsSelect($container, config) {
    config = config || {};

    var _this = this;

    _this.$container = $container;

    if ($container.size()) {
        config.onChange = config.onChange || $container.data('on-change');

        if (config.onChange) {
            if (typeof config.onChange == 'string')
                eval('config.onChange = (typeof ' + config.onChange + ' == "function") ? ' + config.onChange + ': false;');

            if (typeof config.onChange != 'function')
                delete config.onChange;
        }

        _this.$buttons = $container.find('button');
        _this.$select = $container.find('select');
        _this.$options = _this.$select.find('option');

        _this.config = {
            activeClass: _this.$container.data('active-class'),
            defaultClass: _this.$container.data('default-class'),
            multiple: _this.$container.data('multiple'),
            on: {
                change: (typeof config.onChange == 'function') ? config.onChange : function () {},
                click: (typeof config.onClick == 'function') ? config.onClick : function () {}
            }
        };

        _this.values = {};
        _this.curValues = [];

        for (var i = 0; i < _this.$options.size(); i++) {
            var $option = $(_this.$options[i]),
                val = $option[0].value;

            _this.values[val] = $option;

            if ($option.attr('selected') == 'selected' || $option.prop('selected'))
                _this.curValues.push(val);
        }

        _this.$buttons.click(function () {
            var $this = $(this),
                index = _this.$buttons.index($this);

            _this.setActive(index);

            _this.config.on.click(index, _this);
        });

    }

    return _this;
}

ButtonsSelect.prototype.setActive = function (i) {
    var _this = this,
        $button = $(_this.$buttons.get(i)),
        curValue = $button.data('value') + '',
        oldValues = _this.curValues;

    if (!$button.hasClass(_this.config.activeClass)) {
        if (!_this.config.multiple)
            _this.curValues = [];

        _this.curValues.push(curValue);
    } else {
        if (_this.config.multiple)
            _this.curValues = _this.curValues.filter(function (value) {
                return curValue != value;
            });
    }

    _this.update();

    _this.$select.val(!_this.config.multiple ? _this.curValues[0] : _this.curValues);

    if (
        $(oldValues).not(_this.curValues).get().length
        || $(_this.curValues).not(oldValues).get().length
    ) {
        _this.config.on.change(_this.$select, _this);
    }
};

ButtonsSelect.prototype.set = function (index) {
    this.setActive(index);

    this.config.on.click(index, this);
};

ButtonsSelect.prototype.update = function () {
    var _this = this;

    _this.$options.removeAttr('selected');
    _this.$options.prop('selected', null);

    _this.$buttons.each(function () {
        var $this = $(this),
            value = $this.data('value') + '',
            hasClasses = {
                default: $this.hasClass(_this.config.defaultClass),
                active:  $this.hasClass(_this.config.activeClass)
            };

        if (in_array(value, _this.curValues)) {
            if (hasClasses.default)
                $this.removeClass(_this.config.defaultClass);

            if (!hasClasses.active)
                $this.addClass(_this.config.activeClass);
        } else {
            if (!hasClasses.default)
                $this.addClass(_this.config.defaultClass);

            if (hasClasses.active)
                $this.removeClass(_this.config.activeClass);
        }
    });
};