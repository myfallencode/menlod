function initChosen(starOnly) {
    starOnly = starOnly || false;

    if (!starOnly) {
        $('.deleteFromChosen').click(function () { // удаление из избранного через Избранное
            var $this = $(this),
                chosen = $.cookie('chosen') || {},
                objectType = $this.data('object'),
                objectIDs = chosen[objectType] || [],
                deletedID = $this.data('id'),
                parentConfig = {
                    Search: {current: 'chosenSearch'},
                    Builds: {current: 'buildItem'},
                    Blocks: {
                        current: 'blockItem',
                        parent: 'buildItem'
                    }
                },
                currParentConf = parentConfig[objectType],
                $badge = $('.stepTabsMenu .badge[data-object=' + objectType + ']');

            if (objectIDs.length) {
                chosen[objectType] = objectIDs.filter(function (item) {
                    return item.ObjectID != deletedID;
                });

                var $blocksList;

                if (objectType == 'Blocks' && chosen[objectType].length)
                    $blocksList = $this.closest('.blocksBox');

                deleteChosenFromParentBox($this.closest('.' + currParentConf.current), currParentConf.parent);

                if (objectType == 'Blocks' && chosen[objectType].length) {
                    var $moreBtn = $blocksList.parent().find('.open_down_box_bottom');

                    $blocksList.children().each(function (i) {
                        var $item = $(this);

                        $item.removeClass('openDownBoxItem');

                        if (i > 2)
                            $item.addClass('openDownBoxItem');

                        if (!$item.hasClass('openDownBoxItem'))
                            $item.removeClass('activeItem');
                    });

                    var count = $blocksList.children().filter('.openDownBoxItem').size();

                    if (count) {
                        var word = 'помещен',
                            word2 = 'последн',
                            wordEnd = 'ий',
                            wordEnd2 = 'ие';

                        if (count == 1) {
                            wordEnd = 'ие';
                            wordEnd2 = 'ee';
                        } else if (2 <= count && count <= 4) {
                            wordEnd = 'ия';
                        }

                        $moreBtn.find('.moreBlocksCount').html(count);
                        $moreBtn.find('.countWord').html(word + wordEnd);
                        $moreBtn.find('.hideLastWord').html(word2 + wordEnd2);
                    } else {
                        $moreBtn.remove();
                    }
                }

                setChosenCookie(chosen);

                if (!chosen[objectType].length)
                    $badge.hide();
            }

            $badge.html(chosen[objectType].length);

            changeChosenCount();
        });
        
        function deleteChosenFromParentBox($box, parent) {
            var $parent = $box.parent();

            $box.remove();

            var childrenCount = $parent.children().filter(':not(.empty_search)').size();

            if (!childrenCount) {
                if (parent) {
                    deleteChosenFromParentBox($parent.closest('.' + parent), parent.parent);
                } else {
                    $parent.find('.empty_search').show();
                }
            }
        }

        $('#choseSearch').click(function () { //добавление поиска
            var $this = $(this);

            if (!$this.parent().hasClass('active')) {
                var chosen = $.cookie('chosen') || {},
                    searchChosen = chosen['Search'] || [],
                    newSearch = (searchChosen.map(function (item) {
                        var matches = item.Name.match(/^Новый поиск (\d+)$/);

                        return matches ? matches[1] : false;
                    })).filter(function (item) {
                        return item;
                    }),
                    maxNewSearchCount = Math.max.apply(
                            null,
                            newSearch.length ? newSearch : [0]
                        ) + 1,
                    defaultName = 'Новый поиск ' + maxNewSearchCount;

                modal(
                    '<div class="form_wr" style="height: auto;">' +
                    '<span>Название поиска</span>' +
                    '<input type="text" name="chosenSearchName" id="chosenSearchName" value="' + defaultName + '">' +
                    '<label class="error" for="chosenSearchName">Такой поиск уже существует!</label>' +
                    '</div>' +
                    '<span class="btn_green_2" onclick="saveSearchToChosen(\'' + defaultName + '\')">Сохранить</span>'
                );
            }

            return false
        });
    }

    $('.chosenObject').change(function () { //добавление/удаление через Звездочку
        var $this = $(this),
            chosen = $.cookie('chosen') || {},
            list = chosen[$this.data('object')] || [],
            objectID = $this.val();

        if ($this.is(':checked')) {
            var isDouble = list.filter(function (item) {
                return item.ObjectID == objectID;
            }).length;

            if (!isDouble)
                list.push({
                    id: list.length,
                    ObjectID: objectID,
                    time: unix_timestamp()
                });
        } else {
            list = list.filter(function (item) {
                return item.ObjectID != objectID;
            });
        }

        chosen[$this.data('object')] = list;

        setChosenCookie(chosen);
        changeChosenCount();
    });
}

function setChosenCookie(cookie) {
    $.cookie(
        'chosen',
        cookie,
        {
            expires: 3650,
            path: '/'
        }
    );
}

function saveSearchToChosen(defaultName) {
    var name = $('#chosenSearchName').val() || defaultName,
        chosen = $.cookie('chosen') || {},
        searchChosen = chosen['Search'] || [],
        params = {},
        searchParams = $('#searchFilter').serializeArray(),
        doubleName = searchChosen.filter(function (item) {
            return item.Name == name;
        });

    if (!doubleName.length) {
        for (var i = 0; i < searchParams.length; i++) {
            var paramName = searchParams[i].name.replace(/[\[\]]]/g, '');

            if (!params[paramName]) {
                params[paramName] = searchParams[i].value;
            } else {
                if (!$.isArray(params[paramName])) {
                    var currentValue = params[paramName];

                    params[paramName] = [];
                    params[paramName].push(currentValue);
                }

                params[paramName].push(searchParams[i].value);
            }
        }

        params.Name = name;
        params.time = unix_timestamp();
        params.ObjectID = searchChosen.length ? Math.max.apply(null, array_column(searchChosen, 'ObjectID')) + 1 : 1;

        searchChosen.push(params);
        chosen['Search'] = searchChosen;

        setChosenCookie(chosen);

        $('.choseSearch').addClass('active');
        $.arcticmodal('close');

        changeChosenCount();
    } else {
        $('label.error[for=chosenSearchName]').show();
    }
}

function changeChosenCount() {
    var $choseSearchBtn = $('.chosenSearchLink'),
        $counter = $('.read_label_sm', $choseSearchBtn),
        show = false,
        chosen = $.cookie('chosen'),
        count = 0;

    for (var i in chosen) {
        show = show || chosen[i].length;
        count += chosen[i].length;
    }

    if (show && !$choseSearchBtn.is(':visible')) {
        $choseSearchBtn.show();
    } else if (!show) {
        $choseSearchBtn.hide();
    }

    $counter.html(count);
}