function Hash() {
    /*this.hash = '';*/

    return this;
}

/**
 * Возвращает все параметры хэша
 * @return {{}}
 */
Hash.prototype.getAll = function () {
    var hash = location.hash.replace('#', '').split('&'),
        result = {};

    if (location.hash) {
        for (var i in hash) {
            var item = hash[i].split('=');
            result[item[0]] = item[1];
        }
    }

    return result;
};

/**
 * Устанавливает значение value параметру и именем name в хэш
 * @param name  название параметра
 * @param value значение параметра
 */
Hash.prototype.set = function (name, value) {
    var allHash = this.getAll(),
        resultHash = [];

    if (value) {
        allHash[name] = value;
    } else {
        delete allHash[name];
    }

    for (var i in allHash) {
        resultHash.push(i + '=' + allHash[i]);
    }
    location.hash = '#' + resultHash.join('&');
};

/**
 * Удаляет параметр из хэша
 * @param name
 */
Hash.prototype.unset = function (name) {
    this.set(name, false);
};

/**
 * Очищает весь хэш
 */
Hash.prototype.clear = function () {
    location.hash = '';
};

/**
 * Возвращает запрошеный параметр из хэша
 * @param name
 * @return {*}
 */
Hash.prototype.get = function (name) {
    return this.getAll()[name];
};

var Hash = new Hash();