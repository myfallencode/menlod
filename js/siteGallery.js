function SiteGallery($container, params) {
    this.$container = $container; //контейнер
    this.parentID = this.$container.data('parent') || params.parentID; //parentID
    this.gallery = this.$container.data('gallery') || params.gallery; //название галереи
    this.key = this.$container.data('key') || params.key; // ключ галереи
    this.params = {
        single: !!this.$container.data('single'), //только одно изображение
        deleteText: this.$container.data('delete-text') || 'Удалить?'
    };

    this.on = {
        load: (params.on && (typeof params.on.load == 'function')) ? params.on.load : function () {},
        delete: (params.on && (typeof params.on.delete == 'function')) ? params.on.delete : function () {}
    };

    if (this.parentID && this.gallery && this.key) {
        var _this = this,
            $imageContainer = $('<div class="defaultImages"></div>'), //контейнер с исходными изображениями
            $imageList = $('<ul></ul>'); //список с изображениями галереи

        _this.$images = _this.$container.find('img'); //изображения для перемещения

        if (_this.params.single)
            _this.$images = $(_this.$images[0]);

        _this.$galleryContainer = $('<div class="load_img_box clearfix"></div>'); //контейнер для галереи

        $imageContainer.append(_this.$images); //помещаем дефолтные изображения в контейнер
        $imageContainer.hide(); //скрываем его

        _this.$container.append($imageContainer);

        _this.$images.each(function (i) { //перебираем изображения
            var $this = $(this),
                $item = $($.parseHTML( //добавляем каждую фотографию в галерею с элементами управления
                    '<li class="imageItem" data-id="' + $this.data('id') + '">' +
                        '<span class="load_img_operation"><span class="load_img_operation_line" style="width: 0%;"></span></span>' +
                        '<div class="imageItemBox"><img class="' + ($this.width() < $this.height() ? 'vertical' : '') + '" src="' + this.src + '"></div>' +
                    '</li>'
                )),
                $deleteBtn = $($.parseHTML('<span class="load_img_close">×</span>')),
                $deleteConfirm = $($.parseHTML(
                    '<div class="deleteConfirm">' +
                        '<div class="deleteConfirmLabel">' + _this.params.deleteText + '</div>' +
                        '<div class="deleteConfirmButtons">' +
                            '<button type="button" class="agree btn btn-success btn-sm">Да</button>' +
                            '<button type="button" class="cancel btn btn-danger btn-sm">Нет</button>' +
                        '</div>' +
                    '</div>'
                )); //кнопка удаления

            $deleteConfirm.find('button').click(function () {
                var $btn = $(this);

                if ($btn.hasClass('agree'))
                    _this.delete($btn.closest('.imageItem').data('id'));

                $deleteBtn.popover('destroy');
                $deleteBtn.removeClass('active');
            });

            $deleteBtn.click(function () { //обработчик кнопки удаления
                var $this = $(this);

                if (!$this.hasClass('active')) {
                    $deleteBtn.popover({
                        trigger: 'manual',
                        html: true,
                        placement: 'bottom',
                        content: $deleteConfirm
                    });

                    $deleteBtn.popover('show');

                    $this.addClass('active');
                } else {
                    $deleteBtn.popover('destroy');

                    $this.removeClass('active');
                }
            });

            $item.append($deleteBtn);
            $imageList.append($item);
        });

        if (params.add) { // добавление фотографий
            var $addBtn = $($.parseHTML(
                    '<li class="load_img_add">' +
                        '<span class="load_img_plus"></span><br>' +
                        '<span class="loadImageBtnLabel"></span> фото' +
                    '</li>'
                )),
                $input = $($.parseHTML('<input type="file" name="galleryImage" accept="image/*" multiple>'));

            $addBtn.append($input);

            $input.click(function (e) {
                e.stopPropagation();
            });

            $input.change(function () { //обработка добавления
                for (var i = 0; i < this.files.length; i ++) {
                    _this.add(this.files[i]);
                }
                $(this).val('');
            });

            $addBtn.click(function (e) {
                $input.click();
            });

            $imageList.append($addBtn);
        }

        $imageList.disableSelection();

        _this.$galleryContainer.append($imageList);
        _this.$container.append(_this.$galleryContainer);

        _this.sortable = true;

        $imageList.sortable({
            cancel: '.load_img_add, .load_img_active',
            items: '.imageItem:not(.load_img_active)',
            placeholder: '.imageGalleryPlaceholder',
            stop: function () {
                _this.sort();
            }
        });

        _this.$imageList = $imageList;

        _this.setLoadButtonLabel();
    }
}

SiteGallery.prototype.setLoadButtonLabel = function () {
    this.$galleryContainer.find('.load_img_add .loadImageBtnLabel').html(this.getLoadButtonLabel());
};

SiteGallery.prototype.getLoadButtonLabel = function () {
    return ((this.$imageList.find('.imageItem').length == 1 && this.params.single) ? 'Изменить' : 'Добавить');
};

SiteGallery.prototype.getUID = function () {
    return (new Date()).getTime() + '' + (Math.random() * 1000).toFixed(0);
};

SiteGallery.prototype.imageCompression = function (obj , success) {
    var _this = this,
        canvas = obj.canvas,
        ctx = canvas.getContext("2d"),
        data = canvas.toDataURL("image/jpeg"),
        imgSize = _this.getFileSizeByBase64(data),
        kSize = MAX_SIZE / imgSize;

    if (kSize < 1) {
        kSize = (kSize + 1) / 2 - 0.01;

        var newWidth = Math.round(obj.w * kSize),
            newHeight = Math.round(obj.h * kSize);

        canvas.width = newWidth;
        canvas.height = newHeight;
        obj.image.src = data;

        obj.image.onload = function () {
            ctx.drawImage(obj.image, 0, 0, newWidth, newHeight);
            obj.image.src = canvas.toDataURL("image/jpeg");

            obj.image.onload = function () {
                canvas.width = obj.w;
                canvas.height = obj.h;
                ctx.drawImage(obj.image, 0, 0, obj.w, obj.h);
                _this.imageCompression(obj, success);
            };
        };
    } else {
        success(data, imgSize);
    }
};

SiteGallery.prototype.sort = function () {
    var _this = this;

    if (_this.sortable) {
        _this.sortable = false;

        ajax(
            'sortSiteGallery',
            {
                'Images': _this.$imageList.find('li.imageItem').get().map(function (li) {
                    return $(li).data('id');
                }),
                'ParentID': _this.parentID,
                'Gallery': _this.gallery,
                'Key': _this.key
            },
            function () {
                _this.sortable = true;
            }
        );
    }
};

SiteGallery.prototype.add = function (file) {
    var image = new Image(),
        _this = this,
        $galleryItem = $($.parseHTML(
            '<li class="imageItem load_img_active">' +
                '<span class="load_img_operation"><span class="load_img_operation_line" style="width: 0%;"></span></span>' +
                '<span class="load_img_close">×</span>' +
            '</li>'
        )),
        $progressBar = $galleryItem.find('.load_img_operation_line');

    if (_this.params.single)
        _this.$imageList.find('.imageItem').hide();

    _this.$imageList.find('li.load_img_add').before($galleryItem);

    image.src = URL.createObjectURL(file);


    image.onload = function() {
        var $image = $(image),
            size = { //размеры изображения
                width: MAX_WIDTH,
                height: MAX_HEIGHT
            },
            $canvas = $("<canvas></canvas>"),
            ctx = $canvas[0].getContext("2d"),
            $box = $('<div class="imageItemBox"></div>'),
            $nexImage = $image.clone();

        $box.append($nexImage);

        $nexImage[0].onload = function () {
            if ($nexImage.width() < $nexImage.height())
                $(this).addClass('vertical');
        };

        $galleryItem.append($box);

        $image.css({ //добавляем изображение
            position: 'absolute',
            left: -10000,
            top: -10000
        });
        $('body').append($image);

        $canvas.css({ //добавляем канву
            position: 'absolute',
            left: -10000,
            top: -10000
        });
        $('body').append($canvas);

        var iWidth = $image.width(), //текущие размеры изображения
            iHeight = $image.height();

        if (iWidth > size.width || iHeight > size.height) { //если текущие размеры изображения выходят за рамки дозволенного
            var newSizes = _this.resizeImage({ //то посчитываем новые размеры
                w: iWidth,
                h: iHeight,
                maxW: size.width,
                maxH: size.height
            });

            iWidth = newSizes.w; //новые размеры
            iHeight = newSizes.h;
        }

        $canvas[0].width = iWidth; //ресайзим канву
        $canvas[0].height = iHeight;

        ctx.drawImage(image, 0, 0, iWidth, iHeight); //рисуем на канве новое изображение

        _this.imageCompression( //пережимаем изображение
            {
                canvas: $canvas[0],
                w: iWidth,
                h: iHeight,
                image: image
            },
            function (data, size) { //после сжатия отправляем на сервер
                var uid = _this.getUID();
                ajax(
                    'addToSiteGallery',
                    {
                        'images': [data.split(',')[1]],
                        'imagesName': [uid + '.jpg'],
                        'imagesUID': [uid],
                        'Gallery': _this.gallery,
                        'ParentID': _this.parentID,
                        'Key': _this.key
                    },
                    function (result) { //добавляет id изображения и убираем загрузчик
                        _this.setLoadButtonLabel();

                        $galleryItem.removeClass('load_img_active');
                        $galleryItem.removeClass('errorItem');
                        $galleryItem.find('.error, .errorText').remove();

                        if (!result.errors.length) {
                            var newID = result.data[0].id;

                            $galleryItem.data('id', newID);

                            if (_this.params.single) {
                                _this.$imageList.find('.imageItem').each(function () {
                                    var $this = $(this),
                                        id = $this.data('id');

                                    if (newID != id) {
                                        _this.delete(id);

                                        $this.remove();
                                    }
                                });
                            }

                            var $deleteConfirm = $($.parseHTML(
                                    '<div class="deleteConfirm">' +
                                        '<div class="deleteConfirmLabel">' + _this.params.deleteText + '</div>' +
                                        '<div class="deleteConfirmButtons">' +
                                            '<button type="button" class="agree btn btn-success btn-sm">Да</button>' +
                                            '<button type="button" class="cancel btn btn-danger btn-sm">Нет</button>' +
                                        '</div>' +
                                    '</div>'
                                )),
                                $deleteBtn = $galleryItem.find('.load_img_close'); //кнопка удаления

                            $deleteConfirm.find('button').click(function () {
                                var $btn = $(this);

                                if ($btn.hasClass('agree'))
                                    _this.delete($btn.closest('.imageItem').data('id'));

                                $deleteBtn.popover('destroy');
                                $deleteBtn.removeClass('active');
                            });

                            $deleteBtn.click(function () { //обработчик кнопки удаления
                                var $this = $(this);

                                if (!$this.hasClass('active')) {
                                    $deleteBtn.popover({
                                        trigger: 'manual',
                                        html: true,
                                        placement: 'bottom',
                                        content: $deleteConfirm
                                    });

                                    $deleteBtn.popover('show');

                                    $this.addClass('active');
                                } else {
                                    $deleteBtn.popover('destroy');

                                    $this.removeClass('active');
                                }
                            });

                            var $successLoad = $('<div class="successLoad fa fa-check-circle text-success"></div>'),
                                speeds = {
                                    show: 300,
                                    wait: 500,
                                    hide: 500
                                };

                            $galleryItem.append($successLoad);
                            $successLoad.animate({fontSize: '100px'}, speeds.show, null, function () {
                                setTimeout(function () {
                                    $successLoad.animate({opacity: 0}, speeds.hide, null, function () {
                                        $successLoad.remove();
                                    });
                                }, speeds.wait);
                            });

                            _this.sort();
                        } else {
                            var $deleteBtn = $galleryItem.find('.load_img_close');

                            $deleteBtn.click(function () {
                                $galleryItem.remove();

                                _this.$imageList.find('.imageItem').show();
                            });

                            $galleryItem.addClass('errorItem');
                            $galleryItem.append($('<div class="error"></div>'));
                            $galleryItem.append($('<div class="errorText">' + result.errors[0].errors[0] + '</div>'));
                        }

                        _this.on.load($galleryItem);
                    },
                    {
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();

                            //Upload progress
                            xhr.upload.addEventListener(
                                'progress',
                                function (evt) {
                                    if (evt.lengthComputable)
                                        $progressBar.css('width', Math.round(evt.loaded / evt.total * 100) + '%');
                                },
                                false
                            );

                            //Download progress
                            xhr.addEventListener(
                                'progress',
                                function(evt){
                                    if (evt.lengthComputable)
                                        $progressBar.css('width', Math.round(evt.loaded / evt.total * 100) + '%');
                                },
                                false
                            );
                            return xhr;
                        }
                    }
                );

                $image.remove();
                $canvas.remove();
            }
        );
    };
};

SiteGallery.prototype.getFileSizeByBase64 = function (data) {
    return data.split(',')[1].length * 3 / 4;
};

SiteGallery.prototype.resizeImage = function (sizes) {
    if (sizes.w > sizes.maxW || sizes.h > sizes.maxH) {
        if (sizes.w > sizes.maxW) {
            sizes.h = Math.round(sizes.h * sizes.maxW / sizes.w);
            sizes.w = sizes.maxW;
        } else if (sizes.h > sizes.maxH) {
            sizes.w = Math.round(sizes.w * sizes.maxH / sizes.h);
            sizes.h = sizes.maxH;
        }

        sizes = this.resizeImage(sizes);
    }

    return sizes;
};

SiteGallery.prototype.delete = function (id) {
    if (id) {
        var _this = this;

        ajax(
            'deleteFromSiteGallery',
            {
                'ImageID': id,
                'ParentID': _this.parentID,
                'Gallery': _this.gallery,
                'Key': _this.key
            },
            function (result) {
                if (result) {
                    _this.on.load();
                    _this.$imageList.find('li.imageItem').filter(function () {
                        return ($(this).data('id') == id);
                    }).hide(
                        300,
                        function () {
                            $(this).remove();

                            _this.setLoadButtonLabel();
                        }
                    );
                }
            }
        );
    }
};