var Core;

;(function($) {
	"use strict";

	Core = {
		DOMReady: function () {
			var self = this;

			self.contentHeight.init();
		},
		windowLoad: function () {
			var self = this;

			if ($('.gmap').length)
				self.googleMaps();

			if ($('#aside1_inner').length) {
			    // setTimeout(function () {
			     	self.LeftFilterMenu.init();
			    // }, 500);
			}
		},
		contentHeight: {
			init: function () {

				var self  = this;
				self.w = $(window);
				self.header = $('#header');
				self.mainTopBox = $('.main_top_box');
				self.mapBoxLg = $('.map_box_lg');
				self.contentHeight = $('.content');
				self.aside = $('#aside1');
				self.contentHeightTwo = $('.content2');
				self.rightBoxWr = $('.right_box_wr');
				self.footer = $('footer');
				self.rightBoxBuild = $('.right_box_height_100');


				self.calculationHeight();
				self.filterHeight(true);

				self.events();

				self.w.on('resize', function () {
					self.calculationHeight();
				});
			},

			calculationHeight: function () {

				var self = this;

				self.wHeight = self.w.height();
				self.headerHeight = self.header.height();
				self.mainTopBoxHeight = self.mainTopBox.height();
				self.footerHeight = self.footer.height();
				self.otherHeight = self.wHeight - self.headerHeight - self.mainTopBoxHeight;
				self.otherHeightBuild = self.wHeight - self.headerHeight - self.footerHeight;

				var mapContainerHeight = self.otherHeight /*- $('.searchHeader').height() - 17*/ + 17;

				self.mapBoxLg.height(mapContainerHeight);
				self.rightBoxWr.height(mapContainerHeight);
				$('.map_box_lg .map_item').height(mapContainerHeight);
				self.rightBoxBuild.css({ "min-height": self.otherHeightBuild + 'px' });

			},

			events: function () {

				var self = this;

				//Кнопка открытия краты
			    $(".open_map").on('click', function () {
			    	$('.searchContent').css('overflow', 'hidden');

                    if ($(this).hasClass('noAgent'))
                        return false;

			    	if ($(this).hasClass('active')) {
                        $('html,body').css('overflow', '');
                        $('.searchHeader').show();
                        $('.scrollToTop', $(this)).show();

			    		self.closeMap($(this));
			    		self.filterHeight(true);
			    	} else {
                        $('html, body').css({'overflow': 'hidden', 'scrollTop': '0'}); //Убирает прокрутку
                        $('.searchHeader').hide();
                        $('.scrollToTop', $(this)).hide();

						self.openMap($(this));
			    		self.filterHeight(true);
			    	}
				});

				self.w.on('resize', function () {
					if (self.mapBoxLg.hasClass('active')) {
						//Карта открыта
						self.filterHeight(true);
					} else {
						self.filterHeight(true);
					}
				});

				self.w.on('scroll', function () {
                    self.filterHeight(true);
				});
			},
			filterHeight: function (active) {
				var self  = this;

				if (active) {
					self.wHeight = self.w.height();
					self.headerHeight = self.header.height();
					self.mainTopBoxHeight = self.mainTopBox.height();
					self.otherHeight = self.wHeight - self.headerHeight - self.mainTopBoxHeight;

					var $menu = $('.mainMenu'),
						scrollTop = $(window).scrollTop(),
						menuHeight = $menu.outerHeight(true),
						headerHeight = $('#header').outerHeight(true) + menuHeight,
						contentHeight = $('.left_box').height();

					self.contentHeight.height(
                        $(window).height()
                        - (
                            (!$menu.hasClass('fixed') && headerHeight > scrollTop)
                                ? headerHeight - scrollTop
                                : menuHeight
                        )
                        - (
                            (scrollTop + self.wHeight > headerHeight + contentHeight)
                                ? scrollTop + self.wHeight - headerHeight - contentHeight
                                : 0
                        )
					);
					self.contentHeightTwo.height(self.otherHeight);
				} else {
					self.contentHeight.css({'height':'auto'});
					self.contentHeightTwo.css({'height':'auto'});
				}
			},
			openMap: function (el) {
				el.addClass("active");

				//Список зданий
				$('.hide_box').addClass("active").slideUp("medium", function () {
                    $('.searchContent').css('overflow', '');
				});

				$('.hide_box_2').addClass("active");

				$('.map_box_lg').addClass("active");

				//$('#content-2').addClass("content");
				// $('#content-2').addClass("mCustomScrollbar");

				$('.right_box_wr').removeClass("active");

				$('.content').removeClass("active");
			},
			closeMap : function(el) {

				el.removeClass("active");
				
				$('.hide_box').removeClass("active").slideDown("medium", function () {
                    $('.searchContent').css('overflow', '');
                    $('.hide_box_2').removeClass("active");
				});

				$('.map_box_lg').removeClass("active");

				//$('#content-2').removeClass("content");
				// $('#content-2').removeClass("mCustomScrollbar");

				$('.right_box_wr').addClass("active");

				$('.content').addClass("active");
				
			},
		},
		
		/**
		**	Google Map
		**/

		maps: {},
		googleMaps: function () {

			// Create a map object and specify the DOM element for display.

			var styleArray = [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}],

				mapSettings = {
					scrollwheel: false,
				    //styles: styleArray,
				    //mapTypeId: google.maps.MapTypeId.ROADMAP,
				    zoom: 10
				},
				_this = this;


			$('.gmap').each(function(i, el) {
				var dataCoords = $(el).data('coords') ? eval($(el).data('coords')) : [],
					id = $(el).attr('id'),
					markers = [],
					mapPlace = $(this).parent().find('.map_item'),
					qtMarkers = dataCoords.length,
					map = null,
					mapContainer = document.getElementById(id);

				mapSettings.center = ($(el).data('lat') && $(el).data('lng'))
					? {lat: $(el).data('lat'), lng: $(el).data('lng')}
					: dataCoords[0];

				if (!$(this).hasClass('notAutoActive')) {
					map = new google.maps.Map(
						mapContainer,
						mapSettings
					);

					if (qtMarkers) {
						for (var i = 0; i < qtMarkers; i++) {
							var marker = new google.maps.Marker({
								map: map,
								position: dataCoords[i],
								icon: '/img/design/marker.png',
								label: dataCoords[i].count + '',
								title: dataCoords[i].title
							});

							markers.push(marker);

							markerInfo(marker, i, qtMarkers, markers, $(mapPlace[i]));
						}
					}
				}

                Global.maps[id] = {
					map: map,
					container: mapContainer,
					markers: markers,
					settings: mapSettings
				};
			});

			function markerInfo(marker, index, qt, markers, mapPlace) {

				marker.addListener('click', function () {
					for (var i = 0; i <= qt - 1; i++) {
						// var number = i+ 1 + '';

						if (!(i == index)) {
							markers[i].setIcon('/img/design/marker.png');
							// markers[i].setLabel(number);
						}
					}

					marker.setIcon('/img/design/marker_active.png');
					marker.setLabel('');

					mapPlace.show().siblings('.map_item').hide();

					$('.map_item_close').on('click',function () {

						$(this).closest('.map_item').hide();

						for (var i = 0; i <= qt-1; i++) {

							var number = i + 1 + '';

							markers[i].setIcon('/img/design/marker.png');
							markers[i].setLabel(number);
						}
					});
				});
			}
		},

		LeftFilterMenu: {
			init: function () {
				var self = this;

				self.savePosition	=	false; //Для дебага, чтобы отслеживать состояния прокрутки
				self.checkPosition	=	false; //Не изменять позицию меню на экране в ходе каких-либо изменений (при аякс поиске и прокрутке вверх)
                self.timerId		=	null;
				self.mainBox = $('.main_box');
				self.aside = $('#aside1');
				self.asideInner = $('#aside1_inner');
				self.w = $(window);
				self.currentScrollTop = 0;

				self.map = false;

				self.events();

				self.w.on('scroll', function () {
				});

				self.w.on('resize',function () {
					// self.filterHeight();
				});
			},
			events: function(){
                $('.open_map').on('click', function () {
                    var $this = $(this),
                        $searchBtn = $('#searchFilter .searchButton');
                    Global.currentSearchID = $("input[name='SearchID']").val();

                    if ($this.hasClass('active')) {
                        self.map = true;
                        Global.searchMapOpened = true;

                        $searchBtn.data('no-map', '0');
                        $searchBtn.data('no-list', '1');
                        $searchBtn.click();
                    } else {
                        Global.searchMapOpened = false;
                        self.map = false;

                        $searchBtn.data('no-map', '1');
                        $searchBtn.data('no-list', '0');

                        $searchBtn.click();
                    }
                    Global.currentSearchID = 0;
                });

				//Выползание подменю
				var self = this;

				$('.filter_btn').on('click',function () {

					var $this = $(this);

                    /*self.timerId = setInterval(function () {
						self.filterHeight();
					}, 30);*/

					if (!$(this).hasClass('active')) {
						$this.addClass('active');

						$this.next(".filter_box").slideDown(500, function () {
							var offset = $this.offset().top,
								windowScroll = $(window).scrollTop();

							if (offset < windowScroll) {
						    	$('html, body').stop().animate({
									scrollTop: offset
						    	}, 1000, function () {
						    		clearInterval(self.timerId);
						    	});
							}
							else
							{
                                clearInterval(self.timerId);
							}
						});
					} else {
						$this.removeClass('active');

						$this.next(".filter_box").slideUp(500, function () {
							clearInterval(self.timerId);
						});
					}
				});
			}
		}
	};

	$(document).ready(function () {
		Core.DOMReady();
        Core.windowLoad();
	});
})(jQuery);