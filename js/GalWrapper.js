/**
 * Вращалка фотографий
 * TODO доделать анимацию + вертикальный + автовращение
 * config = {
 *      vertical:       boolean     //вертикальная прокрутка
 *      controlButtons: boolean     //кнопки управления прокруткой
 *      counter:        boolean     //счетчик кол-ва фотографий
 *      circle:         boolean     //зацикленная прокрутка
 * }
 * @param $container
 * @param config
 * @constructor
 */
function GalleryWrapper($container, config) {
    config = config || {};

    var _this = this,
        $gallery = $('<div class="galleryItems"></div>'); //элемент с прокручиваемыми элементами

    _this.$container = $container; //главный контейнер wrapper'a
    _this.$items = $container.children(); //прокручиваемые элементы
    _this.currentItem = 0; //номер текущего первого элемента

    $gallery.append(_this.$items); //перемещаем прокручиваемые элементы в их контейнер

    _this.$gallery = $gallery;

    _this.$container.append($gallery);

    if (config.vertical === true) //если вертикальный wrapper, то делаем нормальный перенос прокручиваемых элементов
        $gallery.css('white-space', 'normal');

    if (config.controlButtons !== false && _this.$items.length > 1) { //добавляем управляющие кнопки
        var $pervBtn = $("<div class='ctrlBtns prev'></div>"),
            $nextBtn = $("<div class='ctrlBtns next'></div>");

        $pervBtn.click(function () {
            _this.prev();
        });

        $nextBtn.click(function () {
            _this.next();
        });

        _this.$container.append($pervBtn);
        _this.$container.append($nextBtn);
    }


    var count   =   $container.data('count');
    if(!count)  count   =   _this.$items.size();

    if (config.counter !== false) //добавляем счетчик
        _this.$container.append($('<div class="counter">' + count + '</div>'));

    _this.$container.addClass('active');

    return this;
}

GalleryWrapper.prototype.next = function () {
    this.$gallery.append(this.$items.first());
    this.$items = this.$gallery.children();

    //this.currentItem--;
};

GalleryWrapper.prototype.prev = function () {
    this.$gallery.prepend(this.$items.last());
    this.$items = this.$gallery.children();

    // this.currentItem++;
};