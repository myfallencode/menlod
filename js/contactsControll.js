function deleteContact($btn, type) {
    var $contact = $btn.closest('.contactItem'), //строка с контактом
        $contactParent = $contact.parent(),
        $container = $container.closest('.personContacts');

    $contact.remove();

    if (!$contactParent.find('.contactItem').size()) //если нет контактов то грузим пустой
        addContact($contactParent, type);

    $contactParent.find('.contactItem:first').find('label').show();
    $contactParent.find('.contactItem:not(:first)').find('label').hide();


    var maxCount = $container.data('max-count') || 0,
        curCount = $container.find('.contactItem').length,
        canAdd = (maxCount && curCount < (maxCount - 1) || !maxCount);

    if (canAdd)
        $container.find('tr:last-child .contactsBtns > *').show();
}

function addContact($container, type) {
    var $personContainer = $container.closest('.contactsPerson'),
        $contactParent = $container.closest('.personContacts'),
        counter = type + 'count',
        count = $personContainer.data(counter),
        personID = $personContainer.data('person'),
        maxCount = $contactParent.data('max-count') || 0,
        curCount = $contactParent.find('.contactItem').length,
        canAdd = (maxCount && curCount < (maxCount - 1) || !maxCount);

    if (personID && in_array(type, ['phone', 'email'])) {
            ajax(
                'drawPersonContact',
                {
                    personID: $personContainer.data('person'),
                    type: type,
                    num: count
                },
                function (result) {
                    if (result) {
                        var $lastContact = $container.find('.contactItem').last(),
                            $result = $(result);

                        if ($lastContact.size()) {
                            $lastContact.after($result);
                        } else {
                            $container.prepend($result);
                        }

                        $personContainer.data(counter, count + 1);

                        initMaskedInput();

                        if (!canAdd)
                            $container.find('tr:last-child .contactsBtns > *').hide();
                    }

                    // $container.find('.contactItem:first').find('label').show();
                    // $container.find('.contactItem:not(:first)').find('label').hide();
                }
            );
    }
}

function deletePerson($person) {
    var $personsContainer = $person.closest('.contactsItems'),
        tabID = $person.parents('.tab-pane').attr('id');

    $person.parents('.tab-pane').remove();
    $('[href="#'+tabID+'"]').parent().remove();

    if (!$personsContainer.find('.tab-content').children().size()) {
        addPerson(
            $personsContainer,
            $personsContainer.find('.tab-content'),
            function () {
                $('.plusContactTab').show();
                initPersonNameChange();
                $personsContainer.find('.nav-tabs [data-toggle="tab"]:first').click();
            }
        );
    } else {
        $('.plusContactTab').show();
        initPersonNameChange();
        $personsContainer.find('.nav-tabs [data-toggle="tab"]:first').click();
    }
}

var contactID = 0;

function addPerson($container, $btn, after) {
    ajax(
        'drawPerson',
        {
            num: $container.children().size() + 1
        },
        function (result) {
            if (result) {
                contactID = $btn.parents('.tab-base').find('.nav-tabs [data-toggle="tab"]').length + 1;

                var $tabItem = $($.parseHTML("<li><a data-toggle='tab' id='contactLink" + contactID + "' href='#newContact" + contactID + "'><span>Контакт " + contactID + "</span></a></li>"));

                $btn.parents('.tab-base').find('.tab-content').append("<div id='newContact" + contactID + "' class='tab-pane fade'>" + result + "</div>");
                $btn.parents('.tab-base').find('.plusContactTab').before($tabItem);

                var $result = $(result);

                //$container.append($result);

                initMaskedInput($container);
                initAutoTextareaSize($container);

                $result.find('.personName input').focus();

                var contactsCount = $btn.parents('.tab-base').find('.nav-tabs [data-toggle="tab"]').length;

                if(contactsCount >= 5)
                    $('.plusContactTab').hide();

                initPersonNameChange();

                $tabItem.find('a').click();

                if (typeof after == 'function')
                    after();
            }
        }
    );
}

function clearContactItem($btn) {
    $('input[type=text]', $btn.parent()).val('');

    var $container = $btn.closest('.contactItem').parent(),
        $contactParent = $container.closest('.personContacts'),
        $inputs = $container.find('.contactItem input[type=text]'),
        $emptyInputs = $inputs.filter(function () {
            return !$(this).val();
        });

    if ($inputs.size() > $emptyInputs.size())
        $emptyInputs.closest('.contactItem').remove();

    var maxCount = $contactParent.data('max-count') || 0,
        curCount = $contactParent.find('.contactItem').length,
        canAdd = (maxCount && curCount < (maxCount) || !maxCount);

    if (canAdd)
        $container.find('tr:last-child .contactsBtns > *').show();
}