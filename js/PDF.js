function PDF(params) {
    var _this = this; // this

    _this.name = params.name; // название шаблона
    _this.parents = is_array(params.parents) && params.parents ? params.parents : []; // родители (массивом)

    _this.disable = true; // чтобы лишнего чего не вызывать когда не надо

    if (_this.name && _this.parents.length) {
        params.on = params.on || {};

        _this.loadedImages = {};

        _this.on = { // события
            load: function (pdf) {
                if (typeof params.on.load == 'function')
                    params.on.load(pdf);
            },
            close: function (pdf) {
                if (typeof params.on.close == 'function')
                    params.on.close(pdf);
            },
            save: function (pdf) {
                if (typeof params.on.save == 'function')
                    params.on.save(pdf);
            }
        };

        _this.maxImages = 0;

        _this.$modal = ajaxModal( // создаем PDF
            'createPDF',
            {
                Name: _this.name,
                Parents: _this.parents
            },
            function (data, el, result) {
                _this.key = result.Key;
                _this.PdfID = result.PdfID;

                data.body.closest('.arcticmodal-container').addClass('pdfEditContainer');

                if (result.Success) {
                    _this.initPaginator(0);

                   //var $color = $('.color', data.body);

                    $('.colorPicker').colorPicker({
                        customBG: '#fff',
                        margin: '4px -2px 0',
                        doRender: 'div div',
                        opacity: false,
                        animationSpeed: 0,
                        renderCallback: function ($elm, toggled) {
                        },
                        positionCallback: function($elm) {
                            var $UI = this.$UI, // this is the instance; this.$UI is the colorPicker DOMElement
                                position = $elm.offset(), // $elm is the current trigger that opened the UI
                                gap = this.color.options.gap, // this.color.options stores all options
                                top = 0,
                                left = 0;

                            // $UI.appendTo('#somwhereElse');
                            // do here your calculations with top and left and then...
                            return {
                                left: position.left,
                                top: position.top + $elm.outerHeight(true) + 30
                            };
                        },
                        buildCallback: function ($elm) {
                            var colorInstance = this.color,
                                colorPicker = this,
                                random = function(n) {
                                    return Math.round(Math.random() * (n || 255));
                                },
                                $colorsContainer = $('<div class="cp-memory"></div>'),
                                $buttons = $($.parseHTML(
                                    '<div class="colorButtons">' +
                                        '<button type="button" class="cancelButton btn btn-default btn-sm">Отменить</button>' +
                                        '<button type="button" class="acceptButton btn btn-danger btn-sm pull-right">Сохранить</button>' +
                                    '</div>'
                                ));

                            $elm.append($colorsContainer);
                            $elm.append($buttons);

                            $colorsContainer.append($($.parseHTML(
                                '<div class="colorsRow"><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>' +
                                '<div class="colorsRow"><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>' +
                                '<div class="colorsRow"><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>'
                            )));

                            $colorsContainer.find('.colorsRow > div').click(function(e) {
                                colorInstance.setColor($(this).css('background-color'));
                                colorPicker.render();
                            }).each(function() {
                                !this.className && $(this).css({background:
                                'rgb(' + random() + ', ' + random() + ', ' + random() + ')'
                                });
                            });

                            $buttons.find('.cancelButton').click(function () {
                                colorInstance.setColor($('input.color', data.body).val().substr(1));
                                colorPicker.render();

                                $elm.hide();
                            });

                            $buttons.find('.acceptButton').click(function () {
                                var $colors = $('.colorsRow > div', $colorsContainer).get(),
                                    curColor = '#' + colorInstance.colors.HEX;

                                for (var i = $colors.length - 1; i > 0 ; i--)
                                    $($colors[i]).css('background-color', $($colors[i - 1]).css('background-color'));

                                $($colors[0]).css('background-color', curColor);

                                $('input.color', data.body).val(curColor);
                                _this.changeColor(curColor);

                                $elm.hide();
                            });
                        }
                    });

                    $('input.color').change(function () {
                        _this.changeColor($(this).val());
                    });

                    _this.disable = false; // разрешаем выполнять действия

                    _this.PdfID = result.PdfID; // PdfID

                    _this.on.load(_this); // выполняем событие создания

                    $('button.download', _this.$modal).click(function () {
                        _this.save();

                        return false;
                    });

                    _this.imagesConfig = $('.pdfEdit', _this.$modal).data('config');
                    _this.pageConfig = {};

                    for (var groupName in _this.imagesConfig) {
                        var groupConfig = _this.imagesConfig[groupName];

                        for (var pageName in groupConfig.Pages) {
                            if (!_this.pageConfig[pageName]) {
                                _this.pageConfig[pageName] = {
                                    Groups: [groupName],
                                    Images: groupConfig.Pages[pageName]
                                };
                            } else {
                                _this.pageConfig[pageName].Groups.push(groupName);
                                _this.pageConfig[pageName].Images = _this.pageConfig[pageName].Images.concat(groupConfig.Pages[pageName]);
                            }
                        }
                    }

                    _this.initPages();
                }
            },
            {
                beforeClose: function (data) {
                    if (!data.body.data('close')) {
                        modal(
                            '<div style="width: 270px;">' +
                                '<h4>Закрыть?</h4>' +
                                '<div class="text_gray text_sm">Вы уверены, что хотите закончить редактирование? Ваши изменения на будут сохранены.</div><br>' +
                                '<div>' +
                                    '<button type="button" class="btn btn-default cancel" style="width: 110px;">Отмена</button>' +
                                    '<button type="button" class="closeBtn btn btn-danger pull-right" style="width: 110px;">Закрыть</button>' +
                                '</div>' +
                            '</div>',
                            {
                                onInit: function ($modal) {
                                    $modal.find('button.cancel').click(function () {

                                        $modal.arcticmodal('close');
                                    });

                                    $modal.find('button.closeBtn').click(function () {
                                        data.body.data('close', 1);

                                        logActions(33);
                                        $.arcticmodal('close');
                                    });
                                }
                            }
                        );

                        return false;
                    } else {
                        return true;
                    }
                }
            }
        );
    }
}

PDF.prototype.initPaginator = function (activeIndex) {
    var paginator = new ButtonsSelect(
        $('#PdfPage_buttonsSelect'),
        {
            onClick: function (i) {
                var $container = $('.pdfEditContainer .pdfEditBody');

                $container.customScrollbar("scrollToY", $($container.find('.pdfEditPage').get(i)).position().top);
            }
        }
    );

    $('.pdfEditBody').height( (parseInt(document.documentElement.clientHeight) - 95) + "px") .customScrollbar({
        wheelSpeed: 80,
        animationSpeed: 100,
        onCustomScroll: function (e, data) {
            var $pages = $('.pdfEditPage'),
                curScroll = Math.round($pages.filter(':first').parent().height() * data.scrollPercent / 100),
                curHeight = 0;

            $pages.each(function (i) {
                var $this = $(this),
                    height = $this.outerHeight(true);

                if (curHeight <= curScroll && curScroll <= (curHeight + height))
                    paginator.setActive(i);

                curHeight += height;
            });
        }
    });

    paginator.set(activeIndex || 0);
};

PDF.prototype.initPages = function (groupName) {
    var _this = this,
        groupNames = groupName ? [groupName] : Object.keys(_this.imagesConfig);

    $('.pdfEditPageContent img[data-loaded=1]').each(function () {
        var $this = $(this);

        if (!$this.attr('src'))
            $this.attr('src', 'data:image/jpeg;base64,' + _this.loadedImages[$this.data('id')].data);
    });

    CKEDITOR.replaceAll();

    for (var i in groupNames) {
        for (var pageName in _this.imagesConfig[groupNames[i]].Pages) {
            $('.pdfEditPageContent[data-page=' + pageName + '] .changeImages', _this.$modal).click(function () {
                var pageName = $(this).closest('.pdfEditPageContent').data('page'),
                    groupName = _this.pageConfig[pageName].Groups[0];

                if (pageName && _this.pageConfig[pageName] && groupName) {
                    ajaxModal(
                        'getImagesPicker',
                        {
                            Parents: _this.parents,
                            Group: _this.pageConfig[pageName].Groups[0],
                            Images: _this.imagesConfig[groupName].Images,
                            WithLayout: 1,
                            LoadedImages: Object.keys(_this.loadedImages)
                        },
                        function (modal, el, result) {
                            _this.$changeImagesModal = modal.body;

                            if (result.Success) {
                                modal.body.html(result.HTML);

                                _this.maxImages = result.MaxImagesCount;

                                _this.initImagePicker(modal.body);

                                var $fileLoader = $('input[type=file].pickerImage', modal.body);

                                $fileLoader.click(function (e) {
                                    e.stopPropagation();
                                });

                                $('.imagesContainer .imagesItems', _this.$changeImagesModal).customScrollbar();

                                $('.loadImageItem', modal.body).click(function (e) {
                                    e.stopPropagation();

                                    $fileLoader.click();

                                    return false;
                                });

                                $fileLoader.change(function () {
                                    for (var imagesCount = 0; imagesCount < this.files.length; imagesCount++)
                                        _this.addImage(this.files[imagesCount]);
                                });

                                $('.cancelPicker', modal.body).click(function () {
                                    delete _this.$changeImagesModal;

                                    if (!_this.imagesConfig[groupName].SaveManual)
                                        _this.imagesConfig[groupName].Manual = 0;

                                    modal.body.arcticmodal('close');
                                });

                                $('.acceptPicker', modal.body).click(function () {
                                    _this.imagesConfig[groupName].SaveManual = 1;

                                    delete _this.$changeImagesModal;

                                    _this.imagesConfig[groupName].Images = [];

                                    _this.imagesConfig[groupName].Images = $('.imagesItems .imageItem .checkbox:checked', modal.body).get().map(function (item) {
                                        return {
                                            ImageID: item.value,
                                            Loaded: $(item).data('loaded'),
                                            Date: $(item).data('date')
                                        };
                                    }).sort(function compare(a, b) {
                                        if (a.Date < b.Date)
                                            return -1;

                                        if (a.Date > b.Date)
                                            return 1;

                                        return 0;
                                    });

                                    _this.loadPagesGroup(groupName);

                                    modal.body.arcticmodal('close');
                                });

                                initShowGallery(modal.body);

                                _this.getTemplateForGroup(groupName);
                            }
                        },
                        {
                            closeOnEsc: false,
                            closeOnOverlayClick: false
                        }
                    );
                }
            });
        }
    }
};

PDF.prototype.loadPagesGroup = function () {
    var _this = this,
        data = $('form.pdfEdit', _this.$modal).serializeArray();

    for (var groupName in _this.imagesConfig)
        for (var i in _this.imagesConfig[groupName].Images) {
            var curImage = _this.imagesConfig[groupName].Images[i];

            if (curImage.Loaded && _this.loadedImages[curImage.ImageID].data) {
                data.push({
                    name: 'LoadedImages[' + curImage.ImageID + ']',
                    value: _this.loadedImages[curImage.ImageID].width + 'x' + _this.loadedImages[curImage.ImageID].height
                });
            }
        }

    $('form.pdfEdit textarea.ckeditor').each(function () {
        var $this = $(this);

        data.push({
            name: $this.attr('name'),
            value: CKEDITOR.instances[$this.attr('name')].getData()
        });
    });

    ajax(
        'loadPagesGroup',
        data.concat([
            {
                name: 'Name',
                value: _this.name
            },
            {
                name: 'PdfID',
                value: _this.PdfID
            },
            {
                name: 'Parents[]',
                value: _this.parents
            },
            {
                name: 'Config',
                value: JSON.stringify(_this.imagesConfig)
            }
        ]),
        function (result) {
            if (result.Success) {
                var curPage = $('#PdfPage_buttonsSelect > div > button').index($('#PdfPage_buttonsSelect > div > button.btn-danger'));

                $('.pdfEditBody').customScrollbar();
                $('.pdfEditBody').customScrollbar('remove');

                $('.pdfEditContainer .pdfEditPage:first').parent().html(result.HTML);
                $('#PdfPage_buttonsSelect').replaceWith(result.Paginator);

                _this.initPages();

                _this.initPaginator(Math.min(curPage, $('#PdfPage_buttonsSelect > div > button').length - 1));
            }
        }
    );
};

PDF.prototype.initImagePicker = function ($container) {
    var _this = this,
        getImagesCount = function () {
            return $('.imagesItems .imageItem .checkbox:checked', $container).length;
        };

    initPlugins($container, ['checkbox']);

    $('.imagesItems .imageItem .checkbox:not(:checked)', $container).prop('disabled', getImagesCount() >= _this.maxImages);
    $('.imagesItems .imageItem[data-loaded]', $container).each(function () {
        var $this = $(this),
            id = $this.data('loaded');

        if (!$('img', $this).length && _this.loadedImages[id].data)
            $this.find('> div:first').append('<img src="data:image/jpeg;base64,' + _this.loadedImages[id].data + '">');
    });

    $('.imagePicker .checkbox', $container).change(function () {
        var $this = $(this),
            $imageItem = $this.closest('.imageItem'),
            curCount = getImagesCount(),
            groupName = $this.closest('.pdfImagesPicker').data('group');

        $this.data('date', Date.now());

        if ($this.is(':checked')) {
            $imageItem.addClass('active');
        } else {
            $imageItem.removeClass('active');
        }

        _this.imagesConfig[groupName].Manual = 1;

        $('.imagesItems .imageItem .checkbox:not(:checked)', $container).prop('disabled', curCount >= _this.maxImages);

        _this.getTemplateForGroup(groupName);
    });
};

PDF.prototype.getTemplateForGroup = function (defaultGroupName) {
    var _this = this,
        data = $('form.pdfEdit', _this.$modal).serializeArray(),
        groupImages = $('.imagesItems .imageItem .checkbox:checked', modal.body).get().map(function (item) {
            return {
                ImageID: item.value,
                Loaded: $(item).data('loaded'),
                Date: $(item).data('date')
            };
        }).sort(function compare(a, b) {
            if (a.Date < b.Date)
                return -1;

            if (a.Date > b.Date)
                return 1;

            return 0;
        });


    for (var i in groupImages) {
        var curImage = groupImages[i];

        if (curImage.Loaded && _this.loadedImages[curImage.ImageID].data) {
            data.push({
                name: 'LoadedImages[' + curImage.ImageID + ']',
                value: _this.loadedImages[curImage.ImageID].width + 'x' + _this.loadedImages[curImage.ImageID].height
            });
        }
    }

    var images = JSON.parse(JSON.stringify(_this.imagesConfig));

    images[defaultGroupName].Images = groupImages;

    $('form.pdfEdit textarea.ckeditor').each(function () {
        var $this = $(this);

        data.push({
            name: $this.attr('name'),
            value: CKEDITOR.instances[$this.attr('name')].getData()
        });
    });

    ajax(
        'loadPagesGroup',
        data.concat([
            {
                name: 'Name',
                value: _this.name
            },
            {
                name: 'PdfID',
                value: _this.PdfID
            },
            {
                name: 'Parents[]',
                value: _this.parents
            },
            {
                name: 'Config',
                value: JSON.stringify(images)
            },
            {
                name: 'GroupName',
                value: defaultGroupName
            }
        ]),
        function (result) {
            if (result.Success) {
                $('.pdfImagesPicker .pagesContainer .templatesPages').html(result.HTML);

                $('.pdfImagesPicker .pagesContainer .templatesPages img[data-loaded]').each(function () {
                    var $this = $(this);

                    if (!$this.attr('src'))
                        $this.attr('src', 'data:image/jpeg;base64,' + _this.loadedImages[$this.data('id')].data);
                });
            }
        }
    );
};

PDF.prototype.addImage = function (file) {
    var image = new Image(),
        _this = this;

    image.src = URL.createObjectURL(file); //создаем изображение

    _this.$changeImagesModal.find('.loadImageItem').addClass('loading');

    image.onload = function () { // после загрузки
        var $image = $(image), // формурием dom изображения
            size = { //размеры изображения
                width: MAX_WIDTH,
                height: MAX_HEIGHT
            },
            $canvas = $("<canvas></canvas>"), //канва для ресайза и сжатия
            ctx = $canvas[0].getContext("2d"),
            $imageIcon = $image.clone(); // иконка картинки

        var $imageContainer = $($.parseHTML(
            '<div class="imageItem">' +
                '<div></div>' +
            '</div>'
        ));

        $imageContainer.find('> div').append($imageIcon);

        $imageIcon[0].onload = function () { // после загрузки иконки нового изображения
            if ($imageIcon.width() < $imageIcon.height())
                $(this).addClass('vertical');

            var $container = $('.imagesContainer .imagesItems', _this.$changeImagesModal);

            $container.customScrollbar();
            $container.customScrollbar('remove');

            $container.customScrollbar();
        };


        $('.imagesItems .loadImageItem', _this.$changeImagesModal).after($imageContainer); //добавляем новое

        $image.css({ //добавляем изображение
            position: 'absolute',
            left: -10000,
            top: -10000
        });

        $('body').append($image);

        $canvas.css({ //добавляем канву
            position: 'absolute',
            left: -10000,
            top: -10000
        });

        $('body').append($canvas);

        var iWidth = $image.width(), //текущие размеры изображения
            iHeight = $image.height();

        if (iWidth > size.width || iHeight > size.height) { //если текущие размеры изображения выходят за рамки дозволенного
            var newSizes = _this.resizeImage({ //то посчитываем новые размеры
                w: iWidth,
                h: iHeight,
                maxW: size.width,
                maxH: size.height
            });

            iWidth = newSizes.w; //новые размеры
            iHeight = newSizes.h;
        }

        $canvas[0].width = iWidth; //ресайзим канву
        $canvas[0].height = iHeight;

        ctx.drawImage(image, 0, 0, iWidth, iHeight); //рисуем на канве новое изображение

        _this.imageCompression( //пережимаем изображение
            {
                canvas: $canvas[0],
                w: iWidth,
                h: iHeight,
                image: image
            },
            function (data, size) { //после сжатия отправляем на сервер
                var uid = getUID();

                _this.loadedImages[uid] = {
                    data: data.split(',')[1],
                    width: iWidth,
                    height: iHeight
                };

                $image.remove();
                $canvas.remove();

                _this.$changeImagesModal.find('.loadImageItem').removeClass('loading');

                $imageContainer.append($($.parseHTML(
                    '<div class="imagePicker">' +
                        '<input type="checkbox" class="checkbox" value="' + uid + '" id="checkbox1-' + uid + '" data-loaded="1">' +
                        '<label for="checkbox1-' + uid + '"></label>' +
                    '</div>'
                )));

                _this.initImagePicker(_this.$changeImagesModal);
            }
        );
    };
};

PDF.prototype.save = function () { // скачать pdf
    var _this = this,
        data = $('form.pdfEdit', _this.$modal).serializeArray();

    for (var groupName in _this.imagesConfig) {
        for (var i in _this.imagesConfig[groupName].Images) {
            var curImage = _this.imagesConfig[groupName].Images[i];

            if (curImage.Loaded && _this.loadedImages[curImage.ImageID].data) {
                data.push({
                    name: 'LoadedImage_' + curImage.ImageID,
                    value: _this.loadedImages[curImage.ImageID].data
                });
            }
        }
    }

    $('form.pdfEdit textarea.ckeditor').each(function () {
        var $this = $(this);

        data.push({
            name: $this.attr('name'),
            value: CKEDITOR.instances[$this.attr('name')].getData()
        });
    });

    ajax(
        'savePDF',
        data.concat([
            {
                name: 'Name',
                value: _this.name
            },
            {
                name: 'PdfID',
                value: _this.PdfID
            },
            {
                name: 'Parents[]',
                value: _this.parents
            },
            {
                name: 'Config',
                value: JSON.stringify(_this.imagesConfig)
            }
        ]),
        function (result) {
            if (result.Link) {
                _this.loadedImages = {};

                // $.arcticmodal('close');

                window.open(result.Link);
            }
        }
    );
};


PDF.prototype.resizeImage = function (sizes) {
    if (sizes.w > sizes.maxW || sizes.h > sizes.maxH) {
        if (sizes.w > sizes.maxW) {
            sizes.h = Math.round(sizes.h * sizes.maxW / sizes.w);
            sizes.w = sizes.maxW;
        } else if (sizes.h > sizes.maxH) {
            sizes.w = Math.round(sizes.w * sizes.maxH / sizes.h);
            sizes.h = sizes.maxH;
        }

        sizes = this.resizeImage(sizes);
    }

    return sizes;
};

PDF.prototype.getFileSizeByBase64 = function (data) {
    return data.split(',')[1].length * 3 / 4;
};

PDF.prototype.imageCompression = function (obj , success) {
    var _this = this,
        canvas = obj.canvas,
        ctx = canvas.getContext("2d"),
        data = canvas.toDataURL("image/jpeg"),
        imgSize = _this.getFileSizeByBase64(data),
        kSize = MAX_SIZE / imgSize;

    if (kSize < 1) {
        kSize = (kSize + 1) / 2 - 0.01;

        var newWidth = Math.round(obj.w * kSize),
            newHeight = Math.round(obj.h * kSize);

        canvas.width = newWidth;
        canvas.height = newHeight;
        obj.image.src = data;

        obj.image.onload = function () {
            ctx.drawImage(obj.image, 0, 0, newWidth, newHeight);
            obj.image.src = canvas.toDataURL("image/jpeg");

            obj.image.onload = function () {
                canvas.width = obj.w;
                canvas.height = obj.h;
                ctx.drawImage(obj.image, 0, 0, obj.w, obj.h);
                _this.imageCompression(obj, success);
            };
        };
    } else {
        success(data, imgSize);
    }
};

PDF.prototype.changeColor = function (color) { // меняет цвет
    var _this = this;

    $('.colorize[data-attr]', _this.$modal).each(function () {
        var $this = $(this);

        $this.css($this.data('attr'), color);
    });

    $('svg:not(.notColorize) path, svg:not(.notColorize) polygon, svg:not(.notColorize) rect', _this.$modal).css('fill', color);
    $('.hr', _this.$modal).css('background-color', color);
};