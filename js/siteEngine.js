/**
 * console.log alias
 */
function pre() {
    var execStr = [];

    for (var i in arguments)
        execStr.push('arguments[' + i + ']');

    eval('console.log(' + execStr.join(', ') + ');');
}

/**
 * Ajax
 * @param functionName	имя функции
 * @param data			данные
 * @param func			callback function
 * @param options		ajax options
 */
function ajax(functionName, data, func, options) {
    data = data || {};
    options = options || {};

    var dataType = Array.isArray(data) ? 'array' : (typeof data);

    switch (dataType) {
        case 'object': {
            data.function = functionName;
            data.IsAjax = 1;
        } break;
        case 'array': {
            data.push({
                name: 'IsAjax',
                value: 1
            });
            data.push({
                name: 'function',
                value: functionName
            });
        } break;
        case 'string': {
            data += '&function=' + functionName;
            data += '&IsAjax=1';
        }
    }

    var ajaxParams = {
        dataType: 'json',
        type: 'POST',
        data: data
    };

    func = func || undefined;

    if (typeof func == 'function')
        ajaxParams.success = func;

    if (Object.keys(options).length)
        for (var i in options)
            ajaxParams[i] = options[i];

    $.ajax('/ajax/', ajaxParams);
}

/**
 * number_format
 * @param number
 * @param decimals
 * @param dec_point
 * @param thousands_sep
 * @return {*}
 */
function number_format(number, decimals, dec_point, thousands_sep) {
    var i, j, kw, kd, km;

    // input sanitation & defaults
    if (isNaN(decimals = Math.abs(decimals)))
        decimals = 2;

    if (dec_point == undefined)
        dec_point = ",";

    if (thousands_sep == undefined)
        thousands_sep = ".";

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if ((j = i.length) > 3) {
        j = j % 3;
    } else {
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    return km + kw + kd;
}

/**
 * Выполняет функцию.
 * @param funcName  Имя функции
 * @return {*}
 */
function exec(funcName) {
    var params = [],
        result;

    if (arguments.length > 1)
        for (var i = 1; i < arguments.length; i++)
            params.push('arguments[' + i + ']');

    eval('result = ' + funcName + '(' + params.join(', ') + ');');

    return result;
}

function initAllStepTabs() {
    $('.stepTabs').each(function () {
        new StepTabs($(this));
    });
}

/**
 * Инициализирует сайдеры.
 * Не инициализирует невидимые слайдеры или уже инициализированные.
 */
function initAllSliders() {
    $('.main_slider:not(.manualSlider):not(.active):visible').each(function () {
        new Slider($(this));
    });
}

function number(number) {
    number = number + '';

    return number.replace(/(\d{1,3})(?=(?:\d\d\d)+(?:\D|$))/g,'$1 ');
}

/**
 * Модалка ajax'ом.
 * Запрашивает контент и размещает его в модаль.
 * @param action    function
 * @param params    $_REQUEST
 * @param func      after open
 */
function ajaxModal(action, params, func, modalParams) {
    func = func || function () {};
    params = ((typeof params == 'object') && Object.keys(params).length) ? params : {};
    params.function = action;
    params.IsAjax = 1;

    modalParams = modalParams || {};

    var $ajaxModal = $.arcticmodal({
        type: 'ajax',
        url: '/ajax/',
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: params,
            success: function (data, el, responce) {
                data.body.html((typeof responce == 'string') ? responce : responce.Html);

                func(data, el, responce);
            }
        },
        afterOpen: function () {
            $('body').attr('style', 'overflow: hidden; margin-right: 15px;');
        },
        beforeOpen: function () {
            $('body').attr('style', 'overflow: hidden; margin-right: 15px;');
        },
        beforeClose: function (obj) {
            if (typeof modalParams.beforeClose == 'function')
                return modalParams.beforeClose(obj);

            return true;
        },
        afterClose: function (obj) {
            if (typeof modalParams.afterClose == 'function')
                return modalParams.afterClose(obj);

            if ($('.arcticmodal-container').length == 1)
                $('body').attr('style', '');
        },
        closeOnEsc: (typeof modalParams.closeOnEsc != 'undefined') ? modalParams.closeOnEsc : true,
        closeOnOverlayClick: (typeof modalParams.closeOnOverlayClick != 'undefined') ? modalParams.closeOnOverlayClick : true
    });

    return $ajaxModal;
}

/**
 *
 * @param content
 * @param params
 * @returns {*|jQuery|HTMLElement}
 */
function modal(content, params) {
    params = params || {};

    var $modal;

    if (!params.customContent) {
        var modalClass = params.clear ? '' : ' form_popup';

        $modal = $(
            '<div class="box-modal' + modalClass + '">' +
            '<div class="box-modal_close arcticmodal-close"></div>' +
            '<div class="modal-content"></div>' +
            '</div>'
        );

        $modal.find('.modal-content').append(content);

        $('body').append($modal);
    } else {
        $modal = $(content);
    }

    $modal.arcticmodal({
        beforeClose: function () {
        },
        afterClose: function () {
            if (typeof params.afterClose == 'function')
                params.afterClose($modal);

            $modal.remove();

            if ($('.arcticmodal-container').length == 1)
                $('body').attr('style', '');
        },
        afterOpen: function () {
            $('body').attr('style', 'overflow: hidden; margin-right: 15px;');

            if (typeof params.afterOpen == 'function')
                params.afterOpen($modal);
        },
        beforeOpen: function () {
            $('body').attr('style', 'overflow: hidden; margin-right: 15px;');

            if (params.class)
                $modal.closest('.arcticmodal-container').addClass(params.class);

            if (typeof params.beforeOpen == 'function')
                params.beforeOpen($modal);
        }
    });

    if (typeof params.onInit == 'function')
        params.onInit($modal);

    return $modal;
}

/**
 * in_array
 * @param item      искомый элемент
 * @param array     массив
 * @returns {boolean|int}
 */
function in_array(item, array) {
    var inArray = $.inArray(item, array);

    return inArray != -1;
}

/**
 * array_column
 * @param array
 * @param name
 * @returns {*}
 */
function array_column(array, name) {
    return array.map(function(el) {
        if (el.hasOwnProperty(name)) return el[name];
    }).filter(function(el) { return typeof el != 'undefined'; });
}

/**
 * Возвращает текущую метку времени
 * @returns {number}
 */
function unix_timestamp() {
    return Math.round(new Date().getTime() / 1000);
}

/**
 * explode, но если строка пустая возвращаяет пустой массив
 * @param delimiter
 * @param string
 * @returns {*}
 */
function explode(delimiter, string) {
    return string ? string.split(delimiter) : [];
}

/**
 * function_exists
 * @param functionName
 * @returns {boolean}
 */
function function_exists(functionName) {
    if (typeof functionName == 'string') {
        return (typeof window[functionName] == 'function');
    } else {
        return (functionName instanceof Function);
    }
}

/**
 * Суммирует элементы массива
 * @param array
 * @returns int
 */
function array_sum(array) {
    return array.reduce(
        function (a, b) {
            return a + b;
        },
        0
    );
}

/**
 * Удаляет заданные символы из строки. (По-умолчанию, удаляет только пробелы)
 * @param str сторка
 * @param chars какие символы удалить из строки
 * @returns {*}
 */
function clearStrFrom(str, chars) {
    chars = chars || [' '];
    str = str + '';

    for (var i in chars)
        str = str.replace(new RegExp(chars[i], 'g'), '');

    return str;
}

/**
 * Is mobile?
 * @returns {boolean}
 */
function isMobile() {
    return !!$.cookie('IsMobile');
}

/**
 * time()
 * @returns {Number}
 */
function time() {
    return parseInt(new Date().getTime() / 1000)
}

/**
 * trim
 * @param string
 */
function trim(string) {
    return string.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

/**
 * array intersect
 * @param a
 * @param b
 * @returns {*}
 */
$.arrayIntersect = function(a, b) {
    return $.grep(a, function(i) {
        return $.inArray(i, b) > -1;
    });
};

$.fn.extend({
    /**
     ** product raring
     **/
    productRating : function () {
        return this.each(function () {
            $(this).find('.rating_item').append("<div class='empty_state'><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></div><div class='fill_state'><div><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></div></div>");

            var $this = $(this),
                ratingItem = $this.find('.rating_item'),
                rating = $this.data("rating"),
                offset = ratingItem.offset().left,
                input = $this.find('input'),
                ratingVal = $this.find('.rating_value'),
                fillState = $this.find('.fill_state'),
                w = ratingItem.outerWidth();

            ratingVal.text(rating);

            fillState.css('width', (1 - rating / 10) * w);

            if ($this.hasClass('choice')) {
                ratingItem.on("mousemove", function(e) {
                    if (!$this.hasClass('selected')) {
                        var corX = e.pageX,
                            formula = Core.ISRTL ? (corX - offset) : (offset + w - corX),
                            val = (100 / w * (corX - offset) / 9.8).toFixed(1);

                        fillState.width(formula);

                        if (val >= 10) {
                            input.val(10);
                            ratingVal.text(10);
                        } else {
                            input.val(val);
                            ratingVal.text(val);
                        }

                        $("body").on("click", ratingItem, function(e) {
                            $this.addClass("selected");
                        });
                    }
                }).mouseout(function () {
                    if (!$this.hasClass('selected')) {
                        if (rating) {
                            fillState.css('width', (1 - rating / 10) * w);
                            input.val(rating);
                            ratingVal.text(rating);
                        } else {
                            fillState.width(w);
                            input.val(0.0);
                            ratingVal.text(0.0);
                        }
                    }
                });
            } else {
                fillState.css('width', (1 - rating / 10) * w);
            }
        });
    }
});

function getUID() {
    return (new Date()).getTime() + '' + (Math.random() * 1000).toFixed(0);
}

function is_array(array) {
    return $.isArray(array);
}