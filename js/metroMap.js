/**
 * Управление картой метро на сайте
 * @param $container
 * @constructor
 */
function MetroMap($container) {
    var _this = this;
    this.id = $container.attr('id');

    if (this.id) {

        this.$container = $container;
        this.$modalContainer = this.$container.closest('.box-modal');
        this.$chooseBtn = $('.chooseStations', this.$container);
        this.$openBtns = $('.openMetroMap[data-metromapid=' + this.id + ']');

        this.selectedStations = [];
        this.stations = {};
        this.stationsArray = [];
        this.chouseFunction = this.$chooseBtn.data('func');

        if (this.$modalContainer.size())
            this.$openBtns.each(function () {
                var $this = $(this);

                $this.on(
                    $this.data('action') || 'click',
                    function () {
                        _this.open();
                    }
                )
            });

        if (!this.$modalContainer.size()) //если карта не в модалке, то добавление станций обрабатывается по кнопке
            this.$chooseBtn.click(function () { //обработка кнопки выбора станций
                if (function_exists(_this.chouseFunction)) {
                    eval(_this.chouseFunction + '(_this)'); //передаем в заданую функцию объект карты
                }
            });

        $('.station', this.$container).each(function () {
            var $this = $(this),
                stationID = $this.data('id'),
                data = {
                    StationID: stationID,
                    Name: $this.attr('title'),
                    Left: $this.data('left'),
                    Top: $this.data('top'),
                    Color: $this.data('color'),
                    Transfers: explode(',', $this.data('transfers').toString()).map(
                        function (item) {
                            return parseInt(item);
                        }
                    ),
                    Groups: explode(',', $this.data('groups').toString()),
                    DOM: $this
                };

            _this.stations[stationID] = data;
            _this.stationsArray.push(data);

            if ($this.data('active'))
                _this.selectedStations.push(stationID);
        });

        this.groups = $('.group', this.$container).filter( //убираем группы без станций и без названия группы
            function () {
                var $group = $(this);

                return $group.data('stations') || $group.data('group');
            }
        ).get().map(function (group) {
            var $group = $(group),
                groupName = $group.data('group'), //название группы
                defaultStations = $group.data('stations'), //явно указаные станции группы
                stations = [],
                getTransfers = $group.data('transfer');

            if (defaultStations) { //если станции группы уже заданы
                stations = defaultStations.toString().split(',').map(
                    function (item) {
                        return parseInt(item);
                    }
                );
            } else {
                for (var i in _this.stations)
                    if (in_array(groupName, _this.stations[i].Groups))
                        stations.push(_this.stations[i].StationID);
            }

            if (!defaultStations && getTransfers) {
                var allStations = [];

                for (var i in stations)
                    allStations = allStations.concat(_this.getTransferStations(stations[i]));

                stations = allStations;
            }

            return {
                Name: groupName,
                DOM: $group,
                Stations: stations
            }
        });

        $(this.groups).each(function () {
            var groupItem = this;

            groupItem.DOM.click(function () {
                var $this = $(this);

                if (!$this.hasClass('active')) {
                    _this.select(groupItem.Stations);
                } else {
                    _this.remove(groupItem.Stations);
                }

                return false;
            });
        });

        $(this.stationsArray).each(function () {
            var station = this;

            station.DOM.click(function () {
                _this.select(_this.getTransferStations(station.StationID));

                return false;
            });
        });

        $('.removeAllStations', this.$container).click(function () {
            _this.removeAll();
        });

        if (this.selectedStations.length)
            this.drawSelected();

        return this;
    } else {
        return null;
    }
}

/**
 * Открывает модальное окно с картой (если она изначально лежала в модалке)
 */
MetroMap.prototype.open = function () {
    if (this.$modalContainer.size()) {
        var a = this;

        this.$modalContainer.arcticmodal({
            afterClose: function(){

                $('body').css('overflow','auto');
            },
            beforeClose: function () {

                a.$modalContainer.hide();

                if (function_exists(a.chouseFunction))
                    eval(a.chouseFunction + '(a)'); //передаем в заданую функцию объект карты
            }
        });

        this.$modalContainer.show();
    }
};

MetroMap.prototype.close = function () {
    this.$modalContainer.arcticmodal('close');
};

/**
 * Отрисовка html для текущих выбранных станций (точек на карте и тд)
 */
MetroMap.prototype.drawSelected = function () {
    var _this = this,
        $container = $('.selectedStationContainer', this.$container),
        $listContainer = $('.selectedStations', this.$container);

    $(this.stationsArray).each(function () {
        var station = this,
            forDraw = in_array(station.StationID, _this.selectedStations);

        if (!station.drawed && forDraw) { //если станция не отрисована и должна быть отрисована
            var $point = $('<div class="stationPoint" data-id="' + station.StationID + '" style="left: ' + station.Left + 'px; top: ' + station.Top + 'px;" title="' + station.Name + '"></div>');

            $point.click(function () { //точка на карте
                var currentStations = _this.stations[$(this).data('id')],
                    nearedStations = _this.stationsArray.filter(function (item) {
                        return item.Left == currentStations.Left
                            && item.Top == currentStations.Top;
                    });

                _this.remove(array_column(nearedStations, 'StationID'));
            });

            $container.append($point);

            var $listItem = $($.parseHTML(
                    '<div class="selectedStation" data-id="' + station.StationID + '">' +
                        '<span class="metroIcon" style="background-color: #' + station.Color + ';">М</span>' +
                        '<span class="stationName">' + station.Name + '</span>' +
                    '</div>'
                )),
                $deleteBtn = $('<span class="removeStation close_information_box">×</span>');

            $deleteBtn.click(function () {
                _this.remove(station.StationID);
            });

            $listItem.append($deleteBtn);
            $listContainer.append($listItem);

            station.drawed = true;
        } else if (station.drawed && !forDraw) { // если станция отрисована, но не должна быть отрисована
            station.drawed = false;

            $('.stationPoint[data-id=' + station.StationID + ']', $container).remove();
            $('.selectedStation[data-id=' + station.StationID + ']', $listContainer).remove();
        }
    });

    $(this.groups).each(function () {
        var group = this,
            groupStations = group.Stations,
            hasNotSelectedStations = false;

        for (var i in groupStations) { //перебираем станции группы
            if (!in_array(groupStations[i], _this.selectedStations)) { //если хоть 1а её станция не выбрана
                group.DOM.removeClass('active');
                hasNotSelectedStations = true;

                break;
            }
        }

        if (!hasNotSelectedStations)
            group.DOM.addClass('active');
    });
};

/**
 * По StationID возвращает все станции на которые можно перейти с этой станции
 * @param station
 * @param exceptions
 * @returns {*}
 */
MetroMap.prototype.getTransferStations = function (station, exceptions) {
    exceptions = exceptions || [];

    var transferStations = [station],
        stationTransfer = this.stations[station];

    if (stationTransfer.Transfers.length) {
        var newTransfers = stationTransfer.Transfers.filter(function (stationID) {
            return !in_array(stationID, exceptions);
        });

        transferStations = transferStations.concat(newTransfers);
        exceptions = exceptions.concat(transferStations);

        for (var i in newTransfers)
            transferStations = transferStations.concat(this.getTransferStations(newTransfers[i], exceptions));
    }

    return $.unique(transferStations);
};

/**
 * Добавление станций в выбранное
 * @param stations
 */
MetroMap.prototype.select = function (stations) {
    stations = !$.isArray(stations) ? [stations] : stations;

    this.selectedStations = $.unique(this.selectedStations.concat(stations));

    this.drawSelected();
};

/**
 * Удаление из выбранного
 * @param stations
 */
MetroMap.prototype.remove = function (stations) {
    stations = !$.isArray(stations) ? [stations] : stations;

    this.selectedStations = this.selectedStations.filter(function (station) {
        return !in_array(station, stations);
    });

    this.drawSelected();
};

/**
 * Удаляет все станции из выбранного
 */
MetroMap.prototype.removeAll = function () {
    this.selectedStations = [];

    $(array_column(this.groups, 'DOM')).each(function () {
        this.removeClass('active')
    });

    this.drawSelected();
};