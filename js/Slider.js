function Slider($container, params) {
    this.$container = $container;
    this.$slider = $('.controlBoxes', this.$container);

    params = (typeof params !== 'undefined') ? params : {};

    this.min = params.min || this.$slider.data('min');
    this.max = params.max || this.$slider.data('max');
    this.from = params.from || this.$slider.data('from') || this.min;
    this.to = params.to || this.$slider.data('to') || this.max;
    this.endFunction = params.func || this.$slider.data('function');
    this.k = params.k || this.$slider.data('k');

    this.round = (params.round !== undefined && params.round >= 0) ? params.round : 0;

    this.values = {
        from: this.from,
        to: this.to
    };

    this.on = {
        change: (params.on && typeof params.on.change === 'function') ? params.on.change : function () {},
        load: (params.on && typeof params.on.load === 'function') ? params.on.load : function () {}
    };

    var _this = this;

    this.startEndFunction = function () {
        if (_this.endFunction) {
            if (typeof _this.endFunction === 'function') {
                _this.endFunction(_this);
            } else {
                window[_this.endFunction](_this);
            }
        }
    };

    this.$container.addClass('active');

    var $control = this.$slider.children().first(),
        width = this.$slider.width(),
        $left = $('.cLeft', $control),
        $right = $('.cRight', $control),
        $this = this.$slider,
        round = $this.data("round") || 10,
        min = this.min,
        max = this.max,
        deltaVals = max - min,
        prevX = null,
        k = (this.k) ? this.k : 100,
        fixedVal = (max > 100) ? 0 : 1,
        left = {
            max: 0,
            moving: false,
            padding: 0,
            val: this.from,
            pixelVal: 0,
            width: $left.width(),
            input: $('#' + $left.data('for')),
            updatePos: function (updateInput) {
                $control.css('margin-left', left.padding + 'px');

                if (updateInput) {
                    if (left.pixelVal) {
                        var val = min + k * Math.pow(deltaVals / k + 1, left.pixelVal / valWidth) - k,
                            rnd = getRound(val);

                        val = Math.round(val / rnd) * rnd;
                        val = parseFloat((val < min) ? min : ((val > max) ? max : val.toFixed(fixedVal)));

                        val = val.toFixed(_this.round);

                        left.input.val(val);

                        _this.values.from = parseFloat(val);
                    } else {
                        left.input.val(min);

                        _this.values.from = parseFloat(min);
                    }
                }

                _this.startEndFunction();
            },
            calcPos: function (updateInput) {
                left.pixelVal = valWidth * Math.log((left.val - min + k) / k) / Math.log(deltaVals / k + 1);
                left.padding = Math.round(left.pixelVal);
                left.updatePos(updateInput);
            }
        },
        right = {
            max: 0,
            moving: false,
            padding: 0,
            val: this.to,
            pixelVal: 0,
            width: $right.width(),
            input: $('#' + $right.data('for')),
            updatePos: function (updateInput)
            {
                $control.css('margin-right', right.padding + 'px');

                if (updateInput)
                {
                    if (right.pixelVal)
                    {
                        var val = min + k * Math.pow(deltaVals / k + 1, right.pixelVal / valWidth) - k;
                        var rnd = getRound(val);

                        val = Math.round(val / rnd) * rnd;
                        val = (val < min) ? min : ((val > max) ? max : val.toFixed(val < 0 ? 2 : fixedVal));
                        val = parseFloat((val === 0) ? min : val);

                        val = val.toFixed(_this.round);

                        right.input.val(val);

                        _this.values.to = parseFloat(val);
                    } else {

                        right.input.val(min);

                        _this.values.to = parseFloat(min);
                    }

                }

                _this.startEndFunction();
            },
            calcPos: function (updateInput)
            {
                right.pixelVal = valWidth * Math.log((right.val - min + k) / k) / Math.log(deltaVals / k + 1);
                right.padding = valWidth - Math.round(right.pixelVal);
                right.updatePos(updateInput);
            }
        };

    var valWidth = width - right.width - left.width - 1,
        initVals = function () {
            var leftVal = parseFloat(clearStrFrom(left.input.val()));

            leftVal = isNaN(leftVal) ? 0 : leftVal;
            left.val = Math.max(min, leftVal);

            left.calcPos();

            var rightVal = parseFloat(clearStrFrom(right.input.val()));

            rightVal = isNaN(rightVal) ? 0 : rightVal;
            right.val = Math.min(max, rightVal);

            right.calcPos();
        };

    initVals();

    this.inputed = {};

    left.input.keyup(function () {
        var input = this;

        clearTimeout(_this.inputed.left);

        _this.inputed.left = setTimeout(function () {
            var rightVal = parseInt(clearStrFrom(right.input.val()));
            rightVal = isNaN(rightVal) ? 0 : rightVal;

            if (parseInt(clearStrFrom(input.value)) > rightVal)
                input.value = rightVal;

            left.val = Math.max(min, Math.min(max, clearStrFrom(input.value) || 0));

            left.calcPos();
        }, 700);
    });


    right.input.keyup(function () {
        var input = this;

        clearTimeout(_this.inputed.right);

        _this.inputed.right = setTimeout(function () {
            var leftVal = parseInt(clearStrFrom(left.input.val()));
            leftVal = isNaN(leftVal) ? 0 : leftVal;

            if (parseInt(clearStrFrom(input.value)) < leftVal)
                input.value = leftVal;

            right.val = Math.max(min, Math.min(max, clearStrFrom(input.value) || 0));

            right.calcPos();
        }, 700);
    });

    var getRound = function (val) {
        if (val < 1)
            return 0.1;
        else if (val < 5)
            return 0.5;
        else if (val < 20)
            return 1;
        else if (val < 100)
            return 5;
        else if (val < 1000)
            return 50;
        else if (val < 10000)
            return 500;
        else
            return 1000;
    };


    var getClientX = function (e) {
        if (e.type.match(/touch/)) {
            e.preventDefault();

            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];

            return touch.clientX;
        } else {
            return e.clientX;
        }
    };

    var startRightPosChange = function (e) {
        right.moving = true;
        prevX = getClientX(e);
        right.max = left.padding;

        $(document).on("selectstart", function () {
            return false;
        });
    };

    var startLeftPosChange = function (e) {
        left.moving = true;
        prevX = getClientX(e);
        left.max = valWidth - right.padding;

        $(document).on("selectstart", function () {
            return false;
        });
    };

    var endPosChange = function () {
        if (!left.moving && !right.moving)
            return;

        left.moving = right.moving = false;

        _this.on.change(_this);

        $(document).unbind("selectstart");
    };

    var posChange = function (e) {
        if (!left.moving && !right.moving)
            return;

        var curX = getClientX(e),
            delta = curX - prevX;

        prevX = curX;

        if (left.moving)
        {
            left.pixelVal = Math.max(0, Math.min(left.max, left.pixelVal + delta));
            left.padding = left.pixelVal;
            left.updatePos(true);
        }
        else if (right.moving)
        {
            right.pixelVal = Math.min(valWidth, Math.max(right.max, right.pixelVal + delta));
            right.padding = valWidth - right.pixelVal;
            right.updatePos(true);
        }
    };

    $left.on('touchstart mousedown', function (e) {
        if (!left.moving && !right.moving)
            startLeftPosChange(e);
    });

    $right.on('touchstart mousedown', function (e) {
        if (!left.moving && !right.moving)
            startRightPosChange(e);
    });

    $(document).on('touchend mouseup', endPosChange);
    $(document).on('touchmove mousemove', posChange);

    left.updatePos(true);
    right.updatePos(true);


    if (typeof params.min !== 'undefined' && (typeof params.max !== 'undefined')) {
        this.$container.find('.min_text .value').html(params.min);
        this.$container.find('.max_text .value').html(params.max);
    }

    var $labels = this.$container.find('.form_dimension .postLabel, .post_label'),
        $label = this.$container.find('.main_slider_title');

    if ($labels.size() && (typeof params.postLabel !== 'undefined')) {
        $labels.html('');
        $labels.append(params.postLabel);
    }

    if ($label.size() && (typeof params.label !== 'undefined')) {
        $label.html('');
        $label.append(params.label);
    }

    _this.on.load(this.$container);
}