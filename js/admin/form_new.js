function loadForm($container, type, act, params, func) {
    params = params || {};
    func = (typeof func == 'function') ? func : function () {};

    if (type && act && $container.length) {
        params.type = type;
        params.act = act;

        ajax(
            'loadForm',
            params,
            function (result) {
                $container.html(result.resultText);

                initPlugins($container);
                initShowSqlInfo($container);

                func(result);
            }
        );
    }
}

function initShowSqlInfo($container) {
    $container = $container || $(document);

    $('.formTechInfo button', $container).click(function () {
        var $btn = $(this),
            $table = $btn.parent().find('.sqlExecTimes table'),
            down = 'fa-angle-double-down',
            up = 'fa-angle-double-up';

        if ($table.is(':visible')) {
            $table.hide();
            $btn.removeClass(up);
            $btn.addClass(down);
        } else {
            $table.show();
            $btn.removeClass(down);
            $btn.addClass(up);
        }
    });
}

function checkIntFloat(e, obj, pattern) {
    if (e.charCode && !e.ctrlKey || e.keyCode == 8) {
        var pos = getCursorPosition(obj),
            val = checkInputByRegExp({
                value: $(obj).val(),
                maxLength: $(obj).attr("maxlength"),
                pattern: pattern,
                pressedKey: e.keyCode == 8 ? e.keyCode : e.charCode,
                cursorPos: pos
            });
        if (val !== false) {
            $(obj).closest(".form-group").removeClass("has-error");
        } else {
            if (e.keyCode != 8) {
                return false;
            } else {
                $(obj).closest(".form-group").addClass("has-error");
            }
        }
    }
}

function checkInput(obj, needed, pattern){
    var value = $(obj).val(),
        sost = (needed || !needed && value != '')
            ? checkInputByRegExp({
                value: value,
                maxLength: $(obj).attr("maxlength"),
                pattern: pattern
            })
            : true,
        container = $(obj).closest(".form-group");

    if (sost === false) {
        //container.removeClass("has-success");
        container.addClass("has-error");
    } else {
        container.removeClass("has-error");
        //container.addClass("has-success");
    }
}