//**********Map Window Class**************
function GMapWindowCLS(params)
{
	if(!params.map || !params.marker)
		return false;
	
	this.map = params.map;
	this.marker = params.marker;
	this.content = (params.content) ? params.content : "";
	
	this.iconClicked = params.iconClicked || "clicked";
	this.iconNormal = params.iconNormal || "small";
	
	this.mapContainer = this.map.getDiv();
	//**********
	this.point = this.getPoint();
	
	var winContainer = getElem("windowsContainer");
	var win = createElement('div');
	winContainer.appendChild(win);
	win.className = 'mapWin';
	
	this.offsetX = 25;
	this.offsetY = -10;
	
	/*win.style.top = this.point.y + this.offsetY + 'px';
	win.style.left = this.point.x + this.offsetX + 'px';*/
	win.innerHTML = this.content;
	
	var winClose = createElement('div');
	win.appendChild(winClose);
	winClose.className = 'mapWinClose';
			
	this.win = win;
	
	this.updatePos();
	
	var _this = this;
	this.prevMarker = this.marker.getIcon();
	this.marker.setIcon('/img/icons/gmarkers/' + this.iconClicked + '.png');
	
	Event.add(winClose, "click", 
			function(e) 
			{ 
				_this.close();
			});
	Event.add(window, "click", 
			function(e) 
			{ 
				_this.close();				
			});
	Event.add(this.win, "click", 
			function(e) 
			{ 
				e.stopPropagation();				
			});
			
	Event.add(window, "scroll", 
			function(e) 
			{ 
				_this.updatePos();				
			});
	Event.add(window, "resize", 
			function(e) 
			{ 
				_this.updatePos();				
			});
			
	google.maps.event.addListener(this.map, 'dragstart', 	function(e) 
								{
									//_this.updatePos();
									_this.close();
								});	
	google.maps.event.addListener(this.map, 'idle', 	function(e) 
								{
									//_this.updatePos();
									_this.close();
								});	
	google.maps.event.addListener(this.map, 'zoom_changed', 	function(e) 
								{
									_this.close();
								});	
	return this;
}

//****************************************
GMapWindowCLS.prototype.getPixelOffset = function()
{
	var scale = Math.pow(2, this.map.getZoom());
	var nw = new google.maps.LatLng(
		this.map.getBounds().getNorthEast().lat(),
		this.map.getBounds().getSouthWest().lng()
	);
	
	var worldCoordinateNW = this.map.getProjection().fromLatLngToPoint(nw);
	var worldCoordinate = this.map.getProjection().fromLatLngToPoint(this.marker.getPosition());
	var pixelOffset = new google.maps.Point(
		Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
		Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
	);	
	
	return pixelOffset;
}
//****************************************
GMapWindowCLS.prototype.getPoint = function()
{
	var mapOffset = this.getPixelOffset();
	var mapPos = getElementObjPosition(this.mapContainer);
	
	var x = (mapOffset && mapPos) ? mapOffset.x + mapPos.left: event.clientX; 
	var y = (mapOffset && mapPos) ? mapOffset.y + mapPos.top: event.clientY; 
	
	return {x : x, y : y};
}
//****************************************
GMapWindowCLS.prototype.updatePos = function()
{
	this.point = this.getPoint();
	this.win.className = "mapWin";
	
	var width = this.win.offsetWidth;
	var height = this.win.offsetHeight;
	
	
	var screenWidth = getClientWidth();
	var screenHeight = getClientHeight();
	var scrollLeft = getBodyScrollLeft();
	var scrollTop = getBodyScrollTop();
	
	var screen = {Left : scrollLeft, Right : scrollLeft + screenWidth, Top : scrollTop, Bottom : scrollTop + screenHeight};
	
	//alert(screen.Left + ' - ' + screen.Right + ' - ' + screen.Top + ' - ' + screen.Bottom);
	
	var offsetX = this.offsetX;
	var offsetY = this.offsetY;
	//************
	if( (screen.Right - this.point.x >= width + 20) || (screen.Right - this.point.x > this.point.x - screen.Left) )
		this.win.className += " RightSide";
	else
	{
		this.point.x -= width;		
		offsetX *= -1;
		this.win.className += " LeftSide";
	}
	//************
	if( (screen.Bottom - this.point.y >= height) || (screen.Bottom - this.point.y > this.point.y - screen.Top) )
		this.win.className += " BottomSide";
	else
	{
		this.point.y -= height - 7;		
		offsetY *= -1;
		this.win.className += " TopSide";
	}
	offsetY -= 47;
	this.win.style.top = this.point.y + offsetY + 'px';
	this.win.style.left = this.point.x + offsetX + 'px';
}
//****************************************
GMapWindowCLS.prototype.close = function()
{
	if(this.win && this.win.parentNode)
		this.win.parentNode.removeChild(this.win);
	
	this.marker.setIcon('/img/icons/gmarkers/' + this.iconNormal + '.png');
	delete this;
}