/**
 * Типы ошибок:
 * 1 - лишние фотографии (сверх предела единоразовой загрузки)
 * 2 - ошибка на сервере
 */

var pasting = false,
    moveCutDot = {
        enable: false
    },
    dropZoneText = {
        def: 'Для загрузки, перетащите файл сюда.',
        error: 'Ошибочка.',
        loaded: 'Загрузка завершена.',
        screenShot: 'Вставьте скриншот (CTRL + V)'
    };

function clearMoveCutDot() {
    moveCutDot.enable = false;
    delete moveCutDot.left;
    delete moveCutDot.top;
    delete moveCutDot.className;
}

function getUID() {
    return (new Date()).getTime() + '' + (Math.random()*1000).toFixed(0);
}

function resizeImage(sizes) {
    if (sizes.w > sizes.maxW || sizes.h > sizes.maxH) {
        if (sizes.w > sizes.maxW) {
            sizes.h = Math.round(sizes.h * sizes.maxW / sizes.w);
            sizes.w = sizes.maxW;
        } else if (sizes.h > sizes.maxH) {
            sizes.w = Math.round(sizes.w * sizes.maxH / sizes.h);
            sizes.h = sizes.maxH;
        }
        sizes = resizeImage(sizes);
    }
    return sizes;
}

function getFileSizeByBase64(data) {
    return data.split(',')[1].length * 3 / 4;
}

function imageCompression(obj , success) {
    var canvas = obj.canvas,
        ctx = canvas.getContext("2d"),
        data = canvas.toDataURL("image/jpeg"),
        imgSize = getFileSizeByBase64(data),
        kSize = MAX_SIZE / imgSize;
    if (kSize < 1) {
        kSize = (kSize + 1) / 2 - 0.01;
        var newWidth = Math.round(obj.w * kSize),
            newHeight = Math.round(obj.h * kSize);
        canvas.width = newWidth;
        canvas.height = newHeight;
        obj.image.src = data;
        obj.image.onload = function () {
            ctx.drawImage(obj.image, 0, 0, newWidth, newHeight);
            obj.image.src = canvas.toDataURL("image/jpeg");
            obj.image.onload = function () {
                canvas.width = obj.w;
                canvas.height = obj.h;
                ctx.drawImage(obj.image, 0, 0, obj.w, obj.h);
                imageCompression(obj, success);
            };
        };
    } else {
        success(data, imgSize);
    }
}

function setDOMItem($DOM){
    $DOM.css({
        position: 'absolute',
        left: -10000,
        top: -10000
    });

    $('body').append($DOM);
}

function addFileToMass(file, id, params) {
    var errors = [],
        image = new Image(),
        alias,
        info = file.name.split('.');
    file.name = info[0] + '.jpg';
    /*if (file.size > MAX_SIZE)
        errors.push('Размер изображения (' + bytes2frendlyView(file.size) + ') превышает допустимый (' + bytes2frendlyView(MAX_SIZE) + ')!');*//*
    if (!checkType(file.name))
        errors.push('Расширение .' + info[info.length - 1] + ' не допустимо! Используйте .' + FILE_TYPES.join(', .') + '.');*/
    if (file.name.length > 30) {
        alias = file.name.substr(0, 10) + ' ... ' + file.name.substr(file.name.length - 10);
    } else alias = file.name;

    image.src = params.base64 ? file.data : window.URL.createObjectURL(file);
    image.onload = function() {
        var $image = $(image),
            $canvas = $("<canvas></canvas>"),
            ctx = $canvas[0].getContext("2d");

        setDOMItem($image);
        setDOMItem($canvas);

        var iWidth = $image.width(),
            iHeight = $image.height();

        if (iWidth > MAX_WIDTH || iHeight > MAX_HEIGHT) {
            var newSizes = resizeImage({
                w: iWidth,
                h: iHeight,
                maxW: MAX_WIDTH,
                maxH: MAX_HEIGHT
            });
            iWidth = newSizes.w;
            iHeight = newSizes.h;
        }

        $canvas[0].width = iWidth;
        $canvas[0].height = iHeight;
        ctx.drawImage(image, 0, 0, iWidth, iHeight);

        imageCompression(
            {
                canvas: $canvas[0],
                w: iWidth,
                h: iHeight,
                image: image
            },
            function (data, size) {
                if (GALERY_FORMS.length >= MAX_LOAD_COUNT)
                    errors.push({
                        type: 1,
                        content: 'Лимит фотографий исчерпан!'
                    });

                GALERY_FORMS.push({
                    uid: getUID(),
                    id: id,
                    data: data,
                    name: file.name,
                    alias: alias,
                    size: size,
                    errors: errors
                });
                $image.remove();
                $canvas.remove();
                setLoadedPart(id, params.part);
                getFilesInfo(id, function (){
                    if (params.disableHandler) {
                        hideHandler(id);
                    }
                    pasting = false;
                });
            }
        );
    };
}

function setLoadedPart(id, part){
    $('.imgLoader .progress-bar', $('#' + id)).css('width', part + '%');
}

function deleteNotLoadedImg(obj, uid) {
    for (var item in GALERY_FORMS) {
        if (GALERY_FORMS[item].uid == uid) GALERY_FORMS.splice(item, 1);
    }
    getFilesInfo($(obj).closest('.gallery').attr('id'));
}

function showErrorsNotLoadedImg(obj) {
    var div = $(obj).parent().find('div');
    div.show();
}

function hideErrorsNotLoadedImg(obj) {
    $(obj).parent().find('div').hide();
}

function bytes2frendlyView($bytes)
{
    if($bytes > 0)
    {
        if($bytes < 1024) // < KB
            return $bytes+" B";
        else if($bytes < 1024 * 1024) // < MB
        return round($bytes / 1024, 2)+" KB";
        else if($bytes < 1024 * 1024 * 1024) // < GB
        return round($bytes / 1024 / 1024, 2)+" MB";
    }
    return false;
}

function showHandler(id){
    var container = $('#' + id);
    $('.imgLoader', container).show();
    $('.imgLoader .progress-bar', container).css('width', '0%');
}

function hideHandler(id){
    var container = $('#' + id);
    $('.imgLoader', container).hide();
    $('.imgLoader .progress-bar', container).css('width', '0%');
}

function getListRecursion(mass) {
    var str = '',
        count = mass.length;
    if (count) {
        str += '<ul>';
        for (var i = 0; i < count; i++) {
            var isArray = $.isArray(mass[i]);
            str += '<li' + (isArray ? ' class="arr"' : '') + '>' + (isArray ? getListRecursion(mass[i]) : mass[i]) + '</li>';
        }
        str += '</ul>';
    }
    return str;
}

function getErrorsContent(errors) {
    var result = [];

    for (var i in errors)
        result.push(errors[i].content);

    return result;
}

function getFilesInfo(id, onload) {
    var inputs = $('#' + id).find('.inputs table tbody');
    inputs.empty();
    for (var i = 0,count = GALERY_FORMS.length; i < count; i++) {
        var errorClass = '',
            errorBtn;
        if (GALERY_FORMS[i].errors.length) {
            errorClass = 'errorNotLoadedFile';
            errorBtn = $('<span class="btn btn-icon btn-danger btn-xs fa fa-warning icon-lg bord-no"></span>');
            var errorText = getListRecursion(getErrorsContent(GALERY_FORMS[i].errors));
        }
        if (GALERY_FORMS[i].id == id) {
            var tr = $("<tr class='" + errorClass + "'><td class='fileMini'><span><img src='" + GALERY_FORMS[i].data + "'></span></td><td><span class='fileAlias'>" + GALERY_FORMS[i].alias + "</span></td><td>" + (GALERY_FORMS[i].size/1024).toFixed(2) + "КБ</td><td>" +
                    "<span title='Редактировать' class=' btn btn-icon btn-default btn-xs fa fa-pencil fa-lg' onclick='editNotLoadedImg(this, \"" + GALERY_FORMS[i].uid + "\")'></span>" +
                    "<span title='Удалить' class=' btn btn-icon btn-danger btn-xs fa fa-times fa-lg' onclick='deleteNotLoadedImg(this, \"" + GALERY_FORMS[i].uid + "\")'></span>" +
                    "</td></tr>"),
                image = new Image();
            $(image).css('width', '100%');
            image.src = GALERY_FORMS[i].data;
            $('.fileMini span', tr).popover({
                html: true,
                placement: 'right',
                trigger: 'hover',
                content:  image
            });
            if (GALERY_FORMS[i].alias != GALERY_FORMS[i].name) {
                $('.fileAlias', tr).tooltip({
                    title: GALERY_FORMS[i].name,
                    placement: 'bottom'
                });
            }
            if (errorBtn) {
                tr.children().first().append(errorBtn);

                errorBtn.tooltip({
                    animation: true,
                    trigger: "manual",
                    placement: 'bottom',
                    html: true,
                    title: errorText
                }).on("mouseenter", function () {
                    var _this = this;
                    $(this).tooltip("show");
                    $(".tooltip").on("mouseleave", function () {
                        $(_this).tooltip('hide');
                    });
                }).on("mouseleave", function () {
                    var _this = this;
                    setTimeout(function () {
                        if (!$(".tooltip:hover").length) {
                            $(_this).tooltip("hide")
                        }
                    }, 100);
                });
            }
            inputs.append(tr);
        }
    }
    if (onload)
        onload();
}

function checkType(name) {
    var mass = name.split('.');
    var result = false;
    if ($.inArray(mass[mass.length-1].toLowerCase(), FILE_TYPES)!=-1 || !FILE_TYPES.length) result=true;
    return result;
}

var GALERY_FORMS = [];

function checkFileOnErrors(uid, errors){
    var result = [];
    for (var q in errors) {
        if (errors[q].file == uid) {
            for (var i in errors[q].errors)
                result.push({
                    type: 2,
                    content: errors[q].errors[i]
                });
        }
    }
    return result;
}

function clearErrors(errors, type) {
    type = type != undefined ? type : false;
    var result = [];

    if (type !== false) {
        for (var i in errors)
            if (errors[i].type != type)
                result.push(errors[i]);
    }
    return result;
}

function updateEvents(container) {
    var browser = browserDetect(),
        mass = $('.gallery', $(container));
    for (var i = 0, count = mass.size(); i < count; getFilesInfo($(mass[i]).attr('id')), i++);

    $('.button_upload', $(container)).on('click', function () {
        var container = $(this).closest('.gallery'),
            id = container.attr('id');
        if (GALERY_FORMS.length) {
            $('.imgLoader', $(container)).show();
            var s = $(this).closest('.gallery').find('input[type=hidden]').serialize();
            for (var i = 0, count = GALERY_FORMS.length; i < count; i++) {
                if (!GALERY_FORMS[i].errors.length && GALERY_FORMS[i].id == id) {
                    s += "&images[]=" + encodeURIComponent(GALERY_FORMS[i].data.split(',')[1]) + '&imagesName[]=' + GALERY_FORMS[i].name + '&imagesUID[]=' + GALERY_FORMS[i].uid;
                }
            }
            s += "&function=fileLoader";
            ajax(
                '',
                s,
                function (result){
                    var not_loaded  = [],
                        otherImages = [],
                        response    = result.data,
                        errors      = result.errors;
                    for (var i = 0, count = response.length; i < count; i++) {
                        var imgInfo = response[i].link.split(/[/.]/),
                            imgLink = '/images/' + imgInfo[0] + '/' + imgInfo[1] + '/h94/' + imgInfo[1] + '.' + imgInfo[2];
                        $('#' + id + '_gallery', $(container)).append('<table class="gallery_tbl little_margin"><tbody><tr><td>' +
                            '<a class="group" rel="photos" href="' + imgLink.replace('/h94', '') + '"><span>' +
                            "<img src=\"" + imgLink + "\" alt=\"\" title=\"\" class=\"\" data-hidden=\"\" data-interrior=\"\">" +
                            '</span></a></td><td>' +
                            "<span class=\"img_link fa fa-star-o fa-lg\" title=\"Сделать основным\" onclick=\"if(confirm('Сделать изображение основным?')) setFavouriteImgFromGallery(getElem('"+id+"'), '" + response[i].link + "', this);\"></span><br>" +
                            "<span class=\"img_link fa fa-pencil fa-lg\" title=\"Редактировать\" onclick='erp_modal({\"type\":\"images\",\"act\":\"singleImage\",\"params\":{\"id\":\""+id+"\"}});'></span><br>" +
                            (response[i].original ? '<a target="_blank" title="Оригинал" href="' + response[i].original + '" class="img_link fa fa-folder-open-o fa-lg"></a>' : '') +
                            "<span class=\"img_link fa fa-trash-o fa-lg\" title=\"Удалить\" onclick=\"if (confirm('Удалить изображение?')) deleteImgFromGallery(getElem('"+id+"'),'" + response[i].id + "',this)\"></span></td></tr></tbody></table>");
                    }
                    $('#' + id + ' .inputs table tbody', $(container)).empty();
                    for (var i = 0, count = GALERY_FORMS.length; i < count; i++) {
                        var sost = checkFileOnErrors(GALERY_FORMS[i].uid, errors);

                        if (
                            GALERY_FORMS[i].id == id
                            && (
                                sost.length
                                || GALERY_FORMS[i].errors.length
                            )
                        ) {
                            var newErrors = clearErrors(GALERY_FORMS[i].errors, 1).concat(sost);
                            if (not_loaded.length >= MAX_LOAD_COUNT) {
                                newErrors.push({
                                    type: 1,
                                    content: 'Лимит фотографий исчерпан!'
                                });
                            }
                            GALERY_FORMS[i].errors = newErrors;
                            not_loaded.push(GALERY_FORMS[i]);
                        } else if (GALERY_FORMS[i].id != id) {
                            otherImages.push(GALERY_FORMS[i]);
                        }
                    }
                    GALERY_FORMS = otherImages.concat(not_loaded);
                    $('.dropZone', $(container)).html('Загрузка завершена');
                    getFilesInfo(id);
                    $('.imgLoader', $(container)).hide();
                }
            );
        }
    });

    $('.button_file', $(container)).on('change', function(){
        //if (GALERY_FORMS.length < 10) {
            var file = this.files[0],
                id = $(this).closest('.gallery').attr('id');

            addFileToMass(
                file,
                id,
                {
                    disableHandler: true,
                    part: 100
                }
            );
        //}
        $(this).val('');
        $('#' + id + ' .dropZone', $(container)).html(dropZoneText.def);
    });

    var drops = $('.dropZone', $(container));

    $('.notDrop', $(container)).on('click', function(){
        $(this).closest('.gallery').find('input[type=file]').click();
    });

    $('.cuterBtn', $(container)).on('click', function(e){
        e.stopPropagation();
        e.preventDefault();
        var gallery = $(this).closest('.gallery'),
            drop = gallery.find('.dropZone');
        drop.addClass('insertScreenShot');
        drop.html(dropZoneText.screenShot);
        gallery.find('.printScreenInserter').focus();
    });

    $('.printScreenInserter', $(container)).focusout(function(e){
        e.stopPropagation();
        drops.removeClass('insertScreenShot');
        drops.html(dropZoneText.def);
    });

    // Проверка поддержки браузером
    if (typeof(window.FileReader) == 'undefined') {
        $('.dropZone', $(container)).text('Не поддерживается браузером!');
        $('.dropZone', $(container)).addClass('error');
    }

    // Добавляем класс hover при наведении
    for (var i = 0, count = drops.size(); i < count; i++){
        var gallery = $(drops[i]).closest('.gallery');
        drops[i].ondragover = function() {
            $(this).addClass('hover');
            $(this).html(dropZoneText.def);
            return false;
        };
        // Убираем класс hover
        drops[i].ondragleave = function() {
            $(this).removeClass('hover');
            return false;
        };
        // Обрабатываем событие Drop
        drops[i].ondrop = function(event) {
            event.preventDefault();
            $(this).removeClass('hover');
            var files = event.dataTransfer.files;
            /*if (files.length <= 5) {*/
                var id = $(this).closest('.gallery').attr('id'),
//                    remaind = 10 - GALERY_FORMS.length,
                    count = Math.min(5, files.length/*, remaind*/);

                //if (remaind > 0)
                    showHandler(id);

                for (var i = 0; i < count /*&& i < 5 && i < remaind*/; i++) {
                    addFileToMass(
                        files[i],
                        id,
                        {
                            disableHandler: i == count - 1,
                            part: (i + 1) * 100 / count
                        }
                    );
                }
            /*} else {
                $(this).html('Единовременно можно добавить только 5 фалов!');
            }*/
        };
        //var pasteContaner = drops[i];
        /*if (browser == 'Firefox') {*/
        $(drops[i]).click(function(){
            $(this).closest('.gallery').find('.printScreenInserter').focus();
        });
        /*}*/                                                                                                                                                                                                                                                                                                                                   
        $('.printScreenInserter', gallery)[0].addEventListener("paste", function(e) {
            if ($.inArray(browser, ['Chrome', 'Opera']) != -1) {
                for (var i = 0; i < e.clipboardData.items.length; i++) {
                    var item = e.clipboardData.items[i];
                    if (item.kind == "file" && item.type == "image/png" && !pasting /*&& GALERY_FORMS.length < 10*/) {
                        cuterInit(this, window.URL.createObjectURL(item.getAsFile()));
                    }
                }
            } else if (browser == 'Firefox') {
                setTimeout(function () {
                    var img = $(e.target).find('img');
                    if (img.size())
                        var imgSrc = img[0].src;
                    $(e.target).empty();
                    if (imgSrc)
                        cuterInit(e.target, imgSrc);
                }, 1000);
            }
        });
    }
}

function cuterInit(obj, imgSrc, cfg) {
    cfg = cfg || {};
    clearMoveCutDot();
    pasting = true;
    var $gallery = $(obj).closest('.gallery');
    if ($gallery.size()) {
        var id = $gallery.attr('id'),
            image = new Image();
        image.src = imgSrc;
        image.onload = function() {
            var $image = $(image),
                $cloneImage = $image.clone(),
                $canvas = $("<canvas></canvas>"),
                $content = $("<div class='cuterContainer'></div>"),
                $cuter = $("<div class='cuter'></div>"),
                ctx = $canvas[0].getContext("2d"),
                cuterID = new Date().getTime();
            $content.append($cloneImage);
            $cuter.append("<div class='cutDot left top'><div class='item'></div></div>");
            $cuter.append("<div class='cutDot left bottom'><div class='item'></div></div>");
            $cuter.append("<div class='cutDot right top'><div class='item'></div></div>");
            $cuter.append("<div class='cutDot right bottom'><div class='item'></div></div>");
            var $cutArea = $('<div class="cutArea"></div>');
            $cuter.append($cutArea);
            $content.append($cuter);
            $canvas.addClass('cuter_canvas_' + cuterID);
            $image.addClass('cuter_image_' + cuterID);
            setDOMItem($image);
            setDOMItem($canvas);
            var onclickSaveBtn;
            if (cfg.type == 'edit') {
                onclickSaveBtn = 'updateCutedImage(\'' + cfg.uid + '\', ' + cuterID + ')';
            } else {
                onclickSaveBtn = 'saveCutedImage(\'' + id + '\', ' + cuterID + ')';
            }
            var iWidth = $image.width(),
                iHeight = $image.height(),
                newSizes = resizeImage({
                    w: iWidth,
                    h: iHeight,
                    maxW: Math.round($(window).width() * 0.8),
                    maxH: Math.round($(window).height() * 0.8)
                }),
                $footer = $('<div class="cuterInfo">' +
                        '<div class="bg-gray original">Ширина: <span class="width">' + iWidth + '</span>; Высота: <span class="height">' + iHeight + '</span></div>' +
                        '<div class="bg-gray selected">Ширина: <span class="width">' + iWidth + '</span>; Высота: <span class="height">' + iHeight + '</span></div>' +
                    '</div>' +
                    '<button type="button" class="btn btn-default btn-icon icon-sm fa fa-undo" onclick="rotateCutImage(this, ' + cuterID + ', false)"/>' +
                    '<button type="button" class="btn btn-default btn-icon icon-sm fa fa-repeat" onclick="rotateCutImage(this, ' + cuterID + ', true)"/>' +
                    '<button type="button" class="btn btn-success btn-sm" onclick="getCutedImage(this, ' + cuterID + ')">Обрезать</button>' +
                    '<button type="button" class="btn btn-primary btn-sm" onclick="' + onclickSaveBtn + '" data-dismiss="modal">Cохранить</button>'),
                $cuterInfo = $($footer[0]),
                $dots = $cuter.find('.cutDot');
            moveCutDot.containerW = moveCutDot.w = newSizes.w;
            moveCutDot.containerH = moveCutDot.h = newSizes.h;
            moveCutDot.k = iWidth / newSizes.w;//отношение полотна обрезки к реальному размеру изображения
            moveCutDot.cuterInfo = {
                width: $cuterInfo.find('.selected .width'),
                height: $cuterInfo.find('.selected .height'),
                obj: $cuterInfo.find('.selected')
            };
            $cuter.mousedown(function (e) {
                var offsetX = e.offsetX || e.originalEvent.layerX,
                    offsetY = e.offsetY || e.originalEvent.layerY;
                e.stopPropagation();
                e.preventDefault();
                moveCutDot.enable = true;
                moveCutDot.mode = 'mouseArea';
                moveCutDot.left = offsetX;
                moveCutDot.top = offsetY;
                $cutArea.css({
                    width: 0,
                    height: 0,
                    left: offsetX,
                    top: offsetY
                });
                $cutArea.find('img').css({
                    left: - offsetX,
                    top: - offsetY
                });
                moveCutDot.cuterInfo.width.html(0);
                moveCutDot.cuterInfo.height.html(0);
                $content.find('>img').animate({opacity: 0.5});
                $cutArea.show();
                $dots.hide();
            });

            var mouseSelectAreaFunction = function (e) {
                e.stopPropagation();
                e.preventDefault();
                if (moveCutDot.enable == true) {
                    if (moveCutDot.mode == 'mouseArea') {
                        var left = parseInt($cutArea.css('left')),
                            top = parseInt($cutArea.css('top')),
                            dDotS = 40,
                            offsetX = e.offsetX || e.originalEvent.layerX,
                            offsetY = e.offsetY || e.originalEvent.layerY;
                        if (offsetX == left && offsetY == top) {
                            $content.find('>img').animate({opacity: 1});
                            $cutArea.removeAttr('style');
                            $cutArea.find('img').css({
                                left: '',
                                top: ''
                            });
                            moveCutDot.cuterInfo.obj.hide();
                        } else {
                            if (offsetX < left + dDotS)
                                offsetX = left + dDotS;
                            if (offsetY < top + dDotS)
                                offsetY = top + dDotS;

                            if (offsetX > moveCutDot.w)
                                offsetX = moveCutDot.w;
                            if (offsetY > moveCutDot.h)
                                offsetY = moveCutDot.h;

                            for (var i = 0, count = $dots.size(); i < count; i++) {
                                var $item = $($dots[i]),
                                    classPos = $item.attr('class').replace('cutDot ', ''),
                                    params = {};
                                switch (classPos) {
                                    case 'left top': {
                                        params.left = left;
                                        params.top = top;
                                    } break;
                                    case 'left bottom': {
                                        params.left = left;
                                        params.top = offsetY;
                                    } break;
                                    case 'right top': {
                                        params.left = offsetX;
                                        params.top = top;
                                    } break;
                                    case 'right bottom': {
                                        params.left = offsetX;
                                        params.top = offsetY;
                                    } break;
                                }
                                $item.css(params);
                            }
                            $dots.show();
                            $cutArea.css({cursor: 'move'});
                            var width = offsetX - left,
                                height = offsetY - top;
                            $cutArea.width(width);
                            $cutArea.height(height);
                            setSelectedSizes(width, height);
                        }
                    }
                    clearMoveCutDot();
                }
            };

            $cuter.mouseup(mouseSelectAreaFunction);
            $cuter.mouseleave(mouseSelectAreaFunction);
            $cuter.mousemove(function (e) {
                e.stopPropagation();
                e.preventDefault();
                if (moveCutDot.enable == true && moveCutDot.mode == 'mouseArea') {
                    var top = parseInt($cutArea.css('top')),
                        left = parseInt($cutArea.css('left')),
                        offsetX = e.offsetX || e.originalEvent.layerX,
                        offsetY = e.offsetY || e.originalEvent.layerY;
                    if (left <= offsetX && top <= offsetY) {
                        var dX = offsetX - moveCutDot.left,
                            dY = offsetY - moveCutDot.top;
                        /*if (e.shiftKey)
                         dY = dX;*/
                        resizeCutArea($cutArea, dX, dY);
                        setSelectedSizes($cutArea.width(), $cutArea.height());
                    }
                    moveCutDot.left = offsetX;
                    moveCutDot.top = offsetY;
                }
            });

            $cutArea.mousedown(function (e) {
                e.stopPropagation();
                e.preventDefault();
                moveCutDot.enable = true;
                moveCutDot.mode = 'area';
                moveCutDot.left = e.pageX;
                moveCutDot.top = e.pageY;
            });

            $cutArea.mouseup(function (e) {
                e.stopPropagation();
                e.preventDefault();
                clearMoveCutDot();
            });

            $cutArea.mousemove(function (e) {
                e.stopPropagation();
                e.preventDefault();
                if (moveCutDot.enable) {
                    switch (moveCutDot.mode) {
                        case 'dot': clearMoveCutDot(); break;
                        case 'area': {
                            var top = parseInt($(this).css('top')),
                                left = parseInt($(this).css('left')),
                                width = $(this).width(),
                                height = $(this).height(),
                                dX = getNormalD(e.pageX - moveCutDot.left, left, moveCutDot.w, width),
                                dY = getNormalD(e.pageY - moveCutDot.top, top, moveCutDot.h, height);
                            if (
                                left + width + dX <= moveCutDot.w
                                    && top + height + dY <= moveCutDot.h
                                    && left + dX >= 0
                                    && top + dY >= 0
                                ) {
                                var $items = $(this).closest('.cuter').find(' > div'),//точки и область
                                    $img = $(this).find('img');
                                $items.css({
                                    left: '+=' + dX + 'px',
                                    top: '+=' + dY + 'px'
                                });
                                $img.css({
                                    left: '-=' + dX + 'px',
                                    top: '-=' + dY + 'px'
                                });
                                moveCutDot.left = e.pageX;
                                moveCutDot.top = e.pageY;
                            }
                        } break;
                        case 'mouseArea': {
                            var top = parseInt($(this).css('top')),
                                left = parseInt($(this).css('left')),
                                offsetX = e.offsetX || e.originalEvent.layerX,
                                offsetY = e.offsetY || e.originalEvent.layerY;
                            if (left <= offsetX && top <= offsetY) {
                                resizeCutArea($cutArea, offsetX - moveCutDot.left, offsetY - moveCutDot.top);
                                setSelectedSizes($cutArea.width(), $cutArea.height());
                            }
                            moveCutDot.left = offsetX;
                            moveCutDot.top = offsetY;
                        } break;
                    }
                }
            });

            $dots.mousedown(function (e) {
                e.stopPropagation();
                e.preventDefault();
                moveCutDot.enable = true;
                moveCutDot.mode = 'dot';
                moveCutDot.left = e.pageX;
                moveCutDot.top = e.pageY;
                moveCutDot.className = $(this).attr('class');
            });

            $dots.mousemove(function (e) {
                e.stopPropagation();
                e.preventDefault();
                var top = parseInt($(this).css('top')),
                    left = parseInt($(this).css('left'));
                if (
                    moveCutDot.enable
                        && moveCutDot.mode == 'dot'
                        && moveCutDot.className == $(this).attr('class')
                        && 0 <= top
                        && top <= moveCutDot.h
                        && 0 <= left
                        && left <= moveCutDot.w
                    ) {
                    var classPos = $(this).attr('class').replace('cutDot ', ''),
                        dX = getNormalD(e.pageX - moveCutDot.left, left, moveCutDot.w, 0),
                        dY = getNormalD(e.pageY - moveCutDot.top, top, moveCutDot.h, 0),
                        $container = $(this).closest('.cuter');
                    if (0 <= top + dY && top + dY <= moveCutDot.h && 0 <= left + dX && left + dX <= moveCutDot.w) {
                        var coords = getCuterDotsCoords($dots, 1),
                            topS = coords.rightTop.left - coords.leftTop.left,//текущее расстояние между точками *----topS---*
                            rightS = coords.rightBottom.top - coords.rightTop.top,//                             |           |
                            bottomS = coords.rightBottom.left - coords.leftBottom.left,//                      leftS       rightS
                            leftS = coords.leftBottom.top - coords.leftTop.top,//                                |           |
                            dDotS = 40,//минимальное расстояние между точками                                    *--bottomS--*
                            params = {};
                        var strdX = '+=' + dX + 'px',
                            strdY = '+=' + dY + 'px';
                        switch (classPos) {
                            case 'left top': {
                                if (dDotS <= topS - dX) {
                                    $('.left.bottom', $container).css('left', strdX);
                                    params.left = strdX;
                                }
                                if (dDotS <= leftS - dY) {
                                    $('.right.top', $container).css('top', strdY);
                                    params.top = strdY;
                                }
                            } break;
                            case 'left bottom': {
                                if (dDotS <= bottomS - dX) {
                                    $('.left.top', $container).css('left', strdX);
                                    params.left = strdX;
                                }
                                if (dDotS <= leftS + dY) {
                                    $('.right.bottom', $container).css('top', strdY);
                                    params.top = strdY;
                                }
                            } break;
                            case 'right top': {
                                if (dDotS <= topS + dX) {
                                    $('.right.bottom', $container).css('left', strdX);
                                    params.left = strdX;
                                }
                                if (dDotS <= rightS - dY) {
                                    $('.left.top', $container).css('top', strdY);
                                    params.top = strdY;
                                }
                            } break;
                            case 'right bottom': {
                                if (dDotS <= bottomS + dX) {
                                    $('.right.top', $container).css('left', strdX);
                                    params.left = strdX;
                                }
                                if (dDotS <= rightS + dY) {
                                    $('.left.bottom', $container).css('top', strdY);
                                    params.top = strdY;
                                }
                            } break;
                        }
                        $(this).css(params);
                        var newCoords = getCuterDotsCoords($dots, 1);
                        $cutArea.css({
                            width: newCoords.rightTop.left - coords.leftTop.left,
                            height: newCoords.leftBottom.top - coords.leftTop.top,
                            top: newCoords.leftTop.top,
                            left: newCoords.leftTop.left
                        });
                        $cutArea.find('img').css({
                            top: - newCoords.leftTop.top,
                            left: - newCoords.leftTop.left
                        });
                        setSelectedSizes($cutArea.width(), $cutArea.height());
                        moveCutDot.left = e.pageX;
                        moveCutDot.top = e.pageY;
                    }
                }
            });

            $dots.mouseup(function (e) {
                e.stopPropagation();
                e.preventDefault();
                clearMoveCutDot();
            });

            var modalWidth = newSizes.w + 30,
                minWidth = 666,
                modal = new Modal({
                id: 'photoEditor_' + cuterID,
                content: $content,
                big: true,
                show: false,
                dismissBtn: false,
                width: modalWidth < minWidth ? minWidth : modalWidth,
                footer: $footer
            });

            $content.css({
                width: newSizes.w,
                height: newSizes.h
            });

            $canvas[0].width = iWidth;
            $canvas[0].height = iHeight;
            $cloneImage.width(newSizes.w);
            $cloneImage.height(newSizes.h);
            $cutArea.append($cloneImage.clone());
            ctx.drawImage(image, 0, 0, iWidth, iHeight);
            modal.show();
            pasting = false;
        };
    }
}
function getCutedImage(obj, cuterID) {
    var $modal = $(obj).closest('.modal'),
        $container = $('.cuterContainer', $modal),
        $containerImage = $modal.find('.cuterContainer > img'),
        canvas = $('canvas.cuter_canvas_' + cuterID)[0],
        ctx = canvas.getContext("2d"),
        $image = $('img.cuter_image_' + cuterID),
        k = $image.width() / $containerImage.width(),
        $dots = $modal.find('.cuter > .cutDot'),
        $cutArea = $modal.find('.cutArea'),
        dotsCoords = getCuterDotsCoords($dots, k),
        $buttons = $modal.find('.modal-footer button');

    if (dotsCoords && $dots.is(':visible')) {
        $buttons.prop("disabled", true);
        var sx = dotsCoords.leftTop.left,
            sy = dotsCoords.leftTop.top,
            sWidth = dotsCoords.rightTop.left - sx,
            sHeight = dotsCoords.leftBottom.top - sy;
        canvas.width = sWidth;
        canvas.height = sHeight;
        ctx.drawImage($image[0], sx, sy, sWidth, sHeight, 0, 0, sWidth, sHeight);
        $image[0].src = canvas.toDataURL("image/jpeg");

        $image[0].onload = function () {
            var newSizes = resizeImage({
                    w: $image.width(),
                    h: $image.height(),
                    maxW: $container.width(),
                    maxH: $container.height()
                }),
                $clone = $image.clone(),
                $originSizes = $modal.find('.original'),
                $selectedSizes = $modal.find('.selected');
            $('.width', $originSizes).html($image.width());
            $('.height', $originSizes).html($image.height());
            $('.width', $selectedSizes).html('');
            $('.height', $selectedSizes).html('');
            $selectedSizes.hide();
            $clone.attr({
                style: '',
                class: ''
            });
            $clone.width(newSizes.w);
            $clone.height(newSizes.h);
            $containerImage.replaceWith($clone);
            $container.width(newSizes.w);
            $container.height(newSizes.h);
            $dots.attr('style', '');
            $cutArea.attr('style', '');
            $cutArea.find('img').replaceWith($clone.clone());
            clearMoveCutDot();
            moveCutDot.w = newSizes.w;
            moveCutDot.h = newSizes.h;
            moveCutDot.k = $image.width() / newSizes.w;//отношение полотна обрезки к реальному размеру изображения
            $buttons.prop("disabled", false);
        }
    }
}

function getDotName(className){
    var classes = className.split(' ');
    return classes[1] + classes[2].charAt(0).toUpperCase() + classes[2].substr(1);
}

function getCuterDotsCoords($dots, k){
    var result = {};
    for (var i = 0, count = $dots.size(); i < count; i++) {
        var $item = $($dots[i]);
        result[getDotName($item.attr('class'))] = {
            top: Math.round(parseInt($item.css('top')) * k),
            left: Math.round(parseInt($item.css('left')) * k)
        };
    }
    return result;
}

function saveCutedImage(id, cuterID) {
    var canvas = $('canvas.cuter_canvas_' + cuterID)[0],
        date = new Date();
    addFileToMass(
        {
            data: canvas.toDataURL("image/jpeg"),
            name: 'screenShot_' + date.getDate() + '_' + (date.getMonth() + 1) + '_' + date.getFullYear() + '_' + date.getHours() + '_' + date.getMinutes() + '_' + date.getSeconds()
        },
        id,
        {
            disableHandler: true,
            part: 100,
            base64: true
        }
    );
    $(canvas).remove();
    $('.cuter_image_' + cuterID).remove();
    $('#photoEditor_' + cuterID).on('hidden.bs.modal', function () {
        $(this).remove();
    });
}

function updateCutedImage(uid, cuterID) {
    var canvas = $('canvas.cuter_canvas_' + cuterID)[0];
    for (var item in GALERY_FORMS) {
        if (GALERY_FORMS[item].uid == uid) {
            GALERY_FORMS[item].data = canvas.toDataURL("image/jpeg");
            GALERY_FORMS[item].size = getFileSizeByBase64(GALERY_FORMS[item].data);
            getFilesInfo(GALERY_FORMS[item].id);
            break;
        }
    }
}

function resizeCutArea($cutArea, dX, dY) {
    var width = $cutArea.width(),
        height = $cutArea.height(),
        params = {};
    if (width + dX > 0)
        params.width = width + dX;
    if (height + dY > 0)
        params.height = height + dY;
    $cutArea.css(params);
}

function getNormalD(d, indent, maxIndent, s) {
    s = s || 0;
    var result = 0;
    if (d < 0 && Math.abs(d) > indent) {
        result = - indent;
    } else if (d > 0 && indent + s + d > maxIndent) {
        result = maxIndent - indent - s;
    } else {
        result = d;
    }
    return result;
}

function setSelectedSizes(width, height){
    if (!moveCutDot.cuterInfo.obj.is(':visible')) {
        moveCutDot.cuterInfo.obj.css('display', 'inline-block');
    }
    moveCutDot.cuterInfo.width.html(Math.round(width * moveCutDot.k));
    moveCutDot.cuterInfo.height.html(Math.round(height * moveCutDot.k));
}

function rotateCutImage(obj, cuterID, clockwise) {
    var degree = Math.PI / 2,
        $modal = $(obj).closest('.modal'),
        $container = $('.cuterContainer', $modal),
        $containerImage = $container.find('>img'),
        canvas = $('canvas.cuter_canvas_' + cuterID)[0],
        ctx = canvas.getContext("2d"),
        $image = $('img.cuter_image_' + cuterID),
        $cutArea = $modal.find('.cutArea'),
        $buttons = $modal.find('.modal-footer button'),
        $dots = $modal.find('.cuter > .cutDot'),
        width = $image.width(),
        height = $image.height();

    if (!clockwise)
        degree = -degree;
    $buttons.prop("disabled", true);
    canvas.width = height;
    canvas.height = width;
    ctx.save();
    ctx.translate(degree < 0 ? 0 : height, degree < 0 ? width : 0);
    ctx.rotate(degree);
    ctx.drawImage($image[0], 0, 0);
    ctx.restore();
    $image[0].src = canvas.toDataURL("image/jpeg");
    $image[0].onload = function () {
        var newSizes = resizeImage({
                w: $image.width(),
                h: $image.height(),
                maxW: moveCutDot.containerW,
                maxH: moveCutDot.containerH
            }),
            $clone = $image.clone(),
            $originSizes = $modal.find('.original'),
            $selectedSizes = $modal.find('.selected');
        $('.width', $originSizes).html($image.width());
        $('.height', $originSizes).html($image.height());
        $('.width', $selectedSizes).html('');
        $('.height', $selectedSizes).html('');
        $selectedSizes.hide();
        $clone.attr({
            style: '',
            class: ''
        });
        $clone.width(newSizes.w);
        $clone.height(newSizes.h);
        $containerImage.replaceWith($clone);
        $container.width(newSizes.w);
        $container.height(newSizes.h);
        $dots.attr('style', '');
        $cutArea.attr('style', '');
        $cutArea.find('img').replaceWith($clone.clone());
        clearMoveCutDot();
        moveCutDot.w = newSizes.w;
        moveCutDot.h = newSizes.h;
        moveCutDot.k = $image.width() / newSizes.w;
        $buttons.prop("disabled", false);
    };
}

function editNotLoadedImg(obj, uid) {
    var item = getItemByUID(uid);
    if (item) {
        cuterInit(
            obj,
            item.data,
            {
                type: 'edit',
                uid: item.uid
            }
        );
    }
}

function getItemByUID(uid){
    for (var item in GALERY_FORMS) {
        if (GALERY_FORMS[item].uid == uid)
            return GALERY_FORMS[item];
    }
    return false;
}