	//**********Window Class**************
	function AnimatedDivCLS(parent) 
	{
		this.parent = parent;
		this.frames = 10;
		this.time = 500;
	}	
	//**********starts animation*************
	AnimatedDivCLS.prototype.init = function(parent, newElem, direction)
	{
		this.curFrame = 1;
		if (parent)
			this.parent = parent;
		//alert(this.parent.innerHTML)
		//return;
		this.animID = 0;
		this.direction = (direction) ? direction : "top";
		this.curFrame = 1;
		this.height = (this.parent.offsetHeight) ? this.parent.offsetHeight - 2 : 0;
		this.width = this.parent.offsetWidth - 2;
		this.anim_top = createElement('div', 'anim_top');
		this.anim_bot = createElement('div', 'anim_bot');
		this.anim_top.style.overflow = "hidden";
		this.anim_bot.style.overflow = "hidden";
		this.parent.style.overflow = "hidden";
		switch (this.direction)
		{
			case 'top': 
			{
				for (var i = 0; i < this.parent.children.length; i++)
					this.anim_top.appendChild(this.parent.children[i]);
					
				this.anim_top.style.height = (this.height - 1) + "px";
				this.anim_top.style.verticalAlign = "bottom";
				this.anim_top.style.width = this.width + "px";				
				//*******
				this.anim_bot.appendChild(newElem);
				this.anim_bot.style.height = 1 + "px";
				this.anim_bot.style.width = this.width + "px";
				//*******
				break;
			}
			case 'bottom':
			{
				this.anim_top.appendChild(newElem);
				this.anim_top.style.height = 1 + "px";
				this.anim_top.style.width = this.width + "px";
				//*******
				for (var i = 0; i < this.parent.children.length; i++)
					this.anim_bot.appendChild(this.parent.children[i]);
				
				this.anim_bot.style.height = (this.height - 1) + "px";
				this.anim_bot.style.width = this.width + "px";
				break;
			}
			case 'right':
			{
				this.anim_top.appendChild(newElem);
				this.anim_top.style.height = this.height + "px";
				this.anim_top.style.width = 1 + "px";
				this.anim_top.style.position = "relative";
				this.anim_top.style.styleFloat = "left";
				//*******
				for (var i = 0; i < this.parent.children.length; i++)
					this.anim_bot.appendChild(this.parent.children[i]);
				
				this.anim_bot.style.height = this.height + "px";
				this.anim_bot.style.width = (this.width - 1) + "px";
				this.anim_bot.style.textAlign = "left";
				this.anim_bot.style.styleFloat = "left";
				this.anim_bot.style.position = "relative";
				break;
			}
			case 'left':
			{
				for (var i = 0; i < this.parent.children.length; i++)
					this.anim_top.appendChild(this.parent.children[i]);
				
				this.anim_top.style.height = this.height + "px";
				this.anim_top.style.width = (this.width - 1) + "px";
				this.anim_top.style.textAlign = "right";
				this.anim_top.style.position = "relative";
				this.anim_top.style.styleFloat = "left";
				//*******
				this.anim_bot.appendChild(newElem);
				this.anim_bot.style.height = this.height + "px";
				this.anim_bot.style.width = 1 + "px";
				this.anim_bot.style.position = "relative";
				this.anim_bot.style.styleFloat = "left";
				break;
			}
			default : 
			{
				return;
			}
		}
		
		this.parent.innerHTML = "";
		this.parent.appendChild(this.anim_top);
		this.parent.appendChild(this.anim_bot);
		
		
		var t = this;
		this.animID = setInterval(function(){ t.animate(); }, this.time/this.frames);
		
	}
	
	AnimatedDivCLS.prototype.animate = function()
	{
		var curHeight = Math.floor(this.height*this.curFrame/this.frames);
		var curWidth =  Math.floor(this.width*this.curFrame/this.frames);
		//this.parent.style.overflow = "hidden";
		switch (this.direction)
		{
			case 'top': 
			{
				this.anim_top.style.height = this.height - curHeight + 'px';
				this.anim_bot.style.height = curHeight + 'px';
				break;
			}
			case 'bottom':
			{
				this.anim_top.style.height = curHeight + 'px';				
				this.anim_bot.style.height = this.height - curHeight + 'px';
				break;
			}
			case 'right':
			{
				this.anim_top.style.width = curWidth + 'px';				
				this.anim_bot.style.width = this.width - curWidth + 'px';
				break;
			}
			case 'left':
			{
				this.anim_top.style.width = this.width - curWidth + 'px';				
				this.anim_bot.style.width = curWidth + 'px';
				break;
			}
			default : 
			{
				this.anim_top.style.height = this.height - curHeight + 'px';
				this.anim_bot.style.height = curHeight + 'px';
			}
		}				
		if (this.curFrame < this.frames)
			this.curFrame++;
		else
		{
			clearInterval(this.animID);
			switch (this.direction)
			{
				case 'top': 
				case 'left': 
				{
					this.parent.removeChild(anim_top);
					for (var i = 0; i < this.anim_bot.children.length; i++)
						this.parent.appendChild(this.anim_bot.children[i]);
					this.parent.removeChild(anim_bot);
					break;
				}
				case 'bottom':
				case 'right':
				{
					this.parent.removeChild(anim_bot);
					for (var i = 0; i < this.anim_top.children.length; i++)
						this.parent.appendChild(this.anim_top.children[i]);
					this.parent.removeChild(anim_top);
					break;
				}
			}			
		}
	}