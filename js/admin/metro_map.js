var markers = [];
//******************************/
//храним координаты центра оркужности
//******************************/
function addMetroMarker(left, top, stationID, params) {
	var marker = false;

	if (checkSelectedMetroMarker(stationID)) {
		//removeMetroMarker(stationID);byClick
		return ;
	} else if (params.byClick) {
		addMetroMarkersFromArray(getDiffTransfers(stationID), params.showCheckedStations);
	}

	marker = createElement('div', 'station_' + stationID);
	marker.className = 'marker_selected';
	Event.add(marker, "click",
			function(e) 
			{
				removeMetroMarker(stationID);
			});
	getElem('metro_map_container').appendChild(marker);
	marker.style.left = (left - marker.offsetWidth/2) + 'px';
	marker.style.top = (top - marker.offsetHeight/2) + 'px';
	markers.push(marker);

	addStationBlock(stationID, 'sel_station_', getElem('selectedMetros'));
	
	if (params.showCheckedStations)
		addStationBlock(stationID, 'block_station_', getElem('metroStationsBlocks'));
	
	checkMetroIn(stationID);
}
//******************************/
function checkSelectedMetroMarker(stationID) //проверям, создан ли уже маркер станции или маркер пересадки с него с тем же именем
{
	var $marker = $('#station_' + stationID).size()

	if ($marker) {
		marker = $marker[0];

		return true;
	} else if (stations[stationID]) {
		var transfers = stations[stationID].transfers;

		for (var i = 0; i < transfers.length; i++) {
			if ( (stations[transfers[i]].name == stations[stationID].name) && getElem('station_' + transfers[i]) )
				return true;
		}
	}
	return false;
}
//******************************/
function getDiffTransfers(stationID) //проверям, создан ли уже маркер станции или маркер пересадки с него с тем же именем
{
	var realTransfers = [];
	if (stations[stationID])
	{
		var transfers = stations[stationID].transfers;
		for (var i = 0; i < transfers.length; i++)
		{
			if (stations[transfers[i]].name != stations[stationID].name)
				realTransfers.push(transfers[i]);
		}
	}
	return realTransfers;
}
//******************************/
function addStationBlock(stationID, name, stationsContainer)
{
	if (stationsContainer)
		stationsContainer.innerHTML += "<div class='subway' id='" + name + stationID + "'><span class='metro' style='background-color: #" + lines[stations[stationID].line].color + ";' title='" + lines[stations[stationID].line].name + " линия'>M</span><b>" + stations[stationID].name + "</b><img src='/img/item-obj-del.png' class='clickable metroDel' onclick='removeMetroMarker(" + stationID + ")'/></div>";
}
//******************************/
function selectMetroLine(link, lineID, showCheckedStations)
{
	showCheckedStations = (showCheckedStations);
	var metros = getLineStations(lineID);
	if (metros)
	{
		if (link.className != "bold")
		{
			addMetroMarkersFromArray(metros, showCheckedStations);
			link.className = "bold";
		}
		else
		{
			removeMetroMarkersFromArray(metros, showCheckedStations);
			link.className = "";
		}
	}
}
//******************************/
function getLineStations(lineID)
{
	if (stations)
	{
		var lineStations = [];

		for (var stationID in stations)
			if (stations[stationID].line == lineID)
				lineStations.push(stationID);

		return lineStations;
	}

	return false;
}
//*****************************
function checkMetroIn(id)
{
	var input = getElem('metro_search');

	if (input.form.buildnum)
		input.form.buildnum.value = 'ID';

	var metros = (input.value != '') ? input.value.split(',') : [];

	if (!inArray(id, metros))
	{
		metros.push(id);
		input.value = metros.join(',');
	}

	updateMetroCntStr(input.value);
}
//*****************************
function checkMetroOut(id)
{
	var input = getElem('metro_search');
	var metros = (input.value != '') ? input.value.split(',') : [];
	for (var i = 0; i < metros.length; i++)
		if (metros[i] == id)
		{
			metros.splice(i, 1);
			break;
		}
	input.value = metros.join(',')
	updateMetroCntStr(input.value);
}
//*****************************
function initMetroMap() {
	// $("#metroMap").arcticmodal();

	removeAllMetroMarkers();
	var metros = (getElem('metro_search').value != '') ? getElem('metro_search').value.split(',') : {};
	addMetroMarkersFromArray(metros);
}
//*****************************
function applyMetroFilters(params, link, showCheckedStations)
{
	showCheckedStations = (showCheckedStations);
	var selStations = [];
	if (params.metroPos && link)
	{
		for (var StationID in stations)
			if ( (stations[StationID].line == params.line) && (stations[StationID].metroPos == params.metroPos) )
				selStations.push(StationID);	
	}
	else if (params.metroGeoPos && link)
	{
		for (var StationID in stations)
			if ( (stations[StationID].metroGeoPos > 0) && (stations[StationID].metroGeoPos <= params.metroGeoPos) )
				selStations.push(StationID);
	}
	
	if (selStations.length)
	{
		if (link.className != "bold")
		{
			addMetroMarkersFromArray(selStations, showCheckedStations);
			link.className = "bold";
		}
		else
		{
			removeMetroMarkersFromArray(selStations, showCheckedStations);
			link.className = "";
		}
	}
}
//*****************************
function addMetroMarkersFromArray(metros, showCheckedStations)
{
	showCheckedStations = (showCheckedStations);
	for (var i = 0; i < metros.length; i++)
	{
		if (getElem('marker_' + metros[i]) && getElem('marker_' + metros[i]).coords)
		{
			var coords = getElem('marker_' + metros[i]).coords.split(',');
			addMetroMarker(coords[0], coords[1], metros[i], {showCheckedStations : showCheckedStations});
		}
	}
}
//*****************************
function initCheckedStations()
{
	if (!getElem('metro_search'))
		return;
	var metros = (getElem('metro_search').value != '') ? getElem('metro_search').value.split(',') : {};
	for (var i = 0; i < metros.length; i++)
	{
		addStationBlock(metros[i], 'block_station_', getElem('metroStationsBlocks'));
	}
}
//*****************************
function removeMetroMarkersFromArray(metros, showCheckedStations)
{
	showCheckedStations = (showCheckedStations);
	for (var i = 0; i < metros.length; i++)
	{
		if (getElem('marker_' + metros[i]))
		{
			removeMetroMarker(metros[i])
		}
	}
}
//*****************************
function nullMetroData()
{
	var input = getElem('metro_search');
	input.value = "";
	if (getElem('metroStationsBlocks'))
		getElem('metroStationsBlocks').innerHTML = "";
	if (getElem('selectedMetros'))
		getElem('selectedMetros').innerHTML = "";
	removeAllMetroMarkers();
	updateMetroCntStr(input.value);
}
//*****************************
function showMetroMap(showCheckedStations)
{
	showCheckedStations = (showCheckedStations);
    (new Modal('MetroModal')).show();
	initMetroMap(showCheckedStations);
}
//*****************************
function updateMetroCntStr(metros)
{
	var length = (metros) ? metros.split(';').length : 0;
	var metroCntHint = getElem('metro_selected_cnt');
	if (metroCntHint)
	{
		metroCntHint.title = "Станций выбрано: " + length;
		metroCntHint.innerHTML = '(' + length + ')';
	}
}
//******************************/
function getSelectedMetroMarker()
{
	for (var i = 0; i < markers.length; i++)
		if (markers[i].className == 'marker_selected')
			return i;
	return -1;
}
//******************************/
function getMetroMarker(stationID)
{
	for (var i = 0; i < markers.length; i++)
	{
		if (markers[i].id == 'station_' + stationID)
			return i;
	}
	return -1;
}
//******************************/
function removeMetroMarker(stationID)
{
	var i = getMetroMarker(stationID);
	if (i >= 0)
	{
		removeMetroMarkerPos(i);
		checkMetroOut(stationID);
		var stationBlock, line, filter;
		if (stationID)
		{
			if (stationBlock = getElem('block_station_' + stationID))
				stationBlock.parentNode.removeChild(stationBlock);
			if (stationBlock = getElem('sel_station_' + stationID))
				stationBlock.parentNode.removeChild(stationBlock);
			if (line = getElem('line_' + stations[stationID].line))
				line.className = "";
			if (filter = getElem('geoFilter_' + stations[stationID].metroGeoPos))
				filter.className = "";
			if (filter = getElem('line_' + stations[stationID].line + '_filter_' + stations[stationID].metroPos))
				filter.className = "";
		}
	}
}
//******************************/
function removeMetroMarkerPos(i)
{
	getElem('metro_map_container').removeChild(markers[i]);
	markers.splice(i, 1);
}
//******************************/
function removeAllMetroMarkers()
{
	while (markers.length > 0)
		removeMetroMarkerPos(0);	
	if (getElem('selectedMetros'))
		getElem('selectedMetros').innerHTML = "";
}
//******************************/
//******MODERATE****************/
//******************************/
//******************************/
//******************************/
function initMetroMarker()
{
	Event.add(document.body, "keydown", function (e) { return catchMetroKey(e); } );
	var left = (getElem('Left')) ? getElem('Left').value : 0;
	var top = (getElem('Top')) ? getElem('Top').value : 0;
	if ( (left > 0) && (top > 0) )
	{
		left = parseInt(left);
		top = parseInt(top);
		addMetroMarkerModerate({offsetX: left, offsetY: top});
	}
}
//******************************/
function addMetroMarkerModerate(e)
{
	if (markers.length == 0)
	{
		var marker = document.createElement('div');
		marker.className = 'marker_selected';
		getElem('metro_map_container').appendChild(marker);
		markers.push(marker);
	}
	else
		marker = markers[0];
	marker.style.left = (e.offsetX - marker.offsetWidth/2) + 'px';
	marker.style.top = (e.offsetY - marker.offsetHeight/2) + 'px';
	getElem('Left').value = e.offsetX;
	getElem('Top').value = e.offsetY;
}
//******************************/
function isMetroMarkerIntersects(marker, point)
{
	return ( (marker.offsetLeft >= point.x) && (marker.offsetLeft + marker.offsetWidth <= point.x) && 
			(marker.offsetTop >= point.y) && (marker.offsetTop + marker.offsetHeight <= point.y) );
}
//******************************/
function moveMetroMarketVertical(direction)
{
	var index = getSelectedMetroMarker();
	if (index >= 0)
	{
		markers[index].style.top = (markers[index].offsetTop + direction) + 'px';
		getElem('Top').value = markers[index].offsetTop + 5;
		return false;
	}
	return true;
}
//******************************/
function moveMetroMarketHorizontal(direction)
{
	var index = getSelectedMetroMarker();
	if (index >= 0)
	{
		markers[index].style.left = (markers[index].offsetLeft + direction) + 'px';
		getElem('Left').value = markers[index].offsetLeft + 5;
		return false;
	}
	return true;
}
//******************************/
function catchMetroKey(e)
{
	//******delete*******
	if (e.keyCode == 46)
	{
		removeMetroMarker(getSelectedMetroMarker());
		return false;
	}
	//*****left**********
	if (e.keyCode == 37)
	{
		return moveMetroMarketHorizontal(-1);
	}
	//*****up**********
	if (e.keyCode == 38)
		return moveMetroMarketVertical(-1);
	//*****right**********
	if (e.keyCode == 39)
		return moveMetroMarketHorizontal(1);
	//*****up**********
	if (e.keyCode == 40)
		return moveMetroMarketVertical(1);
}