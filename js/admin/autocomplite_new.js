/*<input type='text' id='users' name='users' />
 <hr/>
 <input type='text' id='moder' name='moder' />
 <script>
 autocomplite(getElem('users'), "/admin/ajax_functions.php", {'function' : "getAutocompliteUsers"});
 autocomplite(getElem('moder'), "/admin/ajax_functions.php", {'function' : "getAutocompliteUsers"});
 </script>*/

var isIE = checkBrowser();
var scrollWidth = 15;
var loadingImg = "/img/loading.gif";


function autocomplite(elem, value, url, URLparams, acParams)
{
    var maxHeight = 400;
    value = (value) ? value : '';
    var errMsg = (acParams && acParams.errMsg) ? acParams.errMsg : "Некорректные данные";
    var events = (acParams && acParams.events) ? acParams.events : {};
    var updateURLparams = (acParams && acParams.updateParamsFunct) ? acParams.updateParamsFunct : null;
    var onsuccessEvent = (acParams && acParams.onsuccessEvent) ? acParams.onsuccessEvent : "";
    scrollWidth = getScrollBarWidth();
    var elemAutocomplite = createElement('input', 'autocompliteInput_' + elem.name, 'autocompliteInput_' + elem.name);
    elemAutocomplite.className = elem.className;
    elemAutocomplite.type = elem.type;
    elemAutocomplite.value = value;
    elemAutocomplite.rel = value;
    elemAutocomplite.autocomplete = elem.autocomplete = "off";
    elemAutocomplite = insertAfter(elemAutocomplite, elem);
    elem.style.display = "none";

    var isChanged = false,
        timeoutID;

    var parentField = elemAutocomplite.parentNode.parentNode.parentNode;
    Event.add(elemAutocomplite, "keyup",
        function(e)
        {
            clearTimeout(timeoutID);
            var $this = this;
            timeoutID = setTimeout(function(){
                $this.value = $this.value.replace(/^\s/, '');
                clearIncorrectDataMsg(elemAutocomplite);
                var container = getElem('autocompliteContainer_' + $this.name);
                if (container)
                {
                    if (e.keyCode == 16) //shift
                    {
                        if (container)
                            container.parentNode.removeChild(container);
                    }
                    if (e.keyCode == 40) // down
                    {
                        var list = container.children;
                        if (list.length > 0)
                        {
                            var prvSelected = -1;
                            var heightToSelected = 0;
                            for (var i = 0; i < list.length; i++)
                            {
                                heightToSelected += list[i].offsetHeight;
                                if (list[i].className == "autocompliteItem selected")
                                {
                                    if (i != list.length - 1)
                                        list[i].className = "autocompliteItem";
                                    prvSelected = i;
                                    break;
                                }
                            }
                            heightToSelected = (prvSelected == -1) ? 0 : heightToSelected;
                            if (prvSelected < list.length - 1)
                            {
                                var selectedItem = prvSelected + 1;
                                list[selectedItem].className = "autocompliteItem selected";
                                elemAutocomplite.value = remove_highlight(list[selectedItem].innerHTML);
                                isChanged = (elem.value != list[selectedItem].rel);
                                elem.value = list[selectedItem].rel;
                                if (container.style.overflow == "auto")
                                {
                                    if (heightToSelected > 0)
                                    {
                                        heightToSelected = heightToSelected - container.scrollTop;
                                        if ((selectedItem - 1 > 0) && (heightToSelected > container.offsetHeight / 2))
                                            container.scrollTop += list[selectedItem - 1].offsetHeight;
                                    }
                                }
                            }
                        }
                        return;
                    }
                    else if (e.keyCode == 38) //up
                    {
                        var list = container.children;
                        if (list.length > 0)
                        {
                            var prvSelected = -1;
                            var heightToSelected = 0;
                            for (var i = 1; i < list.length; i++)
                            {
                                if (list[i].className == "autocompliteItem selected")
                                {
                                    if (i > 0)
                                        list[i].className = "autocompliteItem";
                                    prvSelected = i;
                                    heightToSelected += list[i].offsetHeight;
                                    break;
                                }
                                heightToSelected += list[i].offsetHeight;
                            }
                            if (prvSelected > 0)
                            {
                                var selectedItem = prvSelected - 1;
                                list[selectedItem].className = "autocompliteItem selected";
                                elemAutocomplite.value = remove_highlight(list[selectedItem].innerHTML);
                                isChanged = (elem.value != list[selectedItem].rel);
                                elem.value = list[selectedItem].rel;
                                if (container.style.overflow == "auto")
                                {
                                    heightToSelected = heightToSelected - container.scrollTop;
                                    if ((selectedItem - 1 > 0) && (heightToSelected < container.offsetHeight / 2))
                                        container.scrollTop = Math.max(0, container.scrollTop - list[selectedItem - 1].offsetHeight);
                                }
                            }
                        }
                        return;
                    }
                }
                //**********************
                if (e.keyCode == 13) //enter
                {
                    if (container)
                        container.parentNode.removeChild(container);
                    if (parentField)
                        parentField.style.zIndex = 1;
                    if (elemAutocomplite.value && !elem.value)
                        addIncorrectDataMsg(elemAutocomplite, errMsg);
                    else
                        eval(onsuccessEvent);
                    if (isChanged && elem && events && events.change)
                        events.change(elem);
                    isChanged = false;
                    return;
                }
                //**********************
                if ($this.value.length < 2)
                {
                    if (container)
                        container.parentNode.removeChild(container);
                    if (parentField)
                        parentField.style.zIndex = 1;
                    elem.value = "";
                    isChanged = true;
                    if (isChanged && elem && events && events.change)
                        events.change(elem);
                    isChanged = false;
                    return;
                }

                var container;
                if (!(container = getElem('autocompliteContainer_' + elemAutocomplite.name)))
                    container = createElement('div', 'autocompliteContainer_' + elemAutocomplite.name);
                container.innerHTML = (loadingImg) ? "<img src='" + loadingImg + "' align='left' />" : "";
                var parentPos = getElementPos(elemAutocomplite);
                container.className = 'autocompliteContainer';
                container.style.overflow = "visible";
                container.style.height = "auto";
                //container.style.width = "auto";
                container.style.left = parentPos.left + 'px';
                container.style.top = parentPos.top + parentPos.height + 1 + 'px';
                elemAutocomplite.parentNode.appendChild(container);

                if (parentField)
                    parentField.style.zIndex = 20;
                //**********************
                URLparams['namePart'] = $this.value;
                var updatedParams = (updateURLparams) ? updateURLparams() : {};
                if (updatedParams)
                    for (param in updatedParams)
                        URLparams[param] = updatedParams[param];

                //Дополнительные параметры на запрос
                if(acParams && acParams.params)
                {
                    for (param in acParams.params)
                        URLparams[param] = acParams.params[param];
                }

                var paramStr = "";
                for (param in URLparams)
                    paramStr += ((paramStr) ? "&" : "") + param + "=" + URLparams[param];
                ajax('', paramStr,
                    function(result)
                    {
                        if (result['success'])
                        {
                            var datas = result['data'];
                            container.innerHTML = "";

                            elem.value = "";
                            //isChanged = true;
                            if (datas.length > 0)
                            {
                                for (cnt in datas)
                                {
                                    var item = createElement('div');
                                    var counter = cnt;
                                    //item.appendChild(document.createTextNode(datas[cnt][1]));
                                    item.innerHTML = highlight(URLparams.namePart, datas[cnt][1]);
                                    item.rel = datas[cnt][0];
                                    item.className = 'autocompliteItem';
                                    if (datas[cnt][1].toLowerCase() == elemAutocomplite.value.toLowerCase())
                                    {
                                        item.className += ' selected';
                                        isChanged = (elem.value != datas[cnt][0]);
                                        elem.value = datas[cnt][0];
                                    }
									/*Event.add(item, "mouseover",
									 function(e)
									 {
									 this.className = "autocompliteItemSelected";
									 });
									 Event.add(item, "mouseout",
									 function(e)
									 {
									 this.className = "autocompliteItem";
									 });*/


                                    $(item).click(function(e)
                                        {
                                            var val = remove_highlight(this.innerHTML);

                                            var dataList = $(this).find('.tagAutocompliteItem').data();
                                            if(dataList.value)
                                            {
                                                val = remove_highlight(dataList.value);
                                            }


                                            elemAutocomplite.value = val;
                                            elemAutocomplite.rel = val;
                                            isChanged = isChanged || (elem.value != this.rel);
                                            elem.value = this.rel;
                                            if(dataList.elementValue)
                                            {
                                                elem.value = remove_highlight(dataList.elementValue);
                                            }

                                            this.parentNode.parentNode.removeChild(this.parentNode);
                                            if (parentField)
                                                parentField.style.zIndex = 1;
                                            eval(onsuccessEvent);
                                            if (isChanged && elem && events && events.change)
                                                events.change(elem);
                                            isChanged = false;
                                        });
                                    container.appendChild(item);
                                }

								/*if (container.offsetWidth + scrollWidth < parentPos.width)
								 container.style.width = parentPos.width - 2 + 'px';
								 else
								 container.style.width = container.offsetWidth + scrollWidth + 2 + 'px';*/
                                var needScroll = (container.offsetHeight > maxHeight);
                                if (needScroll)
                                {
                                    container.style.height = maxHeight + 'px';
                                    container.style.overflow = "auto";
                                    //var contOffset = $(container).offset();
                                    $(container).children().width(container.offsetWidth - scrollWidth - 13/*paddings*/ - 2/*borders*/ - 50 /*buildImage*/);
                                    //item.style.width = container.offsetWidth - scrollWidth + 'px';
                                }

                                Event.add(container, "blur",
                                    function(e)
                                    {
                                        if (!elem.value)
                                            addIncorrectDataMsg(elemAutocomplite, errMsg);
                                        this.parentNode.removeChild(this);
                                        if (parentField)
                                            parentField.style.zIndex = 1;
                                        if (isChanged && elem && events && events.change)
                                            events.change(elem);
                                        isChanged = false;
                                    });
                            }
                            else
                            {
                                //container.style.width = parentPos.width - 2 + 'px';
                                container.innerHTML = "<div class='no-matches'>нет совпадений</div>";
                            }
                        }
                        else
                            alert(result['error']);
                    }, true);
            }, 500);
        });
    Event.add(elemAutocomplite, "blur",
        function(e)
        {
            var container;
            if (container = getElem('autocompliteContainer_' + this.name))
            {
                if (!isMouseOver(container))
                {
                    container.parentNode.removeChild(container);
                    if (parentField)
                        parentField.style.zIndex = 1;
                    if (!elem.value)
                        addIncorrectDataMsg(elemAutocomplite, errMsg);
                    else
                        eval(onsuccessEvent);

                    if (isChanged && elem && events && events.change)
                        events.change(elem);
                }
            }
            else
            {
                if (this.value == "")
                {
                    this.value = this.rel = "";
                    elem.value = "";
                }
                else
                {
                    if (!elem.value)
                        addIncorrectDataMsg(elemAutocomplite, errMsg);
                    else
                        eval(onsuccessEvent);

                    if (isChanged && elem && events && events.change)
                        events.change(elem);
                }
                isChanged = false;
            }
        });
    return elemAutocomplite;
}

function addIncorrectDataMsg(field, msg)
{
    if (!field)
        return;
    field.parentNode.className = field.parentNode.className.replace(/error/, '');
    field.parentNode.parentNode.parentNode.className = field.parentNode.parentNode.parentNode.className.replace(/error/, '');

    field.parentNode.className += " error";
    field.parentNode.parentNode.parentNode.className += " error";
    var errSpan;
    if (errSpan = getElem('status_' + field.previousSibling.name))
        errSpan.innerHTML = msg;
}

function clearIncorrectDataMsg(field)
{
    if (!field)
        return;
    field.parentNode.className = field.parentNode.className.replace(/error/, '');
    field.parentNode.parentNode.parentNode.className = field.parentNode.parentNode.parentNode.className.replace(/error/, '');
    if (errSpan = getElem('status_' + field.previousSibling.name))
        errSpan.innerHTML = "";
}


function getElem(id)
{
    return document.getElementById(id);
}

function isMouseOver(elem)
{
    if (!elem)
        return false;
    var elemPosParams = getElementPos(elem, true);
    return	(mousex > elemPosParams.left) && (mousex < elemPosParams.left + elemPosParams.width) &&
        (mousey > elemPosParams.top) && (mousey < elemPosParams.top + elemPosParams.height);
}

//********************************
function insertAfter(newElement, targetElement)
{
    var parent = targetElement.parentNode;

    //if the parents lastchild is the targetElement...
    if (parent.lastchild == targetElement)
        return parent.appendChild(newElement);
    else
        return parent.insertBefore(newElement, targetElement.nextSibling);
}

function getElementPos(elem, fromScreenZero)
{
    if (!elem)
        return false;

    var w = elem.offsetWidth;
    var h = elem.offsetHeight;

    var l = 0;
    var t = 0;
    var positionStyle = "";

    while (elem)
    {
        l += elem.offsetLeft;
        t += elem.offsetTop;
        elem = elem.offsetParent;
        if (!elem)
            break;
        positionStyle = (isIE) ? elem.currentStyle['position'] : getComputedStyle(elem, null).position;
        if (((positionStyle == "relative") || (positionStyle == "absolute")) && !fromScreenZero)
            break;
    }
    return {"left": l, "top": t, "width": w, "height": h};
}

//*************************************
function getElementComputedStyle(elem, prop)
{
    if (typeof elem != "object")
        elem = document.getElementById(elem);

    // external stylesheet for Mozilla, Opera 7+ and Safari 1.3+
    if (document.defaultView && document.defaultView.getComputedStyle)
    {
        if (prop.match(/[A-Z]/))
            prop = prop.replace(/([A-Z])/g, "-$1").toLowerCase();
        return document.defaultView.getComputedStyle(elem, "").getPropertyValue(prop);
    }
    // external stylesheet for Explorer and Opera 9
    if (elem.currentStyle)
    {
        var i;
        while ((i = prop.indexOf("-")) != - 1)
            prop = prop.substr(0, i) + prop.substr(i + 1, 1).toUpperCase() + prop.substr(i + 2);
        return elem.currentStyle[prop];
    }
    return "";
}
//*****************************************
/*function createElement(type, id, name)
 {
 var element;
 if (isIE)
 element = document.createElement("<" + type + ( (id) ? " id='" + id + "'" : "") + ( (name) ? " name='" + name + "'" : "") + ">");
 else
 {
 element = document.createElement(type);
 if (id)
 element.id = id;
 if (name)
 element.name = name;
 }
 return element;
 }*/


var prvmousex = mousex = prvmousey = mousey = 0;
if (isIE) //ie
{
    document.onmousemove = function()
    {
        prvmousex = mousex;
        prvmousey = mousey;
        mousex = event.clientX + document.body.scrollLeft;
        mousey = event.clientY + document.body.scrollTop;
        return true
    }
}
else // !ie
{
    document.onmousemove = function(e)
    {
        prvmousex = mousex;
        prvmousey = mousey;
        mousex = e.pageX;
        mousey = e.pageY;
        return true
    }
}

function checkBrowser()
{
    if (navigator.appName.indexOf("Microsoft") != -1)
    {
        return true //IE
    }
    return false;
}

function getScrollBarWidth()
{
    var inner = createElement('p');
    inner.style.width = '100%';
    inner.style.height = '200px';

    var outer = createElement('div');
    outer.style.position = 'absolute';
    outer.style.top = '0px';
    outer.style.left = '0px';
    outer.style.visibility = 'hidden';
    outer.style.width = '200px';
    outer.style.height = '150px';
    outer.style.overflow = 'hidden';
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2)
        w2 = outer.clientWidth;
    document.body.removeChild(outer);

    return (w1 - w2);
}
;

function highlight(find, text)
{
    if (text && find)
        return text.replace(new RegExp("(^|[#\\s\\(\>«])" + find, "gi"), highlight_backend)
}

function highlight_backend(matches)
{
    var pre = (matches.charAt(0) == ">") ? ">" : "";
    matches = (matches.charAt(0) == ">") ? matches.replace('>', '') : matches;
    return pre + "<b class='matched'>" + matches + "</b>";
}

function remove_highlight(text)
{
    if (text)
        return text.replace(new RegExp("<.*?>", "gi"), "");
}
