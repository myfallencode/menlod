function ckeck_address(input)
{
	if( (input.form.PrevAddress.value == input.value) )
		return;
	var CityID = input.form.CityID;
	var status = getElem('status_Address');
	input.form.PrevAddress.value = input.value;
    $('#MetroID').closest('.form-group').css('display', 'none');
	if(input.value && CityID && CityID.value && status)
	{
		showBuildMap("none");
		status.innerHTML = "<img src='/img/loading.gif' />";
		ajax("", "function=ckeckAddress&Address=" + encodeURIComponent(input.value) + "&CityID=" + CityID.value,
				function(result)
				{
					if (result.debug)
						alert(result.debug);
					var pointParams = result.pointParams;
					status.innerHTML = "<span style='color: #" + ( (result.success) ? "0f0" : "EC2028" ) + ";'>" + 
										result.title + " - " + "<span class='chooseAddress' onclick='setAddress(\"" + pointParams.yaAddress + "\");' title='Выбрать этот адрес'>'" + pointParams.yaAddress + "'</span></span>";
					if(pointParams.latDegrees && pointParams.lngDegrees)
					{
						input.form.LatDegrees.value = pointParams.latDegrees;
						input.form.LngDegrees.value = pointParams.lngDegrees;
					}
					
					
					if(result.success)
					{
						setAddress(pointParams.yaAddress);
					}
					else
					{
						if(pointParams.precision == 2)
							getBuildMetros();
						status.innerHTML += "<span class='chooseMap'>уточните точку на карте</span>";
					}
					input.form.AddressPrecision.value = pointParams.precision;
					showBuildMap("", pointParams.latDegrees, pointParams.lngDegrees, pointParams.zoom);
				}
			);
	}
}

function setAddress(address)
{
	var input = getElem('Address');
	if(input)
	{
		input.value = address;
		input.form.PrevAddress.value = address;
		getBuildMetros();
	}
}

function checkCity(input)
{
	if( (input.form.PrevCityID.value == input.value) )
		return;
    $(input.form.Address).closest('.form-group').css('display', input.value ? "" : "none");
	input.form.Address.value = "";	
	getElem('status_Address').innerHTML = "Дмитровское шоссе, 18 стр 1, к 1";
    $('#MetroID').closest('.form-group').css('display', 'none');
	input.form.PrevCityID.value = input.value;
	showBuildMap("none");
}

function showBuildMap(display, latDegrees, lngDegrees, zoom)
{
	display = (display) ? display : "";
	if(getElem('СhooseBuildMap'))
	{
        $("#СhooseBuildMap").closest('.form-group').css('display', display);
		if(display == "")
		{
			var center = new google.maps.LatLng(latDegrees, lngDegrees);
			map.setCenter(center);
			map.setZoom(zoom);
			if(chooseBuildMarker)
				chooseBuildMarker.setMap(null);
			chooseBuildMarker = addDruggableMarker(center, zoom) ;
		}			
	}
}

var map;
var defZoom = 14;
var baseIcon;
var markerOptions;
var chooseBuildMarker;
//************************************
function load_gmap(mapID, iddleFunction, useMiniControls) 
{	
	mapID = (mapID) ? mapID : "mapContainer";
	useMiniControls = (useMiniControls) ? true : false;
	var hide = (getElem(mapID).style.display == "none");
	//**********************************
	var mapOptions = {	
						scrollwheel : false, 
						panControl: !useMiniControls,  
						streetViewControl: !useMiniControls,  
						overviewMapControl: !useMiniControls,
						mapTypeControl: !useMiniControls					
					}; 
	if(useMiniControls)
	{
		mapOptions['zoomControlOptions'] = {
												style: google.maps.ZoomControlStyle.LARGE,
												position: google.maps.ControlPosition.LEFT_CENTER
											};
	}
	/* Создать точку - центр */
	var center = new google.maps.LatLng(55.7558, 37.6177);
	/* Создать основной объект карты */
	var mapContainer = getElem(mapID);
	
	mapContainer.style.display = "";
	
	var map = new google.maps.Map(mapContainer, mapOptions);
	
	//map.addControl(map_scale_ctrl);
	map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
	map.setCenter(center);
	map.setZoom(defZoom);
	
	google.maps.event.addListenerOnce(map, 'idle', function()
	{
		if(hide)
			mapContainer.style.display = "none";
		if (iddleFunction)
			iddleFunction();
	});	
		
	return map;
}

function mapResize()
{
	alert('resized');
}

function addDruggableMarker(point, zoom) 
{
	zoom = (zoom) ? zoom : defZoom;
	var marker = new google.maps.Marker({draggable : true, map: map, position : point});
	google.maps.event.addListener(marker, 'dragend', function()
			{
				var LatLng = marker.getPosition();
				getElem('LatDegrees').value = LatLng.lat();
				getElem('LngDegrees').value = LatLng.lng();
				getBuildMetros();
			});
	map.setCenter(point);
	map.setZoom(zoom);
	return marker;
}

var infowindow = false;
function addSmallMapMarker(point, zoom, clickable, buildID, blocks) 
{
	zoom = (zoom) ? zoom : defZoom;
	clickable = (clickable);
	var marker = new google.maps.Marker({map: map, position : point, icon : '/img/icons/gmarkers/small.png'});
	map.setCenter(point);
	map.setZoom(zoom);
	
	if(clickable)
		google.maps.event.addListener(marker, 'click', 	function(e) 
														{
															var marker = this;
															if(infowindow)
																infowindow.close();
															var searchForm = getElem('searchForm');
															var params = "";
															if(searchForm)
																params = "&valute=" + searchForm.valute.value + "&view=" + searchForm.view.value;
															ajax("/ajax/", "function=getMapBuildBlocks&blocks=" + blocks + "&buildID=" + buildID + params, 
																		function(result)
																		{
																			if (result['debug'])					
																				alert(result['debug']);
																			if (result.success)
																			{
																				infowindow = new GMapWindowCLS({ content : result.text, map : map, marker : marker, iconNormal : 'small' });
																				var form = getElem('formBuild_' + buildID);
																				if(form)
																					initCheckbox(form);	
																			}
																		}
																	);
														});
	google.maps.event.addListener(marker, 'mouseover', 	function(e) 
														{
															if(this.getIcon() != '/img/icons/gmarkers/clicked.png')
																this.setIcon('/img/icons/gmarkers/over.png');
															if(buildID)
																backlightListBlocks(buildID, true);
														});
	google.maps.event.addListener(marker, 'mouseout', 	function(e) 
														{
															if(this.getIcon() != '/img/icons/gmarkers/clicked.png')
																this.setIcon('/img/icons/gmarkers/small.png');
															if(buildID)
																backlightListBlocks(buildID, false)
														});
	return marker;
}

function addMarker(point, zoom, clickable) 
{
	zoom = (zoom) ? zoom : defZoom;
	clickable = (clickable);
	var marker = new google.maps.Marker({map: map, position : point, icon : '/img/icons/gmarkers/normal.png'});
	map.setCenter(point);
	map.setZoom(zoom);
	
	if(clickable)
		google.maps.event.addListener(marker, 'click', 	function(e) {
            if(infowindow)
                infowindow.close();

            infowindow = new GMapWindowCLS({ content : "test ttttteeeeeeeeeeexxxxxxxt	<br /> test <br />", map : map, marker : this});
        });
	return marker;
}

function hideChoosingBuildMap()
{
	if( !getElem('LatDegrees') || !getElem('LatDegrees').value)
		showBuildMap("none");
	else
		showBuildMap("", getElem('LatDegrees').value, getElem('LngDegrees').value);
}

function getBuildMetros()
{	
	checkBuildDouble();
	
	var form = getElem('add_by_owner_step1');
	form = (form) ? form : getElem('edit_main');
	var metroContainer = getElem('MetroID');
	if(metroContainer && form && form.LngDegrees.value && form.LatDegrees.value && form.CityID.value)
	{
		var container = metroContainer.parentNode;
		container.innerHTML = "<img src='/img/loading.gif' />";
		ajax("/ajax/", "function=get_build_neares_metros&LngDegrees=" + form.LngDegrees.value + "&LatDegrees=" + form.LatDegrees.value + "&CityID=" + form.CityID.value,
					function(result)
					{
						if (result.debug)
							alert(result.debug);
                        $(container).closest('.form-group').css('display', result.text == '' ? "none" : "");
						container.innerHTML = result.text;
						if(initCheckbox)
							initCheckbox(form);
					}
				);
	}
}

function checkBuildDouble()
{
	var form = getElem('add_by_owner_step1');
	if (form)
	{
		ajax("/ajax/", "function=check_build_double&LngDegrees=" + form.LngDegrees.value + "&LatDegrees=" + form.LatDegrees.value,
						function(result)
						{
							if (result.debug)
								alert(result.debug);
								
							if(result.success)
								showShadowWin(result.text);
						}
					);
	}
}

function getMyBuild(buildID)
{
	ajax("/ajax/", "function=get_my_build&buildID=" + buildID,
					function(result)
					{
						if (result.debug)
							alert(result.debug);
							
						if(result.success)
							location = "/my/objects/";
					}
				);
}

function showShadowWin(content)
{
	var shadowContainer = createElement('div');
	shadowContainer.className = "shadowWindow";
	shadowContainer.unselectable = "on";
	document.body.appendChild(shadowContainer);
	
	var container = createElement('div');
	container.className = "shadowContainer";
	document.body.appendChild(container);
	
	container.innerHTML = content;
	
	shadowWinResize();
	$( window ).resize(function(){ shadowWinResize();  });
}

function shadowWinResize()
{
	var $shadowContainer = $('.shadowWindow');
	var $container = $('.shadowContainer');
	
	if($shadowContainer.length && $container.length)
	{
		$shadowContainer.css(	{width :  $(document.body).width()}	);
		$shadowContainer.height(	{width :  $(document.body).height()}	);
		
		$container.css(	{left : ($shadowContainer.width() - $container.width())/2}	);
		$container.css(	{top : ($shadowContainer.height() -$container.height())/2}	);
	}
}

function hideShadowWin()
{
	$('.shadowWindow').remove();
	$('.shadowContainer').remove();
}
//*******************************************
function bigMapMarkerClick(buildID)
{
	if(gmapClusterBuildMarkers[buildID])
	{
		google.maps.event.trigger(gmapClusterBuildMarkers[buildID].Marker, 'click');
	}
}
//*******************************************
//*******************************************
var gmapCluster;
var gmapClusterMarkers = [];
var gmapClusterBuildMarkers = [];
var gmapBoundedBuilds = [];
function initBigMapCluster()
{
	var mcOptions = {gridSize: 40, maxZoom: 14};
	var gmapCluster = new MarkerClusterer(bigMap, gmapClusterMarkers, mcOptions);
	bigMap.setOptions({scrollwheel : true});
	google.maps.event.addListener(gmapCluster, 'clusteringend', function(cluster) 
								{
									showBoundedBuildsList();
								});
	mapIntoView(buildsOnMap, bigMap);
	bigMapResize();
}

function showBoundedBuildsList()
{
	var mapBoundMarkers = getBoundedBuilds();
	if(mapBoundMarkers.Builds.join(',') != gmapBoundedBuilds.join(','))
		getBuildsInfo(mapBoundMarkers.Blocks);
	gmapBoundedBuilds = mapBoundMarkers.Builds;
}

function getBoundedBuilds()
{
	var bound = bigMap.getBounds();
	var boundedBuilds = [];
	var boundedBlocks = [];
	for (var buildID in gmapClusterBuildMarkers)
	{
		if( (gmapClusterBuildMarkers[buildID].Marker.getMap() == bigMap) && bound.contains(gmapClusterBuildMarkers[buildID].LatLng) )
		{
			boundedBuilds.push(buildID);
			boundedBlocks.push(gmapClusterBuildMarkers[buildID].Blocks);
		}
	}
	return { Builds : boundedBuilds, Blocks : boundedBlocks };
}

function getBuildsInfo(blocks)
{
	if(blocks && blocks.length)
	{
		var searchForm = getElem('searchForm');
		var params = "";
		if(searchForm)
			params = "&searchID=" + searchForm.search.value;// + "&valute=" + searchForm.valute.value + "&view=" + searchForm.view.value + "&deal=" + searchForm.deal.value + "&obj=" + searchForm.obj.value;
		ajax("/ajax/", "function=getBuildsInfo&blocks=" + blocks.join(',') + params, 
				function(result)
				{
					if (result['debug'])					
						alert(result['debug']);
					if (result.success)
					{
						searchObjectsScrollApi.getContentPane().html(result.list);
						searchObjectsScrollApi.reinitialise();
						searchObjectsScrollApi.scrollTo(0, 0);
					}
				}
			);
	}
	else
	{
		searchObjectsScrollApi.getContentPane().html('Наведите карту на метки объектов, чтобы отобразился список');
		searchObjectsScrollApi.reinitialise();
		searchObjectsScrollApi.scrollTo(0, 0);
	}
}

function getMarkers()
{
	var form = getElem('searchForm');
	if(form && gmapClusterMarkers)
	{
		if(gmapClusterMarkers.length != 0)
			return;
		
		form.page.value = form.nextPage.value;
		var params = "function=getSearchMarkers";
		for (var i = 0; i < form.length; i++)
		{
			if ( ( (form[i].type != 'button') && (form[i].type != 'submit') && (form[i].type != 'checkbox')  && (form[i].type != 'radio') ) ||
				 ( (form[i].type == 'checkbox') && form[i].checked ) || ( (form[i].type == 'radio') && form[i].checked ) )
			{
				params += "&" + form[i].name + "=" + encodeURIComponent(form[i].value);
			}
		}
		
		ajax("/ajax/", params, 
			function(result)
			{
				if (result['debug'])					
					alert(result['debug']);
				if (result.success)
				{
					if(result.coords)
					{
						for (var buildID in result.coords)
						{
							var latLng = new google.maps.LatLng(result.coords[buildID].Lat, result.coords[buildID].Lng);
							var marker = createBigMapMarker(latLng, buildID, result.coords[buildID].Blocks);
							gmapClusterBuildMarkers[buildID] = { LatLng : latLng, Marker : marker, Blocks : result.coords[buildID].Blocks };
							gmapClusterMarkers.push(marker);							
						}
						initBigMapCluster();
						mapIntoView(buildsOnMap, bigMap);
					}
				}
			}
		);
	}
}

function createBigMapMarker(point, buildID, blocks)
{
	var marker = new google.maps.Marker({position : point, icon : '/img/icons/gmarkers/normal.png'});
	google.maps.event.addListener(marker, 'click', 	function(e) 
    {
        if(infowindow)
            infowindow.close();
        var searchForm = getElem('searchForm');
        var params = "";
        if(searchForm)
            params = "&searchID=" + searchForm.search.value;//"&valute=" + searchForm.valute.value + "&view=" + searchForm.view.value;
        ajax("/ajax/", "function=getMapBuildBlocks&blocks=" + blocks + "&buildID=" + buildID + params,
                    function(result)
                    {
                        if (result['debug'])
                            alert(result['debug']);
                        if (result.success)
                        {
                            infowindow = new GMapWindowCLS({ content : result.text, map : bigMap, marker : gmapClusterBuildMarkers[buildID].Marker, iconNormal : 'normal' });
                            var form = getElem('formBuild_' + buildID);
                            if(form)
                                initCheckbox(form);
                        }
                    }
                );
    });
	google.maps.event.addListener(marker, 'mouseover', 	function(e) 
    {
        if(this.getIcon() != '/img/icons/gmarkers/clicked.png')
            this.setIcon('/img/icons/gmarkers/over.png');

        var smallList = getElem('smallListBuild_' + buildID);
        if(smallList)
            smallList.className += " over";
    });
	google.maps.event.addListener(marker, 'mouseout', 	function(e) 
    {
        if(this.getIcon() != '/img/icons/gmarkers/clicked.png')
            this.setIcon('/img/icons/gmarkers/normal.png');

        var smallList = getElem('smallListBuild_' + buildID);
        if(smallList)
            smallList.className = smallList.className.replace(/\sover/, "");
    });
	return marker;
}
		
var bigMap = null;
var map = null;
function bigMapResize()
{	
	var ne = null;
	var sw = null;
	if(!bigMap)
		return;
	var mapBound = bigMap.getBounds();
	if(mapBound)
	{
		ne = mapBound.getNorthEast();
		sw = mapBound.getSouthWest();
	}
	var zoom = bigMap.getZoom();
	
	var width = ($("#mapShow").is(':visible')) ? 0 : 180;
	var height = Math.max(640, getClientHeight());
	
	var left = ($("#mapShow").is(':visible')) ? -180 : 0;
	$("#map_new_container").width($(".search-results-container").width() - 270 - width);
	$("#map_new_container").height(height - 140);
	$(".search-objects").height(height - 140);
	$("#mapContainerBig").height(height - 140);
	$(".search-results-map").height(height - 130);
	$("#map_new_container").css({ left: left + 'px' });
		
	searchObjectsScrollApi.reinitialise();
	google.maps.event.trigger(bigMap, 'resize');
	if(ne && sw)
		bigMap.fitBounds(new google.maps.LatLngBounds(sw, ne));
	bigMap.setZoom(zoom);
}

function panoramaLoad(containerID, pano, location)
{
	if(!map)
		return false;
	
	var container = getElem(containerID);
	if( !container )
	{
		container = createElement('div', containerID);
		document.body.appendChild(container);
		container.innerHTML = "<div id='gmapPano'></div>";
	}
	var gmapPano = getElem('gmapPano');
	if(!gmapPano)
		return;
	
	container.style.display = "";
	gmapPano.style.display = "";
	
	var panoramaOptions = {};
	var panorama = new google.maps.StreetViewPanorama(gmapPano, panoramaOptions);
    panorama.setPano(pano);
    var setPov = function () {
        var panPos = panorama.getPosition();
        if (panPos) {
            panorama.setPov({
                heading: google.maps.geometry.spherical.computeHeading(panPos, location),
                pitch: 15
            });
        } else {
            setTimeout(setPov, 100);
        }
    };
    setPov();
	container.style.display = "none";
}



function panoramaInit(pos, outdoor)
{
	var sv = new google.maps.StreetViewService();

    sv.getPanorama(
        {
            location: pos,
            radius: 500,
            preference: /*outdoor? */google.maps.StreetViewPreference.BEST/* : google.maps.StreetViewPreference.NEAREST*/,
            source: outdoor ? google.maps.StreetViewSource.OUTDOOR : google.maps.StreetViewSource.DEFAULT
        },
        function (data, status)
        {
            if (status != google.maps.StreetViewStatus.OK) {
                $('.streetView').hide();
            } else {
                panoramaLoad('gmapPanorama', data.location.pano, pos);
            }
        }
    );
//	sv.getPanoramaByLocation(pos, 250, checkPanoramaAccess);
}

function showPanorama()
{
	var shadowContainer = getElem('panoramaContainerShadow');
	if(!shadowContainer)
	{
		shadowContainer = createElement('div', 'panoramaContainerShadow');
		document.body.appendChild(shadowContainer);
		shadowContainer.innerHTML = "&nbsp;";
		
		Event.add(shadowContainer, "click", 		
					function(e) 
					{ 
						hidePanorama();					
					});
	}
	shadowContainer.style.display = "block";
	/*****************/
	var container = getElem('gmapPanorama');
	if(container)
	{
		container.style.display = "block";
		updatePanoramaContainer();
	}
}

function updatePanoramaContainer()
{
	var shadowContainer = getElem('panoramaContainerShadow');
	var container = getElem('gmapPanorama');
	if ( (container.style.display != "") && (shadowContainer.style.display != "") )
	{
		shadowContainer.style.width = getClientWidth() + 'px';
		shadowContainer.style.height = getClientHeight() + 'px';
		
		container.style.left = (getClientWidth() - container.offsetWidth)/2 + 'px';
		container.style.top = (getClientHeight() - container.offsetHeight)/2 + 'px';
	}
}

function hidePanorama()
{
	var shadowContainer = getElem('panoramaContainerShadow');
	var container = getElem('gmapPanorama');
	container.style.display = shadowContainer.style.display = "none";
}
