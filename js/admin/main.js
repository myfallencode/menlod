$(function()
{
	initIntFields();	
	initFltFields();
    initFormTree();
    initMaskedInput();
    initButtonsSelect();

    $(".phoneCode").mask("(999) 999-9999");
    $('#fastJumpToForm input').keydown(function (e){
        if (event.keyCode == 13) {
            toEditForm();
        }
    });
    $('#is_filter').change(function () {
        var trackedBox = $('#tracked_forms').closest('.form-group'),
            filterBox = $('#filterForm').closest('.form-group');

        if ($(this).is(':checked')) {
            trackedBox.show();
            filterBox.hide();
        } else {
            trackedBox.hide();
            filterBox.show();
        }
    });

    var forms = $('.gallery').closest('form');
    for (var i = 0, count = forms.size(); i < count; i++) {
        updateEvents(forms[i]);
    }
}
);

function initMaskedInput($container) {
    var $maskedInput = $('.maskedInput', $container || $(document));

    if ($maskedInput.size() && (typeof $().mask == 'function'))
        $maskedInput.each(function () {
            var $this = $(this),
                mask = $this.data('mask') || '(999) 999-9999';

            if (mask)
                $this.mask(mask);
        });
}

function initButtonsSelect() {
    $('.buttonsSelect:not([data-manual="1"])').each(function () {
        new ButtonsSelect($(this));
    });
}
//************************************
//************************************
var initIntFields = function(form)
{
	$inputs = (form) ? $('input[type=text].intVal', form) : $('input[type=text].intVal');
	$inputs.keyup( function()
					{
						checkInt(this);
					});
};
//************************************
var initFltFields = function(form)
{
	$inputs = (form) ? $('input[type=text].fltVal', form) : $('input[type=text].fltVal');
	$inputs.keyup( function()
					{
						checkFloat(this);
					});
};

//************************************
function highlight(text, elem)
{
	if (elem && text.match(/[а-я\s0-9]+/gi))
		elem.innerHTML = elem.innerHTML.replace(new RegExp(text, "gi"), highlight_backend)
}

function highlight_backend(matches)
{
	return "<b style='font-size: 100%;'>" + matches + "</b>";
}
//***************************************************
//***************************************************
function show_elem(elem, isClose)
{
	if(elem)
	{
		isClose = (isClose) ? isClose : false;
		elem.style.display = ( (elem.style.display == "none") && !isClose) ? "block" : "none";
	}
}
//*****************************************************
//*****************************************************
function hide_video()
{
	getElem('videoContainerShadow').style.display = "none";
	var container = getElem('videoContainer');
	container.innerHTML = "";
	container.style.display = "none";
}

function show_video(link, autoplay)
{
	var shadowContainer = getElem('videoContainerShadow');
	autoplay = (autoplay) ? true : false;
	if(!shadowContainer)
	{
		shadowContainer = createElement('div', 'videoContainerShadow');
		document.body.appendChild(shadowContainer);
		shadowContainer.innerHTML = "&nbsp;";
		
		Event.add(shadowContainer, "click", 		
					function(e) 
					{ 
						hide_video();					
					});
	}
	shadowContainer.style.display = "block";
	/*************/
	var container = getElem('videoContainer');
	if(!container)
	{
		container = createElement('div', 'videoContainer');
		document.body.appendChild(container);
		container.className = "videoContainer";
	}
	container.innerHTML = "<div class='closeVideo' onclick='hide_video();'></div><div id='videoPlayer' class='player'></div>";
	container.style.display = "block";
	/************/
	if( (navigator.userAgent.indexOf("iPhone") != -1) || (navigator.userAgent.indexOf("iPad") != -1)  || (navigator.userAgent.indexOf("Android") != -1) )
	{
		this.player = new Uppod({"m" : "video", "uid": "videoPlayer", "file" : link, "auto" : "play"});
	}
	else
	{
		var flashvars = {"comment" : "test", "st": "/flash/video107-579.txt", "file" : link, "play" : true};
		var params = {bgcolor : "#ffffff",  allowFullScreen : "true", allowScriptAccess : "always", id: "videoPlayer"}; 
		new swfobject.embedSWF("/flash/uppod.swf", "videoPlayer", "500", "375", "9.0.115.0", false, flashvars, params);
	}
	
	update_video_container();
}

function update_video_container()
{
	var shadowContainer = getElem('videoContainerShadow');
	var container = getElem('videoContainer');
	if ( (container.style.display != "") && (shadowContainer.style.display != "") )
	{
		shadowContainer.style.width = getClientWidth() + 'px';
		shadowContainer.style.height = getClientHeight() + 'px';
		
		container.style.left = (getClientWidth() - container.offsetWidth)/2 + 'px';
		container.style.top = (getClientHeight() - container.offsetHeight)/2 + 'px';
	}
}
//*****************************
//*****************************
function loadAjaxSelect(selectName, params)
{
	if (!selectName || !params)
		return;
	var paramStr = "";
	for (param in params)
		paramStr += ( (paramStr) ? "&" : "" ) + param + "=" + params[param];
	ajax("/admin/ajax/", paramStr, 
				function(result)
				{
					var select;
					if (select = getElem(selectName))
					{
						select.options.length = 0;
						select[0] = new Option("-", "", false, true);
						if (result['debug'])
							alert(result['debug']);
						if (!result['success'])
							alert(result['errors']);
						else
						{
							select = fillAjaxSelect(select, result['items'], 0, true);	
						}
					}
					else
						alert("Ошибка! Не найден элемент " + selectName);
				}
			);
}
//*****************************
function fillAjaxSelect(select, items, selectedIndex, canBeEmpty)
{
	selectedIndex = (selectedIndex) ? selectedIndex : 0;
	canBeEmpty = (canBeEmpty) ? 1 : 0;
	select.options.length = 0;
	var cnt = 0;
	if (canBeEmpty)
		select[cnt++] = new Option("-", "", false, !(selectedIndex));
	for (var i = 0; i < items.length; i++, cnt++)
		select[cnt] = new Option(items[i][1], items[i][0], false, false);
	return select;
}
//*****************************
function getElem(id)
{
	return document.getElementById(id);
}
//*****************************
function inArray(val, arr)
{
	for (var i = 0; i < arr.length; i++)
		if (val == arr[i])
			return true;

	return false;
}
//*****************************************
function getElementObjPosition(elem)   
{   
   
	var w = elem.offsetWidth;   
	var h = elem.offsetHeight;   
	   
	var l = 0;   
	var t = 0;   
	   
	while (elem)   
	{   
		l += elem.offsetLeft;   
		t += elem.offsetTop; 
		if(getComputedStyle(elem, null).position == 'fixed')
		{
			t += $(window).scrollTop(); 
			l += $(window).scrollLeft(); 
			break;
		}
			
		elem = elem.offsetParent;   
	}   
  
	return {"left":l, "top":t, "width": w, "height":h};   
}

var isIE = checkBrowser();	
function checkBrowser() 
{
	if (navigator.appName.indexOf ("Microsoft") !=-1) 
	{
		return true //IE
	}	
	return false;
}

//******************************************
function visual(_targ, _show)
{
    if (!_show)
        _show = "auto";
    var target = getElem(_targ);
    if (target)
    {
        if (_show == "auto")
        {
            if (target.style.display == "none")
                target.style.display = "";
            else
                target.style.display = "none";
        }
        else
        {
            target.style.display = _show;
        }
    }
}
//****************************
//*********img preload********
//****************************
var shadowOpacity = 1;
var imgShadowFrames = 20;
var imgShadowAnimationTime = 500;
var deltaShadowWidth = 0;
var deltaShadowHeight = 0;
var oldShadowWidth = 0;
var oldShadowHeight = 0;
var scrollLeft = 0;
var scrollTop = 0;
function setImg(src, alt, event, img)
{
	src = (src) ? src : "";
	alt = (alt) ? alt : "";
	var shadow = getElem('static_window_shadow');
	var myScreen = getElem('screen');
	var window_container = getElem('static_window_container');
	var imgWin = getElem('img_win');
	var imgContainer = getElem('img_container');
	var imgShadow = getElem('img_shadow');
	
	scrollLeft = (isIE) ? document.documentElement.scrollLeft : ( (event) ? event.pageX - event.clientX : scrollLeft);
	scrollTop = (isIE) ? document.documentElement.scrollTop : ( (event) ? event.pageY - event.clientY : scrollLeft);
	
	var screenCenterY = detectIE6() ? scrollTop + ( (event) ? event.clientY : 0 ) : scrollTop + myScreen.offsetHeight/2;
	var screenCenterX = scrollLeft + myScreen.offsetWidth/2;
	
	imgShadow.style.display = "";
	imgContainer.style.zIndex = -1;
	shadowOpacity = 1;
	if (!isIE)
		imgShadow.style.opacity = shadowOpacity;
	else
		imgShadow.style.filter = "Alpha(opacity=" + shadowOpacity*100 + ")";
	//******BIG SHADOW*********************
	shadow.style.left = scrollLeft + 'px';
	shadow.style.top = scrollTop + 'px';
	shadow.style.width = myScreen.offsetWidth + 'px';
	shadow.style.height = myScreen.offsetHeight + 'px';
	//******STATIC WINDOW CONTAINER********
	window_container.style.left = scrollLeft + 'px';
	window_container.style.top = scrollTop + 'px';
	window_container.style.width = myScreen.offsetWidth + 'px';
	window_container.style.height = myScreen.offsetHeight + 'px';
	//******IMAGE WINDOW*******************
	imgWin.style.width = myScreen.offsetWidth + 'px';
	imgWin.style.height = myScreen.offsetHeight + 'px';
	
	var images = img.parentNode.getElementsByTagName('img');
	var imagesCnt = images.length;
	var imgPos = 0;
	for (var i = 0; i < imagesCnt; i++)
		if (images[i] == img)
		{
			imgPos = i + 1;
			break;
		}
	var imgControls = "none";
	if (imagesCnt > 1)
	{
		switch (imgPos)
		{
			case 1:
				imgControls = "right";
				break;
			case imagesCnt:
				imgControls = "left";
				break;
			default:
				imgControls = "both";
				break;
		}
	}
	imgContainer.innerHTML = "<img src='" + src +"' alt='" + alt + "' title='" + alt + "' onload='runOnImgLoad(\"" + src + "\", \"" + imgControls + "\", \"" + imgPos + "\", \"" + img.parentNode.id + "\");' class='imgPreload'/>";
	imgShadow.title = alt;
	imgContainer.title = alt;
}

function runOnImgLoad(src, imgControls, imgPos, parentID)
{
	var imgShadow = getElem('img_shadow');
	var imgContainer = getElem('img_container');
	deltaShadowWidth = (imgContainer.children[0].offsetWidth - imgShadow.offsetWidth)/20;
	deltaShadowHeight = (imgContainer.children[0].offsetHeight - imgShadow.offsetHeight)/20;
	oldShadowWidth = imgShadow.offsetWidth;
	oldShadowHeight = imgShadow.offsetHeight;
	imgContainer.style.backgroundImage = "url('" + src + "')";
	imgContainer.style.backgroundPosition = "center center";
	imgContainer.style.backgroundRepeat = "no-repeat";
	var text =  "<table style='width: 100%;'><tr><td colspan='3' style='text-align: right; height: 20px;'><img src='/img/icons/close.png' class='img_link' onclick='hide_img();' title='Закрыть' alt='Закрыть'/></td></tr><tr><td style='width: 20px;'>&nbsp;";
	if ( (imgControls == "both") || (imgControls == "left") )
		text += "<img src='/img/icons/prev.png' class='img_link' onclick='hide_img(); getElem(\"" + parentID + "\").children[\"" + (imgPos-2) + "\"].onclick();' title='Предыдущая' alt='Предыдущая'/>";
	text += "</td><td>&nbsp;</td><td style='width: 20px; text-align: right;'>";
	if ( (imgControls == "both") || (imgControls == "right") )
		text += "<img src='/img/icons/next.png' class='img_link' onclick='hide_img(); getElem(\"" + parentID + "\").children[\"" + imgPos + "\"].onclick();' title='Следующая' alt='Следующая'/>&nbsp;";
	text += "</td></tr></table>";
	imgContainer.innerHTML = text;
	imgShadow.innerHTML = "";
	setTimeout(hideShadow, imgShadowAnimationTime/imgShadowFrames);
}

function hideShadow()
{
	var imgShadow = getElem('img_shadow');
	var imgContainer = getElem('img_container');
	if (shadowOpacity > 0)
	{
		if (!isIE)
			imgShadow.style.opacity = shadowOpacity;
		else
			imgShadow.style.filter = "Alpha(opacity=" + shadowOpacity*100 + ")";
		
		var step = imgShadowFrames - Math.round(shadowOpacity*imgShadowFrames) + 1;
		
		imgShadow.style.width = (oldShadowWidth + Math.floor(deltaShadowWidth*step)) + 'px';
		imgShadow.style.height = (oldShadowHeight + Math.floor(deltaShadowHeight*step)) + 'px';
		
		imgContainer.style.left = imgShadow.offsetLeft + 'px';
		imgContainer.style.top = imgShadow.offsetTop + 'px';
		imgContainer.style.width = imgShadow.offsetWidth + 'px';
		imgContainer.style.height = imgShadow.offsetHeight + 'px';
		imgContainer.children[0].style.height = imgContainer.style.height;
		
		shadowOpacity -= 1/imgShadowFrames;
		setTimeout(hideShadow, imgShadowAnimationTime/imgShadowFrames);
	}
	else
	{
		imgShadow.style.display = 'none';
		imgContainer.style.zIndex = 9999;
	}
}

function hide_img()
{
    $('#static_window_shadow').css({
        left: '-10000px',
        top: '-10000px'
    });
    $('#static_window_container').css({
        left: '-10000px',
        top: '-10000px'
    });
    $('#img_container').css({
        left: '-10000px',
        top: '-10000px'
    });
}


function detectIE6(){
  var browser = navigator.appName;
  if (browser == "Microsoft Internet Explorer"){
    var b_version = navigator.appVersion;
    var re = /\MSIE\s+(\d\.\d\b)/;
    var res = b_version.match(re);
    if (res[1] <= 6){
      return true;
    }
  }
  return false;
}

function check_genForm()
{
	var name = getElem('name');
	var email = getElem('email');
	var body = getElem('body');

    return name.value && email.value && body.value;
}

function check_orderForm()
{
	var name = getElem('name');
	var email = getElem('email');
	var phone = getElem('phone');

    return name.value && (email.value || phone.value);
}

function getClientWidth()
{
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
}

function getClientHeight()
{
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}

function defScroll() 
{
      var x = y = 0;
      // Gecko поддерживает свойства scrollX(scrollY)
      // Для IE & Opera приходится идти в обход
      x = (window.scrollX) ? window.scrollX : document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft;
      y = (window.scrollY) ? window.scrollY : document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
      return {x : x, y : y};
}

//********************************
//********************************
//********************************
function calcBaseMAP(form)
{
	if(form)
	{
		//(bl.Cost + if(bl.ExplID=1, bl.ExplValue*eCbr.Value/cbr.Value, 0))*if(bl.TaxesID=2, 1.18, 1)
		var valute = [];
		valute[0] = {caption : '', value : 1};
		valute[1] = {caption : '$', value : form.USD.value};
		valute[2] = {caption : '€', value : form.EUR.value};
		valute[3] = {caption : 'руб', value : 1};
		
		var cost = form.Cost.value;
		var square = form.Square.value;
		var valuteID = (form.CostValuteID.value) ? form.CostValuteID.value : 0;
		var taxesID = $('input:radio[name=TaxesID]:checked', form).val();
		
		var explID = $('input:radio[name=ExplID]:checked', form).val();
		var explValue = form.ExplValue.value;
		var explValuteID = (form.ExplValuteID.value) ? form.ExplValuteID.value : 0;
		var explTaxesID = $('input:radio[name=ExplTaxesID]:checked', form).val();
		var nds = 1.18;
		
		var elemBMAP = getElem('CostMonth');
		var elemBCost = getElem('Cost');
		if(elemBMAP && elemBCost)
		{

			var expl = (explID == 1) ? explValue*valute[explValuteID].value/valute[valuteID].value*( (explTaxesID == 2) ? nds : 1 ) : 0;
			var roundCost = Math.round( (cost*( (taxesID == 2) ? nds : 1 ) + expl*1) );
			//alert(expl + ' | taxesID = ' + taxesID + ' | cost = ' + cost + ' | roundCost = ' + roundCost);
			
			elemBCost.innerHTML = roundCost + ' ' + valute[valuteID].caption + ' за м² в год';
			elemBMAP.value =  Math.round(roundCost*square/12) + ' ' + valute[valuteID].caption + ' в месяц';
		}

	}
}

function calcObjCost(form)
{
	if(form)
	{
		var cost = form.Cost;
		var square = form.Square;
		var valute = form.CostValuteID;
		var costTotal = form.CostTotal;
		if(costTotal && cost && square && valute)
		{			
			costTotal.value = Math.round(square.value*cost.value) + ' ' + ( (valute.selectedIndex) ? valute[valute.selectedIndex].text : "" );
		}
	}
}

function calcRentCommition(form, reverseOrder)
{
	if(form && form.Commition && form.CommitionYear)
	{
		if(reverseOrder)
			form.Commition.value = Math.round(form.CommitionYear.value*1200)/100;
		else
			form.CommitionYear.value = Math.round(form.Commition.value/0.12)/100;
	}
}


function dealTypeChangeAdvSearch(elem)
{
    $('#view1').parent().css('display', elem.value == 1 ? "" : "none");
    $('#view2').parent().css('display', elem.value == 1 ? "none" : "");
}

function objTypeChangeAdvSearch(elem)
{
	var show = hide = [];
	if(elem.value == 1)
	{
		show = [{ID : "office_type"}, {ID : "office_class"}, {ID : "furnish"}]; 
		hide = [{ID : "trade_type"}, {ID : "warehouse_class"}, {ID : "warehouse_type"}, {ID : "railway"}];
	}
	else if(elem.value == 2)
	{
		show = [{ID : "trade_type"}, {ID : "furnish"}]; 
		hide = [{ID : "office_type"}, {ID : "office_class"}, {ID : "warehouse_class"}, {ID : "warehouse_type"}, {ID : "railway"}];
	}
	else if(elem.value == 3)
	{
		show = [{ID : "warehouse_class"}, {ID : "warehouse_type"}, {ID : "railway"}]; 
		hide = [{ID : "office_type"}, {ID : "office_class"}, {ID : "trade_type"}, {ID : "furnish"}];
	}
	changeVis(show, hide);
}

function objTradeTypeChangeAdvSearch(elem)
{
	var show = hide = [];
	if(elem.value == 1)
	{
		show = [{ID : "tc_class"}]; 
		hide = [{ID : "retail_class"}];
	}
	else if(elem.value == 2)
	{
		show = [{ID : "retail_class"}]; 
		hide = [{ID : "tc_class"}];
	}
	changeVis(show, hide, elem);
}

function objTypeChange(elem)
{
	var show = hide = [];
	if(elem.value == 1)
	{
		show = [{ID : "OfficeTypeID", Change : addTitleNeeded}, {ID : "OfficeClassID", Change : addTitleNeeded}]; 
		hide = [{ID : "TradeTypeID", Change : objTradeTypeChange}, {ID : "RetailClassID"}, {ID : "TCClassID"}, {ID : "WHTypeID"}, {ID : "WHClassID"}];
	}
	else if(elem.value == 2)
	{
		show = [{ID : "TradeTypeID", Change : addTitleNeeded}]; 
		hide = [{ID : "OfficeTypeID"}, {ID : "OfficeClassID"}, {ID : "WHTypeID"}, {ID : "WHClassID"}];
	}
	else if(elem.value == 3)
	{
		show = [{ID : "WHTypeID", Change : addTitleNeeded}, {ID : "WHClassID", Change : addTitleNeeded}];
		hide = [{ID : "OfficeTypeID"}, {ID : "OfficeClassID"}, {ID : "TradeTypeID"}, {ID : "RetailClassID"}, {ID : "TCClassID"}];
	}
	changeVis(show, hide);
}

function objTradeTypeChange(elem)
{
	var show = hide = [];
	if(elem.value == 1)
	{
		show = [{ID : "TCClassID", Change : addTitleNeeded}]; 
		hide = [{ID : "RetailClassID"}];
	}
	else if(elem.value == 2)
	{
		show = [{ID : "RetailClassID", Change : addTitleNeeded}]; 
		hide = [{ID : "TCClassID"}];
	}
	changeVis(show, hide, elem);
}

function telecommunChange(elem, whMode)
{
	var show = hide = [];
	whMode = (whMode == 3) ? true : false;
	if(elem.checked)
	{
		if(whMode)
			show = [{ID : "Provider"}]; 
		else
			show = [{ID : "Provider", Change : addTitleNeeded}, {ID : "OwnProvider"}]; 
	}
	else
		hide = [{ID : "Provider"}, {ID : "OwnProvider"}];	
	changeVis(show, hide, elem);
}

function addTitleNeeded(elem)
{
	var tr = getParentTagName(elem, 'tr');
	if(tr)
	{
		var label = tr.children[0].children[0];
		if(label.children.length == 0)
		{
			var neededSpan = createElement('span');
			neededSpan.innerHTML = "*";
			neededSpan.className = "needed";
			label.appendChild(neededSpan);
		}
	}
}

function elevatorsChange(elem)
{
	var show = hide = [];
	if(elem.checked)
		show = [{ID : "ElevatorsCnt"}, {ID : "ElevatorsLabel"}]; 
	else
		hide = [{ID : "ElevatorsCnt"}, {ID : "ElevatorsLabel"}];	
	changeVis(show, hide, elem);
}


function changeVis(show, hide, elem)
{

	var visElem = $(elem).closest('.form-group')[0];
	if(visElem && (visElem.style.display == "none") )
		return;


	for (var i = 0; i < show.length; i++)
	{
		var element = show[i];
		var ok=false;
		if(typeof element.operation=='undefined') {
			element.operation="=="
		}


		if(typeof element.value!='undefined')
		{
			eval("if(element.value"+element.operation+"elem.value){ok=true;}");
		}
		else
		{
			ok	=	true;
		}

		if(ok && getElem(element.ID))
		{
            $('#' + element.ID).closest(element.ParentTag ? element.ParentTag : ".form-group")[0].style.display = "";
			if(element.Change)
				element.Change(getElem(element.ID));
		}
	}

	for (var i = 0; i < hide.length; i++)
	{
		var element = hide[i];
		var ok=false;
		if(typeof element.operation=='undefined') {
			element.operation="=="
		}
		if(typeof element.value != 'undefined')
		{
			eval("if(element.value"+element.operation+"elem.value){ok=true;}");
		}
		else
		{
			ok	=	true;
		}

		if(ok && getElem(element.ID))
			$('#' + element.ID).closest(element.ParentTag ? element.ParentTag : ".form-group")[0].style.display = "none";
	}

}

//*************************************
function getContacts(ownerID)
{
	ajax("/ajax/", "function=getContacts&id=" + ownerID, 
				function(result)
				{
					if (result['debug'])
						alert(result['debug']);
					if(result['data'])
					{
						getElem('ownersContacts').innerHTML = result['data'];
					}
				}
			);
}

function addOwnersContactsPhone(phonesContainer, contactUID)
{
	ajax("/ajax/", "function=getOwnersContactsPhones&num=" + (phonesContainer.children.length + 1) + "&contactUID=" + contactUID, 
				function(result)
				{
					if (result['debug'])
						alert(result['debug']);
					if(result['data'])
					{
						var div = createElement('div');
						div.innerHTML = result['data'];
						phonesContainer.appendChild(div);
					}
				}
			);
}

function addOwnersContactsPanel(contactsContainer, email)
{
	ajax("/ajax/", "function=getOwnersContactsPanel&num=" + (contactsContainer.children.length + 1) + "&email=" + ( (email) ? 1 : 0 ), 
				function(result)
				{
					if (result['debug'])
						alert(result['debug']);
					if(result['data'] && result['UID'])
					{
						var tr = createElement('tr');
						tr.innerHTML = result['data'];
						contactsContainer.appendChild(tr);
					}
				}
			);
}

function showClientCommentWin(btn)
{
	windows.sysWin.setTitle('Введите комментарий для выбранных клиентов');
	windows.sysWin.setContent('<form><textarea name="comment" class="agentClientComment"></textarea><br /><input type="button" class="button-v3" value="Добавить" onclick="saveClientComment(this.form);" /></form>');
	//windows.sysWin.setWidth(400);
	windows.sysWin.show(btn);
}

function saveClientComment(form)
{
	if(form.comment.value)
	{
		var parentForm = getElem('agentClientsForm');
		if(parentForm)
		{
			parentForm.comment.value = form.comment.value;
			parentForm.act.value = 'comment';
			parentForm.submit();
		}
	}
	else
		alert("Необходимо ввести текст комментария");
}

function getAction(actionID, clientID, btn, recordID)
{
	actionID = (actionID) ? actionID : 0;	
	clientID = (clientID) ? clientID : 0;	
	recordID = (recordID) ? recordID : false;
	var onceEnabled = false;
	
	var selectedBlocks = [];
	$(".search-results-table input[type='checkbox']:checked").each(function() 
    {
        selectedBlocks.push($(this).val());
    });
	if ( (actionID > 0) || (clientID > 0) || recordID )
	{		
		if (!recordID && !selectedBlocks.length)
		{
			alert('Ни один блок не отмечен');
			return;
		}
		/*title = (title) ? title : "Действие";
		windows.sysWin2.setTitle(title);*/
		windows.sysWin2.setWidth(320);
		windows.sysWin2.show(btn);
		windows.sysWin2.setContent("<div id='actionContainer'>Получение данных...</div>");
		if (!recordID)
			loadSiteForm(getElem('actionContainer'), "function=formLoader&type=action_form&act=add&Blocks=" + selectedBlocks.join(",") + "&ClientID=" + clientID + "&ActionID=" + actionID, "add");
		else
			loadSiteForm(getElem('actionContainer'), "function=formLoader&type=action_form&act=edit&id=" + recordID, "edit");
		//windows.sysWin.setCenter(true);
	}
	else
		alert('Не выбран клиент');
}

function updateAgentBlockActionsInfo(blocks, clientID)
{
	blocks = blocks.split(',');
	if(blocks.length)
		for (var i in blocks)
			updateActionsInfo(blocks[i], clientID);
}

function updateActionsInfo(blockID, clientID)
{
	ajax("/ajax/", "function=getAgentClientBlockActions&blockID=" + blockID + "&clientID=" + clientID, 
				function(result)
				{
					if (result['debug'])
						alert(result['debug']);
					if(result['success'] && result['data'])
						getElem('block_' + blockID).innerHTML = result['data'];
				}
			);
}

function getBodyScrollTop()
{
	return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}

function getBodyScrollLeft()
{
	return self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || (document.body && document.body.scrollLeft);
}

function setCookie (name, value, expires, path, domain, secure) 
{
    var now = new Date();
	if(expires)
		expires = new Date(now.getTime() + expires*1000);
	document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires.toUTCString() : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie(name) 
{
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return (setStr);
}
/**
 * Инициализирует скрипты для отображения дерева форм
 */
var initFormTree = function()
{
    var $home = $('#form_tree');
    if(!$home.length)
        return;

    //Дерево форм
    $('.group_title', $home).click(function()
    {
        $(this).parent().siblings('ul').toggle();
        $(this).toggleClass('active');
    });

    var activeGroup = window.location.hash.replace('#','');
    if (activeGroup) {
        var active = $('.form_tree span[name='+activeGroup+']');
        active.click();
        active.parents('.form_group').parent().find('>.branch .group_title').click();
    }

    var open = false;
    $('#treeRoot').click(function(){
        var actual = $('.form_group .branch>.group_title.active'),
            notActual = $('.form_group .branch>.group_title:not(.active)');
        if (open) {
            actual.click();
        } else {
            notActual. click();
        }
        open = !open;
    });

    //Показать корневые группы
    $('> ul', $home).show();

    /**
     * Рекурсивно раскрашивает зебру
     * @param $parent
     * @param startPos
     */
    var zebra = function($parent, startPos)
    {
        startPos = startPos || 0;
        $('> ul > li', $parent).each(function(num)
        {
            var $ths = $(this);
            if(num % 2 == startPos)
                $ths.addClass('zebra');
            // var $ul = $('> ul', $ths);
            // if($ul.length)
            zebra($ths, (num + startPos + 1) % 2 );
        });

    };

    zebra($home);
};


function addContactsPhone(phonesContainer, personID)
{
    var val = 0;
    var cnt = $(phonesContainer.parentNode).children().length;
    $(phonesContainer.parentNode).find('input[type=text]').each(function(indx, element){
        var id = $(element).attr('id');
        var expr = /(?:new_phone_)?(\d+)/;
        val = Math.max(val, id.match(expr)[1]);
    });

    ajax("/admin/ajax/", "function=getContactPanel&num="+cnt+"&phoneID=new_phone_" + (val + 1) + "&personID=" + personID,
        function(result)
        {
            if (result['debug'])
                alert(result['debug']);
            if (result['data'])
            {
                var div = createElement('div');
                $(div).addClass('oneOwnerPhone');
                div.innerHTML = result['data'];
                $(phonesContainer).before(div);
                $(".phoneCode", $(div)).mask("(999) 999-9999");
            }
        }
    );
}

function removeContactsPhone(phoneContainer, personID)
{
    if(confirm("Удалить этот контакт?"))
    {
        var $phoneContainer = $(phoneContainer),
            $divs = $phoneContainer.closest('.owners_contacts_phones');

        $phoneContainer.closest('.oneOwnerPhone').remove();

        renumDivList($divs[0]);

        if($divs.find('.oneOwnerPhone').size() == 0)
            addContactsPhone($divs[0], personID);
    }
}


function renumDivList(parentDiv, cntElemDeep)
{
    if (parentDiv)
    {
        cntElemDeep = (cntElemDeep) ? cntElemDeep : 1;
        var divs = parentDiv.children;
        for (var i = 0; i < divs.length; i++)
        {
            var cntElem = divs[i];
            for (var deep = 0; deep < cntElemDeep; deep++)
                if (cntElem)
                    cntElem = cntElem.children[0];
            cntElem.innerHTML = (i + 1) + ".";
        }
    }
}

function setInputsContactType(obj) {
    var $obj = $(obj),
        personID = $obj.data('personid'),
        contactID = $obj.data('contactid'),
        type = $obj.val().split('-').shift();

    $('.contactInput_' + contactID + '_' + personID).hide();
    $('.contactInput_' + contactID + '_' + personID + '_' + type).show();
}

function addContactsPanel($contactsContainer)
{
    ajax("", "function=getContactsPanel&num=" + ($contactsContainer.children().length + 1),
        function(result)
        {
            if (result.debug)
                alert(result.debug);
            if (result.data && result.UID)
            {
                var $tr = $('<tr>');
                $tr.html(result.data);
                $contactsContainer.append($tr);

                $(".phoneCode", $tr).mask("(999) 999-9999");
            }
        }
    );
}

function removeContactsPanel(removeBtn)
{
    if(confirm("Удалить это контактное лицо?"))
    {
        var tbody = removeBtn.parentNode.parentNode.parentNode.parentNode;
        tbody.removeChild(removeBtn.parentNode.parentNode.parentNode);

        renumDivList(tbody, 2);

        if(tbody.children.length == 0)
            addContactsPanel($(tbody));
    }
}