var checkTimeout = 0;
var animHint = false;
var animHintInterval = false;
var animHintTimeout = false;
var animationTime = 600;
var opacityStep = 10;
var maxOpacity = 90;
var curOpacity = 0;

function check_ajax(functionName, aParams, target)
{
	if (!functionName || !target)
		return;
	if (!target.value)
	{
		drawErrorStatus(target, false, "Неободимо заполнить это поле");
		return;
	}
	var str = paramsToString(aParams);
	str = (str) ? "&" + str : "";
	ajax("/ajax/", "function=" + functionName + str, 
			function(result)
			{
				if (result['debug'])
					alert(result['debug']);
				if (result['error'])
					alert(result['error']);
				drawErrorStatus(target, result['success'], result['resultText']);
			}
		);
}

function paramsToString(aParams)
{
	var str = "";
	for (var key in aParams)
		str += ( (str) ? "&" : "" ) + key + "=" + encodeURIComponent(aParams[key]);
	return str;
}

function save_form_ajax(form, functionName, btn)
{
	btn.disabled = true;
	var paramsStr = "";
	for (var i = 0; i < form.length; i++)
	{
		if ( (form[i].type != 'button') && (form[i].type != 'submit') )
		{
			paramsStr += "&" + form[i].name + "=" + form[i].value;
			if ($(form[i]).data('needed') && !form[i].value)
			{
				drawErrorStatus(form[i], false, "Неободимо заполнить это поле");
				btn.disabled = false;
				return false;
			}
		}
	}
	ajax(
		functionName,
		paramsStr,
		function(result)
		{
			if (result['error'])
				alert(result['error']);
			var div = "";
			if (form.previousSibling && form.previousSibling.className == "formResult")
				div = form.previousSibling;
			else
			{
				div = document.createElement('div');
				div.className = "formResult";
				form.parentNode.insertBefore(div, form);
			}
			div.innerHTML = result['resultText'];
			if (result['success'])
				form.parentNode.removeChild(form);
			btn.disabled = false;
		}
	);
}

function check_password_confirm(pwd_confirm, password)
{
	if (pwd_confirm.value)
	{
		if (pwd_confirm.value == password.value)
			drawErrorStatus(pwd_confirm, true, '');
		else
			drawErrorStatus(pwd_confirm, false, 'Пароль и его подтверждение не совпадают');
	}
	else
		drawErrorStatus(pwd_confirm, false, "Неободимо заполнить это поле");
}

function drawErrorStatus(parent, success, text)
{
    if (parent)
    {
        var container = $(parent).closest('.form-group');
        if (!success) {
            container.addClass('has-error');
        } else {
            container.removeClass('has-error');
        }
        if (text)
            drawErrorHint(parent, text);
        else
        if (parent.previousSibling && (parent.previousSibling.className == 'hintDiv'))
            parent.previousSibling.style.display = "none";
    }
}

function drawErrorHint(parent, text)
{
	if (animHint)
		animHint.style.display = "none";
	if ( parent.previousSibling && (parent.previousSibling.className == 'hintDiv') )
	{
		var hintDiv = parent.previousSibling;
		hintDiv.innerHTML = text;
	}
	else
	{
		var hintDiv = document.createElement('div');
		hintDiv.className = 'hintDiv';
		parent.parentNode.insertBefore(hintDiv, parent);		
	}	
	var pos = getElementPos(parent);
	if (isIE)
		hintDiv.style.filter = "alpha(opacity=0)";
	else
		hintDiv.style.opacity = 0;
	hintDiv.style.display = "";		
	hintDiv.innerHTML = text;		
	hintDiv.style.left = pos.left + 'px';
	hintDiv.style.top = pos.top - hintDiv.offsetHeight - 2 + 'px';	
	animHint = hintDiv;
	curOpacity = 0;
	if (animHintInterval)
		clearInterval(animHintInterval);
	if (animHintTimeout)
		clearTimeout(animHintTimeout);
	animHintInterval = setInterval(appearHint, animationTime/(maxOpacity/opacityStep));
}

function appearHint()
{
	if (animHint)
	{
		animHint.style.display = "";
		curOpacity += opacityStep;
		if (isIE)
			animHint.style.filter = "alpha(opacity=" + curOpacity + ")";
		else
			animHint.style.opacity = curOpacity/100;
		if (curOpacity >= maxOpacity)
		{
			clearInterval(animHintInterval);
			animHintInterval = 0;
			animHintTimeout = setTimeout(disappearHint, 1500);
		}		
	}
}

function disappearHint()
{
	if (animHint)
	{
		if (isIE)
			animHint.style.filter = "alpha(opacity=0)";
		else
			animHint.style.opacity = 0;
		animHintTimeout = 0;
		animHint.style.display = "none";
	}
}

function drawHint(parent, text)
{
	if ( parent.previousSibling && (parent.previousSibling.className == 'hintDiv') )
	{
		var hintDiv = parent.previousSibling;
		hintDivClose = hintDiv.children[0];
		hintDivText = hintDiv.children[1];
	}
	else
	{
		var hintDiv = document.createElement('div');
		var hintDivClose = document.createElement('div');
		hintDivClose.className = "hintDivClose";
		Event.add(hintDivClose, "click", function(e) { hintDiv.style.display = "none";	 });
		Event.add(hintDivClose, "mouseover", function(e) { this.className = "hintDivCloseOver" });
		Event.add(hintDivClose, "mouseout", function(e) { this.className = "hintDivClose" });
		hintDiv.appendChild(hintDivClose);
		var hintDivText = document.createElement('div');
		hintDiv.appendChild(hintDivText);
		hintDiv.className = 'hintDiv';
		parent.parentNode.insertBefore(hintDiv, parent);		
	}
	fakeHint.innerHTML = text;
	var pos = getElementPos(parent);
	hintDiv.style.display = "";	
	hintDivText.style.width = fakeHint.offsetWidth + 'px';
	hintDivText.innerHTML = text;
	hintDiv.style.left = pos.left + 'px';
	hintDiv.style.top = pos.top - hintDiv.offsetHeight - 2 + 'px';	
	hintDivClose.style.left = (hintDiv.offsetWidth - 15) + 'px';
}

function getElementPos(elem)   
{   
	if (!elem)
		return false;   
	   
	var w = elem.offsetWidth;   
	var h = elem.offsetHeight;   
	   
	var l = 0;   
	var t = 0;   
	var positionStyle = "";
	
	while (elem)   
	{   
		l += elem.offsetLeft;   
		t += elem.offsetTop;   
		elem = elem.offsetParent;
		if (!elem)
			break;
		positionStyle = (isIE) ? elem.currentStyle['position'] : getComputedStyle(elem, null).position;
		if (positionStyle == "relative")
			break;
	} 
	return {"left":l, "top":t, "width": w, "height":h};   
}

function checkFloat(input, precision)
{
	input.value = input.value.replace(/[^\.\d]/g, '');
	var pointsArray = input.value.split(".");
	if (pointsArray.length > 2)
		input.value = pointsArray[0] + "." + pointsArray[1];
	if (precision && !input.value.match(/\.$/))
		input.value = round(input.value, precision);
}

function round(value, precision)
{
	precision = (precision) ? precision : 0;
	value = (value) ? value : 0;
	if (value)
		value = Math.round(value*Math.pow(10, precision))/Math.pow(10, precision);	
	return value;
}

