var inputs_cnt = 0;
var selectedInputContainer;
var input2move = "";
var inputsTabOpen = false;
//*****************************************
//*****************************************
function loadForm(container, params, act, form, funct)
{
	if (form)
	{
        for (var i = 0; i < form.length; i++)
        {
            if (((form[i].type != 'button') && (form[i].type != 'submit') && (form[i].type != 'checkbox') && (form[i].type != 'radio')) ||
                ((form[i].type == 'checkbox') && form[i].checked) ||
                ((form[i].type == 'radio') && form[i].checked))
            {
                params += "&" + form[i].name + "=" + encodeURIComponent(form[i].value);
            }
        }
	}

	if ( container && (act != "delete") )
		container.innerHTML = '<div class="progress progress-striped active"><div style="width: 100%;" class="progress-bar progress-bar-primary"></div></div>';



	ajax('', params,
				function(result)
				{
					if (result['debug'])
						alert(result['debug']);

					act = result['act'];

					if (container)
					{
						if (result['selOnEmpty'])
							container.innerHTML = result['resultText'];
						else
						{
							if (act == "delete")
							{								
								if (result['success']) 
									container.parentNode.removeChild(container);
							}
							else
							{								
								var form = createElement('form', act);
								var resultArr = result['resultText'].split(/\<\/?script.*?\>/); //выбираем что находится в теге <script> и сразу выполняем
								var textArr = [];
								var evalArr = [];

                                form.className = 'form-horizontal';

								for (var i = 0; i < resultArr.length; i++)
								{
									if (i % 2)
										evalArr.push(varsToGlobal(resultArr[i]));
									else
										textArr.push(resultArr[i]);
								}
								form.innerHTML = textArr.join('');
								container.innerHTML = "";//"<form name='" + act + "' id='" + act + "'>" + textArr.join('') + "</form>";

								container.appendChild(form);
								for (var i = 0; i < evalArr.length; i++)
									eval(evalArr[i]);
                                $('.form-checkbox', container).niftyCheck();
                                $('.form-radio', container).niftyCheck();
                                updateEvents(form);
							}
						}
					}
					if (result['success'] && result['onAjaxSubmit'])
						eval(result['onAjaxSubmit']);
                    if (funct)
                        funct();
                    $(".phoneCode").mask("(999) 999-9999");


                    $(container).find('.hashparams').each(function(){

                        var hash = $(this).data('hash');
                        hash = hash.split('=');
                        if(typeof hash[0] != 'undefined' && hash[1] != 'undefined')
                        {
                            if(Hash.get(hash[0])==hash[1])
                            {
                                $(this.click())
                            }
                        }
                        $(this).click(function(){
                            Hash.set(hash[0],hash[1]);
                        });
                    })
				}
			);
}
function getFormVars(form) {

    var dataType = Array.isArray(form) ? 'array' : (typeof form);
    switch(dataType)
    {
        case 'string':
            form = $('form[name="'+form+'"]')[0];
            break;
    }


    var formData = new FormData(form);
    var formKeys    = formData.keys();
    var formEntries = formData.entries();
    var formVars    =   {};
    do {
        var params = formEntries.next().value;
        if(typeof params != 'undefined')
        {
            var matches = params[0].match(/^([^\[]*)\[([^\]]*)\]/);
            if(matches === null)
            {
                formVars[params[0]] =   $.trim(params[1]);
            }
            else
            {
                if(typeof formVars[matches[1]] == 'undefined')
                    formVars[matches[1]]    =   [];

                formVars[matches[1]][formVars[matches[1]].length]    =   params[1];
            }
        }
    } while (!formKeys.next().done);

    return formVars;
}

//*****************************************
function loadSiteForm(container, params, act, form, alternLoadCont)
{
	var paramAct = act;
	if (form)
	{
		for (var i = 0; i < form.length; i++)
		{
			if ( ( (form[i].type != 'button') && (form[i].type != 'submit') && (form[i].type != 'checkbox')  && (form[i].type != 'radio') ) ||
				 ( (form[i].type == 'checkbox') && form[i].checked ) || ( (form[i].type == 'radio') && form[i].checked ) )
			{
				params += "&" + form[i].name + "=" + encodeURIComponent(form[i].value);
			}
		}
	}
	
	if ( container && (act != "delete") )
	{
		container.style.display = "none";
		if(!alternLoadCont)
		{
			var loadingContainer = createElement('div');
			loadingContainer.innerHTML = "<img src='/img/loading.gif' />";
			container.parentNode.appendChild(loadingContainer);
		}
		else
			alternLoadCont.innerHTML = "<img src='/img/loading.gif' />";
	}
	ajax("/ajax/", params, 
				function(result)
				{
					if (result['debug'])
						alert(result['debug']);
					act = result['act'];
					if (container)
					{
						if (result['selOnEmpty'])
							container.parentNode.parentNode.parentNode.innerHTML = result['resultText'];
						else
						{
							if (act == "delete")
							{								
								if (result['success']) 
									container.parentNode.removeChild(container);
							}
							else 
							{
								if(!alternLoadCont)
									loadingContainer.parentNode.removeChild(loadingContainer);
								else
									alternLoadCont.innerHTML = "";

								if(paramAct != "edit" || !result['success'])
								{
									var resultArr = result['resultText'].split(/\<\/?script.*?\>/); //выбираем что находится в теге <script> и сразу выполняем
									var textArr = [],
                                        evalArr = [];

									for (var i = 0; i < resultArr.length; i++)
									{
										if (i % 2)
											evalArr.push(varsToGlobal(resultArr[i]));
										else
											textArr.push(resultArr[i]);
									}
									
									var form = createElement('form', act);
                                    $(form).attr('class', 'form-horizontal');
									form.innerHTML = textArr.join('');
									container.innerHTML = "";//"<form name='" + act + "' id='" + act + "'>" + textArr.join('') + "</form>";
									
									container.appendChild(form);
									container.style.display = "";

									initSelects(form);
									initRadio(form);
									initCheckbox(form);									
									initCheckbox(form);
									
									initIntFields(form);	
									initFltFields(form);

									for (var i = 0; i < evalArr.length; i++)
										eval(evalArr[i]);
								}
								container.style.display = "";

                                if ('niftyCheck' in $()) {
                                    $('.form-checkbox', container).niftyCheck();
                                    $('.form-radio', container).niftyCheck();
                                }
                                updateEvents(form);
							}
						}
					}
					if (result['success'] && result['onAjaxSubmit'])
						eval(result['onAjaxSubmit']);
                    $(".phoneCode").mask("(999) 999-9999");
				}
			);
}
//*****************************************
function varsToGlobal(str)
{
	return str.replace(/var\s+/g, "window.");
}
//*****************************************
var saveFormTimeout = 0;

function saveForm(form)
{
	if (form.form_name.value)
	{
		var paramsStr = "";
		for (var i = 0; i < form.length; i++) {
			if ( ( (form[i].type != 'button') && (form[i].type != 'submit') && (form[i].type != 'checkbox') && (form[i].type != 'radio') ) ||
				 ( (form[i].type == 'checkbox') && form[i].checked ) ||
				 ( (form[i].type == 'radio') && form[i].checked ))
			{
				paramsStr += "&" + form[i].name + "=" + encodeURIComponent(form[i].value);
			}
		}

		ajax(
		    'saveForm',
            paramsStr,
            function(result) {
                if (result['formid'])
                    form.formid.value = result['formid'];

                if (!result.success) {
                    alert(result['resultText']);
                } else {
                    clearTimeout(saveFormTimeout);
                    alert(result.resultText);
                    saveFormTimeout = setTimeout(hideSaveFormMsg, 5000);
                }
            }
        );
	}
}

//*****************************************
function hideSaveFormMsg()
{
	getElem('formSaveResult').innerHTML = "";
}
//*****************************************
function switchDays(form, dateName)
{
	var daysList, monthList, yearList, selected;
	if ( (daysList = form[dateName + '_D']) && (monthList = form[dateName + '_M']) && (yearList = form[dateName + '_Y']) )
	{
		var daysCnt = day_in_month(monthList.value - 1, yearList.value);
		var selectedDay = (daysList.value) ? daysList.value : 1;
		selectedDay = Math.min(selectedDay, daysCnt);
		daysList.options.length = 0;
		for (var i = 1; i <= daysCnt; i++)
		{
			selected = (i == selectedDay);
			daysList[i] = new Option(i, i, false, selected);
		}
	}
}
//*****************************************
function day_in_month(month, year)
{
	if (month < 0)
	{
		month = 12 + month;
		year--;
	}
	var myDate = new Date();
	myDate.setFullYear(year, month + 1, 0);
	return myDate.getDate();
}
//***********************************************************************************************
//***********************************************************************************************
//********************FORMS EDITING**************************************************************
//***********************************************************************************************
function add_input(type, name, templateBlock, updateSelect)
{
    type = (type) ? type : "";
    name = (name) ? name : "";
    templateBlock = (templateBlock) ? templateBlock : 0;
    updateSelect = (updateSelect) ? updateSelect : true;
    var container = getElem('inputs_container');
    var propertiesContainer = getElem('inputs_properties_container');
    propertiesContainer.style.display = "";
    if (!container)
        return;
    var inputs_container = createElement('div', 'inputs_container_' + inputs_cnt);
    inputs_container.className = 'inputs_container well well-sm';
    Event.add(inputs_container, "mouseover",
        function(e)
        {
            if (input2move)
            {
                this.style.backgroundColor = '#775757';
                this.style.cursor = 'pointer';
                this.style.cursor = 'hand';
            }
        });
    Event.add(inputs_container, "mouseout",
        function(e)
        {
            if (input2move)
            {
                this.style.backgroundColor = '';
                this.style.cursor = 'default';
            }
        });
    Event.add(inputs_container, "click",
        function(e)
        {
            if (input2move)
            {
                if (input2move != this.id)
                {
                    var parent;
                    if (getElem(input2move) && (parent = getElem(input2move).parentNode))
                    {
                        parent.insertBefore(getElem(input2move), this);
                        setInputTypesPos(getElem(input2move));
                    }
                    input2move = "";
                    this.style.backgroundColor = '';
                    this.style.cursor = 'default';
                }
            }
        });
    //******input label*********************
    var label = createElement('div');
    label.className = "input_label label";
    label.appendChild(document.createTextNode((name) ? name + ' ' : "Элемент " + (inputs_cnt + 1) + ' '));
    var img_drop = createElement('span');
    img_drop.className = 'img_link fa fa-trash-o fa-lg';
    img_drop.title = 'Удалить элемент';
    Event.add(img_drop, "click",
        function(e)
        {
            if (confirm('Действительно удалить элемент?'))
            {
                var container = this.parentNode.parentNode;
                var isThisSelected = (selectedInputContainer == container);
                var parentContainer = container.parentNode;
                parentContainer.removeChild(container);
                if (isThisSelected && parentContainer.children.length)
                    selectedInputContainer = parentContainer.children[0];
                if (parentContainer.children.length)
                    select_row(selectedInputContainer);
                else
                    getElem('inputs_properties_container').style.display = "none";
            }
        });
    var img_move = createElement('span');
    img_move.className = 'img_link fa fa-arrows fa-lg';
    img_move.title = 'Переместить элемент';
    Event.add(img_move, "click",
        function(e)
        {
            if (input2move == this.parentNode.parentNode.id)
                input2move = "";
            else
            if (confirm('Укажите элемент, перед которым разместится выбранный'))
            {
                input2move = this.parentNode.parentNode.id;
            }
        });
    label.appendChild(img_drop);
    label.appendChild(img_move);
    inputs_container.appendChild(label);
    Event.add(label, "click", function(e) {
        select_row(this.parentNode);
    });
    //******input block*********************
    var div = createElement('div', 'input_table_' + inputs_cnt);
    //******input types*********************
    var input_types_list = getElem('input_types');
    var input_types = createElement('select', 'Type_' + inputs_cnt, 'Type[val_' + inputs_cnt + ']');
    copySelectOption(input_types, input_types_list);
    if (type)
        input_types.selectedIndex = getIndexByValue(input_types, type);
    Event.add(input_types, "change", function(e) {
        loadSelect("function=load_inputs_properties&type=" + this.value, getElem('inputs_properties'), setDblClickAddInputParam);
    });
    input_types.className = "select form-control";
    insertRow(div, {
        label: document.createTextNode("Type:"),
        content: input_types
    });
    //******input name*********************
    var input_name = createElement('input', 'Name_' + inputs_cnt, 'Name[val_' + inputs_cnt + ']');
    input_name.type = "text";
    input_name.className = "text short form-control";
    if (name)
        input_name.value = name;
    Event.add(input_name, "blur", function(e) {
        check_double_value(this.form, this, 'Name');
    });
    Event.add(input_name, "keyup",
        function(e)
        {
            var label = selectedInputContainer.children[0];
            if (this.value)
                label.childNodes[0].data = this.value + " ";
            else
                label.childNodes[0].data = "Элемент " + (new Number(selectedInputContainer.id.match(/\d+/)) + 1) + " ";
        });
    insertRow(div, {
        label: document.createTextNode("Name: "),
        content: input_name
    });
    //******input template*********************
    var input_templates_list = getElem('form_template');
    var input_template_blocks_list = getElem('input_template_blocks');
    var input_template = createElement('select', 'Template_' + inputs_cnt, 'TemplateBlock[val_' + inputs_cnt + ']');
    copySelectOption(input_template, input_template_blocks_list);
    input_template.selectedIndex = getIndexByValue(input_template, templateBlock);
    input_template.className = "select form-control";
    insertRow(div, {
        label: document.createTextNode("Template: "),
        content: input_template
    });

    div.lastChild.style.display = (input_templates_list.value > 0) ? "" : "none";
    //****************************************
    inputs_container.appendChild(div);
    container.appendChild(inputs_container);
    selectedInputContainer = inputs_container;
    select_row(selectedInputContainer, updateSelect);
    if (!name) //зря не будем прокручивать - только при ручном добавлении, а то опера глючит что-то
        div.scrollIntoView(true);
    inputs_cnt++;
}
setDblClickAddInputParam = function () {
    $('option', $(getElem('inputs_properties'))).dblclick(function(){
        insertInput(getElem("inputs_properties").value, getElem("inputs_properties")[getElem("inputs_properties").selectedIndex].text, selectedInputContainer);
    });
};
//*****************************************
function get_template_blocks(templateID)
{
	var input_template_blocks_list = getElem('input_template_blocks');
	if (!input_template_blocks_list)
		return;
	if (!templateID)
		update_template_blocks();
	else
		ajax("/admin/ajax/", "function=load_template_blocks&template_id=" + templateID, 
			function(result)
			{
				if (result['debug'])					
					alert(result['debug']);
				if (result['success'])
				{
					input_template_blocks_list.options.length = 0;
					var list = result['list'];
					input_template_blocks_list[0] = new Option("-", "", false, true);
					for (var i = 0; i < list.length; i++)
						input_template_blocks_list[i + 1] = new Option(list[i].text, list[i].value, false, false);
					input_template_blocks_list.selectedIndex = 0;					
					update_template_blocks();
				}
			}
		);
}
//*****************************************
function update_template_blocks()
{
	var input_template_blocks_list = getElem('input_template_blocks');
	var elements = getElem('new_form');
	var input_templateID = getElem('form_template').value;
	for (var i = 0; i < elements.length; i++)
	{
		if (elements[i].id.match(/Template_(\d+)/))
		{
			elements[i].options.length = 0;
			copySelectOption(elements[i], input_template_blocks_list);
			elements[i].parentNode.parentNode.style.display = (input_templateID > 0) ? "" : "none";
		}
	}		
}
//*****************************************
function select_row(input_container, updateSelect)
{
    updateSelect = (updateSelect) ? updateSelect : 1;
    if (!input_container)
        return;
    var containers = input_container.parentNode.children;
    var cnt = input_container.id.match(/\d+/);
    var selected = "";
    enableElements(cnt);
    selectedInputContainer = input_container;
    if (updateSelect > 0)
        loadSelect("function=load_inputs_properties&type=" + getElem('Type_' + cnt).value, getElem('inputs_properties'), setDblClickAddInputParam);
    for (var i = 0; i < containers.length; i++)
    {
        if (containers[i] == input_container)  {
            $(containers[i]).addClass('selected');
        } else {
            $(containers[i]).removeClass('selected');
        }
        //containers[i].children[0].className = "input_label" + selected;//???
    }
}
//*****************************************
function showSelectedContainer_propertyList()
{
    if(!selectedInputContainer)
        return;
    var cnt = selectedInputContainer.id.match(/\d+/);
    select_row(selectedInputContainer);
    loadSelect(
        "function=load_inputs_properties&type=" + getElem('Type_' + cnt).value,
        getElem('inputs_properties'),
        setDblClickAddInputParam
    );
    selectedInputContainer.scrollIntoView(true);
    inputsTabOpen = true;
    var container = $('#inputs_container').children()[0];
    select_row(container);
    /*setInputTypesPos(container);*/
}
//*****************************************
function enableElements(id) // передаем тот id, который нужно сделать доступным
{
    var elements = getElem('new_form');
    var elemID;
    for (var i = 0; i < elements.length; i++)
    {
        if (elemID = elements[i].name.match(/\[val_(\d+)\]/))
            elements[i].disabled = (elemID[1] != id);
    }
}
//*****************************************
function check_double_value(form, elem, name, inputType)
{
    inputType = (inputType) ? inputType : 'input';
    if (elem.value)
    {
        var pattern = new RegExp(name + '\\[val_\\d+\\]');
        var elements = form.getElementsByTagName(inputType);
        for (var i = 0; i < elements.length; i++)
        {
            if (elements[i].name.match(pattern))
                if ((elements[i].name != elem.name) && (elements[i].value == elem.value))
                    drawHint(elem, "Такое значение уже существет!");
        }
    }
}
//*****************************************
function insertRow(container, row)
{
    if (container && row)
    {
        var label = $('<label class="col-sm-3 control-label"></label>'),
            content = $('<div class="col-sm-9"></div>'),
            div = $('<div class="col-sm-12"></div>'),
            groupDiv = $('<div class="form-group"></div>');
        if (row.label) {
            label.append(row.label);
            if (row.content.name) {
                label.attr('for', row.content.name);
            }
        }
        if (row.content) content.append(row.content);
        div.append(label);
        div.append(content);
        groupDiv.append(div);
        $(container).append(groupDiv);
    }
}
//*****************************************
function createElement(type, id, name, alt)
{
    var element;
    /*if (isIE)
     element = document.createElement("<" + type + ( (id) ? " id='" + id + "'" : "") + ( (name) ? " name='" + name + "'" : "") + ">");
     else*/
    {
        element = document.createElement(type);
        if (id)
            element.id = id;
        if (name)
            element.name = name;
        if (alt)
            element.alt = alt;
    }
    return element;
}
//*****************************************
function copySelectOption(newSelect, parentSelect)
{
	if (parentSelect.length)
	{
		for (var i = 0;  i < parentSelect.length; i++)
			newSelect[i] = new Option(parentSelect[i].text, parentSelect[i].value, false, parentSelect[i].selected);
	}
}
//*****************************************
function loadSelect(str, select, func)
{
    if (!str || !select)
        return;
    ajax("/admin/ajax/", str,
        function(result)
        {
            if (result['debug'])
                alert(result['debug']);
            if (result['success'])
            {
                select.multiple = true;
                select.options.length = 0;
                var list = result['list'];
                for (var i = 0; i < list.length; i++)
                    select[i] = new Option(list[i].text, list[i].value, false, false);
                select.selectedIndex = 0;
                select.size = select.length;
                setInputTypesPos(selectedInputContainer);
                if (func) {
                    func();
                }
            }
        }
    );
}
//***************************************
function insertInput(propertyID, text, inputContainer, value)
{
    if (!propertyID || !inputContainer)
        return;
    var cnt = inputContainer.id.match(/\d+/);
    if (getElem(text + '_' + cnt))
        return;
    var tbody = inputContainer.children[1];
    ajax("/admin/ajax/", "function=load_input_property_info&property_id=" + propertyID,
        function(result)
        {
            if (result['debug'])
                alert(result['debug']);
            if (result['success'])
            {
                var list = result['list'];
                var input_type;
                var type, formControl = '';
                if ((list.HTMLtype == 'textarea') || (list.HTMLtype == 'select'))
                {
                    input_type = list.HTMLtype;
                    type = '';
                    formControl = ' form-control';
                }
                else
                {
                    input_type = 'input';
                    type = list.HTMLtype;
                    if (type != 'checkbox')
                        formControl = ' form-control';
                }
                var input = createElement(input_type, list.Name + '_' + cnt, list.Name + '[val_' + cnt + ']', propertyID);
                input.className = list.ClassName + formControl;
                if (type)
                    input.type = type;
                input.value = (value) ? value : '';
                insertRow(tbody, {
                    label: document.createTextNode(list.Name + ":"),
                    content: input
                });
                if (list.HTMLtype == "checkbox")
                {
                    input.value = 1;
                    input.checked = 1;//(value) ? 1 : "";
                }
                setInputTypesPos(inputContainer);
            }
        }
    );
}
//***************************************
function deleteInput(property)
{
	if (!property || !selectedInputContainer)
		return;
	var cnt = selectedInputContainer.id.match(/\d+/);
	var input;
	if ( !(input = getElem(property[property.selectedIndex].text + '_' + cnt)) )
		return;
	var tr = input.parentNode.parentNode;
	tr.parentNode.removeChild(tr);
}
//***************************************
function setInputTypesPos(input_container)
{
    var inputTypesTable = getElem('inputs_properties_container');
    var pos = getElementPos(input_container);
    var center = input_container.offsetTop + input_container.offsetHeight / 2;
    var top = center - inputTypesTable.offsetHeight / 2;
    if (center - inputTypesTable.offsetHeight / 2 < 0)
        top = 3;
    else
    {
        if (center + inputTypesTable.offsetHeight / 2 > input_container.parentNode.offsetHeight)
            top = input_container.parentNode.offsetHeight - inputTypesTable.offsetHeight - 3;
    }
    inputTypesTable.style.top = top + 'px';
}
//***************************************
function getIndexByValue(list, value)
{
	value = (value) ? value : 0;
	for (var i = 0; i < list.length; i++)
		if (list[i].value == value)
			return i;
	return 0;
}
//***************************************
function show_checked(formID, img)
{
    var form,
        $parent = $(img).closest('.controlButtons');
    if (form = getElem(formID))
    {
        for (var i = 0; i < form.length; i++)
            if (form[i].type == "checkbox") {
                var label = $(form[i]).closest('label.form-checkbox')[0];
                label.style.display = (label.style.display) ? "" : "none";
            }
        if ($(img).hasClass('delmulty')) {
            $(img).removeClass('delmulty');
            $(img).addClass('delmulty_cancle');
            $(img).attr('title', 'Отменить');
            $parent.find('.delmulty:hidden').show();
        } else if ($(img).hasClass('delmulty_cancle')) {
            $parent.find('.delmulty').hide();
            $(img).removeClass('delmulty_cancle');
            $(img).addClass('delmulty');
            $(img).attr('title', 'Отметить для удаления');
        }
        /*img.className = (img.className == "delmulty") ? "delmulty_cancle" : "delmulty";
         img.title = (img.className == "delmulty") ? "Отметить для удаления" : "Отменить";
         img.nextSibling.style.display = (img.nextSibling.style.display) ? "" : "none";
         img.nextSibling.nextSibling.style.display = (img.nextSibling.nextSibling.style.display) ? "" : "none";*/
    }
}
//***************************************
function check_all(formID, checked)
{
	var form;
	if (form = getElem(formID))
	{
		for (var i = 0; i < form.length; i++)
			if(form[i].type == "checkbox")
				form[i].checked = checked;
	}
}
function multyInsertInput(){
    var propertyID = $('#inputs_properties').val();
    if (!Array.isArray(propertyID)){
        propertyID = [propertyID];
    }
    for (var i = 0, count = propertyID.length; i<count; i++) {
        insertInput(propertyID[i], $('#inputs_properties option[value=' + propertyID[i] + ']').html(), selectedInputContainer);
    }
}
//****************************************
function init_form_key_events()
{	
	var textareaClipboard = null;
	Event.add(document.body, "keydown", 
			function(e) 
			{
				if( e.ctrlKey && (e.keyCode == 'S'.charCodeAt(0)) )
				{
					if(getElem('new_form'))
					{
						if(codeMirrorActiveInstance)
							codeMirrorActiveInstance.save();
						saveForm(getElem('new_form'));
					}
					return false;
				}
				else if( e.ctrlKey && (e.keyCode == 'V'.charCodeAt(0)) )
				{
					//alert(inputsTabOpen)
					if( !inputsTabOpen || (document.activeElement.nodeName.toLowerCase() == "textarea") || 
						( (document.activeElement.nodeName.toLowerCase() == "input") && (document.activeElement.type == "text") ) )
						return;
					textareaClipboard = createElement('textarea');
					textareaClipboard.className = "hiddenCtrlV";
					document.body.appendChild(textareaClipboard);
					textareaClipboard.focus();
				}
			});
	
	Event.add(document.body, "keyup", 
			function(e) 
			{
				if( e.ctrlKey && (e.keyCode == 'V'.charCodeAt(0)) )
				{
					if( !inputsTabOpen || 
						( (document.activeElement.nodeName.toLowerCase() == "textarea") && (document.activeElement.className != "hiddenCtrlV") ) || 
						( (document.activeElement.nodeName.toLowerCase() == "input") && (document.activeElement.type == "text") ) )
						return;
					
					if(textareaClipboard)
					{
						if( textareaClipboard.value && textareaClipboard.value.match(/#coppied_by_rengine#/) );
						{
							var evalText = textareaClipboard.value.replace(/#coppied_by_rengine#/, '');
							eval(evalText);
						}
						document.body.removeChild(textareaClipboard);
					}
				}
			});
}
//**************************************
function addslashes(str)
{
	return str.replace(/\\?("|')/g, '\\$1');
}
//**************************************
var clip = null;

//**************************************
function prepareInputCopy(container)
{
    var str = "";
    if(container && container.id)
    {
        var num = container.id.match(/\d+$/);
        var $inputs = $("[name$='[val_" + num[0] + "]']:visible", container);
        var inputName = getElem('Name_' + num[0]).value;
        var inputType = getElem('Type_' + num[0]).value;
        str = "#coppied_by_rengine#";
        str += "add_input('" + inputType + "', '" + inputName + "', '', '0');\n";
        $inputs.each(function() {
            if (this.alt) {
                var type = this.name.match(/^\w+/);
                var value = null;
                if (this.type == "checkbox")
                {
                    if(this.checked)
                        value = 1;
                    else
                        return;
                }
                else
                    value = addslashes(this.value);
                str += "insertInput('" + this.alt + "', '" + type + "', selectedInputContainer, '" + value + "');\n";
            }
        });
    }
    return str;
}
//**************************************
//**************************************
function checkPhone(input)
{
    var str = input.value.replace(/\D/g, '');

    if (str.length >= 10)
    {
        var nextInput = getNextFormInput(input);
        nextInput.focus();
    }
}
//**************************************
function getNextFormInput(input)
{
	var form = input.form;
	if(form)
	{
		for(var i = 0; i < form.length; i++)
			if( (form[i] == input) && (i != form.length - 1) )
				return form[i + 1];
	}
	return false;
}
//*****************************
function checkInt(input)
{
	if (!input.value)
		return;
	var value = input.value.replace(/\D/g, '');
	input.value = (isNaN(value)) ? "" : value;
}
//*****************************
function scrollTo(elemID, goToParent)
{
	var elem = false;
	if(elemID && (elem = getElem(elemID)) )
	{
		if(goToParent)
			elem = getParentTagName(elem, goToParent);
		elem.scrollIntoView(true);
		elem.focus();
	}
}
//****************************
function getParentTagName(elem, tagName)
{
	if(elem && tagName)
	{
		if(tagName == "this")
			return elem;
		while(elem = elem.parentNode)
		{
			if(elem.tagName == tagName.toUpperCase())
				return elem;
		}
	}
	return false;
}

function browserDetect() {
    var browser = []; //Opera
    if (window.opera) {
        browser = "Opera";
    }
    else
    //Chrome
    if (window.chrome) {
        browser = "Chrome";
    }
    else
    //Firefox
    if (window.sidebar) {
        browser = "Firefox";
    }
    else
    //Safari
    if (!window.external && browser!=="Opera") {
        browser = "Safari";
    }
    else
    //IE
    if (window.ActiveXObject) {
        browser = "MSIE";
    }
    if (!browser) return(false)
    else return(browser);
}

function erp_modal(params)
{
    if(typeof params.title  ==  'undefined')
    {
        params.title    =   ''
    }
    if(typeof params.params  ==  'undefined')
    {
        params.params    =   ''
    }
    if(typeof params.type   ==  'undefined' || typeof params.act   ==  'undefined')
    {
        alert('Не выбрано действие!');
        return false;
    }



    var modal = new Modal({
        id: 'erp_modal',
        show: true,
        title: "<h4>" + params.title + "</h4>",
        content: "<div id='erpModalContainer' style='padding: 5px;'>container</div>"
    });

    if(params   &&  params.modalCss)
    {
        $('#erpModalContainer').parents('.modal-dialog').css(params.modalCss);
    }


    if(typeof params.params == 'object')
    {
        params.params   =   $.param(params.params);
    }

    if(params.params!='')
    {
        params.params   =   "&"+params.params;
    }

    loadForm(getElem('erpModalContainer'), "function=formLoader&type="+params.type+"&act="+params.act+params.params, "edit");
}

//*****************************************
function deleteImgFromGallery(gallery, imageID ,obj){
    ajax(
        "",
        "function=delGalleryImg&gallery=" + $(gallery).find('input[name=name]').val() + "&parentid=" + $(gallery).find('input[name=id]').val() + "&imgID=" + imageID,
        function (result){
            if (result['debug'])  alert(result['debug']);
            if (result['error']) {
                alert(result['error']);
            } else $(obj).closest('table.gallery_tbl').remove();
        }
    );
}
function loadFunction(functionName, params, additionalParams)
{
    waitLayer(true);
    ajax(functionName, params,  function(result){
        waitLayer(false);
        if(result.content) additionalParams.container.innerHTML = result.content;
        if(typeof result.alert != 'undefined'	&&	result.alert) 	alert(result.alert);
        if(typeof result.refresh != 'undefined'	&&	result.refresh	== 1)
        {
            location.reload();
        }
        if(typeof result.callback != 'undefined')
        {
            result.callback();
        }
        if(additionalParams.callback)
        {
            additionalParams.callback();
        }
    });
}
function setFavouriteImgFromGallery(frame, imgID, btn)
{
    if (!frame) return false;
    var imgTable = $(btn).closest('table');
    var iDoc = $(frame);
    var parentID = iDoc.find('input[name=id]').val();
    var tableBox = imgTable.parent();
    var firstImg = tableBox.children().first();
    var gallery = iDoc.attr('id').split("_")[0];
    if (firstImg != imgTable)
    {
        ajax("", "function=favouriteGalleryImg&gallery=" + gallery + "&parentid=" + parentID + "&img=" + imgID,
            function(result)
            {
                if (result['debug'])
                    alert(result['debug']);
                if (result['error'])
                    alert(result['error']);
                else
                {
                    tableBox.children().first().before(imgTable);
                    btn.style.display = "none";
                    firstImg.find('.fa-star-o').show();
                }
            }
        );
    }
}

function initClipboardCopy()
{
    var browser = browserDetect();
    /*if (jQuery.inArray(browser, []) != -1) {
     $('.input_label_selected').on("copy", function(e){
     e.stopPropagation();
     e.preventDefault();
     var cd = e.originalEvent.clipboardData,
     str = prepareInputCopy(selectedInputContainer);
     cd.setData("Text", str);
     });
     }*/
    if (jQuery.inArray(browser, ['Chrome', 'Opera']) != -1) {
        $(document).bind("paste", function(e){
            if( !inputsTabOpen || (document.activeElement.nodeName.toLowerCase() == "textarea") ||
                ( (document.activeElement.nodeName.toLowerCase() == "input") && (document.activeElement.type == "text") ) )
                return;
            e.preventDefault();
            var cd = e.originalEvent.clipboardData,
                copiedInput = cd.getData("Text");
            if (copiedInput && copiedInput.match(/#coppied_by_rengine#/)) {
                eval(copiedInput.replace(/#coppied_by_rengine#/, ''));
                e.originalEvent.clipboardData.clearData("text/plain");
            }
        });
    }

    if (jQuery.inArray(browser, ['Chrome', 'Opera', 'Firefox']) != -1) {
        Event.add(document.body, "keydown",
            function(e)
            {
                if( e.ctrlKey && (e.keyCode == 'C'.charCodeAt(0)) )
                {
                    if( !inputsTabOpen || (document.activeElement.nodeName.toLowerCase() == "textarea") ||
                        ( (document.activeElement.nodeName.toLowerCase() == "input") && (document.activeElement.type == "text") ) )
                        return;
                    var textareaClipboard = createElement('textarea');
                    textareaClipboard.className = "hiddenCtrlV";
                    textareaClipboard.innerHTML = 'test';
                    document.body.appendChild(textareaClipboard);

                    $(textareaClipboard).on("copy", function(e){
                        $(textareaClipboard).remove();
                        e.stopPropagation();
                        e.preventDefault();
                        var cd = e.originalEvent.clipboardData,
                            str = prepareInputCopy(selectedInputContainer);
                        cd.setData("Text", str);
                    });
                    textareaClipboard.focus();
                    textareaClipboard.select();
                }
            }
        );
    }
    if (jQuery.inArray(browser, ['Firefox']) != -1) {
        Event.add(document.body, "keydown",
            function(e)
            {
                if( e.ctrlKey && (e.keyCode == 'V'.charCodeAt(0)) )
                {
                    if( !inputsTabOpen || (document.activeElement.nodeName.toLowerCase() == "textarea") ||
                        ( (document.activeElement.nodeName.toLowerCase() == "input") && (document.activeElement.type == "text") ) )
                        return;
                    var textareaClipboard = createElement('textarea');
                    textareaClipboard.className = "hiddenCtrlV";
                    document.body.appendChild(textareaClipboard);

                    $(textareaClipboard).bind("paste", function(e){
                        $(textareaClipboard).remove();
                        e.preventDefault();
                        var cd = e.originalEvent.clipboardData,
                            copiedInput = cd.getData("Text");
                        if (copiedInput && copiedInput.match(/#coppied_by_rengine#/)) {
                            eval(copiedInput.replace(/#coppied_by_rengine#/, ''));
                            //e.originalEvent.clipboardData.clearData("text/plain");
                        }
                    });
                    textareaClipboard.focus();
                }
            }
        );
    }
}

function getCursorPosition(ctrl) {
    return {
        start: ctrl.selectionStart,
        end: ctrl.selectionEnd
    };
}

function checkInputByRegExp(params){
    var value = params.value,
        maxLength = params.maxLength || false,
        pressedKey = params.pressedKey || false,
        valueForCheck = value/* + (pressedKey ? String.fromCharCode(pressedKey) : '')*/,
        result = false,
        regExp = params.pattern,
        cursorPos = params.cursorPos || false;
    if (pressedKey) {
        var str = String.fromCharCode(pressedKey);
        if (pressedKey == 8 ){
            str = '';
            if (cursorPos.start == cursorPos.end)
                cursorPos.start--;
        }
        valueForCheck = value.substr(0, cursorPos.start) + str + value.substr(cursorPos.end);
    }
    if (
        regExp.test(valueForCheck)
        && (!maxLength || valueForCheck.length <= maxLength)
    ) {
        result = valueForCheck;
    }
    return result;
}

function checkInput(obj, needed, pattern){
    var value = $(obj).val(),
        sost = (needed || !needed && value != '')
            ? checkInputByRegExp({
            value: value,
            maxLength: $(obj).attr("maxlength"),
            pattern: pattern
        })
            : true,
        container = $(obj).closest(".form-group");

    if (sost === false) {
        //container.removeClass("has-success");
        container.addClass("has-error");
    } else {
        container.removeClass("has-error");
        //container.addClass("has-success");
    }
}


function checkIntFloat(e, obj, pattern) {
    if (e.charCode && !e.ctrlKey || e.keyCode == 8) {
        var pos = getCursorPosition(obj),
            val = checkInputByRegExp({
                value: $(obj).val(),
                maxLength: $(obj).attr("maxlength"),
                pattern: pattern,
                pressedKey: e.keyCode == 8 ? e.keyCode : e.charCode,
                cursorPos: pos
            });
        if (val !== false) {
            $(obj).closest(".form-group").removeClass("has-error");
        } else {
            if (e.keyCode != 8) {
                return false;
            } else {
                $(obj).closest(".form-group").addClass("has-error");
            }
        }
    }
}

function showSqlTimes($btn) {
    var $table = $btn.parent().find('.sqlExecTimes table'),
        down = 'fa-angle-double-down',
        up = 'fa-angle-double-up';

    if ($table.is(':visible')) {
        $table.hide();
        $btn.removeClass(up);
        $btn.addClass(down);
    } else {
        $table.show();
        $btn.removeClass(down);
        $btn.addClass(up);
    }
}