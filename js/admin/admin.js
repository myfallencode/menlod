
$(document).ready(function(){

    initTabs();
    initAutoCompliteSearch();
});

function initSearch() {
    $('#navbarSearchBtn').click(function(){
       loadFunction('searchByQuery', getFormVars('searchForm'), {'container' : $('.searchFormResult')[0]});

       return false;
    });
}

function initSortGallery(selector)
{
    $(selector).sortable({
        items: '.gallery_tbl',
        stop: function () {
            var imagesList =  $(this).find('.gallery_tbl').get().map(function (obj) {
                return $(obj).data('imageid');
            });
            console.log(imagesList);
            ajax(
                'sortSiteGallery',
                {
                    'Images': _this.$imageList.find('li.imageItem').get().map(function (li) {
                        return $(li).data('id');
                    }),
                    'ParentID': _this.parentID,
                    'Gallery': _this.gallery,
                    'Key': _this.key
                },
                function () {
                    _this.sortable = true;
                }
            );
        }
    });
}


function initChangeBuildButton()
{
    $('#changeBuild').off('click').click(function(){
        var $input =$('#singleImage .js-autoComplite');
        $input.parents('.form-group').show();
        $input.data('func', 'searchByQuery');
        $input.data('mode', 'select');
        initAutoCompliteSearch($input);
        $(this).hide();
    });
}

function initMultiActionBtns() {
    $(document).ready(function(){
        $('.multiActionBtn').off('click').each(function(ind, obj){

            var actionClasses = ".multiActionBtn.saveAction, .multiActionBtn.cancelAction, input[name='buildID'], .alreadyInitAutoComplite, .acClear";

            $(obj).click(function(){
                var action = $(this).data('action');
                var $gallery = $(this).parents('.gallery');
                if(action == 'changeBuild')
                {
                    $gallery.find('.imageMultiActionSelect').show();
                    $gallery.find(actionClasses).removeClass('hidden');
                    $(this).addClass('hidden');
                    initAutoCompliteSearch($("input[name='buildID']"));

                    $gallery.find("[name='multiAction']").val(action);
                }

                if(action == 'cancel')
                {
                    $gallery.find(".multiActionBtn[data-action='changeBuild']").removeClass('hidden');
                    $gallery.find(actionClasses).addClass('hidden');
                    $gallery.find('.imageMultiActionSelect').hide();
                }

                if(action == 'save')
                {
                    var $form = $(this).closest('form');
                    var updateData = getFormVars($form[0]);
                    var error = "";

                    if(!updateData.multiActionSelect)
                    {
                        error = "Необходимо выбрать фотографии";
                    }
                    if(!updateData.buildID)
                    {
                        error = "Необходимо выбрать здание";
                    }
                    if(error)
                    {
                        alert(error);
                    }
                    else
                    {
                        ajax('multiActions', updateData, function(result){
                            if(result.success)
                            {

                                loadForm($form.parent()[0], 'function=formLoader&type=buildings&act=photo&id=' + updateData.id, 'edit');
                            }
                        });
                    }
                    console.log(updateData)
                }
            });
        });
    });

}
function initAutoCompliteSearch($obj)
{
    if($obj)
    {
        var $input = $obj;
    }
    else
    {
        var $input = $('.js-autoComplite');
    }

    if ($input.length == 0)
        return;

    $input.each(function(num)
    {
        if($(this).hasClass('alreadyInitAutoComplite'))
        {
            return false;
        }
        $(this).addClass('alreadyInitAutoComplite');

        var defVal = $(this).val();
        var backEndFunc = $(this).data('func');
        if (backEndFunc)
        {
            var errMsg = $(this).data('errMsg');
            errMsg = (errMsg) ? errMsg : "Выберите значение!";
            var $acInput = $(autocomplite(this, defVal, "/ajax/", {'function': backEndFunc}, {'errMsg': errMsg, 'params': $(this).data()}));

            var $clear = $('<span>');
            $clear.addClass("acClear fa fa-times-circle fa-lg");
            $clear.insertAfter($acInput);
            $clear.attr({title: 'очистить'});

            $clear.click(function()
            {
                $acInput.val('');
                $acInput.focus();
            });

        }
        else
        {
            var $acInput = $(this);
            var fieldNum = $(this).data('fieldnum');
            $acInput.keyup(function()
            {
                search_form(this.value, getElem("search_list"), fieldNum);
            });
        }

        $acInput.focus(function()
        {
            if (this.value == defVal)
                this.value = "";
        });

        $acInput.blur(function()
        {
            if (this.value == "")
                this.value = defVal;
        });

    });
};

function waitLayer(show)
{
    var $waitLayer = $('.waitLayer'),
        $loadBox = $('.loadBox'),
        $content = $('#article');

    if (!$waitLayer.length) {
        $waitLayer = $('<div class="waitLayer"></div>');

        $('body').append($waitLayer);
    }

    if (!$loadBox.length)
        $loadBox = $('<div class="loadBox"><div><i class="fa fa-spin fa-refresh fa-5x fa-fw"></i></div></div>');

    if ($content.length) {
        $content.append($loadBox);
    } else {
        $waitLayer.append($loadBox);
    }

    var scrollHeight = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );

    if (show) {
        $waitLayer.css('height', scrollHeight + "px");
        $waitLayer.show();
        $loadBox.show();
    } else {
        $waitLayer.hide();
        $loadBox.remove();
    }
}

function buildTypeChange(obj)
{
    var value = $('#OfficeClassID').val()
    $('#OfficeClassID > option').remove();
    $('#OfficeClassID').parents('.form-group').show();
    $('#OfficeClassID optgroup').hide();

    var $optGroup =  $('#OfficeClassID').find("[label='"+obj.value+"']");
    if($optGroup.length)
    {
        $('#OfficeClassID').prepend($optGroup.html());
        var oldValueOption  =   $('#OfficeClassID > option[value="'+value+'"]')
        if(oldValueOption.length)
        {
        }
        else
            value   =   $('#OfficeClassID').find('option:first').val();
        $('#OfficeClassID').val( value);
    }
    else
    {
       $('#OfficeClassID').parents('.form-group').hide();
    }


}

function elevatorsChange(elem)
{
    var show = hide = [];
    if(elem.value==1)
        show = [{ID : "ElevatorsCnt"}, {ID : "ElevatorsLabel"}];
    else
        hide = [{ID : "ElevatorsCnt"}, {ID : "ElevatorsLabel"}];
    changeVis(show, hide, elem);
}

function telecommunChange(elem, whMode)
{
    var show = hide = [];
    whMode = (whMode == 3) ? true : false;
    if(elem.checked)
    {
        if(whMode)
            show = [{ID : "Provider"}];
        else
            show = [{ID : "Provider", Change : addTitleNeeded}, {ID : "OwnProvider"}];
    }
    else
        hide = [{ID : "Provider"}, {ID : "OwnProvider"}];
    changeVis(show, hide, elem);
}

function showNonActiveBlocks(tHeaderTD)
{
	if(tHeaderTD)
	{
		var TRs = $(tHeaderTD).closest('thead').next().children();
		for (var i = 0; i < TRs.length; i++)
		{
			if (TRs[i].className == 'blocksTR_none')
				TRs[i].className = 'blocksTR';
			else if(TRs[i].className == 'blocksTR')
				TRs[i].className = 'blocksTR_none';
		}
		tHeaderTD.className = (tHeaderTD.className.match(/switchActiveBlocksYes/)) ? tHeaderTD.className.replace('switchActiveBlocksYes', 'switchActiveBlocksNo')  : tHeaderTD.className.replace('switchActiveBlocksNo', 'switchActiveBlocksYes');
	}
}

function visForm($form)
{
    var checkConditions =   function(conditions)
    {
        var visible = false;
        var list = conditions.split('||');

        for (var key in list) {
            var conds = list[key].split('&&');
            var andVisible = true;

            for (var condKey in conds) {
                var cond = conds[condKey];
                var pattern = new RegExp('(.*?)(==|=|!=|<|>|<=|>=)(.*?)$', 'i');
                var matches = cond.match(pattern);
                if (matches) {
                    if (matches[2] == "=") matches[2] = "==";

                    var element = $form.find('#' + matches[1]);
                    // console.log(element[0].tagName)
                    if(element[0].tagName=='INPUT' && element.attr('type')=='radio')
                    {

                        value = null;
                        $form.find('[name='+element.attr('name')+']').each(function(i,input)
                        {
                            if(value==null && input.checked)
                            {
                                value = input.value;
                            }
                        });
                    }
                    else
                        var value = element.val();

                    eval("stepVisible=value " + matches[2] + " " + matches[3]);
                    andVisible = andVisible && stepVisible;
                }

            }
            visible = visible || andVisible
        }
        return visible;
    }
    var groupVisible = function(group){
        var visConditions       = $(group).data('visconditions');
        var needElement = Array($(group).parents('.form-group'));


        var disabledConditions  = $(group).data('disabledconditions');
        if($(group).prev().hasClass('inputDiv') || $(group).next().hasClass('inputDiv'))
        {
            needElement[needElement.length] = $(group);
        }


        if(typeof visConditions != 'undefined') {

            if(checkConditions(visConditions))
            {
                for(k in needElement)
                {
                    needElement[k].show();
                }

            }
            else
            {
                for(k in needElement)
                {
                    needElement[k].hide();
                }
            }

        }
        if(typeof disabledConditions!='undefined')
        {
            var elementInd  =   needElement.length-1;
            console.log(elementInd)
            if(checkConditions(disabledConditions))
            {
                needElement[elementInd].find('input,select').attr('disabled','disabled');
            }
            else
                needElement[elementInd].find('input,select').removeAttr('disabled');
        }
    }

   var  formVisible = function(){
       $form.find('.inputDiv').each(function(ind,group){
           groupVisible(group);
       });
   };

    formVisible();
    $form.find('input,select').on('change',function(){
        formVisible();
    })

}

function selectOwnerLine(obj, id, OwnerID)
{

    $(obj).parents('table').find('.triggerLine.open').removeClass('open').hide();

    selectLineTrigger(obj,{"callback":
        function(container){
            loadForm(container, "function=formLoader&type=owners&act=ownerCard&id=" + OwnerID + "&masterid=", "", "edit", "", function(){
                loadForm(getElem("buildOwnersMiddle"), "function=formLoader&type=blocks&act=list&OwnerID=" + OwnerID + "&masterid=" + id , "edit")
            });
            $("#buildOwnersRight").html("");
        }
    });
}

function selectLineTrigger(obj,params)
{
    if(typeof params == 'undefined')
        params  =   {};



    var $nextObj = $(obj).parents('tr').next();
    var countTd  = $(obj).parents('tr').find('td').length;
    if(!$nextObj.hasClass('triggerLine'))
    {
        $(obj).parents('tr').after("<tr class='triggerLine'><td colspan='"+countTd+"'></td></tr>");
        $nextObj = $(obj).parents('tr').next();
    }
    var $container  =   $nextObj.find('td');
    if($nextObj.hasClass('open'))
    {
        $nextObj.hide();
        $nextObj.removeClass('open');
    }
    else {
        $nextObj.show();
        $nextObj.addClass('open');
        if(typeof params.callback != 'undefined' && typeof params.callback == 'function')
        {
            params.callback($container[0]);
        }
    }
}

var editWindow;
var hashParams = Hash.getAll();

$(document).ready(function () {

    editWindow  = new Modal({
        id: 'erp_edit',
        show: false,
        content: $('<div id="editContainer"></div>')
    });
	$('.datepickerElement').datepicker({
		'language': 'ru',
		'format': 'dd.mm.yyyy',
	});

});

function getOrgInfo(buildID, orgID) {
    loadForm(getElem("ownerBlocksBox"), "function=formLoader&type=blocks&act=list&masterid=" + buildID + '&orgID=' + orgID, "edit");
}
var initCheckbox = function (form) {
    $('.form-checkbox', form).niftyCheck();
    $('.form-radio', form).niftyCheck();
};