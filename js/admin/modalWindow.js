/**
 * opt = {
 *         id: string   id модали
 *       show: boolean  создать скрытой или показать сразу
 *      title: string   заголовок
 *    content: string   контент
 *     footer: string   футер
 * dismissBtn: boolean  кнопка закрытия
 *  big/small: boolean  ширина (классовая)
 *      width: string   ширина
 * }
 * @param opt
 * @returns {*}
 * @constructor
 */
function Modal(opt) {
    var result, $win;

    if (typeof opt != 'object')
        opt = {id: opt};

    if (opt && opt.id) {
        this.id = opt.id;
        $win = $('#'+opt.id);
        opt.dismissBtn = opt.dismissBtn !== false;
        if (!$win.size()) {
            $win = $('<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>');
            var $dialog = $('<div class="modal-dialog"></div>'),
                $content = $('<div class="modal-content"></div>'),
                $header = $('<div class="modal-header"></div>'),
                $closeButton = $('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'),
                $body = $('<div class="modal-body"></div>');
            $win.attr('id', opt.id);

            if (opt.title) {
                $header.append($closeButton);
                $content.append($header);
            } else if (opt.dismissBtn){
                $body.append($closeButton);
            }
            if (!opt.content) {
                $body.prepend($('<span class="glyphicon glyphicon-unchecked" style="visibility: hidden;"></span>'));
            }

            $content.append($body);
            $dialog.append($content);
            $win.append($dialog);
            $(document.body).append($win);
        }
        this.modalWindow = $win;
        this.modalWindow.find('.modal-dialog').removeClass('modal-lg');
        this.modalWindow.find('.modal-dialog').removeClass('modal-sm');
        if (opt.big) {
            this.modalWindow.find('.modal-dialog').addClass('modal-lg');
        } else if (opt.small) {
            this.modalWindow.find('.modal-dialog').addClass('modal-sm');
        }

        if (opt.width)
            this.modalWindow.find('.modal-dialog').css('width', opt.width ? opt.width : '');

        if (opt.title) {
            this.setTitle(opt.title);
        }
        if (opt.content) {
            this.setContent(opt.content);
        }
        if (opt.footer) {
            this.setFooter(opt.footer);
        }

        if (opt.show) {
            this.show();
        }
        result = this;
    } else {
        result = false;
    }
    return result;
}
Modal.prototype.setOption = function(opt) {
    this.modalWindow.find('.modal-dialog').removeClass('modal-lg');
    this.modalWindow.find('.modal-dialog').removeClass('modal-sm');
    if (opt.big) {
        this.modalWindow.find('.modal-dialog').addClass('modal-lg');
    } else {
        if (opt.small) {
            this.modalWindow.find('.modal-dialog').addClass('modal-sm');
        }
    }
    if (opt.title) {
        this.setTitle(opt.title);
    }
    if (opt.footer) {
        this.setFooter(opt.footer);
    }
    this.modalWindow.find('.modal-dialog').css('width',opt.width ? opt.width : '');
    if (opt.show) {
        this.show();
    } else {
        this.hide();
    }
}
Modal.prototype.setFooter = function (content) {
    var newFooter = $('<div class="modal-footer"></div>');
    this.modalWindow.find('.modal-content .modal-footer').remove();
    newFooter.append(content);
    this.modalWindow.find('.modal-content').append(newFooter);
}
Modal.prototype.setTitle = function (content) {
    var header = this.modalWindow.find('.modal-content .modal-header'),
        dismiss = header.find('[data-dismiss=modal]'),
        newHeader = $('<div class="modal-header"></div>'),
        body = this.modalWindow.find('.modal-content .modal-body'),
        dismissBody = body.find('[data-dismiss=modal]');
    header.remove();
    newHeader.append(content);
    newHeader.prepend(dismiss.size() ? dismiss : dismissBody);
    //dismissBody.remove();
    this.modalWindow.find('.modal-content').prepend(newHeader);
}
Modal.prototype.setContent = function (content) {
    var body = this.modalWindow.find('.modal-content .modal-body'),
        dismiss = body.find('[data-dismiss=modal]'),
        newBody = $('<div class="modal-body"></div>'),
        header = this.modalWindow.find('.modal-content .modal-header');
    body.remove();
    newBody.append(content);
    if (dismiss.size()) {
        newBody.prepend(dismiss);
    }
    if (header.size()) {
        header.after(newBody);
    } else {
        this.modalWindow.find('.modal-content').prepend(newBody);
    }
}
/*Modal.prototype.setCenter = function () {
    var $dialog = this.modalWindow.find(".modal-dialog");
    $dialog.css('display', 'block');
    var offset = ($(window).height() - $dialog.height()) / 2;
    $dialog.css("margin-top", offset);
}*/
Modal.prototype.css = function (opt) {
    this.modalWindow.find('.modal-dialog').css(opt);
}
Modal.prototype.show = function () {
    this.modalWindow.modal('show');
    /*var modal = this;
    this.modalWindow.on('shown.bs.modal', function(){
        modal.setCenter();
    });*/
}
Modal.prototype.hide = function () {
    this.modalWindow.modal('hide');
}