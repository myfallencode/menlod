function SiteSingleGallery($container, params) {
    this.$container = $container; // контейнер
    this.parentID = this.$container.data('parent') || params.parentID; // parentID
    this.gallery = this.$container.data('gallery') || params.gallery; // название галереи
    this.key = this.$container.data('key') || params.key; // ключ галереи

    if (this.parentID && this.gallery && this.key) { // если есть название галереи и ID родителя
        var _this = this;

        _this.on = {
            load: function () {
                if (params.on && (typeof params.on.load == 'function')) {
                    params.on.load(_this);
                }
            },
            delete: function () {
                if (params.on && (typeof params.on.delete == 'function')) {
                    params.on.delete(_this);
                }
            }
        };

        _this.$image = _this.$container.find('img'); // текущее изображение

        if (_this.$image.length && _this.$image.data('id')) { // если у если изобрадение и у него есть ID
            _this.imageID = _this.$image.data('id'); // устанавливаем ID зображения
        } else { // в противном случае
            _this.$image = {}; // нет изображения
            _this.imageID = 0; // нет ID изображения
        }

        _this.$container.empty(); //очиаем контейнер

        _this.$input = $('<input type="file" accept="image/*"></div>');

        _this.$input.change(function () {
            _this.add(this.files[0]);

            $(this).val('');
        });

        _this.$container.append(_this.$input);

        _this.controls = { // элементы контроля
            add: { // кнопка добавить
                dom: $($.parseHTML('<button type="button" class="addButton">Добавить</button>')), //dom
                visible: !_this.$image.length, // если нет текущего изображения, то скрываем
                on: { // события
                    click: function (e) {
                        _this.$input.click();
                    }
                }
            },
            edit: { // кнопка редактирования
                dom: $($.parseHTML('<button type="button" class="editButton">Изменить</button>')),
                visible: !!_this.$image.length,
                on: {
                    click: function (e) {
                        _this.$input.click();
                    }
                }
            },
            delete: { // кнопка удаления
                dom: $($.parseHTML('<button type="button" class="deleteButton popovered">Удалить</button>')),
                visible: !!_this.$image.length,
                on: {
                    click: function (e) {
                        var $btn = $(this);

                        if ($btn.hasClass('active')) {
                            $btn.popover('hide');
                            $btn.removeClass('active');
                        } else {
                            $btn.popover('show');
                            $btn.addClass('active');
                        }
                    }
                }
            },
            loader: { // загрузчик
                dom: $($.parseHTML('<div class="loader"><div></div></div>')),
                visible: false
            }
        };

        _this.$deleteConfirm = $($.parseHTML(
            '<div class="deleteConfirm">' +
                '<div class="deleteConfirmLabel">Удалить?</div>' +
                '<div class="deleteConfirmButtons">' +
                    '<button type="button" class="agree btn btn-success btn-sm">Да</button>' +
                    '<button type="button" class="cancel btn btn-danger btn-sm">Нет</button>' +
                '</div>' +
            '</div>'
        ));

        _this.$deleteConfirm.find('.agree').click(function () {
            _this.controls.delete.dom.popover('hide');
            _this.controls.delete.dom.removeClass('active');

            _this.delete(_this.imageID);
        });

        _this.controls.delete.dom.popover({
            html: true,
            content: _this.$deleteConfirm,
            placement: 'bottom',
            trigger: 'manual'
        });

        _this.$deleteConfirm.find('.cancel').click(function () {
            _this.controls.delete.dom.popover('hide');
            _this.controls.delete.dom.removeClass('active');
        });

        $(document).click(function (e) {
            var $target = $(e.target);

            if (!$target.hasClass('popovered') && !$target.closest('.popover').length) {
                _this.controls.delete.dom.popover('hide');
                _this.controls.delete.dom.removeClass('active');
            }
        });

        _this.$imageContainer = $('<div class="imageContainer"></div>'); // контейнер с текущим изображением
        _this.$controlsContainer = $('<div class="controlsContainer"></div>'); // контейнер с элементами контроля

        if (_this.$image.length) { // если есть текущее изображение
            _this.$imageContainer.append(_this.$image); // добавляем его в контейнер
        } else { // в противном случае контейнер скрываем
            _this.$imageContainer.hide();
        }

        _this.$container.append(_this.$imageContainer); // добавляем контейнер изображения в общий контейнер

        for (var i in _this.controls) { // перебираем элементы контроля
            var $controlItem = _this.controls[i]; // текйщий элемент контроля

            if ($controlItem.on) // если есть события
                for (var eventName in $controlItem.on) // перебираем их
                    $controlItem.dom.on(eventName, $controlItem.on[eventName]); // устанавливаем обработчик события

            _this.$controlsContainer.append($controlItem.dom); // добавляем dom элемента контроля в контейнер элементов контроля

            if (!$controlItem.visible)
                $controlItem.dom.hide();
        }

        _this.$container.append(_this.$controlsContainer); // добавляем контейнер элементов контроля в общий контейнер
    }
}

SiteSingleGallery.prototype.updateContainers = function () {
    var _this = this;

    for (var i in _this.controls) {
        if (_this.controls[i].visible) {
            _this.controls[i].dom.show();
        } else {
            _this.controls[i].dom.hide();
        }
    }
};

SiteSingleGallery.prototype.getUID = function () {
    return (new Date()).getTime() + '' + (Math.random() * 1000).toFixed(0);
};

/**
 * загрузка изображения
 * @param file ресурс файла
 */
SiteSingleGallery.prototype.add = function (file) {
    var image = new Image(),
        _this = this;

    image.src = URL.createObjectURL(file); //создаем изображение

    _this.controls.add.visible = false;
    _this.controls.edit.visible = false;
    _this.controls.delete.visible = false;
    _this.controls.loader.visible = true;

    _this.updateContainers();

    image.onload = function () { // после загрузки
        var $image = $(image), // формурием dom изображения
            size = { //размеры изображения
                width: MAX_WIDTH,
                height: MAX_HEIGHT
            },
            $canvas = $("<canvas></canvas>"), //канва для ресайза и сжатия
            ctx = $canvas[0].getContext("2d"),
            $imageIcon = $image.clone(); // иконка картинки

        if (_this.$image.length)
            _this.$image.hide(); //скрываем старое изображение

        _this.$imageContainer.append($imageIcon); //добавляем новое

        $imageIcon[0].onload = function () { // после загрузки иконки нового изображения
            if ($imageIcon.width() < $imageIcon.height())
                $(this).addClass('vertical');
        };

        _this.$imageContainer.show();

        $image.css({ //добавляем изображение
            position: 'absolute',
            left: -10000,
            top: -10000
        });

        $('body').append($image);

        $canvas.css({ //добавляем канву
            position: 'absolute',
            left: -10000,
            top: -10000
        });

        $('body').append($canvas);

        var iWidth = $image.width(), //текущие размеры изображения
            iHeight = $image.height();

        if (iWidth > size.width || iHeight > size.height) { //если текущие размеры изображения выходят за рамки дозволенного
            var newSizes = _this.resizeImage({ //то посчитываем новые размеры
                w: iWidth,
                h: iHeight,
                maxW: size.width,
                maxH: size.height
            });

            iWidth = newSizes.w; //новые размеры
            iHeight = newSizes.h;
        }

        $canvas[0].width = iWidth; //ресайзим канву
        $canvas[0].height = iHeight;

        ctx.drawImage(image, 0, 0, iWidth, iHeight); //рисуем на канве новое изображение

        _this.imageCompression( //пережимаем изображение
            {
                canvas: $canvas[0],
                w: iWidth,
                h: iHeight,
                image: image
            },
            function (data, size) { //после сжатия отправляем на сервер
                var uid = _this.getUID();

                ajax(
                    'addToSiteGallery',
                    {
                        'images': [data.split(',')[1]],
                        'imagesName': [uid + '.jpg'],
                        'imagesUID': [uid],
                        'Gallery': _this.gallery,
                        'ParentID': _this.parentID,
                        'Key': _this.key
                    },
                    function (result) { //добавляет id изображения и убираем загрузчик
                        _this.controls.add.visible = false;
                         _this.controls.edit.visible = true;
                         _this.controls.delete.visible = true;
                         _this.controls.loader.visible = false;

                         _this.updateContainers();
                         _this.setLoaderValue(0);

                        if (!result.errors.length) {
                            var newID = result.data[0].id;

                            if (newID) {
                                if (_this.imageID)
                                    _this.delete(_this.imageID, true);

                                _this.imageID = newID;
                            }
                        } else { //TODO вывод ошибок

                        }

                        _this.on.load();
                    },
                    {
                        xhr: function(){
                            var xhr = $.ajaxSettings.xhr(); // получаем объект XMLHttpRequest

                            xhr.upload.addEventListener('progress', function (evt) { // добавляем обработчик события progress (onprogress)
                                /*if(evt.lengthComputable) { // если известно количество байт
                                    // высчитываем процент загруженного
                                    var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
                                    // устанавливаем значение в атрибут value тега <progress>
                                    // и это же значение альтернативным текстом для браузеров, не поддерживающих <progress>
                                    progressBar.val(percentComplete).text('Загружено ' + percentComplete + '%');
                                }*/
                                _this.setLoaderValue(Math.round((evt.loaded / evt.total) * 100));
                            }, false);

                            return xhr;
                        }
                    }
                );

                $image.remove();
                $canvas.remove();
            }
        );
    };
};

SiteSingleGallery.prototype.setLoaderValue = function (percents) {
    var _this = this;

    _this.controls.delete.dom.find('div').css('width', percents + '%');
};

SiteSingleGallery.prototype.resizeImage = function (sizes) {
    if (sizes.w > sizes.maxW || sizes.h > sizes.maxH) {
        if (sizes.w > sizes.maxW) {
            sizes.h = Math.round(sizes.h * sizes.maxW / sizes.w);
            sizes.w = sizes.maxW;
        } else if (sizes.h > sizes.maxH) {
            sizes.w = Math.round(sizes.w * sizes.maxH / sizes.h);
            sizes.h = sizes.maxH;
        }

        sizes = this.resizeImage(sizes);
    }

    return sizes;
};

SiteSingleGallery.prototype.getFileSizeByBase64 = function (data) {
    return data.split(',')[1].length * 3 / 4;
};

SiteSingleGallery.prototype.imageCompression = function (obj , success) {
    var _this = this,
        canvas = obj.canvas,
        ctx = canvas.getContext("2d"),
        data = canvas.toDataURL("image/jpeg"),
        imgSize = _this.getFileSizeByBase64(data),
        kSize = MAX_SIZE / imgSize;

    if (kSize < 1) {
        kSize = (kSize + 1) / 2 - 0.01;

        var newWidth = Math.round(obj.w * kSize),
            newHeight = Math.round(obj.h * kSize);

        canvas.width = newWidth;
        canvas.height = newHeight;
        obj.image.src = data;

        obj.image.onload = function () {
            ctx.drawImage(obj.image, 0, 0, newWidth, newHeight);
            obj.image.src = canvas.toDataURL("image/jpeg");

            obj.image.onload = function () {
                canvas.width = obj.w;
                canvas.height = obj.h;
                ctx.drawImage(obj.image, 0, 0, obj.w, obj.h);
                _this.imageCompression(obj, success);
            };
        };
    } else {
        success(data, imgSize);
    }
};

/**
 * Удаление узображения по ID
 * @param id ID изображения
 * @param hideDelete Удаление без видимых последствий
 */
SiteSingleGallery.prototype.delete = function (id, hideDelete) {
    if (id) {
        var _this = this;

        hideDelete = hideDelete || false;

        ajax(
            'deleteFromSiteGallery',
            {
                'ImageID': id,
                'ParentID': _this.parentID,
                'Gallery': _this.gallery,
                'Key': _this.key
            },
            function (result) {
                if (result) {
                    if (!hideDelete) {
                        _this.imageID = 0;

                        _this.$imageContainer.empty();
                        _this.$imageContainer.hide();

                        _this.controls.add.visible = true;
                        _this.controls.edit.visible = false;
                        _this.controls.delete.visible = false;

                        _this.updateContainers();

                        _this.on.delete();
                    }
                }
            }
        );
    }
};