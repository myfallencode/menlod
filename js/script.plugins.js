;(function($){

	"use strict";

	$(document).ready(function(){



		/* ------------------------------------------------
				jQueryFormStyler
		------------------------------------------------ */

		if($('.formstyler_input, .formstyler_select').length){ 

	            $('.formstyler_input, .formstyler_select').styler({
	                selectSearch: false
	            });
		       
		}
		/* ------------------------------------------------
				jQueryFormStyler
		------------------------------------------------ */


		/* ------------------------------------------------
				Counter-Up-master
		------------------------------------------------ */

		if($('.counter').length){ 

	
		        $('.counter').counterUp({
		            delay: 10,
		            time: 1000
		        });

		}
		/* ------------------------------------------------
				Counter-Up-master
		------------------------------------------------ */

		/* ------------------------------------------------
				High slide
		------------------------------------------------ */

		if($('.highslide').length){ 

			hs.graphicsDir = 'plugins/highslide-4.1.12/highslide/graphics/';
            hs.align = 'center';
            hs.transitions = ['expand', 'crossfade'];
            hs.fadeInOut = true;
            //hs.wrapperClassName = 'borderless';
            hs.dimmingOpacity = 0.8;
            hs.outlineType = 'rounded-white';
            hs.captionEval = 'this.thumb.alt';
            hs.marginBottom = 105; // make room for the thumbstrip and the controls
            hs.numberPosition = 'caption';

            // Add the slideshow providing the controlbar and the thumbstrip
            hs.addSlideshow({
                //slideshowGroup: 'group1',
                interval: 5000,
                repeat: false,
                useControls: true,
                overlayOptions: {
                    className: 'text-controls',
                    position: 'bottom center',
                    relativeTo: 'viewport',
                    offsetY: -60
                },
                thumbstrip: {
                    position: 'bottom center',
                    mode: 'horizontal',
                    relativeTo: 'viewport'
                }
            });
		}
		/* ------------------------------------------------
				High slide
		------------------------------------------------ */

		/* ------------------------------------------------
				Vertical Tab
		------------------------------------------------ */
        /* ------------------------------------------------
				End of Vertical Tab
		------------------------------------------------ */

		/* ------------------------------------------------
				Horizontal Tab
		------------------------------------------------ */

		if($('.parentHorizontalTab').length){ 	

	         $('.parentHorizontalTab').easyResponsiveTabs({
	            type: 'default', //Types: default, vertical, accordion
	            width: 'auto', //auto or any width like 600px
	            inactive_bg: '#ffffff',
                active_border_color: '#ffffff',
                active_content_border_color: '#ffffff',
	            fit: true, // 100% fit in a container
	            tabidentify: 'hor_1', // The tab groups identifier
	            activate: function(event) { // Callback function if tab is switched
	                var $tab = $(this);
	                var $info = $('#nested-tabInfo');
	                var $name = $('span', $info);
	                $name.text($tab.text());
	                $info.show();
	            }
	        });

		}

        /* ------------------------------------------------
				End of Horizontal Tab
		------------------------------------------------ */

		/* ------------------------------------------------
				ui-slider
		------------------------------------------------ */

		if($('#slider2').length){ 
			jQuery("#slider2").slider({
				min: 999,
				max: 50000000,
				values: [999,40000000],
				range: true,

				slide: function(event, ui){
					jQuery("input#minCost").val(jQuery("#slider2").slider("values",0));
					jQuery("input#maxCost").val(jQuery("#slider2").slider("values",1));


					var minCostText = jQuery("#slider2").slider("values",0),
					    maxCostText = jQuery("#slider2").slider("values",1);

					minCostText = minCostText + "";
					maxCostText = maxCostText + "";


					jQuery("#minCostText").text(minCostText.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
					jQuery("#maxCostText").text(maxCostText.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));

			    }
				
				/*s
				top: function(event, ui) {
					jQuery("input#minCost").val(jQuery("#slider2").slider("values",0));
					jQuery("input#maxCost").val(jQuery("#slider2").slider("values",1));
					
			    },
			    slide: function(event, ui){
					jQuery("input#minCost").val(jQuery("#slider2").slider("values",0));
					jQuery("input#maxCost").val(jQuery("#slider2").slider("values",1));
			    }
			    */
			});

			jQuery("input#minCost").change(function(){

				var value1=jQuery("input#minCost").val();
				var value2=jQuery("input#maxCost").val();

			    if(parseInt(value1) > parseInt(value2)){
					value1 = value2;
					jQuery("input#minCost").val(value1);
				}
				jQuery("#slider2").slider("values",0,value1);	
			});

				
			jQuery("input#maxCost").change(function(){
					
				var value1=jQuery("input#minCost").val();
				var value2=jQuery("input#maxCost").val();
				
				if (value2 > 50000000) { value2 = 50000000; jQuery("input#maxCost").val(50000000)}

				if(parseInt(value1) > parseInt(value2)){
					value2 = value1;
					jQuery("input#maxCost").val(value2);
				}
				jQuery("#slider2").slider("values",1,value2);
			});
		}


		if($('#slider3').length){ 
			jQuery("#slider3").slider({
				min: 1999999,
				max: 9999999,
				values: [1999999,6999999],
				range: true,

			    slide: function(event, ui){
					jQuery("input#minSizeCost").val(jQuery("#slider3").slider("values",0));
					jQuery("input#maxSizeCost").val(jQuery("#slider3").slider("values",1));

					var minSizeCostText = jQuery("#slider3").slider("values",0),
					    maxSizeCostText = jQuery("#slider3").slider("values",1);

					minSizeCostText = minSizeCostText + "";
					maxSizeCostText = maxSizeCostText + "";


					jQuery("#minSizeCostText").text(minSizeCostText.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
					jQuery("#maxSizeCostText").text(maxSizeCostText.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));

			    }
			});

			jQuery("input#minSizeCost").change(function(){

				var value1=jQuery("input#minSizeCost").val();
				var value2=jQuery("input#maxSizeCost").val();

			    if(parseInt(value1) > parseInt(value2)){
					value1 = value2;
					jQuery("input#minSizeCost").val(value1);
				}
				jQuery("#slider3").slider("values",0,value1);	
			});

				
			jQuery("input#maxSizeCost").change(function(){
					
				var value1=jQuery("input#minSizeCost").val();
				var value2=jQuery("input#maxSizeCost").val();
				
				if (value2 > 9999999) { value2 = 9999999; jQuery("input#maxSizeCost").val(9999999)}

				if(parseInt(value1) > parseInt(value2)){
					value2 = value1;
					jQuery("input#maxSizeCost").val(value2);
				}
				jQuery("#slider3").slider("values",1,value2);
			});

		}

			// фильтрация ввода в поля
			//jQuery('input').keypress(function(event){
			//	var key, keyChar;
			//	if(!event) var event = window.event;
			//	
			//	if (event.keyCode) key = event.keyCode;
			//	else if(event.which) key = event.which;
			//
			//	if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
			//	keyChar=String.fromCharCode(key);
			//	
			//	if(!/\d/.test(keyChar))	return false;
			//
			//});


        /* ------------------------------------------------
				End of ui-slider
		------------------------------------------------ */

		 /* ------------------------------------------------
                validateform + mask
        ------------------------------------------------ */

        if($('.validateform').length){ 

	        $(".validateform").validate({

	           rules:{

	                login:{
	                    required: true,
	                    minlength: 2,
	                    maxlength: 16,
	                    characters: true,
	                },
	                email:{
	                    required: true,
	                    email: true
	                },
	                number:{
	                    required: true,
	                    //number: true
	                },

	                necessarily:{
	                    required: true
	                },
	                necessarily2:{
	                    required: true
	                },



	           },

	           messages:{

	                login:{
	                    required: "Это поле обязательно для заполнения",
	                    minlength: "Имя должно быть минимум 2 символа",
	                    maxlength: "Максимальное число символо - 16",
	                    characters: "Пожалуйста, введите имя без цифр",
	                },

	                email:{
	                    required: "Это поле обязательно для заполнения",
	                    email: "Неверный E-mail",
	                },

	                number:{
	                    required: "Это поле обязательно для заполнения",
	                    number: "Неверный номер (только цифры)",
	                },
	                necessarily:{
	                    required: "Это поле обязательно для заполнения",
	                },
	                necessarily2:{
	                    required: "Это поле обязательно для заполнения",
	                },
	                

	           }

	        });

        }

        if($('.phone_mask').length){ 

        	jQuery(function($){
			   $(".phone_mask").mask("+7 (999) 999-9999");
			   });

        }


        /* ------------------------------------------------
                End of validateform + mask
        ------------------------------------------------ */

        /* ------------------------------------------------
                Calendar
        ------------------------------------------------ */

        if($('#myDatePicker-1').length){ 

        	$("#myDatePicker-1").ionDatePicker();

        }

        /* ------------------------------------------------
                End of Calendar
        ------------------------------------------------ */



	});

	$(window).load(function(){

		/* ------------------------------------------------
				Rating
		------------------------------------------------ */

			if($('.rating').length){

				$('.rating').productRating();

			}


        /* ------------------------------------------------
				End of Rating
		------------------------------------------------ */

		/* ------------------------------------------------
				Scrollbar
		------------------------------------------------ */

			if($('.content2').length){

				$(".content2").mCustomScrollbar();

			}

        /* ------------------------------------------------
				End of Scrollbar
		------------------------------------------------ */

				/* ------------------------------------------------
				tooltip
		------------------------------------------------ */

			if($('.tooltip').length){ 

		            $('.tooltip').tooltipster();
			       
			}
		/* ------------------------------------------------
				tooltip
		------------------------------------------------ */


	});

})(jQuery);