/**
 * Управление пошаговыми табами.
 * @param $container
 * @constructor
 */
function StepTabs($container) {
    this.$container = $container; //корневой элемент
    this.$menu = $('.stepTabsMenu', $container); //меню
    this.$contents = $('.stepTabsContents', $container); //контейнер с элементами контента

    var _this = this;

    this.setActive(($('.active', this.$menu) || this.$menu.children().first()).data('step')); //инициализируем активный шаг, если такового нет, то делаем первый шаг активным

    $('.nextStepTabs, .prevStepTabs', this.$contents).click(function () { //кнопки далее/назад
        var $this = $(this), //кнопка
            activeFunc = $this.data('function'); //имя функции, которая будет выполнена перед переходом к следущему/предыдущему шагу

        if (activeFunc) { //если была указана функция, то выполняем ее
            exec(
                activeFunc, //название функции
                $this, //сама кнопка
                $this.closest('.stepTabsContent'), //первый аргумент функции контейнер с контентом текущей вкладки
                _this //объект StepTabs для дальнейшего управления
            );
        } else { //если не указана, то переходим на следущий/предыдущий шаг
            if ($this.hasClass('nextStepTabs')) {
                _this.next();
            } else {
                _this.prev();
            }
        }

        return false;
    });

    $('li > a', this.$menu).click(function () { //переключение по элементам меню
        var $menuItem = $(this).parent();

        if (!$menuItem.hasClass('active') && !$menuItem.attr('disabled')) //если он не активен и не забанен
            _this.setActive($menuItem.data('step'));

        return false;
    });
}

/**
 * Переходит на указаный шаг (таб)
 * @param step
 */
StepTabs.prototype.setActive = function (step) { //функция переключения вкладок на определенный шаг добавления
    var $contents = $('.stepTabsContent', this.$contents),
        $menuItems = $('li', this.$menu);

    $contents.hide(); //скрываем весь контент
    $contents.filter('[data-step=' + step + ']').show(); //показываем только контент запрошенного шага

    $menuItems.removeClass('active'); //делаем нужный пункт меню активным
    $menuItems.filter('[data-step=' + step + ']').addClass('active');
};

/**
 * Переходит на следущий шаг
 */
StepTabs.prototype.next = function () {
    var nextStep = this.getNextStep(); //следущий элемент меню относительно активного

    if (nextStep) // если он есть
        this.setActive(nextStep); // переходим
};

/**
 * Переходит на предыдущий шаг
 */
StepTabs.prototype.prev = function () {
    var prevStep = this.getPrevStep(); //предыдущий элемент меню относительно активного

    if (prevStep) // если он есть
        this.setActive(prevStep); // переходим
};

/**
 * Возвращает текущий активный шаг
 * @return string
 */
StepTabs.prototype.getCurStep = function () {
    return $('.active', this.$menu).data('step');
};

/**
 * Возвращает следущий шаг
 * @return string|boolean
 */
StepTabs.prototype.getNextStep = function () {
    var $nextStepItem = $('.active', this.$menu).next();

    return $nextStepItem.size() ? $nextStepItem.data('step') : false;
};

/**
 * Возвращает предыдущий шаг
 * @return string|boolean
 */
StepTabs.prototype.getPrevStep = function () {
    var $prevStepItem = $('.active', this.$menu).prev();

    return $prevStepItem.size() ? $prevStepItem.data('step') : false;
};