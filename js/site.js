﻿var Global = {
    selectedBlocks: [],
    deleteSelectedBlocks: [],
    maps: {},
    searchMapOpened: false,
    mapFirstSearch: false,
    searchDelay: 0,
    currentSearchID: 0,
    searchDelayTime: 1500,
    markerPath: '/img/design/gmarkers/new/',
    pdf: {}
};

$(document).ready(function () {
    $(window).resize(autoResizeWindow);

    $.cookie.json = true;

    initPlugins();

    initDefaultMaps();
    initClientPicker();
    initAddBuildSteps();
    initRegister();
    initAllStepTabs();
    initAllSliders();
    //initAddClient();
    initMaskedInput();
    initSiteGalleries();
    initChosen();
    initBlocksFilter();
    initMetroMap();
    initAutoOpenSearchFilters();
    initClientSearch();
    initClientListSearch();
    initPersonNameChange();
    initGalleryWrappers();
    initShowGallery();
    initPDFEditor();

    initAgentBar();
    initAutoTextareaSize();
    initBuildSlider();
    initSelectPickers();
    initButtonsSelect();
    initNumberInput();

    initRedirectButtons();
    //initFullpage();
    initScrollToTop();
    initScroll();

    initClientListPage();

    initMapMenu();
    initSearchOrders();
    //initSearch();
    initSearchButtons();
    initModal();

    initPDFLink();

    initProfile();
    initSendSupportRequest();

    initNoAgent();

    if (typeof $().niftyCheck == 'function')
        $('.form-checkbox').niftyCheck();

    /*$('.parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        inactive_bg: '#ffffff',
        active_border_color: '#ffffff',
        active_content_border_color: '#ffffff',
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        },
        linkHash: true
    });*/

    $('#searchResultType button').click(function () {
        var $this = $(this),
            $searchSortBox = $('.searchSort');

        $this.parent().find('button').prop('disabled', false);
        $this.prop('disabled', true);

        if ($this.data('type') == 'map') {
            $searchSortBox.hide();
        } else {
            $searchSortBox.show();
        }
    });

    $('.about-page .faqGroup .faq .question').click(function () {
        $(this).parent().find('.answer').toggle();
        if ($(this).find('.fa').hasClass('fa-plus'))
            $(this).find('.fa').removeClass('fa-plus').addClass('fa-minus');
        else
            $(this).find('.fa').removeClass('fa-minus').addClass('fa-plus');
    });

    autoResizeWindow();

    $('#showBlockContacts .ownerLinkText').click(function () {
        var $btn = $(this).parents('#showBlockContacts'),
            owner = $btn.data('owner'),
            $container = $('#blockContactsContainer');

        if (owner) {
            if (!$container.is(':visible')) {
                ajax(
                    'loadBlockContacts',
                    {
                        ownerID: $btn.data('owner'),
                        blockID: $btn.data('block')
                    },
                    function (result) {
                        if (result) {
                            $container.html(result || '');
                            $container.slideDown();

                            $btn.find('.ownerLinkText').html('Скрыть контакты');
                        } else {
                            modal('Достигнут лимит просмотров!');
                        }
                    }
                );
            } else {
                $container.slideUp();
                $btn.find('.ownerLinkText').html('Показать контакты');
            }
        }
    });

    $('.registerConfirm .panel-body input').keyup(function () {
        var $form = $(this).closest('form'),
            $btn = $form.find('button[type=submit]'),
            name = $form.find('input[name=Name]').val(),
            password = $form.find('input[name=Password]').val(),
            phone = $form.find('input[name=Phone]').val(),
            code = $form.find('input[name=Code]').val();

        if (name !== '' && password !== '' && phone !== '' && code !== '')
            $btn.prop('disabled', false);
        else
            $btn.prop('disabled', true);
    })
});

function initNoAgent() {
    $('.noAgent').off('click').click(function () {
        ajaxModal(
            $.cookie('auth') ? 'getLoginForm' : 'getRegistrationForm',
            {},
            function (data) {
                var $container = $(data.body);

                initMaskedInput($container);

                $('.loginLink a', $container).click(function () {
                    $.arcticmodal('close');
                    ajaxModal('getLoginForm');

                    return false;
                });
            }
        );

        return false;
    });
}

function htmlspecialchars(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function (m) {
        return map[m];
    });
}

function waitLayer(show) {
    var $waitLayer = $('.waitLayer'),
        $loadBox = $('.loadBox'),
        $content = $('#article');

    if (!$waitLayer.length) {
        $waitLayer = $('<div class="waitLayer"></div>');

        $('body').append($waitLayer);
    }

    if (!$loadBox.length)
        $loadBox = $('<div class="loadBox"><div><i class="fa fa-spin fa-refresh fa-5x fa-fw"></i></div></div>');

    if ($content.length) {
        $content.append($loadBox);
    } else {
        $waitLayer.append($loadBox);
    }

    var scrollHeight = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );

    if (show) {
        $waitLayer.css('height', scrollHeight + "px");
        $waitLayer.show();
        $loadBox.show();
    } else {
        $waitLayer.hide();
        $loadBox.remove();
    }
}

function initPlugins($container, pluginNames) {
    if (typeof pluginNames == 'string')
        pluginNames = [pluginNames];

    var plugins = {
        counter: function ($cont) {
            if (!$.cookie('IsMobile')) {
                $('.counter', $cont).counterUp({
                    delay: 10,
                    time: 1000
                });
            }
        },
        rating: function ($cont) {
            $('.rating', $cont).productRating();
        },
        scrollBar: function ($cont) {
            $('.content2', $cont).mCustomScrollbar({
                mouseWheel: {preventDefault: true}
            });
        },
        tooltipster: function ($cont) {
            $('.tooltip', $cont).tooltipster();
        },
        openDownBox: function ($cont) {
            var openFunc = function () {
                    var $this = $(this);

                    $this.toggleClass("active");
                    $this.prev().children().filter('.openDownBoxItem').toggleClass("activeItem");
                },
                $btn = $(".open_down_box_bottom", $cont);

            if (!$btn.hasClass('noAgent')) {
                $btn.unbind('click', openFunc);
                $btn.on('click', openFunc);
            }
        },
        controlBox: function ($cont) {
            var doFunc = function () {
                var $checkbox = $(this),
                    blockID = $checkbox.val();

                if ($checkbox.is(':checked')) {
                    Global.selectedBlocks.push(blockID);
                } else {
                    Global.selectedBlocks = Global.selectedBlocks.filter(function (item) {
                        return item != blockID;
                    });
                }

                Global.selectedBlocks = $.unique(Global.selectedBlocks);

                $checkbox.toggleClass("active");

                var count = Global.selectedBlocks.length,
                    $controlBox = $(".control_box"),
                    $buttons = $controlBox.find('button'),
                    $counterBox = $('.selectedBlocksCount', $controlBox),
                    ending = 'ий';

                if (count) {
                    $controlBox.addClass("active");
                } else {
                    $controlBox.removeClass("active");
                }

                if (count == 1) {
                    ending = 'ие';
                } else if (1 < count && count < 5) {
                    ending = 'ия';
                }

                $('.checkedBlocksCount', $counterBox).html(count);
                $('.ending', $counterBox).html(ending);

                $buttons.prop('disabled', !count);

                $('input[type=checkbox].blockSelecter[value=' + blockID + ']').not($checkbox).prop('checked', $checkbox.is(':checked'));

                if (Global.maps.searchMap && Global.maps.searchMap.markers.length)
                    Global.maps.searchMap.markers.map(function (item) {
                        var hasSelectedBlocks = $(item.data.BlocksStr.split(',')).filter(Global.selectedBlocks).get().length;

                        item.icons.default = hasSelectedBlocks ? 'poisk-green' : 'poisk';
                        item.icons.active = hasSelectedBlocks ? 'poisk-green-active' : 'poisk-active';
                        item.icons.watched = hasSelectedBlocks ? 'poisk-green-watched' : 'poisk-watched';

                        item.icons.current = ($('.map_item[data-parent=' + item.data.BuildID + ']').length) ? item.icons.active : (item.icons.used ? item.icons.watched : item.icons.default);

                        item.marker.setIcon(Global.markerPath + item.icons.current + '.png');

                        return item;
                    });
            };

            var $checkBox = $('input[type=checkbox].blockSelecter', $cont);

            $checkBox.unbind('click', doFunc);
            $checkBox.on('click', doFunc);
        },
        activeButton: function ($cont) {
            var setFunc = function () {
                var $btn = $(this);

                if (!$btn.hasClass('manualActive'))
                    $btn.addClass("active");
            };

            var $buttons = $('.btn_active', $cont);

            $buttons.unbind('click', setFunc);
            $buttons.on('click', setFunc);
        },
        openDownBoxMenu: function ($cont) {
            $(".open_down_box", $cont).on('click', function () {
                $(this).toggleClass("active").next().slideToggle("medium");
            });

            $('.open_down_box_one', $cont).click(function () {
                if ($(this).next('.open_down_box_one_cont').css("display") == "none") {
                    $('.open_down_box_one_cont').slideUp("medium");
                    $(this).next('.open_down_box_one_cont').slideToggle("medium");
                }

                else $('.open_down_box_one_cont').slideUp("medium");

                return false;
            });

            $(document).on('click', function (e) {
                if (!$(e.target).closest('.open_box_cl_wr').length)
                    $('.form_variant').slideUp("medium");

                e.stopPropagation();
            });

            $(document).on('click', function (e) {
                if (!$(e.target).closest('.open_down_box_one_cont_cl_wr').length)
                    $('.open_down_box_one_cont').slideUp("medium");

                e.stopPropagation();
            });
        },
        stickBox: function ($cont) {
            var $stickBox = $('.stick_box', $cont);

            if ($stickBox.length) {
                var start_pos = $stickBox.offset().top;

                $(window).scroll(function () {
                    var scrolled = $(window).scrollTop();

                    if (scrolled >= start_pos) {
                        if ($stickBox.hasClass() == false) {
                            $stickBox.addClass('to_top');
                        }
                    } else {
                        $stickBox.removeClass('to_top');
                    }
                });
            }
        },
        selectDropDown: function ($cont) {
            $(document).bind('click', function (e) {
                var $clicked = $(e.target);

                if (!$clicked.parents().hasClass("dropdown_form"))
                    $(".dropdown_form dd").hide();
            });

            $('.mutliSelect input[type="checkbox"]', $cont).on('click', function () {
                var $this = $(this),
                    title = $this.data('label'),
                    $dropCont = $this.closest('.dropdown');

                if ($(this).is(':checked')) {
                    $('.multiSel', $dropCont).append('<span title="' + title + '">' + title + '</span>');
                } else {
                    $('span[title="' + title + '"]', $dropCont).remove();
                }
            });
        },
        selectDropDownRadio: function ($cont) {
            $(document).bind('click', function (e) {
                var $clicked = $(e.target);

                if (!$clicked.parents().hasClass("dropdown_form"))
                    $(".dropdown_form dd").hide();
            });

            $('.mutliSelectRadio input[type="radio"]', $cont).on('click', function () {
                var title = $(this).attr("data-label") + " ";

                if ($(this).is(':checked')) {
                    var html = '<span title="' + title + '">' + title + '</span>';

                    $('.multiSelRadio').html(html);
                } else {
                    $('span[title="' + title + '"]').remove();

                    var ret = $(".hida");

                    $('.dropdown_radio dt .select_form').append(ret);
                }
            });
        },
        checkbox: function ($cont) {
            var $checkbox = $('input[type=checkbox].checkbox', $cont),
                $iCheckbox = $('input[type=checkbox].iCheckbox', $cont),
                checkFunc = function () {
                    var $this = $(this);

                    if (!$this.is(':checked')) {
                        $this.removeClass('active');
                    } else {
                        $this.addClass('active');
                    }
                };

            $checkbox.each(checkFunc);
            $checkbox.change(checkFunc);

            $iCheckbox.each(checkFunc);
            $iCheckbox.change(checkFunc);
        }
        /*googleMaps: function ($cont) {
            var styleArray = [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}],
                mapSettings = {
                    scrollwheel: false,
                    styles: styleArray,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 10
                };

            $('.gmap').each(function(i, el){
                var dataCoords = $(el).data('coords') ? eval($(el).data('coords')) : [],
                    id = $(el).attr('id'),
                    markers = [],
                    mapPlace = $(this).parent().find('.map_item'),
                    qtMarkers = dataCoords.length,
                    map = null,
                    mapContainer = document.getElementById(id);

                mapSettings.center = ($(el).data('lat') && $(el).data('lng'))
                    ? {lat: $(el).data('lat'), lng: $(el).data('lng')}
                    : dataCoords[0];

                if (!$(this).hasClass('notAutoActive')) {
                    map = new google.maps.Map(
                        mapContainer,
                        mapSettings
                    );

                    if (qtMarkers.length) {
                        for (var i = 0; i < qtMarkers; i++) {
                            var marker = new google.maps.Marker({
                                map: map,
                                position: dataCoords[i],
                                icon: '/img/design/marker.png',
                                label: dataCoords[i].count + '',
                                title: dataCoords[i].title
                            });

                            markers.push(marker);

                            markerInfo(marker, i, qtMarkers, markers, $(mapPlace[i]));
                        }
                    }
                }

                Global.maps[id] = {
                    map: map,
                    container: mapContainer,
                    markers: markers,
                    settings: mapSettings
                };
            });

            function markerInfo(marker, index, qt, markers, mapPlace) {
                marker.addListener('click', function () {
                    for (var i = 0; i <= qt - 1; i++) {
                        // var number = i+ 1 + '';
                        if (!(i == index)){
                            markers[i].setIcon('/img/design/marker.png');
                            // markers[i].setLabel(number);
                        }
                    }

                    marker.setIcon('/img/design/marker_active.png');
                    marker.setLabel('');

                    mapPlace.show().siblings('.map_item').hide();

                    $('.map_item_close').on('click',function () {
                        $(this).closest('.map_item').hide();

                        for (var i = 0; i <= qt - 1; i++) {
                            var number = i + 1 + '';

                            markers[i].setIcon('/img/design/marker.png');
                            markers[i].setLabel(number);
                        }
                    });
                });
            }
        }*/
    };

    pluginNames = pluginNames || Object.keys(plugins);
    $container = $container || $(document);

    for (var i in pluginNames) {
        var pluginFunction = plugins[pluginNames[i]];

        if (typeof pluginFunction == 'function')
            pluginFunction($container);
    }

    $(".initHtmlPopover").popover({
        container: 'body',
        html: true,
        content: function () {
            var content = $(this).attr("data-popover-content");
            return $(content).children(".popover-body").html();
        },
        title: function () {
            var title = $(this).attr("data-popover-content");
            return $(title).children(".popover-heading").html();
        }
    });
}


function initSendSupportRequest() {
    $('.sendSupportRequest').off('click').click(function () {
        var $this = $(this);

        ajax(
            'sendSupportRequest',
            $('#supportRequestForm').serialize(),
            function (result) {
                if (result.success) {

                    $this.parents('form').find('[name="Request"]').val("");
                    $this.tooltip({
                        placement: 'right',
                        title: result.info,
                        trigger: 'manual'
                    });

                    $this.tooltip('show');

                    setTimeout(function () {
                        $this.tooltip('destroy');
                    }, 10000);
                } else {
                    if (result.errors) {
                        modal(result.errors.join('<br>'));
                    }
                }
            }
        );
        return false;
    });
}

function initProfile() {
    initUpdatePdfContactInfo();

    $("#Birthday").datepicker({
        format: "dd.mm.yyyy",
        language: "ru",
        orientation: "bottom auto"
    });

    $('.saveProfileForm').click(function () {
        var $this = $(this);

        ajax(
            'saveProfile',
            $('#profileForm').serialize(),
            function (result) {
                if (result.success) {
                    $this.tooltip({
                        placement: 'left',
                        html: true,
                        title: result.info.join('<br>'),
                        trigger: 'manual'
                    });

                    $('input[name=Password],input[name=NewPassword]').val('');

                    $this.tooltip('show');

                    setTimeout(function () {
                        $this.tooltip('destroy');
                    }, 10000);

                    updateProfilePDF();
                } else {
                    if (result.errors) {
                        modal(result.errors);
                    }
                }
            }
        );

        return false;
    });

    $('#phoneConfirmContainer').click(function () {
        ajaxModal(
            'getPhoneModal',
            {
                Modal: 1
            },
            function (modal) {
                setUserPhoneContainer(modal.body);
            }
        );
    });
}

function setUserPhoneContainer($container) {
    var $saveButton = $('.saveStatus', $container),
        $form = $('form', $container),
        $sendCodeButton = $('.resetCodeBtn', $container);

    initMaskedInput($container);

    if ($sendCodeButton.length) {
        var interval;

        $sendCodeButton.click(function (e) {
            e.stopPropagation();

            ajax(
                'sendSmsCode',
                {
                    Part: 'UserPhone',
                    Phone: $form.find('input[name=Phone]').val()
                },
                function (result) {
                    if (result.Success) {
                        var $timerBox = $('.confirmTimer', $container),
                            timeout = result.Timeout || 0;

                        $timerBox.attr('data-timeout', timeout);

                        clearInterval(interval);

                        interval = setInterval(
                            function () {
                                var time = parseInt($timerBox.attr('data-timeout')) - 1;

                                if (time > 0) {
                                    $timerBox.html(time + ' c.');
                                    $timerBox.attr('data-timeout', time);
                                } else {
                                    $timerBox.html('');
                                    $timerBox.attr('data-timeout', 0);

                                    clearInterval(interval);
                                }
                            },
                            1000
                        );
                    }
                }
            );

            return false;
        });
    }

    if ($saveButton.length) {
        $saveButton.click(function () {
            var data = $form.serializeArray();

            data.push({
                name: 'StatusID',
                value: $saveButton.data('status')
            });

            ajax(
                'saveUserPhoneStatus',
                data,
                function (result) {
                    if (result.end) {
                        $.arcticmodal('close');

                        ajax(
                            'getPhoneModal',
                            {
                                Modal: 0
                            },
                            function (result) {
                                $('#phoneConfirmContainer').html(result);
                            }
                        );
                    } else if (result.html) {
                        $container.html(result.html);

                        setUserPhoneContainer($container);
                    }
                }
            );

            return false;
        });
    }
}

function updateProfilePDF() {
    var $container = $('#profileForm .profilePdfInfo');

    if ($container.length) {
        $container.animate({opacity: 0}, 400, null, function () {
            ajax(
                'getProfilePdf',
                {},
                function (result) {
                    $container.html(result);
                    initUpdatePdfContactInfo(true);

                    $container.animate({opacity: 1}, 400);
                }
            );
        });
    }
}

function initSelectPickers() {
    $('.selectpicker').selectpicker();
}

function getPDF(params) {
    params = {
        name: params.name,
        parents: is_array(params.parents) ? params.parents.join(',') : params.parents,
        additions: params.additions || {}, //доп параметры
        before: (typeof params.before == 'function') ? params.before : function () {
        },
        after: (typeof params.after == 'function') ? params.after : function () {
        }
    };

    var timer;

    if (params.name && params.parents) {
        params.before();
        var request = params.additions;

        request.Name = params.name;
        request.Parents = params.parents;

        // timer = setInterval(
        //     function () {
        ajax(
            'pdfGenerate',
            request,
            function (result) {
                if (result.success) {
                    clearInterval(timer);

                    params.after({
                        id: result.PdfID,
                        link: result.Link
                    });
                }
            }
        );
        /* },
            500
        );*/
    }
}

function initPDFEditor($container) {
    $('.pdfEditor', $container).click(function () {
        var $this = $(this);

        /*ajaxModal(
            'getPdfEditorConfirm',
            {link: $this.attr('href')},
            function (data, el, responce) {
                $('a', data.body).click(function () {
                    $.arcticmodal('close');
                });

                $('button', data.body).click(function () {
                    $.arcticmodal('close');

                    var pdf = new PDF({
                        name: $this.data('name'),
                        parents: ($this.data('parents') + '').split(',')
                    });
                });
            }
        );*/

        var pdf = new PDF({
            name: $this.data('name'),
            parents: ($this.data('parents') + '').split(',')
        });

        return false;
    });
}

function initPDFLink($container) {
    $('.pdfLink:not(.manualActive)', $container || $(document)).click(function () {
        var $this = $(this);

        if (!$this.hasClass('loading')) {
            var $icon = $this.find('.loadIcon'),
                $oldContent = $icon.html();

            getPDF({
                name: $this.data('name'),
                parents: $this.data('parents'),
                before: function () {
                    $icon.html($('<span class="fa fa-spinner fa-pulse middle"></span>'));
                    $this.addClass('loading');
                },
                after: function (result) {
                    $icon.html($oldContent);
                    $this.removeClass('loading');

                    window.open(result.link);
                }
            });
        }

        return false;
    });
}

function initModal() {
    $('.box-modal[data-opened=1]').each(function () {
        modal($(this), {customContent: true});
    });
}

function initNumberInput($container) {
    var $numberInputs = $('input.number', $container || $(document));

    $numberInputs.keypress(function (e) {
        if (e.ctrlKey || e.altKey || e.metaKey)
            return;

        var allowedChars = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        if (!in_array(e.key * 1, allowedChars))
            return false;
    });

    $numberInputs.each(function () {
        var $this = $(this);

        setInterval(function () {
            var val = $this.val(),
                newVal = number(clearStrFrom(val));

            if (!(window.getSelection().toString().length && $this.is(':focus')) && newVal != val)
                $this.val(newVal);
        }, 300);
    });
}

function initButtonsSelect($container) {
    return $('.buttonsSelect:not([data-manual="1"])', $container || $(document)).get().map(function (item) {
        return new ButtonsSelect($(item));
    });
}

function initPersonNameChange() {
    var $addBtn = $('.plusContactTab'),
        width = $addBtn.is(':hidden') ? 100 : 88,
        $personsLabels = $('#contactsTabs .nav-tabs > li:not(.plusContactTab)'),
        count = $personsLabels.length;

    $personsLabels.css('max-width', 'calc(' + parseInt(width / count) + "% + 1px)");

    $('.personName input').off('keyup').on('keyup', function () {
        var $this = $(this),
            text = htmlspecialchars($this.val());

        if (text === '')
            text = "Контакт " + ($('[href="#' + $this.parents('.tab-pane').attr('id') + '"]').parents('li').index() + 1);

        $('[href="#' + $this.parents('.tab-pane').attr('id') + '"] span', $personsLabels).html(text);
    });
}

function initAutoTextareaSize($container) {
    $('textarea.autoSize', $container || $('body')).each(function () {
        var $this = $(this),
            maxHeight = $this.data('max-height');

        function resize() {
            $this[0].style.height = 'auto';

            var defaultHeight = $this[0].scrollHeight,
                height = (maxHeight ? Math.min(maxHeight, defaultHeight) : defaultHeight) + 'px';

            $this.height(height);
            $this.css('max-height', height);
        }

        function delayedResize() {
            window.setTimeout(resize, 0);
        }

        $this.on('change', resize);
        $this.on('cut', delayedResize);
        $this.on('paste', delayedResize);
        $this.on('drop', delayedResize);
        $this.on('keydown', delayedResize);

        this.focus();
        this.select();

        resize();

        this.selectionStart = this.selectionEnd;
    });
}

function autoResizeWindow() { // todo - посмотреть связь с core.js - там тоже полно расчетов на высоту
    var $section = $('section:not(.notAutoSize)'),
        newHeight = $(window).height() - $('header').outerHeight(true) - $('.mainMenu').outerHeight(true) - ($section.data('no-footer') ? 0 : $('footer').outerHeight(true));

    if ($section.size())
        $section.css('minHeight', newHeight);

    if ($('.searchContent').length)
        $('.searchContent').css('min-height', newHeight - $('.searchHeader').height());
}

function initClientListSearch(blockID) {
    var clientKeyTimer2,
        $bar = $('.agentBar');

    var setClientsList = function (result) {
            $('#selectClientForSearch .search_agent_list').html(result);
        },
        currentValue = $('#selectClientForSearch .searchInput').val();

    $('#selectClientForSearch .searchInput').on('keyup', function () {
        var $this = $(this);

        clearTimeout(clientKeyTimer2);

        if ($this.val().length >= 2 || !$this.val()) {
            clientKeyTimer2 = setTimeout(function () {
                getClientList(
                    $this.val(),
                    {blockID: blockID, link: $bar.data('link')},
                    setClientsList
                );

            }, 500);
        }
    });

    $('#selectClientForSearch .btn_search_agent').click(function () {
        getClientList(
            $('#selectClientForSearch .searchInput').val(),
            {blockID: blockID},
            setClientsList
        );
    });
}

function initClientSearch() {
    var clientKeyTimer,
        $container = $('.clientsList');

    $('.clientSearch', $container).on('keyup', function () {
        var $this = $(this);

        clearTimeout(clientKeyTimer);

        if ($this.val().length >= 2 || !$this.val()) {
            clientKeyTimer = setTimeout(function () {
                updateClientsList(
                    1,
                    $this.val(),
                    $container.find('table > thead .tableSort:not([data-column=""])').get().map(function (item) {
                        var $item = $(item);

                        return {
                            Column: $item.data('column'),
                            Sort: $item.data('sort') ? $item.data('sort') : ''
                        };
                    })
                );
            }, 500);
        }
    });

    $('.pagination button', $container).click(function () {
        updateClientsList(
            $(this).data('page'),
            $('.clientSearch', $container).val(),
            $container.find('table > thead .tableSort:not([data-column=""])').get().map(function (item) {
                var $item = $(item);

                return {
                    Column: $item.data('column'),
                    Sort: $item.data('sort') ? $item.data('sort') : ''
                };
            })
        );
    });

    $('button.searchButton', $container).click(function () {
        updateClientsList(
            1,
            $('.clientSearch', $container).val(),
            $container.find('table > thead .tableSort:not([data-column=""])').get().map(function (item) {
                var $item = $(item);

                return {
                    Column: $item.data('column'),
                    Sort: $item.data('sort') ? $item.data('sort') : ''
                };
            })
        );
    });

    $('.tableSort:not([data-column=""])', $container).click(function () {
        var $this = $(this),
            $allSorts = $this.closest('thead').find('.tableSort:not([data-column=""])'),
            sorts = [
                {name: '', class: ''},
                {name: 'ASC', class: 'icon_arrow icon_arrow_active'},
                {name: 'DESC', class: 'icon_arrow'}
            ],
            sortsClasses = trim(array_column(sorts, 'class').join(' ')),
            sortNum = ($this.data('sortNum') || 0) + 1;

        sortNum = (sortNum < 3) ? sortNum : 0;

        $allSorts.each(function () {
            var $sortBtn = $(this);

            $sortBtn.removeClass(sortsClasses);
            $sortBtn.data('sort', '');
            $sortBtn.data('sortNum', 0);
        });

        $this.addClass(sorts[sortNum].class);

        $this.data('sort', sorts[sortNum].name);
        $this.data('sortNum', sortNum);

        updateClientsList(
            0,
            $('.clientSearch', $container).val(),
            /*$thead.find('.tableSort:not([data-column=""])')*/
            $this.get().map(function (item) {
                var $item = $(item);

                return {
                    Column: $item.data('column'),
                    Sort: $item.data('sort') ? $item.data('sort') : ''
                };
            })
        );
    });
}

function getClientList(value, params, func) {
    params = params || {};
    params.value = value;

    ajax(
        'getClientList',
        params,
        func
    );
}

function initAutoOpenSearchFilters() {
    var $searchFilter = $('#searchFilter');

    if ($searchFilter.size()) {
        $('#searchFilter ul.left_nav > li:not(.notSave)').click(function (e) {
            e.stopPropagation();

            var cookie = $.cookie('openedSearchFilters') || {},
                $this = $(this);

            cookie[$this.attr('id')] = $this.find('.filter_btn').hasClass('active');

            $.cookie('openedSearchFilters', cookie);
        });
    }
}

function updateClientsList(page, searchValue, sorts) { //аяксовая загрузка таблицы с пагинацией и поиском (можно переделать не только для клиентов)
    var $container = $('.clientsList');

    if ($container.size()) {
        ajax(
            'clientsSearch',
            {
                value: searchValue,
                page: page || 0,
                sorts: sorts ? sorts : [],
                archived: ($('[data-table]', $container).data('table') == 'archived') + 0 //TODO переделать?
            },
            function (result) {
                var $trContainer = $container.find('> table > tbody');

                $trContainer.empty();
                $trContainer.append(result.rows);

                $container.find('.pagination').remove();

                if (result.pagination) {
                    $container.append(result.pagination);

                    $('.pagination button', $container).click(function () {
                        updateClientsList(
                            $(this).data('page'),
                            $('.clientSearch', $container).val()
                        );
                    });
                }

                initPlugins($container, ['openDownBoxMenu']);
            }
        );
    }
}

function initShowGallery($container) {
    $container = $container || $(document);

    var groups = $.unique($('.showGallery[data-group]:not(.manualActive)', $container).get().map(
        function (item) {
            return $(item).data('group') || '';
        }
    )).filter(function (item) {
        return item;
    });

    $(groups).each(function () {
        new ModalGallery(
            $('.showGallery[data-group=' + this + ']:not(.manualActive)', $container),
            {
                removeOnClose: true
            }
        );
    });

    $('.showGallery:not([data-group]):not(.manualActive)', $container).each(function () {
        new ModalGallery(
            $(this),
            {
                removeOnClose: true
            }
        );
    });

    $('.searchContent .buildItem', $container).each(function () {
        new ModalGallery(
            $('.showGallery', $(this)),
            {
                removeOnClose: true,
                handlers: {
                    miniGalleryItem: function ($item) {
                        var result = false;

                        if ($item.hasClass('lastImage')) {
                            var additionalClass = '';
                            if ($item.hasClass('noAgent')) {
                                additionalClass = 'noAgent';
                            }
                            result = $(
                                '<a target="_blank" class="lastImage ' + additionalClass + '" data-title="' + $item.data('title') + '" href="' + $item.attr('href') + '"></a>'
                            );
                            result.append($item.find('img').clone())
                        }

                        return result;
                    }
                }
            }
        );
    });
}

function initGalleryWrappers($container, params) {
    $('.galleryWrapper:not(.active)', $container || $(document)).each(function () {
        new GalleryWrapper(
            $(this),
            params || {}
        );
    });
}

function initMetroMap() {
    $('.metroMap').each(function () {
        new MetroMap($(this));
    });
}

function initMaskedInput($container) {
    var $maskedInput = $('.maskedInput', $container || $(document));

    if ($maskedInput.size() && (typeof $().mask == 'function'))
        $maskedInput.each(function () {
            var $this = $(this),
                mask = $this.data('mask') || '(999) 999-9999';

            if (mask)
                $this.mask(mask);
        });
}

function searchChangeCostSliders() {
    var $costBox = $('#SearchCost'),
        $labels = $('.labels', $costBox);

    $labels.find('[data-dir=min]').html(number($('#CostFrom', $costBox).val()));
    $labels.find('[data-dir=max]').html(number($('#CostTo', $costBox).val()));
}

function searchChangeSquareSliders() {
    var $squareBox = $('#SearchSquares'),
        $labels = $('.labels', $squareBox);

    $labels.find('[data-dir=min]').html(number($('#SquareFrom', $squareBox).val()));
    $labels.find('[data-dir=max]').html(number($('#SquareTo', $squareBox).val()));
}

function initSiteGalleries() {
    $('.siteGallery:not(.manualActive)').each(function () {
        var gallery = new SiteGallery(
            $(this),
            {
                add: true
            }
        );
    });

    $('.siteSingleGallery:not(.manualActive)').each(function () {
        var gallery = new SiteSingleGallery($(this));
    });

    $('#profileForm .siteSingleGallery.manualActive').each(function () {
        var gallery = new SiteSingleGallery(
            $(this),
            {
                on: {
                    load: updateProfilePDF,
                    delete: updateProfilePDF
                }
            }
        );
    });

    $('#profileForm .siteGallery.manualActive').each(function () {
        var gallery = new SiteGallery(
            $(this),
            {
                add: true,
                on: {
                    load: updateProfilePDF,
                    delete: updateProfilePDF
                }
            }
        );
    });
}

function initRegister() {
    var $form = $('.registerConfirm form');

    if ($form.length) {
        var $codeButton = $('.confirmCode button', $form),
            interval;

        $codeButton.click(function (e) {
            e.stopPropagation();

            ajax(
                'sendSmsCode',
                {
                    Part: 'Registration',
                    Phone: $form.find('input[name=Phone]').val(),
                    Key: $form.find('input[name=Key]').val()
                },
                function (result) {
                    if (result.Success === true) {
                        $codeButton.addClass('btn-labeled');
                        $codeButton.attr('data-label', result.Timeout || 0);
                        $('.phoneError').html('');

                        clearInterval(interval);

                        interval = setInterval(
                            function () {
                                var time = parseInt($codeButton.attr('data-label')) - 1;

                                if (time > 0) {
                                    $codeButton.attr('data-label', time);
                                } else {
                                    $codeButton.removeClass('btn-labeled');

                                    clearInterval(interval);
                                }
                            },
                            1000
                        );

                        $codeButton.prop("disabled", true);
                        setInterval(
                            function () {
                                $codeButton.prop("disabled", false);
                            },
                            30000
                        );
                    } else {
                        console.log(result.Errors);
                        $('.phoneError').html(result.Errors['Phone']);
                    }
                }
            );

            return false;
        });
    }


    $('.registerTabs li a').click(function () {
        var href = $(this).attr('href').substr(1);
        if (href == 'forAgent') {
            $("input[name='Group']").val('agent');
        }
        else {
            $("input[name='Group']").val('owner');
        }

        $('.changeInformation').removeClass('active');
        $('.changeInformation.' + href).addClass('active');
    });

    if ($("input[name='Group']").val() == 'owner') {
        $(".registerTabs li a[href='#forOwner']").click();
    }
}

/**
 * Добавление зданий
 */
function initAddBuildSteps() {
    var $stepLinks = $('#addBuildSteps'); //левое меню

    if ($stepLinks.size()) {
        var setActive = function (step) { //функция переключения вкладок на определенный шаг добавления
            var $contents = $('.addBuild'),
                $lis = $('li', $stepLinks);

            $contents.hide(); //скрываем весь контент
            $contents.filter('[data-step=' + step + ']').show(); //показываем только контент запрошенного шага

            $lis.removeClass('active'); //делаем нужный пункт меню активным
            $lis.filter('[data-step=' + step + ']').addClass('active');
        };

        $('a', $stepLinks).click(function () { //активируем меню
            var $li = $(this).parent();

            if (!$li.hasClass('active'))
                setActive($li.data('step'));

            return false;
        });

        $('.prevStep, .nextStep', $('.addBuild')).click(function () { //кнопки назад и далее
            var $this = $(this), //кнопка
                inc = $this.hasClass('nextStep') ? 1 : -1, //инкремент шага (следущий/предыдущий)
                activeFunc = $this.data('function'), //имя функции, которая будет выполнена перед переходом к следущему/предыдущему шагу
                defaultFunc = function () { //функция перехода к слудущему/предыдущему шагу
                    setActive($('li.active', $stepLinks).data('step') + inc);
                };

            if (activeFunc) { //если была указана функция, то выполняем ее (в качестве аргументов форма текущего шага и функция перехода)
                exec(activeFunc, $this.closest('form'), defaultFunc)
            } else { //если не указана, то просто переходим на следущий/предыдущий шаг
                defaultFunc();
            }
        });
    }
}

function initDefaultMaps($map) {
    var $defaultMaps = $map || $('.defaultMap');

    if ($defaultMaps.size())
        $defaultMaps.each(function () {
            var $map = $(this),
                coords = {
                    lat: $map.data('lat'),
                    lng: $map.data('lng')
                },
                bcMap = new google.maps.Map(
                    $map[0],
                    {
                        center: coords,
                        zoom: $map.data('zoom') || 8,
                        scrollwheel: false
                    }
                ),
                markerName = $map.data('marker');

            if (markerName)
                var marker = new google.maps.Marker({
                    map: bcMap,
                    position: coords,
                    icon: '/img/design/gmarkers/mredact.png',
                    title: $map.data('title')
                });
        });
}

function initClientPicker() {
    /*var $clientsForm = $('#selectClientForSearch');

    if ($clientsForm.size()) {
        $('a', $clientsForm).click(function (e) {
            $('#selectedClient', $clientsForm).val($(this).data('client'));
            $clientsForm.submit();

            return false;
        });
    }*/
}

function initBlocksFilter() {
    var $blocksFilter = $('#blocksFilter a');

    if ($blocksFilter.size()) {
        var $blocksBox = $('#blocksBox li');

        $blocksFilter.click(function () {
            var $this = $(this),
                sortParam = $this.data('filter');

            $blocksFilter.removeClass('active');
            $this.addClass('active');

            if (sortParam) {
                $blocksBox.hide();
                $blocksBox.filter('[data-filter=' + sortParam + ']').show();
            } else {
                $blocksBox.show();
            }

            return false;
        });
    }
}

/**
 * Валидация формы добавления клиента.
 * @param $btn            Кнопка
 * @param $container    Див с контентов шага (таба)
 * @param stepCtrl        StepTabs
 */
function validateClient($btn, $container, stepCtrl) {
    var curStep = stepCtrl.getCurStep(),
        $form = $container.closest('form'),
        errors = {},
        valParams = {
            Contacts: {
                Name: {
                    regExp: /^.+$/gi,
                    required: true
                },
                Comment: /^.*$/gi
            },
            SearchParams: {
                TypeID: {
                    regExp: /^[\d]+$/gi
                },
                DealTypeID: {
                    regExp: /^[\d]+$/gi,
                    required: true
                },
                SquareFrom: {
                    regExp: /^[\d]+$/gi,
                    required: true
                },
                SquareTo: {
                    regExp: /^[\d]+$/gi,
                    required: true
                },
                CostFrom: {
                    regExp: /^[\d]+$/gi,
                    required: true
                },
                CostTo: {
                    regExp: /^[\d]+$/gi,
                    required: true
                }
            }
        },
        curValParams = valParams[curStep];

    for (var filedName in curValParams) {
        var pattern = curValParams[filedName],
            $field = $($form[0][filedName]),
            patternSuccess;

        patternSuccess = (pattern.regExp || pattern).test($field.val());

        if (!patternSuccess) {
            errors[filedName] = 'Не' + (!(pattern.required && !$field.val()) ? 'правильно' : '') + ' заполнено поле ' + $field.data('label') + '!';
        }
    }

    $('.error', $form).hide();
    $('.error', $form).html('');

    if (!Object.keys(errors).length) {

        if ($btn.attr('type') == 'submit') {
            $form.submit();
        } else {
            stepCtrl.next();
            initAllSliders();
        }
    } else {
        for (var fieldName in errors) {
            var $error = $('.error[for=' + fieldName + ']', $form);

            $error.html(errors[fieldName]);
            $error.css('display', 'block');
        }
    }
}

function checkBuildParams($form, func) {
    func = func || false;

    pre($form);
}

function initAddClient() {
    $('.newClientParams .dealTypeRadio input[type=radio][name=DealTypeID]').change(function () {
        var $this = $(this),
            conf = {
                'DealTypeID1': 'Rent',
                'DealTypeID2': 'Sale'
            },
            curDealType = conf['DealTypeID' + $this.val()],
            minMaxParams = $this.data('minmax'),
            $squareSlider = $('#SquareClientSlider'),
            $costSlider = $('#CostClientSlider'),
            $costRanger = $('.costParam');

        $costRanger.find('.costLabel').html(minMaxParams[curDealType + 'CostLabel']);
        $costRanger.find('.costPostLabel').html(minMaxParams[curDealType + 'CostPostLabel']);

        console.log(minMaxParams);

        var s1 = new Slider($squareSlider, {
            min: parseInt(clearStrFrom(minMaxParams['Min' + curDealType + 'Square'])),
            max: parseInt(clearStrFrom(minMaxParams['Max' + curDealType + 'Square'])),
            from: parseInt(clearStrFrom($('[name=SquareFrom]', $squareSlider).val())),
            to: parseInt(clearStrFrom($('[name=SquareTo]', $squareSlider).val()))
        });

        var s2 = new Slider($costSlider, {
            min: parseInt(clearStrFrom(minMaxParams['Min' + curDealType + 'Cost'])),
            max: parseInt(clearStrFrom(minMaxParams['Max' + curDealType + 'Cost'])),
            from: parseInt(clearStrFrom($('[name=CostFrom]', $costSlider).val())),
            to: parseInt(clearStrFrom($('[name=CostTo]', $costSlider).val())),
            postLabel: minMaxParams[curDealType + 'CostPostLabel'],
            label: minMaxParams[curDealType + 'CostLabel']
        });

        //console.log(s1, s2);
    });
}

/**
 * Отбор блока клиенту
 * @param btn
 * @param blockID
 */
function selectBlockForClient(btn, blockID) {
    var $btn = $(btn),
        clientID = $btn.data('client'), //clientID
        saveBlockForClient = function (pClientID, $modal) { //отбор блока клиенту
            ajax(
                'selectBlockForClient',
                {
                    clientID: pClientID,
                    blockID: blockID
                },
                function (result) {

                    if (result.success) {
                        $btn.closest('.blockItem').addClass('selected');

                        if (!$btn.hasClass('noActive'))
                            $btn.addClass('active');
                    }

                    var $counter = $('.agentSearchPage [data-page="selected"] span.counter');

                    $counter.html(result.SelectedCount);

                    if ($modal && $modal.length)
                        $modal.arcticmodal('close');

                    if (!result.success && result.isDouble)
                        modal('Данное помещение уже отобрано для клиента «' + result.clientName + '»!');

                    /*if (!result.SelectedCount) {
                        $counter.hide();
                    } else {
                        $counter.show();
                    }*/
                }
            );
        },
        $form = $('#searchFilter');

    if ($btn.hasClass('noAgent'))
        return false;

    if (!clientID) {
        getModalClientList(
            {
                addClient: true/*$form.size() > 0*/,
                afterAddClient: saveBlockForClient,
                blockID: blockID
            },
            function (data, el, responce, $modal) {
                var $clientsList = data.body.children().first().find('.search_agent_list');

                if ($clientsList.size()) {
                    var searchID = $('input[type=hidden][name=SearchID]', $form).val();

                    $clientsList.find('li a').click(function () {
                        var listClientID = $(this).data('id');

                        saveBlockForClient(listClientID, $modal); //отбираем блок для клиента
                        loadClient(listClientID); //подгружаем клиента

                        /*ajax( //добавляем клиенту этот поиск
                            'logBrokerSearch',
                            {
                                clientID: clientID,
                                searchID:  searchID
                            }
                        );*/

                        return false;
                    });
                }
            }
        );
    } else {
        saveBlockForClient(clientID);
    }
}

function loadClient(clientID) {
    if (clientID) { //если клиент был создан, то отбираем ему блок и подтягиваем интерфейс клиента
        var $form = $('#searchFilter'),
            $selectedBtns = $('.selectBlockForClientBtn'),
            activePage = $form.find('.activeSearchPage').val(),
            $bar = $('.agentBar');

        if ($form.length && $bar.length) {
            $form.find('input[type=hidden].clientValue').val(clientID);
            $selectedBtns.data('client', clientID);
            $selectedBtns.removeClass('manualActive');

            ajax(
                'getAgentClientBar',
                {
                    ClientID: clientID,
                    link: $bar.data('link'),
                    currentSearchCount: $('.agentSearchPage [data-page=""] .read_label_2').html(),
                    activeSearchPage: (activePage == undefined) ? 'none' : activePage,
                    addclient: $bar.data('addclient')
                },
                function (result) {
                    $bar.replaceWith(result);

                    initAgentBar();
                }
            );
        }
    }

    $.arcticmodal('close');
}

function addNewClient(name, searchID, func) {
    name = name.trim();
    func = func || function () {
    };

    if (name && searchID) { //если есть имя клиента и id поиска
        ajax( //создаем клиента
            'saveNewClient',
            {
                searchID: searchID,
                clientName: name
            },
            func
        );
    }
}

function initAgentBar() {
    var $bar = $('.agentBar');

    $('.openClientsList', $bar).click(function () {
        if (!$(this).hasClass('noAgent'))
            getModalClientList(
                {
                    addClient: $bar.data('addclient'),
                    link: $bar.data('link')
                }
            );

        return false;
    });
}

function initBuildSlider() {
    var $slider = $('.buildImagesSlider');

    if ($slider.size()) {
        $slider.each(function () {
            var $this = $(this), //old
                $items = $('.buildImages', $this),
                gallery = new ModalGallery(
                    $items.clone(),
                    {removeOnClose: false}
                ),
                $autoOpenItem = $items.filter('.autoOpen');

            if ($items.filter('.autoOpen'))
                gallery.open($items.index($autoOpenItem));

            $items.click(function () {
                var item = $items.index(this);

                gallery.open((item != -1) ? item : 0);

                return false;
            });

            /*var $this = $(this), //old
                $items = $('.buildImagesSliderList', $this).children(),
                $shower = $('.buildImagesSliderShower', $this),
                updateOpenModalGalleryEvent = function ($shower) {
                    var $showerA = $shower.find('a'),
                        clickFunc = function () {
                            var gallery = new ModalGallery(
                                    $items.find('a').clone(),
                                    {removeOnClose: true}
                                ),
                                item = $items.index($items.filter('.active')[0]);

                            gallery.open((item != -1) ? item : 0);

                            return false;
                        };

                    if ($showerA.size()) {
                        $showerA.click(clickFunc);
                        $shower.click(clickFunc);
                    } else {
                        $shower.unbind(clickFunc);
                    }
                };

            if ($shower.is(':empty')) {
                var $activeItem = $items.filter('.active'),
                    $firstItem = $items.first(),
                    $item = $activeItem.size() ? $($activeItem[0]) : $firstItem;

                $shower.append($item.find('a').clone());

                $items.removeClass('active');
                $item.addClass('active');
            }

            updateOpenModalGalleryEvent($shower);

            $('a', $items).click(function () {
                var $a = $(this);

                $items.removeClass('active');
                $a.parent().addClass('active');

                $shower.empty();
                $shower.append($a.clone());

                updateOpenModalGalleryEvent($shower);

                return false;
            });*/
        });
    }
}

function getModalClientList(params, func) {
    func = func || function () {
    };
    params = params || {};

    ajaxModal(
        'getModalClientList',
        {
            addClient: params.addClient + 0,
            link: params.link || '',
            blockID: params.blockID
        },
        function (data, el, responce) {
            initClientListSearch(params.blockID);

            var $modal = data.body.children().first(), //модалка
                $container = $modal.find('.newClientName'),
                $error = $container.find('.error'),
                $clientListContainer = $modal.find('.search_agent_list'),
                searchID = $('#searchFilter input[type=hidden][name=SearchID]').val() || $.cookie('LastSearchID');

            $clientListContainer.css('min-height', $clientListContainer.height());

            if (params.addClient && searchID) {
                $('.saveNewClient', $container).click(function () {
                    var name = $container.find('#newClientName').val().trim();

                    if (searchID && name) {
                        addNewClient(
                            name,
                            searchID,
                            function (result) {
                                var clientID = result.clientID;

                                if (clientID) {
                                    loadClient(clientID);

                                    if (typeof params.afterAddClient == 'function')
                                        params.afterAddClient(clientID);
                                } else {
                                    $error.html(result.errors.join('<br>'));
                                    $error.css('display', 'inline-block');
                                }
                            }
                        );
                    } else { //обработка противных случаев
                        var errors = {
                            name: 'Укажите название клиента!',
                            searchID: 'Требуется перезагрузка страницы!'
                        };

                        $error.html(errors[!searchID ? 'searchID' : 'name']);
                        $error.css('display', 'inline-block');
                    }
                });

                $('.cancelAddClient', $container).click(function () {
                    $modal.removeClass('openedAddClient');
                });

                $modal.find('.addClientFromSearch').click(function () {
                    $modal.toggleClass('openedAddClient');

                    return false;
                });

                if ($modal.find('.modal_list').hasClass('addClientFromList'))
                    $modal.find('.addClientFromSearch').click();
            }

            func(data, el, responce, $modal);
        }
    );
}

function clearSelectedBlocks() {
    $('input[type=checkbox].blockSelecter:checked').click();
}

function sendEmailFromSelected(clientID) {
    var blocks = Global.selectedBlocks;

    if (blocks.length) {
        ajax(
            'getSendEmailForm',
            {
                clientID: clientID
            },
            function (result) {
                if (result.success && result.html) {
                    modal(
                        result.html,
                        {
                            customContent: true,
                            afterOpen: function ($modal) {
                                emailModalCalculateTextareaHeight($modal);

                                $modal.find('#blocksCnt').html(blocks.length + ' ' + getWord(blocks.length, ['варианту', 'вариантам', 'вариантам']));

                                $modal.find('.sendPdfToEmail').click(function () {
                                    var currectEmails = [],
                                        contacts = $modal.find('.contact:checked').map(function () { // checkboxes
                                            return $(this).val();
                                        }).get(),
                                        addEmails = [],
                                        countCorrect = 0,
                                        $emails = $modal.find('.newEmails .newEmailItem'),
                                        $addEmails = $modal.find('.addEmail input.newEmailItem'),
                                        errors = [],
                                        checkEmails = true,
                                        goodEmail,
                                        value,
                                        $email,
                                        $addEmail;

                                    for (i = 0; i < $addEmails.length; i++) {
                                        $addEmail = $($addEmails[i]);
                                        value = trim($addEmail.val());
                                        goodEmail = value && /^[-._a-z0-9]+@([a-z0-9][\-a-z0-9]*\.)+[a-z]{2,6}$/im.test(value);
                                        if (goodEmail) {
                                            countCorrect++
                                        }
                                        goodEmail = goodEmail || !value;

                                        if (!goodEmail) {
                                            $addEmail.addClass('has-error');
                                        } else {
                                            $addEmail.removeClass('has-error');
                                            addEmails.push([$addEmail.parent().data('personid'), value]);
                                        }

                                    }

                                    for (var i = 0; i < $emails.length; i++) {
                                        $email = $($emails[i]);
                                        value = trim($email.val());
                                        goodEmail = value && /^[-._a-z0-9]+@([a-z0-9][\-a-z0-9]*\.)+[a-z]{2,6}$/im.test(value);

                                        if (goodEmail)
                                            countCorrect++;

                                        goodEmail = goodEmail || !value;

                                        if (!goodEmail) {
                                            $email.addClass('has-error');
                                        } else {
                                            $email.removeClass('has-error');
                                            currectEmails.push(value);
                                        }

                                        checkEmails = checkEmails && goodEmail;
                                    }

                                    if (countCorrect == 0 && !contacts.length && !addEmails.length) {
                                        checkEmails = false;
                                        errors.push('Необходимо выбрать хотя бы одного адресата.');
                                    }

                                    if (errors)
                                        $modal.find('.emailErrors').html(errors.join('<br>'));

                                    if (contacts.length || checkEmails) {
                                        waitLayer(true);

                                        ajax(
                                            'sendPDF',
                                            {
                                                addEmails: addEmails,
                                                contacts: contacts,
                                                clientID: clientID,
                                                blocks: blocks,
                                                text: $modal.find('.emailText').val(),
                                                emails: currectEmails
                                            },
                                            function (result) {
                                                if (result.success) {
                                                    var timer;

                                                    // timer = setInterval(
                                                    //     function () {
                                                    ajax(
                                                        'checkSendPDFStatus',
                                                        {
                                                            SendID: result.SendID
                                                        },
                                                        function (answer) {
                                                            if (answer.success) {
                                                                $modal.arcticmodal('close');
                                                                modal('Письмо отправлено!');

                                                                clearInterval(timer);
                                                                waitLayer(false);

                                                                clearSelectedBlocks();
                                                            }
                                                        }
                                                    );
                                                    /* },
                                                     500
                                                 );*/
                                                } else {
                                                    $modal.arcticmodal('close');
                                                    modal('Ошибка отправки!');
                                                }

                                                /*if (result.success) {
                                                    $modal.arcticmodal('close');
                                                } else {

                                                }*/
                                            }
                                        )
                                    }
                                });

                                var $emailsContainer = $modal.find('.newEmails'),
                                    $emailItems = $emailsContainer.find('.newEmailsItem'),
                                    maxEmailsCount = $emailsContainer.data('max-count') || 3;

                                $emailsContainer.find('.addNewEmailsItem button').click(function () {
                                    var $emails = $emailItems.children();

                                    if ($emails.length < maxEmailsCount) {
                                        var $newEmail = $emails.first().clone();

                                        $newEmail.val('');
                                        $newEmail.removeClass('has-error');

                                        $emailItems.append($newEmail);

                                        if ($emails.length == maxEmailsCount - 1)
                                            $(this).hide();
                                    }
                                });
                            }
                        }
                    )
                }
            }
        );
    }

    return false;
}

function getPDFFromSelected(clientID) {
    var blocks = Global.selectedBlocks,
        pdfName = 'blocks'/*(blocks.length == 1) ? 'block' : 'blocks'*/;

    if (blocks.length)
        if (blocks.length <= 15) {
            window.open('/PDF/m1/blocks/' + blocks.join(',') + '/');
            clearSelectedBlocks();
            /*waitLayer(true);

            getPDF({
                name: pdfName,
                parents: blocks,
                additions: {
                    ClientID: clientID
                },
                after: function (result) {
                    waitLayer(false);
                    clearSelectedBlocks();

                    window.open(result.link);
                }
            });*/
        } else {
            modal('Можно выбрать не более 15 предложений для одной презентации!');
        }

    return false;
}

function deleteFromSelected(clientID, blocks, $confirmBtn) {
    var successFunction = function () {
        var $blocksForDelete;

        if (blocks && blocks.length) {
            $blocksForDelete = $('.search_items .checkBlocks .blockSelecter').filter(function () {
                return in_array($(this).data('block'), blocks);
            });

            blocks = blocks.map(function () {
                return blocks + '';
            });
        } else {
            $blocksForDelete = $('.search_items .checkBlocks .checkbox:checked');
            blocks = Global.selectedBlocks;
        }

        if (blocks.length)
            ajax(
                'deleteFromSelected',
                {
                    clientID: clientID,
                    blocks: blocks
                },
                function (result) {
                    if (result.success) {
                        Global.selectedBlocks = $(Global.selectedBlocks).not(blocks).get();
                        Global.deleteSelectedBlocks = $.unique($.merge(Global.deleteSelectedBlocks, blocks));

                        $blocksForDelete.each(function () {
                            var $this = $(this),
                                $blockBox = $this.closest('.blockItem'),
                                $parentBlockBox = $blockBox.parent();

                            $('.map_item .blockSelecter[data-block=' + $this.data('block') + ']').closest('.blockItem').remove();

                            $blockBox.remove();

                            if (!$parentBlockBox.children().size()) {
                                var $buildBox = $parentBlockBox.closest('.buildItem'),
                                    buildID = $buildBox.data('parent');

                                // Global.maps.searchMap.markers[$('.map_item').data('marker-number')].marker.setMap(null);

                                $(Global.maps.searchMap.markers.filter(function (item) {
                                    return item.data.BuildID == buildID;
                                })).each(function () {
                                    this.marker.setMap(null);
                                    $('.map_item[data-parent=' + this.data.BuildID + ']').remove();
                                });

                                $buildBox.remove();
                            } else {
                                var $moreBtn = $parentBlockBox.parent().find('.open_down_box_bottom');

                                $parentBlockBox.children().each(function (i) {
                                    var $item = $(this);

                                    $item.removeClass('openDownBoxItem');

                                    if (i > 2)
                                        $item.addClass('openDownBoxItem');

                                    if (!$item.hasClass('openDownBoxItem'))
                                        $item.removeClass('activeItem');
                                });

                                var count = $parentBlockBox.children().filter('.openDownBoxItem').size();

                                if (count) {
                                    var word = 'помещен',
                                        word2 = 'последн',
                                        wordEnd = 'ий',
                                        wordEnd2 = 'ие';

                                    if (count == 1) {
                                        wordEnd = 'ие';
                                        wordEnd2 = 'ee';
                                    } else if (2 <= count && count <= 4) {
                                        wordEnd = 'ия';
                                    }

                                    $moreBtn.find('.moreBlocksCount').html(count);
                                    $moreBtn.find('.countWord').html(word + wordEnd);
                                    $moreBtn.find('.hideLastWord').html(word2 + wordEnd2);
                                } else {
                                    $moreBtn.remove();
                                }

                                if (Global.maps.searchMap.markers.length)
                                    Global.maps.searchMap.markers.map(function (item) {
                                        var itemBlocks = item.data.BlocksStr.split(','),
                                            newBlocks = $(itemBlocks).not(blocks).get();

                                        if (newBlocks.length) {
                                            item.data.BlocksStr = newBlocks.join(',');
                                            item.data.BlocksCount = newBlocks.length;
                                            item.marker.setLabel(item.data.BlocksCount + '');
                                        }

                                        if ($('.map_item[data-parent=' + item.data.BuildID + ']').length) {
                                            item.marker.setIcon('/img/design/gmarkers/mredact.png');
                                            item.marker.setLabel('');
                                        }

                                        return item;
                                    });
                            }
                        });

                        if (!$('.searchContent .search_items .buildItem').length) {
                            $('.searchContent .searchButton').click();
                        }

                        clearSelectedBlocks();
                    }

                    var $counter = $('.agentSearchPage [data-page="selected"] span.counter');

                    $counter.html(result.SelectedCount);

                    /*if (!result.SelectedCount) {
                     $counter.hide();
                     } else {
                     $counter.show();
                     }*/
                    // $('#showedBlocksItems .searchCounter').html(number(result.SelectedCount));

                    if (!$confirmBtn) {
                        var $controlBox = $(".control_box");
                        $controlBox.removeClass("active");
                        $controlBox.find('button').prop('disabled', true);
                    }

                }
            );
    };

    $confirmBtn = $confirmBtn || false;

    if ($confirmBtn && $confirmBtn.length) {
        var $deleteConfirm = $($.parseHTML(
            '<div class="deleteConfirm">' +
            '<div class="deleteConfirmLabel">Удалить?</div>' +
            '<div class="deleteConfirmButtons">' +
            '<button type="button" class="agree btn btn-success btn-sm">Да</button>' +
            '<button type="button" class="cancel btn btn-danger btn-sm">Нет</button>' +
            '</div>' +
            '</div>'
            )),
            $parent = $confirmBtn.closest('.checkBlocks');

        $deleteConfirm.find('button').click(function () {
            var $btn = $(this);

            if ($btn.hasClass('agree'))
                successFunction();

            $confirmBtn.popover('destroy');
            $confirmBtn.removeClass('active');
            $parent.removeClass('active');
        });

        if (!$confirmBtn.hasClass('active')) {
            $confirmBtn.popover({
                trigger: 'manual',
                html: true,
                placement: 'left',
                content: $deleteConfirm
            });

            $confirmBtn.popover('show');

            $confirmBtn.addClass('active');
            $parent.addClass('active');
        } else {
            $confirmBtn.popover('destroy');

            $confirmBtn.removeClass('active');
            $parent.removeClass('active');
        }
    } else {
        successFunction();
    }

    return false;
}

/**
 * Модалка с данными собственника.
 * @param blockID
 * @param obj
 */
function loadOwnerModal(blockID, obj) {

    if (blockID && !$(obj).hasClass('noAgent'))
        ajaxModal(
            'getOwnerModal',
            {blockID: blockID},
            function (data, el, responce) {
                /*if (!responce)
                    el.arcticmodal('close');*/
            }
        );
}

function loadSupportModal(blockID, obj, params) {
    params = params || {};
    params.QuestionID = params.QuestionID || 0;

    if (blockID && !$(obj).hasClass('noAgent')) {
        ajaxModal(
            'loadSupportModal',
            {
                'BlockID': blockID,
                'QuestionID': params.QuestionID
            },
            function (data, el, responce) {
                /*if (!responce)
                    el.arcticmodal('close');*/

                initSendSupportRequest();
            }
        );
    }

    return false;
}

function initSearch() {
    var $searchForm = $('#searchFilter'),
        $submitButton = $('.searchButton[data-search]', $searchForm),
        $squareSlider = $('#SquareSearchSlider'),
        $costSlider = $('#CostSearchSlider'),
        $dealType = $('input[type=radio][name=DealTypeID]', $searchForm),
        initSearchSliders = function (minMaxParams, curDealType) {
            if (minMaxParams) {
                new Slider($squareSlider, {
                    min: parseInt(minMaxParams['Min' + curDealType + 'Square']),
                    max: parseInt(minMaxParams['Max' + curDealType + 'Square']),
                    from: parseInt($('[name=SquareFrom]', $squareSlider).val()),
                    to: parseInt($('[name=SquareTo]', $squareSlider).val()),
                    on: {
                        load: function ($container) {
                            // initNumberInput($container);
                        },
                        change: function () {
                            // pre(1);
                        }
                    }
                });

                new Slider($costSlider, {
                    min: parseInt(minMaxParams['Min' + curDealType + 'Cost']),
                    max: parseInt(minMaxParams['Max' + curDealType + 'Cost']),
                    from: parseInt($('[name=CostFrom]', $costSlider).val()),
                    to: parseInt($('[name=CostTo]', $costSlider).val()),
                    postLabel: minMaxParams[curDealType + 'CostPostLabel'],
                    label: minMaxParams[curDealType + 'CostLabel'],
                    on: {
                        load: function ($container) {
                            // initNumberInput($container);
                        },
                        change: function () {
                            // pre(2);
                        }
                    }
                });
            }
        };

    if ($searchForm.length) {
        var $rangeDropDownFilters = $('.rangeDropDownFilter', $searchForm);

        if ($rangeDropDownFilters.length) {
            $rangeDropDownFilters.each(function () {
                var $this = $(this);

                $('.multiSel input[type=text]', $this).click(function () {
                    return false;
                });

                $('.filter_btn', $this).click(function () {
                    var $openBtn = $(this),
                        $from = $('.multiSel .fromValue', $this),
                        $to = $('.multiSel .toValue', $this);

                    if (!$this.hasClass('active')) {
                        $this.addClass('active');
                        setTimeout(function () {
                            new Slider($('.main_slider', $this), {
                                func: function (slider) {
                                    $from.html(slider.values.from);
                                    $to.html(slider.values.to);

                                },
                                on: {
                                    change: function (slider) {
                                        startSearch($submitButton);
                                    }
                                }
                            });
                        }, 300);
                    } else {
                        $this.removeClass('active');
                    }
                });
            });
        }

        var $radioDropDownFilters = $('.radioDropDownFilter', $searchForm);

        if ($radioDropDownFilters.length) {
            $radioDropDownFilters.each(function () {
                var $this = $(this),
                    $labels = $('.mutliSelect ul > li label', $this),
                    $curLabel = $('.multiSel span', $this);

                $labels.click(function () {
                    $curLabel.html($(this).html());
                });
            });
        }

        initSearchSliders($dealType.data('minmax'), ($('input[type=radio][name=DealTypeID]:checked', $searchForm).val() == 1) ? 'Rent' : 'Sale');

        $dealType.change(function () {
            var $this = $(this),
                minMaxParams = $this.data('minmax'),
                curDealType = ($('input[type=radio][name=DealTypeID]:checked', $searchForm).val() == 1) ? 'Rent' : 'Sale';

            // $('.main_slider.active').removeClass('active');

            $('#SearchCost .costLabel, .costLabel').html(minMaxParams[curDealType + 'CostLabel']);
            $('#SearchCost .postLabel, .costPostLabel').html(minMaxParams[curDealType + 'CostPostLabel']);

            initSearchSliders(minMaxParams, curDealType);
        });

        $('#SearchCost > div.labels, #SearchSquares > div.labels').click(function () {
            setTimeout(function () {
                initSearchSliders($dealType.data('minmax'), ($('input[type=radio][name=DealTypeID]:checked', $searchForm).val() == 1) ? 'Rent' : 'Sale');
            }, 300);
        });

        $('.stepTabsMenu li[data-step="SearchParams"] a').click(function () {
            initSearchSliders($dealType.data('minmax'), ($('input[type=radio][name=DealTypeID]:checked', $searchForm).val() == 1) ? 'Rent' : 'Sale');
        });

        $('input[type=checkbox]', $searchForm).change(function () {
            startSearch($submitButton);
        });

        $('.radioDropDownFilter input[type=radio]', $searchForm).change(function () {

            startSearch($submitButton, undefined, true);
        });

        $('.showSelectedBlocksItem').change(function () {
            var $isChecked = $(this).is(':checked'),
                $allCheckbox = $('input[type=checkbox].showSelectedBlocksItem');

            $('input[type=hidden].withoutSelected', $searchForm).val($isChecked + 0);

            $allCheckbox.prop('checked', $isChecked);

            if ($isChecked) {
                $allCheckbox.addClass('active');
            } else {
                $allCheckbox.removeClass('active');
            }

            startSearch($submitButton, 0);
        });

        $('#showResultOnly').change(function () {
            $('input[type=hidden].showResultOnly', $searchForm).val($(this).is(':checked') + 0);

            startSearch($submitButton, 0);
        });

        $('input[type=checkbox].showNewBlocksOnly').change(function () {
            var $isChecked = $(this).is(':checked'),
                $allCheckbox = $('input[type=checkbox].showNewBlocksOnly');

            $('input[type=hidden].showNewBlocksOnly', $searchForm).val($isChecked + 0);
            $allCheckbox.prop('checked', $isChecked);

            if ($isChecked) {
                $allCheckbox.addClass('active');
            } else {
                $allCheckbox.removeClass('active');
            }

            startSearch($submitButton, 0);
        });

        var $searchFilter = $('#searchFilter');

        if ($searchFilter.size()) {
            var openedFilters = $.cookie('openedSearchFilters') || {};

            for (var i in openedFilters)
                if (openedFilters[i])
                    $searchFilter.find('li#' + i + ' .filter_btn').click();
        }
    }
}

/*function openMap($btn) {
    $btn.addClass("active");

    $('.hide_box').addClass("active").slideUp("medium");
    $('.hide_box_2').addClass("active");
    $('.map_box_lg').addClass("active");
    //$('#content-2').addClass("content");
    $('#content-2').addClass("mCustomScrollbar");

    $('.right_box_wr').removeClass("active");
    $('.content').removeClass("active");
}

function filterHeight() {
    if (Global.maps.searchMap)
    	return false;

    self.aside.css({'height':'auto'});

    self.asideH = self.mainBox.get(0).getBoundingClientRect().bottom - self.mainBox.get(0).getBoundingClientRect().top;

    self.aside.outerHeight(self.asideH);

    self.checkSize();
}

function closeMap($btn) {
    $btn.removeClass("active");

    $('.hide_box').removeClass("active").slideDown("medium");

    setTimeout(function () {
        $('.hide_box_2').removeClass("active");
    }, 200);

    $('.map_box_lg').removeClass("active");
    //$('#content-2').removeClass("content");
    $('#content-2').removeClass("mCustomScrollbar");

    $('.right_box_wr').addClass("active");
    $('.content').addClass("active");
}*/

function startSearch($submitButton, delay, skipSearchID) {
    clearTimeout(Global.searchDelay);

    if (typeof skipSearchID == 'undefined') {
        skipSearchID = false;
    }

    Global.searchDelay = setTimeout(
        function () {
            if (!skipSearchID) {
                Global.currentSearchID = $("input[name='SearchID']").val();
            }

            $submitButton.click();
            Global.currentSearchID = 0;
        },
        delay != undefined ? delay : Global.searchDelayTime
    )
}

function initSearchButtons($container) {
    var $searchForm = $('#searchFilter');

    $('.searchButton[data-search]:not(.noAgent)', $container || $(document)).click(function () {
        var $this = $(this),
            currentSearchID = 0,
            searchMode = '';

        if ($this.data('search-id'))
            currentSearchID = $this.data('search-id');

        if ($this.data('search-mode')) {
            searchMode = $this.data('search-mode');
        }

        if (Global.currentSearchID) {
            currentSearchID = Global.currentSearchID;
        }

        //Совершается новый поиск
        if (!currentSearchID) {
            var $allCheckbox = $('input[type=checkbox].showNewBlocksOnly');
            $allCheckbox.attr('checked', false);
            $allCheckbox.removeClass('active');
            $('input[type=hidden].showNewBlocksOnly', $searchForm).val(0);
        }


        doSearch({
            type: $this.data('search'),
            limit: parseInt($this.data('limit')) || 10,
            offset: ($this.data('offset') != undefined) ? parseInt($this.data('offset')) : $('#article .search_items > .buildItem .blockItem').size(),
            clear: parseInt($this.data('clear')) || false,
            params: [
                {
                    name: 'CurrentSearchID',
                    value: currentSearchID
                },
                {
                    name: 'SearchMode',
                    value: searchMode
                },
                {
                    name: 'SelectedBlocks',
                    value: Global.selectedBlocks
                }
            ],
            parts: {
                map: !parseInt($this.data('no-map')),
                list: !parseInt($this.data('no-list'))
            },
            before: function (params) {
                if (!params.offset)
                    $('input.blocksCounter', $searchForm).val(0);
            },
            after: function (params, result) {
                if (result.List.Info) {
                    var $counter = $('input.blocksCounter', $searchForm);

                    $counter.val(parseInt($counter.val()) + result.List.Info.Count.Blocks);

                    if (!currentSearchID && result.List.htmlCounter)
                        $('.searchCounter').html(result.List.htmlCounter);

                    $('.searchContent .search_items input[type=checkbox].blockSelecter').each(function () {
                        var $this = $(this);

                        if (in_array($this.data('block') + '', Global.selectedBlocks)) {
                            $this.prop('checked', true);
                            $this.addClass('active');
                        }
                    });
                }

                /*if (result.) { //TODO
			        $('input[type=hidden][name=SearchID]', $searchForm).val();
                }*/
            }
        });

        return false;
    });
}

function doSearch(params) {
    if (typeof params.before == 'function')
        params.before(params);

    if (!params.offset) {
        $('input.pageID').val(1);
    }
    else {
        $('input.pageID').val(parseInt($('input.pageID').val()) + 1); //Счетчик страницы
    }

    var searchFilterParams = $('#searchFilter').serializeArray(),
        $moreBtn = $('#addSearchItems');

    $moreBtn.addClass('active');

    params.parts = params.parts || {map: true, list: true};

    if (params.parts.map || params.parts.list) {
        var searchParts = [];

        if (params.parts.map)
            searchParts.push('Map');

        if (params.parts.list)
            searchParts.push('List');

        waitLayer(true);
        ajax(
            'search',
            searchFilterParams.concat([
                {
                    name: 'Offset',
                    value: params.offset
                },
                {
                    name: 'Limit',
                    value: params.limit
                },
                {
                    name: 'Search',
                    value: params.type
                },
                {
                    name: 'Parts[]',
                    value: searchParts
                }
            ]).concat(params.params || []),
            function (result) {
                if (params.parts.list) {
                    var $searchContainer = $('#article'),
                        $itemsContainer = $('.search_items', $searchContainer),
                        $result = $($.parseHTML(result.List.html));

                    if (params.clear) {
                        $itemsContainer.height($itemsContainer.height());
                        $itemsContainer.empty();

                        Core.LeftFilterMenu.checkPosition = true;
                        Core.LeftFilterMenu.savePosition = true;
                        $('html, body').animate({scrollTop: 0}, 800, null, function () {
                            $itemsContainer.css('height', '');

                            if ($itemsContainer.height() > $('.searchContent').height()) {
                                // $('.searchContent').height($('.search_items').height());
                            }
                            Core.LeftFilterMenu.savePosition = false;

                        });
                    }

                    $itemsContainer.append($result);

                    // $moreBtn.removeClass('active');
                    $moreBtn.remove();
                    $itemsContainer.append($.parseHTML(result.List.moreButton));

                    initNoAgent();
                    initPDFEditor($searchContainer);
                    initSearchButtons($searchContainer);
                    initGalleryWrappers();
                    initChosen(true);
                    initShowGallery();
                    initPDFLink($result);
                    initPlugins(
                        $result,
                        [
                            'openDownBox',
                            'rating',
                            'controlBox',
                            // 'activeButton',
                            'tooltipster',
                            'checkbox'
                        ]
                    );

                    //Core.LeftFilterMenu.sticky(true);
                }

                if (params.parts.map) {
                    $('#searchMap').parent().find('.map_item').remove(); // удаляем блок с инфой по зданию на карте

                    var mapInfo = Global.maps.searchMap, // информация о карте
                        markers = mapInfo.markers; // массив маркеров

                    if (mapInfo.gmapCluster) {
                        mapInfo.gmapCluster.setMap(null);

                        delete mapInfo.gmapCluster;
                    }

                    if (markers.length) { // если есть маркеры
                        for (var i = 0; i < markers.length; i++)
                            markers[i].marker.setMap(null); // стираем их с карты

                        Global.maps.searchMap.markers = []; // очищаем массив маркеров
                    }

                    delete mapInfo.settings.scrollwheel;

                    var noMap = !mapInfo.map;

                    if (noMap) // если нет карты, то инициируем её
                        mapInfo.map = new google.maps.Map(
                            mapInfo.container,
                            mapInfo.settings
                        );

                    var gmapCluster = new MarkerClusterer(
                        mapInfo.map,
                        [],
                        {
                            gridSize: 40,
                            maxZoom: 15,
                            minimumClusterSize: 5
                        }
                        ),
                        zoomLimit = 15; // создаем кластер

                    if (noMap) {
                        var lastZoom = mapInfo.settings.zoom, //последний зум
                            clusterClick = false; // граница скрытия/показа маркеров

                        mapInfo.map.addListener('zoom_changed', function () { // событие масштабирования
                            var curZoom = mapInfo.map.getZoom(),
                                isClusterClick = clusterClick
                                    && (gmapCluster.getMaxZoom() + 1 >= zoomLimit)
                                    && curZoom >= zoomLimit
                                    && curZoom > lastZoom
                                    && lastZoom < zoomLimit; // текущий зум

                            if (
                                in_array(zoomLimit, [curZoom, lastZoom]) // если перешли с границы зума на больший/меньший зум
                                && in_array(zoomLimit - 1, [curZoom, lastZoom])
                                || isClusterClick
                            ) {
                                $(Global.maps.searchMap.markers).each(function () { // перебираем маркеры
                                    var item = this;

                                    if (!item.inCluster) // те маркеры, которые не в кластере
                                        item.marker.setMap((curZoom - lastZoom >= 0 || isClusterClick) ? mapInfo.map : null); // скрываем или показываем
                                });

                                if (isClusterClick)
                                    clusterClick = false;
                            }

                            lastZoom = curZoom;
                        });
                    }

                    var markersForCluster = [], // массив маркеров для кластеризации
                        allBuilds = result.AllBuilds || {}; // все здания

                    for (var index = 0, count = result.Map.Result.length; index < count; index++) { // перебираем результаты поиска
                        var build = result.Map.Result[index], // информация по зданию
                            inSelected = $.arrayIntersect(build.BlocksStr.split(','), Global.selectedBlocks).length || parseInt(build.NotShowedBlock),
                            markerName = inSelected ? 'poisk-green' : 'poisk'; // маркер по-умолчанию

                        var marker = new google.maps.Marker({ // создаем маркер
                            map: mapInfo.map,
                            position: new google.maps.LatLng(build.Lat + 0, build.Lng + 0),
                            icon: Global.markerPath + markerName + '.png',
                            // label: build.BlocksCount + '',
                            title: build.InternalName/*,
	                        zIndex: 5*/
                        });

                        Global.maps.searchMap.markers.push({ // добавляем информацию о маркере в массив мареров
                            marker: marker, // сам маркер
                            data: build, // информация о маркере
                            // label: build.BlocksCount + '',
                            icons: { // иконки маркеров
                                current: markerName, // текущий
                                default: markerName, // по-умолчанию
                                active: inSelected ? 'poisk-green-active' : 'poisk-active', // активный
                                watched: inSelected ? 'poisk-green-watched' : 'poisk-watched' // использованный
                            },
                            inCluster: true
                        });

                        delete allBuilds[build.BuildID]; // удаляем здание из массива всех зданий

                        markersForCluster.push(marker);
                    }

                    var currentZoom = mapInfo.map.getZoom();

                    for (var buildID in allBuilds) { // перебираем оставшиеся здания
                        var build = allBuilds[buildID], // информация о здании
                            hasBlocks = parseInt(build.BlocksCount),
                            markerName = hasBlocks ? 'other' : 'none', // иконка маркера по-умолчанию
                            markerLabel = hasBlocks ? build.BlocksCount + '' : '', // лейбл маркера по-умолчанию
                            markerParams = { // параметры маркера
                                map: currentZoom < zoomLimit ? null : mapInfo.map, // по-умолчанию не показываем маркеры без блоков
                                position: new google.maps.LatLng(build.Lat + 0, build.Lng + 0),
                                icon: Global.markerPath + markerName + '.png',
                                title: build.InternalName,
                                zIndex: -1
                            };

                        // if (markerLabel) // если есть лейбл
                        //     markerParams.label = markerLabel; // то надо его нарисовать

                        var marker = new google.maps.Marker(markerParams); // создаем маркер

                        Global.maps.searchMap.markers.push({ // добавляем информацию о маркере в массив маркеров
                            marker: marker,
                            data: build,
                            // label: markerLabel,
                            icons: {
                                current: markerName,
                                default: markerName,
                                active: hasBlocks ? 'other-active' : 'none-active',
                                watched: hasBlocks ? 'other-watched' : 'none-watched'
                            },
                            inCluster: false/*hasBlocks*/
                        });
                    }

                    for (var i in Global.maps.searchMap.markers) {
                        setMarkerEvent('searchMap', i);

                        if (Global.maps.searchMap.markers[i].inCluster)
                            markersForCluster.push(Global.maps.searchMap.markers[i].marker);
                    }

                    google.maps.event.addListener(gmapCluster, 'clusterclick', function (cluster) {
                        clusterClick = true;
                    });

                    gmapCluster.clearMarkers();
                    gmapCluster.addMarkers(markersForCluster);

                    Global.maps.searchMap.gmapCluster = gmapCluster;
                }

                if (result.searchCounter) {
                    $('.searchCounterBox').html(result.searchCounter);
                }

                if (result.SearchID) {
                    $("input[name='SearchID']").val(result.SearchID);
                }

                if (typeof params.after == 'function')
                    params.after(params, result);

                waitLayer(false);
            }
        );
    }

    return false;
}

function setMarkerEvent(mapName, number) {
    var allMarkers = Global.maps[mapName].markers,
        markerInfo = allMarkers[number],
        marker = markerInfo.marker,
        data = markerInfo.data;

    marker.addListener('click', function () {
        if (Global.activeMapItem) { //возвращаем вид по-умолчанию последнему активному маркеру
            allMarkers[Global.activeMapItem].icons.current = allMarkers[Global.activeMapItem].icons.watched;

            allMarkers[Global.activeMapItem].icons.used = true;

            allMarkers[Global.activeMapItem].marker.setIcon(Global.markerPath + allMarkers[Global.activeMapItem].icons.current + '.png');

            if (allMarkers[Global.activeMapItem].label)
                allMarkers[Global.activeMapItem].marker.setLabel(allMarkers[Global.activeMapItem].label);
        }

        markerInfo.icons.current = markerInfo.icons.active;

        marker.setIcon(Global.markerPath + markerInfo.icons.current + '.png'); //делаем активным текущий маркер

        marker.setLabel('');

        Global.activeMapItem = number; //запоминаем его номер

        var ajaxParams = $('#searchFilter').serializeArray();

        ajaxParams =
            ajaxParams.concat([
                {
                    name: 'CurrentSearchID',
                    value: $('#searchFilter input[type=hidden][name=SearchID]').val()
                },
                {
                    name: 'BuildID',
                    value: data.BuildID
                },
                {
                    name: 'blocks',
                    value: $(data.BlocksStr.split(',')).not(Global.deleteSelectedBlocks).get().join(',')
                },
                {
                    name: 'selectedBlocks',
                    value: Global.selectedBlocks
                },
                {
                    name: 'searchPage',
                    value: Global.selectedBlocks
                },
                {
                    name: 'clientID',
                    value: $('.selectedClient').data('id')
                },
                {
                    name: 'searchName',
                    value: $('#searchFilter').data('search')
                },
                {
                    name: 'IsMap',
                    value: 1
                }
            ]);


        ajax( // запрос плашки здания
            'getMapSearchInfo',
            ajaxParams,
            function (mapBuildInfo) {
                if (mapBuildInfo) {
                    var $info = $(mapBuildInfo),
                        $container = $('#searchMap').parent();

                    $container.find('.map_item').remove();

                    $container.append($info);

                    $info.find('.map_item_close').click(function () {
                        $(this).closest('.map_item').remove();

                        markerInfo.icons.current = markerInfo.icons.watched;
                        markerInfo.icons.used = true;

                        marker.setIcon(Global.markerPath + markerInfo.icons.current + '.png');

                        if (markerInfo.label)
                            marker.setLabel(markerInfo.label);

                        Global.activeMapItem = 0;
                    });

                    $info.show();
                    $info.height($('.searchContent').height());

                    initGalleryWrappers($info, {counter: false});
                    initShowGallery($info);
                    initPDFLink($info);

                    initPlugins(
                        $info,
                        [
                            'activeButton',
                            'controlBox',
                            'checkbox'
                        ]
                    );

                    $info.customScrollbar({
                        hScroll: false
                    });

                    $info.data('marker-number', number);

                    $('.tooltip', $info).tooltipster();

                    initPDFEditor($info);
                }
            }
        );
    });
}

function showBuildMap(lat, lng, obj) {
    if ($(obj).hasClass('noAgent'))
        return false;

    var $modal = $(
        '<div class="box-modal buildItemMap">' +
        '<div class="box-modal_close arcticmodal-close"></div>' +
        '<div class="buildItemMap" data-lat="' + lat + '" data-lng="' + lng + '" data-marker="active" data-zoom="14"></div>' +
        '</div>'
    );

    $('body').append($modal);

    $modal.arcticmodal({
        afterClose: function () {
            $modal.remove();
        }
    });

    initDefaultMaps($modal.find('.buildItemMap'));
}

function setSearchMetroStations(metroMap) {
    var $searchForm = $('#searchFilter'),
        $inputContainer = $('.placement', $searchForm),
        isMainTitle = $('.openMetroMap p', $searchForm).length,
        $label = isMainTitle ? $('.openMetroMap p', $searchForm) : $('.openMetroMap', $searchForm),
        count = metroMap.selectedStations.length;

    $('input[type=hidden]', $inputContainer).remove();

    if (count) {
        $(metroMap.selectedStations).each(function () {
            $inputContainer.append('<input type="hidden" name="Metros[]" value="' + this + '">');
        });

        var endings = ['о', 'й'];

        if (count == 1) {
            endings[0] = 'а';
            endings[1] = 'я';
        } else if (count < 5) {
            endings[1] = 'и';
        }

        $label.html((isMainTitle ? '' : 'Выбран' + endings[0] + ' ') + count + ' станци' + endings[1]);
    } else {
        $label.html(isMainTitle ? 'любое' : 'Выбрать');
    }

    // startSearch($('.searchButton[data-search]', $searchForm));
}

function setClientsMetroStations(metroMap) {
    var $inputContainer = $('.clientLocation .locations'),
        $label = $('.openMetroMap'),
        stations = metroMap.selectedStations;

    $inputContainer.empty();
    var endings = ['о', 'й'];

    if (stations.length == 1) {
        endings[0] = 'а';
        endings[1] = 'я';
    } else if (stations.length < 5) {
        endings[1] = 'и';
    }

    $label.html('Выбран' + endings[0] + ' ' + stations.length + ' станци' + endings[1]);

    /*if (stations.length) {
        $label.show();
    } else {
        $label.hide();
    }*/

    for (var i = 0; i < stations.length; i++)
        $inputContainer.append('<input type="hidden" name="Metros[]" value="' + stations[i] + '">');
}

function initScrollToTop() {
    var $scroller = $('.scrollToTop');

    if ($scroller.size()) {
        $scroller.click(function (e) {
            e.stopPropagation();

            $('html, body').animate({scrollTop: 0}, 800);

            return false;
        });

        var startPos = $('section').offset().top;

        $(window).scroll(function () {
            var scrolled = $(window).scrollTop();

            if ($(window).height() + scrolled <= $('footer').offset().top) {
                if ($scroller.hasClass('bottom'))
                    $scroller.removeClass('bottom');

                if (scrolled >= startPos) {
                    if (!$scroller.hasClass('active'))
                        $scroller.addClass('active');
                } else {
                    $scroller.removeClass('active');
                }
            } else {
                $scroller.addClass('bottom');
            }
        });
    }
}

//Главное меню
function initScroll() {
    var $scrolledBlocks = $('.scrolled');

    if ($scrolledBlocks.size()) {
        $scrolledBlocks.each(function () {
            var $this = $(this),
                startPos = $this.offset().top,
                $fakeBlock = $('<div style="width:' + $this.outerWidth(true) + 'px; height:' + $this.outerHeight(true) + 'px;"></div>');

            $(window).scroll(function () {
                var scrolled = $(window).scrollTop();

                if (scrolled >= startPos) {
                    if (!$this.hasClass('fixed')) {
                        $this.addClass('fixed');
                        $this.after($fakeBlock);
                    }
                } else {
                    $this.removeClass('fixed');
                    $fakeBlock.remove();
                }
            });
        });
    }

    var $aboutPageHeader = $('.bootstrapScrolled');

    if ($aboutPageHeader.size()) {
        $aboutPageHeader.each(function () {
            var $this = $(this),
                startPos = $this.offset().top,
                $fakeBlock = $('<div style="width:' + $this.outerWidth(true) + 'px; height:' + $this.outerHeight(true) + 'px;"></div>');

            $(window).scroll(function () {
                var scrolled = $(window).scrollTop();

                if (scrolled >= startPos) {
                    if (!$this.hasClass('navbar-fixed-top')) {
                        $this.addClass('navbar-fixed-top');
                        $this.after($fakeBlock);
                    }
                } else {
                    $this.removeClass('navbar-fixed-top');
                    $fakeBlock.remove();
                }
            });
        });
    }

}

function initRedirectButtons() {
    $('[data-redirect]').click(function () {
        window.location = $(this).data('redirect');
    });
}

function initFullpage() {
    $('#fullpage').fullpage({
        sectionSelector: '.pageForFullpage',
        navigation: true
    });
}

function destroyFullpage() {
    $.fn.fullpage.destroy('all');
}

function initClientListPage() {
    var $page = $('[data-page-name="client.list"]');

    if ($page.size()) {
        $('.clientLink', $page).click(function () {
            ajaxModal('getClientModalInfo', {
                    clientID: $(this).data('id')
                },
                function () {
                    $('.sitePopover').popover();
                });

            return false;
        });

        $('.deleteBtn', $page).click(function () {
            return confirm('Удалить клиента?');
        });
    }
}

function initMapMenu() {
    var $container = $('.mapMenu .dropdown .mapMenuContent');

    $container.click(function (e) {
        e.stopPropagation();
    });

    $container.find('*').click(function (e) {
        e.stopPropagation();
    });
}

function initSearchOrders() {
    var $form = $('#searchFilter'),
        $selectors = $('.searchOrder'),
        $searchSortContainer = $('.searchSort'),
        $sortButton = $('.mainSortButton', $searchSortContainer),
        $sortContent = $('.mainSortContent', $searchSortContainer),
        $searchMenu = $('.searchHeader .searchMenuContent'),
        $acceptButton = $sortContent.find('button');

    $searchMenu.click(function (e) {
        e.stopPropagation();
    });
    $sortContent.click(function (e) {
        e.stopPropagation();
    });

    $searchMenu.find('*').click(function (e) {
        e.stopPropagation();
    });
    $sortContent.find('*').click(function (e) {
        e.stopPropagation();
    });

    $acceptButton.click(function (e) {
        e.stopPropagation();

        $sortButton.removeClass('active');
        $sortButton.popover('hide');

        $form.find('input[type=hidden].ordersInputs').remove();

        $selectors.filter(':checked').each(function () {
            $form.append($('<input type="hidden" class="ordersInputs" name="Orders[]" value="' + $(this).val() + '">'));
        });
        $sortButton.closest('.dropdown').removeClass('open');

        startSearch($form.find('.searchButton[data-search]'), 0);
    });
}

function initUpdatePdfContactInfo(force) {
    force = force || false;

    var $form = $('#profileForm'),
        timesPerSecond = 10,
        $containers = $('.pdfContactPreview.updatable', $form),
        inputs = {},
        containers = $containers.get().map(function (item) {
            var fields = {};

            $('[data-field]', $(item)).each(function () {
                var $field = $(this);

                fields[$field.data('field')] = {
                    dom: $field,
                    emptyHide: $field.data('hide')
                };
            });

            return fields;
        }),
        blocksForHide = {};

    $containers.find('[data-hide=1][data-hide-field], [data-hide=1][data-field]').each(function () {
        var $block = $(this),
            fieldName = $block.data('hide-field') || $block.data('field');

        if (!blocksForHide[fieldName])
            blocksForHide[fieldName] = [];

        blocksForHide[fieldName].push($block);
    });

    $form.find(':not([data-from-field]) > input[type=text], [data-from-field]').each(function () {
        var $input = $(this);

        inputs[$input.data('from-field') || $input.attr('name')] = $input.data('from-field') ? $input.find('input[type=text]') : $input;
    });

    if ($form.length) {
        var updateInfo = function (forceUpdate) {
            var focused = false;

            if (!forceUpdate)
                for (var i in inputs)
                    focused = focused || inputs[i].is(':focus');

            if (focused || forceUpdate)
                for (var i in containers) {
                    for (var j in containers[i]) {
                        var value = htmlspecialchars(inputs[j].val());

                        if (inputs[j] && inputs[j].length)
                            containers[i][j].dom.html(value);

                        if (blocksForHide[j] && blocksForHide[j].length)
                            for (var k in blocksForHide[j])
                                if (!value) {
                                    blocksForHide[j][k].hide();
                                } else {
                                    blocksForHide[j][k].show();
                                }
                    }
                }
        };

        for (var i in inputs)
            inputs[i].focusout(function () {
                setTimeout(function () {
                    updateInfo(true);
                }, 100);
            });

        if (force)
            updateInfo(true);

        setInterval(
            function () {
                updateInfo(false);
            },
            Math.round(1000 / timesPerSecond)
        )
    }
}

function setPdfColor($select, _this) {
    var color = '#' + $select.val(),
        $container = $('.pdfPagesPreview'),
        $colorize = $container.find('.colorize[data-style]');

    $colorize.each(function () {
        var $this = $(this);

        $this.css($this.data('style'), color);
    });

    $container.find('svg path, svg polygon, svg rect').each(function () {
        $(this).css('fill', color);
    });
}

function toggleEmail(_this) {
    var $this = $(_this);

    if ($this.hasClass('active')) {
        $this.find('input').prop('checked', false);
        $this.removeClass('active');
    } else {
        $this.find('input').prop('checked', true);
        $this.addClass('active');
    }
}

function changeTextToEmail(_this) {
    var $this = $(_this);
    $this.closest('.addEmail').html('<input type="email" class="newEmailItem form-control" placeholder="E-mail">');
}

function emailModalCalculateTextareaHeight($modal) {
    var contactsHeight = $modal.find('.right').height();
    $modal.find('.emailText').height(contactsHeight - 41);
}

function getWord(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];

    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

function logActions(actionID) {
    ajax('log', {actionID: actionID});
}

function setFooterPosition() {
    var headerH = $('#header').outerHeight(),
        mainMenuH = $('.mainMenu').outerHeight(),
        footerH = $('footer').outerHeight(),
        wH = $(window).height();
    $('.pageContent').height(wH - (headerH + mainMenuH + footerH));
}


function clientListInitElements() {
    console.log('InitElements');
    console.log($('.btn_nav_three_points').length);
    $('.btn_nav_three_points').off('click').on('click', function () {
        if ($(this).next('.open_down_box_one_cont').css("display") === "none") {
            $('.open_down_box_one_cont').slideUp("medium");
            $(this).next('.open_down_box_one_cont').slideToggle("medium");
        }
        else $('.open_down_box_one_cont').slideUp("medium");

        return false;
    });

    $('body').off('click').on('click', function (event) {
        if (event.target.className.indexOf('nav_three_points_cont') === -1) {
            $('.nav_three_points_cont').slideUp('medium');
        }
    });
}