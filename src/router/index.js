import Vue from 'vue'
import Router from 'vue-router'
//Common
import Footer from '../FooterView.vue'
//Search
import PremiseSearch from '../PremiseSearchView.vue'
//Clients
import ClientList from '../client/ClientListView.vue'
import ClientAdd from '../client/ClientAddView.vue'
//Catalog
import CatalogObject from '../catalog/CatalogObjectView.vue'
import CatalogObjectDetails from '../catalog/CatalogObjectDetailsView.vue'
import codMain from '../catalog/CatalogObjectDetailsMainView.vue'
import codOffices from '../catalog/CatalogObjectDetailsOfficesView.vue'
import codRenters from '../catalog/CatalogObjectDetailsRentersView.vue'
import codLocation from '../catalog/CatalogObjectDetailsLocationView.vue'
//About
import About from '../about/AboutView.vue'
import AboutMain from '../about/AboutMainView.vue'
import AboutAgent from '../about/AboutAgentView.vue'
import AboutOwner from '../about/AboutOwnerView.vue'
import Privacy from '../PrivacyView.vue'
import Agreement from '../AgreementView.vue'

Vue.use(Router);

export default new Router({
    mode: 'history', //HTML5 History API mode
    routes: [
        {path: '/', redirect: '/clients'},
        {
            path: '/catalog/:symbol?',
            components: {
                main: CatalogObject,
                footer: Footer,
            },
            props: true,
            name: 'catalog_search'
        },
        {
            path: '/bc/:address',
            components: {
                main: CatalogObjectDetails,
                footer: Footer
            },
            props: true,
            children: [
                {
                    path: 'bc_main',
                    components: {
                        main: codMain,
                        footer: Footer,
                    },
                    name: 'bc_main'
                },
                {
                    path: 'offices',
                    components: {
                        main: codOffices,
                        footer: Footer,
                    },
                    name: 'bc_offices'
                },
                {
                    path: 'renters',
                    components: {
                        main: codRenters,
                        footer: Footer,
                    },
                    name: 'bc_renters'
                },
                {
                    path: 'location',
                    components: {
                        main: codLocation,
                        footer: Footer,
                    },
                    name: 'bc_location'
                }

            ]
        },
        {
            path: '/search',
            components: {
                main: PremiseSearch,
                footer: Footer,
            }
        },
        {
            path: '/clients',
            components: {
                main: ClientList,
                footer: Footer,
            }
        },
        {
            path: '/clients/add',
            components: {
                main: ClientAdd,
                footer: Footer,
            },
            name: 'client_add'
        },
        {
            path: '/about',
            components: {
                main: About
            },
            children: [
                {
                    path: '',
                    components: {
                        aboutmain: AboutMain
                    },
                    name: 'about_main_content'
                },
                {
                    path: 'agent',
                    components: {
                        aboutmain: AboutAgent
                    },
                    name: 'about_agent_content'
                },
                {
                    path: 'owner',
                    components: {
                        aboutmain: AboutOwner
                    },
                    name: 'about_owner_content'
                }
            ]
        },
        {
            path: '/privacy',
            components: {
                main: Privacy,
                footer: Footer,
            },
            name: 'privacy'
        },
        {
            path: '/agreement',
            components: {
                main: Agreement,
                footer: Footer,
            },
            name: 'agreement'
        }
    ]
});